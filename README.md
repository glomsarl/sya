# Getting Started with SYA

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Execute Project

To run the project, follow the steps below:

1. Clone the project
### `git clone https://gitlab.com/glomsarl/sya.git`

2. Update env file to match env.skeleton in base directory

3. Install project dependencies
### `npm install`

4. Clone [backend](https://gitlab.com/glomsarl/sya-backend) and execute it's Readme file as stated

5. Update env file by changing variable `REACT_APP_BASE_URL` value to the running backend url (ex: `http://localhost:[port]`)

6. Launch frontend app
### `npm start`

7. To login as admin and manage requests, structures and platform, use access: 
### `email: merlindjeumesi@gmail.com`
### `password: sya-password`
This will permit you to do initial configurations. These include: 
- Define the commision rate of the structures on the platform
- Set the emission value at validation
- enable/disable signup bonus (for customers)
- set SY value (bonus point)
- define the referral bonus

8. HAPPY HACKING
