#download the node image
FROM node:16.14.0-alpine3.14 as builder

# #set the working directory
WORKDIR /app

# # install app dependencies
COPY package.json ./

# #clean npm cache
# RUN npm cache clean --force

# #clean install dependecies
RUN npm install

# # add app
COPY . ./

# # expose port 3011 to outer environment
EXPOSE 3000

# # start app
CMD ["npm", "start"]

# FROM abiosoft/caddy:latest

# COPY /Caddyfile /etc/Caddyfile
# COPY --from=builder ./app/build /usr/share/caddy/html
