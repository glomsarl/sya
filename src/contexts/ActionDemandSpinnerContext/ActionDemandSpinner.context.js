import { createContext } from "react";

const ActionDemandSpinnerContext = createContext();
export default ActionDemandSpinnerContext;
