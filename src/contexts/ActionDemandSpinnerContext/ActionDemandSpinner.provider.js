import { useReducer, useContext } from "react";
import ActionDemandSpinnerContext from './ActionDemandSpinner.context';

/**
 * reducer function to switch the different
 * actions and carry payload
 * @param {Object} state containing the different state data used by the app
 * @param {Object} action holding the action type and payload
 * @returns a new states
 */
function actionDemandDialogReducer(state, action) {
  switch (action.type) {
    case "OPEN_SPINNER": {
      return {
        ...state,
        loading: true,
        demandAction: action.payload,
      };
    }
    case "CLOSE_SPINNER": {
      return {
        ...state,
        loading: false,
        demandAction: "",
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

/**
 * Provider for the demand Context. used to send data to the children
 * needing to consume this context's data.
 * @param {JSX} children the children jsx elements passed inbetween
 * @returns {JSX} the children loaded with the needed acced to data
 */
export default function ActionDemandSpinnerProvider({ children }) {
  const initialState = {
    loading: false,
    demandAction: "",
  };

  const [actionDemandSpinnerState, actionDemandSpinnerDispatch] = useReducer(
    actionDemandDialogReducer,
    { ...initialState }
  );
  const value = {
    actionDemandSpinnerState,
    actionDemandSpinnerDispatch,
  };

  return (
    <ActionDemandSpinnerContext.Provider value={value}>
      {children}
    </ActionDemandSpinnerContext.Provider>
  );
}

/**
 * custom hook to avoid using useContext
 * before specifying the data needed from the context
 * @returns Demand Context data
 */
function useActionDemandSpinner() {
  const context = useContext(ActionDemandSpinnerContext);
  if (context === undefined) {
    throw new Error(`useActionDemandSpinner must be used 
                within a DemandActionSpinnerProvider`);
  }
  return context;
}

export { useActionDemandSpinner };
