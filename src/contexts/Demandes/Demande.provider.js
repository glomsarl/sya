import { useState } from 'react';
import { injectIntl } from 'react-intl';
import InfosDemande from './Demande.context';

/**
 * this function provides the InfosDemande 
 * context to any component that needs it
 * @param {component} children any component
 * @returns {component} this component, in addition to the fact 
 * that it can already consume the InfosDemande context
 */

function InfosDemandeProvider({children, intl}) {
    let [ infosStructure, setInfosStructure] = useState();
    let [ ownerInfos, setOwnerInfos] = useState();
    const [activeStep, setActiveStep] = useState(0);
    const { formatMessage } = intl
    const steps = [
        `1- ${formatMessage({ id:"stepOneMessage" })}`,
        `2- ${formatMessage({ id: "stepTwoMessage" })}`,
        `3- ${formatMessage({ id: "stepThirdMessage" })}`
    ];
    
    /**
     * this function allows to change the type of obj 
     * that is active in the InfosDemande context
     * @param {String} obj takes as parameter a InfosDemande 
     * which defines the new active structure 
     */

    const changeInfosStructure = (obj) =>{
        setInfosStructure(obj);
    }
    const changeOwnerInfos = (obj) =>{
        setOwnerInfos(obj);
    }

    return (
        <InfosDemande.Provider value={{ infosStructure, ownerInfos, changeInfosStructure, changeOwnerInfos, activeStep, setActiveStep, steps}}>
            {children}
        </InfosDemande.Provider>
    )
}

export default injectIntl(InfosDemandeProvider)
