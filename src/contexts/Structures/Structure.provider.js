import { useContext, useReducer } from "react";
import StructureContext from "./Structure.context";

/**
 * reducer function to switch the different
 * actions and carry payload
 * @param {Object} state containing the different state data used by the app
 * @param {Object} action holding the action type and payload
 * @returns a new states
 */
function structureReducer(state, action) {
  switch (action.type) {
    case "DECREMENT_PAGE_NUMBER": {
      if (state.pageNumber > 1)
        return { ...state, pageNumber: state.pageNumber - 1 };
      else return state;
    }
    case "INCREMENT_PAGE_NUMBER": {
      let maxNumberOfPages =
        state.totalNumberOfStructures /
        process.env.REACT_APP_NUMBER_OF_STRUCTURES_PER_PAGE;
      if (state.pageNumber < Math.ceil(maxNumberOfPages))
        return { ...state, pageNumber: state.pageNumber + 1 };
      else return state;
    }
    case "ADD_NEW_STRUCTURES": {
      if (action.payload.length <= 0) return state;
      else
        return {
          ...state,
          structures: action.payload,
        };
    }
    //disableStructure
    case "SET_ACTIVE_STRUCTURE": {
      return {
        ...state,
        activeStructure: action.payload,
        isActiveStructureLoading: false,
      };
    }
    case "CLOSE_ACTIVE_STRUCTURE": {
      return {
        ...state,
        activeStructure: {},
      };
    }
    case "SET_TOTAL_NUMBER_OF_STRUCTURES": {
      return {
        ...state,
        totalNumberOfStructures: action.payload,
      };
    }
    case "TOGGLE_STRUCTURE_ABILITY": {
      let structures = state.structures.map((structure) => {
        if (structure.structure_id === action.payload) {
          structure.is_disabled = !structure.is_disabled;
          return structure;
        }
        return structure;
      });

      return {
        ...state,
        structures,
        activeStructure: { ...state.activeStructure, is_disabled: !state.activeStructure.is_disabled },
        actionnedStructureId: "",
      };
    }
    case "SET_ACTIONNED_STRUCTURE_ID": {
      return {
        ...state,
        actionnedStructureId: action.payload,
      };
    }
    case "DEACTIVATE_SKELETON_SCREEN": {
      return {
        ...state,
        isStructureDataLoading: false,
      };
    }
    case "LOAD_ACTIVE_STRUCTURE": {
      return {
        ...state,
        isActiveStructureLoading: true,
      };
    }
    case "CLOSE_LOAD_ACTIVE_STRUCTURE": {
      return {
        ...state,
        isActiveStructureLoading: false,
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

/**
 * Provider for the structure Context. used to send data to the children
 * needing to consume this context's data.
 * @param {JSX} children the children jsx elements passed inbetween
 * @returns {JSX} the children loaded with the needed acced to data
 */
export default function StructureContextProvider({ children }) {
  const initialState = {
    structures: [],
    activeStructure: {},
    pageNumber: 1,
    totalNumberOfStructures: 233,
    actionnedStructureId: "",
    isStructureDataLoading: true,
    isActiveStructureLoading: true,
  };

  const [state, dispatch] = useReducer(structureReducer, { ...initialState });
  const value = { state, dispatch };

  return (
    <StructureContext.Provider value={value}>{children}</StructureContext.Provider>
  );
};

/**
 * custom hook to avoid using useContext
 * before specifying the data needed from the context
 * @returns Structure Context data
 */
function useStructure() {
  const context = useContext(StructureContext);
  if (context === undefined) {
    throw new Error("useStructure must be used within a StructureContextProvider");
  }
  return context;
}

export { useStructure };
