import { useState } from 'react';
import ResetPassword from './ResetPassword.context';

/**
 * this function provides the InfosDemande 
 * context to any component that needs it
 * @param {component} children any component
 * @returns {component} this component, in addition to the fact 
 * that it can already consume the InfosDemande context
 */

function ResetPasswordProvider({children}) {
    let [ infoReset, setInfoReset] = useState({});
    const [activeStep, setActiveStep] = useState(2);

    const steps = ['1', '2', '3'];
    
    /**
     * this function allows to change the type of obj 
     * that is active in the InfosDemande context
     * @param {String} obj takes as parameter a InfosDemande 
     * which defines the new active structure 
     */

    const changeInfoReset = (ResetPasswordData) =>{
        setInfoReset({...infoReset, ...ResetPasswordData});
    }
    return (
        <ResetPassword.Provider value={{ infoReset, changeInfoReset, activeStep, setActiveStep, steps}}>
            {children}
        </ResetPassword.Provider>
    )
}

export default ResetPasswordProvider
