import { useContext, useReducer }  from 'react';
import LandingStructureContext from './LandingStructure.context';

function landingStructureReducer(state, action){
    switch (action.type) {
        case "LOAD_STRUCTURES_DATA":{
            return {...state, structures: action.payload}
        }
        case "LOAD_SELECTED_STRUCTURE":{
            return {...state, selectedStructure:action.payload}
        }
        case "SELECT_SERVICE_FORM_TODAY_CATALOG":{
            return {...state, selectedService:action.payload}
        }
        case "LOAD_CLOSEST_STRUCTURES_DATA":{
            return {...state, closestStructures: action.payload}
        }
        case "LOAD_SAME_CATEGORY_STRUCTURES_DATA":{
            return {...state, sameCategoryStructures: action.payload}
        }
        default:{
            throw new Error(`Action with type ${action.type} is not supported by the context`)
        }
    }
}

function LandingStructureContextProvider({children}){

    let initialState ={
        structures: [],
        selectedStructure: {
            publications: [],
            schedules: [],
            today_catalog: {}
        },
        selectedService:{},
        closestStructures:[],//made with thesame properiety like <<structures>>
        sameCategoryStructures:[],
    }

    let [landingStructureState, landingStructureDispatch] = useReducer(landingStructureReducer, initialState)
    let value = {landingStructureState, landingStructureDispatch}

    return (
        <LandingStructureContext.Provider value={value}>
            {children}
        </LandingStructureContext.Provider>
    )
}

export default LandingStructureContextProvider;

function useLandingStructure(){
    const context = useContext(LandingStructureContext);
    if(context === undefined){
        throw new Error ("useLandingStructure must be used within a LandingStructureContextProvider")
    };
    return context;

}

export { useLandingStructure };