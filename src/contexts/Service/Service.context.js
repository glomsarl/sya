import { createContext } from 'react';

let ServiceContext = createContext();

export default ServiceContext;