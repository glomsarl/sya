import { useContext, useReducer } from 'react'
import ServiceContext from './Service.context'

/**
 * reducer function to switch the different
 * actions and carry payload data
 * @param {Object} state state containing the different state data used by the app
 * @param {Object} action action holding the action type and payload
 * @returns a new state
 */
function serviceReducer(state, action) {
  switch (action.type) {
    case 'CLOSE_LOADING_SERVICE_DATA': {
      return { ...state, services: action.payload, isServiceDataLoading: false }
    }
    case 'ACTIVATE_SERVICE': {
      return {
        ...state,
        activeService: { service_id: action.payload },
        isActiveServiceDataLoading: true,
      }
    }
    case 'CLOSE_ACTIVE_SERVICE': {
      return { ...state, activeService: {} }
    }
    case 'LOAD_ACTIVE_SERVICE_DATA': {
      return {
        ...state,
        activeService: action.payload,
        isActiveServiceDataLoading: false,
      }
    }
    case 'EDIT_SERVICE': {
      let services = state.services.map((service) => {
        if (service.service_id === action.payload.service_id) {
          return { ...service, ...action.payload }
        }
        return service
      })

      return {
        ...state,
        services,
        activeService: { ...state.activeService, ...action.payload },
      }
    }
    case 'CREATE_SERVICE': {
      return {
        ...state,
        isServiceDataLoading: false,
        services: [...state.services, action.payload],
      }
    }
    case 'DELETE_SERVICE': {
      let services = state.services.filter(
        (service) => service.service_id !== action.payload,
      )
      return { ...state, services }
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`)
    }
  }
}

export default function ServiceContextProvider({ children }) {
  const initialState = {
    services: [],
    activeService: {},
    isServiceDataLoading: true,
    isActiveServiceDataLoading: true,
  }

  const [serviceState, serviceDispatch] = useReducer(
    serviceReducer,
    initialState,
  )
  const value = { serviceState, serviceDispatch }

  return (
    <ServiceContext.Provider value={value}>{children}</ServiceContext.Provider>
  )
}

function useService() {
  const context = useContext(ServiceContext)
  if (context === undefined) {
    throw new Error('useService must be used within a ServiceContextProvider')
  }
  return context
}

export { useService }
