import { useReducer, useContext } from "react";
import ToggleStructureAbilityContext from "./ToggleStructureAbility.context";

/**
 * reducer function to switch the different
 * actions and carry payload
 * @param {Object} state containing the different state data used by the app
 * @param {Object} action holding the action type and payload
 * @returns a new states
 */
function toggleStructureAbilityReducer(state, action) {
  switch (action.type) {
    case "OPEN_DIALOG": {
      return {
        ...state,
        isDialogOpen: true,
      };
    }
    case "CLOSE_DIALOG": {
      return {
        ...state,
        isDialogOpen: false,
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

/**
 * Provider for the demand Context. used to send data to the children
 * needing to consume this context's data.
 * @param {JSX} children the children jsx elements passed inbetween
 * @returns {JSX} the children loaded with the needed acced to data
 */
export default function ToggleStructureAbilityProvider({ children }) {
  const initialState = {
    isDialogOpen: false,
  };

  const [toggleStructureAbilityState, toggleStructureAbilityDispatch] = useReducer(
    toggleStructureAbilityReducer,
    { ...initialState }
  );
  const value = {
    toggleStructureAbilityState,
    toggleStructureAbilityDispatch,
  };

  return (
    <ToggleStructureAbilityContext.Provider value={value}>
      {children}
    </ToggleStructureAbilityContext.Provider>
  );
}

/**
 * custom hook to avoid using useContext
 * before specifying the data needed from the context
 * @returns Demand Context data
 */
function useToggleStructureAbility() {
  const context = useContext(ToggleStructureAbilityContext);
  if (context === undefined) {
    throw new Error(`useToggleStructureAbility must be used 
                within a useToggleStructureAbilityProvider`);
  }
  return context;
}

export { useToggleStructureAbility };
