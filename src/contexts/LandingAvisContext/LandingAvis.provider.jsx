import { useContext, useReducer }  from 'react';
import LandingAvisContext from './LandingAvis.context';

function landingAvisReducer(state, action){
    switch (action.type) {
        case "LOAD_AVIS_DATA":{
            return {...state, usersAvis:action.payload}
        }
        default:{
            throw new Error(`Action with type ${action.type} is not supported by the context`)
        }
    }
}

function LandingAvisContextProvider({children}){
    let initialState ={
        usersAvis: [],
    }

    let [landingAvisState, landingAvisDispatch] = useReducer(landingAvisReducer, initialState)
    let value = {landingAvisState, landingAvisDispatch}
    
    return (
        <LandingAvisContext.Provider value={value}>
            {children}
        </LandingAvisContext.Provider>
    )
}

export default LandingAvisContextProvider;

function useLandingAvisContext(){
    const context = useContext(LandingAvisContext);
    if(context === undefined){
        throw new Error ("useLandingAvisContext must be used within a LandingAvisContextProvider")
    };
    return context;

}

export { useLandingAvisContext };

// let exampleUsersAvis = [
//     {
//         id:'',
//         comment:'Pas mal... mais pas au niveau d’un 9/10 annoncé. La viande n’est pas extraordinaire, le service est sympa, les toilettes au fond de l’immeuble ofnt un peu tiers monde ...',
//         rating: 7.5,
//         othor:'Kuidja Marco',
//         commentedDate: '10/05/2021 06:30 PM',
//     },
//     {
//         id:'',
//         comment:'Pas mal... mais pas au niveau d’un 9/10 annoncé. La viande n’est pas extraordinaire, le service est sympa, les toilettes au fond de l’immeuble ofnt un peu tiers monde ...',
//         rating: 7.5,
//         othor:'Kuidja Marco',
//         commentedDate: '10/05/2021 06:30 PM',
//     },
//     {
//         id:'',
//         comment:'Pas mal... mais pas au niveau d’un 9/10 annoncé. La viande n’est pas extraordinaire, le service est sympa, les toilettes au fond de l’immeuble ofnt un peu tiers monde ...',
//         rating: 7.5,
//         othor:'Kuidja Marco',
//         commentedDate: '10/05/2021 06:30 PM',
//     },
// ]