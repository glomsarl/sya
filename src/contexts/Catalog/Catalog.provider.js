import { useContext, useReducer } from "react";
import CatalogContext from "./Catalog.context";

function catalogReducer(state, action){
    switch (action.type) {
      case "LOAD_CATALOGS": {
        return { ...state, catalogs: action.payload };
      }
      case "CREATE_CATALOG": {
        return { ...state, catalogs: [...state.catalogs, action.payload] };
      }
      case "EDIT_CATALOG": {
        let catalogs = state.catalogs.map((catalog) => {
          if (catalog.catalog_id === action.payload.catalog_id) {
            return action.payload;
          }
          return catalog;
        });
        return { ...state, catalogs };
      }
      case "OPEN_CATALOG": {
        let activeCatalog = { ...action.payload, services: [] };
        return { ...state, activeCatalog };
      }
      case "LOAD_ACTIVE_CATALOG_SERVICES": {
        let activeCatalog = {
          ...state.activeCatalog,
          services: action.payload,
        };
        return { ...state, activeCatalog };
      }
      case "CLOSE_ACTIVE_CATALOG": {
        return { ...state, activeCatalog: {} };
      }
      case "REMOVE_SERVICE_FROM_CATALOG": {
        let services = state.activeCatalog.services.filter((service) => {
          return service.service_id !== action.payload;
        });
        let activeCatalog = { ...state.activeCatalog, services };
        return { ...state, activeCatalog };
      }
      case "DELETE_CATALOG": {
        let catalogs = state.catalogs.filter(
          (catalog) => catalog.catalog_id !== action.payload
        );
        return { ...state, catalogs };
      }
      case "ADD_SERVICES_TO_CATALOG": {
        let newCatalogServices = [
          ...action.payload,
          ...state.activeCatalog.services,
        ];
        let activeCatalog = {
          ...state.activeCatalog,
          services: newCatalogServices,
        };
        return { ...state, activeCatalog };
      }
      default: {
        throw new Error(`Unhandled action type: ${action.type}`);
      }
    }
}

export default function CatalogContextProvider({ children }) {
  const initialState = {
    catalogs: [],
    activeCatalog: {},
    isCatalogDataLoading: true,
    isActiveCatalogDataLoading: true,
  };

  const [catalogState, catalogDispatch] = useReducer(
    catalogReducer,
    initialState
  );
  const value = { catalogState, catalogDispatch };

  return (
    <CatalogContext.Provider value={value}>
      {children}
    </CatalogContext.Provider>
  );
}

function useCatalog() {
  const context = useContext(CatalogContext);
  if (context === undefined) {
    throw new Error(
      "useCatalog must be used within a CatalogContextProvider"
    );
  }
  return context;
}

export { useCatalog };