import PersonContext from './Person.context'
import { useContext, useReducer } from 'react'

function personContextReducer(state, action) {
  switch (action.type) {
    case 'LOAD_PERSON_DATA': {
      return { ...state, person: { ...state.person, ...action.payload, isConnected: true } }
    }
    case 'LOAD_ORDER_HISTORY': {
      return { ...state, orderHistory: action.payload }
    }
    case 'DISCONNECT_USER': {
      return {
        ...state,
        person: {
          person_id: undefined,
          email: undefined,
          first_name: undefined,
          last_name: undefined,
          gender: undefined,
          telephone: undefined,
          birthdate: undefined,
          isConnected: false,
          sya_amount: 0,
          is_admin: false,
          is_owner: false,
        },
      }
    }
    default:
      throw new Error(
        `Action with type ${action.type} is not supported by the context`,
      )
  }
}

function PersonContextProvider({ children }) {
  let initialState = {
    person: {
      person_id: undefined,
      email: undefined,
      first_name: undefined,
      last_name: undefined,
      gender: undefined,
      telephone: undefined,
      birthdate: undefined,
      isConnected: false,
      sya_amount: 0,
      is_admin: false,
      is_owner: false,
    },
    orderHistory: [],
  }

  let [personState, personDispatch] = useReducer(
    personContextReducer,
    initialState,
  )
  let value = { personState, personDispatch }

  return (
    <PersonContext.Provider value={value}>{children}</PersonContext.Provider>
  )
}

export default PersonContextProvider

function usePerson() {
  const context = useContext(PersonContext)
  if (context === undefined) {
    throw new Error('usePerson must be used within a PersonContextProvider')
  }
  return context
}

export { usePerson }
