import { useContext, useReducer }  from 'react';
import LandingCategoryContext from './LandingCategory.context';

function landingCategoryReducer(state, action){
    switch (action.type) {
        case "LOAD_CATEGORIES_DATA":{
            return {...state, categories: action.payload}
        }
        default:{
            throw new Error(`Action with type ${action.type} is not supported by the context`)
        }
    }
}

function LandingCategoryContextProvider({children}){

    let initialState ={
        categories: [
            {
                id:1,
                name:'restaurant',
            },
            {
                id:2,
                name:'manege',
            },
            {
                id:3,
                name:'cinema',
            },
            {
                id:4,
                name:'beautySalon',
            },
        ],
    }

    let [landingCategoryState, landingCategoryDispatch] = useReducer(landingCategoryReducer, initialState)
    let value = {landingCategoryState, landingCategoryDispatch}
    

    return (
        <LandingCategoryContext.Provider value={value}>
            {children}
        </LandingCategoryContext.Provider>
    )
}

export default LandingCategoryContextProvider;

// let exampleCategories = [
//     {
//         id:1,
//         name:'Restaurant',
//     },
//     {
//         id:2,
//         name:'arcadeManege',
//     },
//     {
//         id:3,
//         name:'cinema',
//     },
//     {
//         id:4,
//         name:'beautySalon',
//     },
// ]

function useLandingCategory(){
    const context = useContext(LandingCategoryContext);
    if(context === undefined){
        throw new Error ("useLandingCategory must be used within a LandingCateoryContextProvider")
    };
    return context;
}

export { useLandingCategory };