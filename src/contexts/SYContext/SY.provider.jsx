import SYContext from './SY.context'
import { useContext, useReducer } from 'react'

function SYContextReducer(state, action) {
  switch (action.type) {
    case 'LOAD_SY_VALUE': {
      return { ...state, sy_value: action.payload }
    }
    default:
      throw new Error(
        `Action with type ${action.type} is not supported by the context`
      )
  }
}

function SYContextProvider({ children }) {
  let initialState = {
    sy_value: 0,
  }
  let [syState, syDispatch] = useReducer(SYContextReducer, initialState)
  let value = { syState, syDispatch }

  return <SYContext.Provider value={value}>{children}</SYContext.Provider>
}

export default SYContextProvider

// let exempleSyValue = {
//     sy_value:100,
// }

function useSYContext() {
  const context = useContext(SYContext)
  if (context === undefined) {
    throw new Error('useSYContext must be used within a useSYContext')
  }
  return context
}

export { useSYContext }
