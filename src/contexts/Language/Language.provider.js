import { useState } from 'react';
import LanguageContext from './Language.context';

/**
 * this function provides the LanguageContext 
 * context to any component that needs it
 * @param {component} children any component
 * @returns {component} this component, in addition to the fact 
 * that it can already consume the LanguageContext context
 */ 

export default function LanguageContextProvider({children}) {
    let [ activeLanguage, setActiveLanguage ] = useState(localStorage.getItem('syaLang')||'fr');
    
    /**
     * this function allows to change the type of structure 
     * that is active in the ActiveStructureType context
     * @param {String} language takes as parameter a string 
     * which defines the new active language 
     * @returns no return
     */
    const changeLanguage=(language)=>{
        localStorage.setItem('syaLang', language)
        setActiveLanguage(language)
    }
    return (
        <LanguageContext.Provider value={{activeLanguage, changeLanguage}}>
            {children}
        </LanguageContext.Provider>
    )
}
