import { createContext } from 'react';

let PromotionContext = createContext();

export default PromotionContext;