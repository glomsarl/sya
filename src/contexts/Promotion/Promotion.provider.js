import { useContext, useReducer } from "react";
import PromotionContext from "./Promotion.context";

/**
 * reducer function to switch the different
 * actions and carry payload data
 * @param {Object} state state containing the different state data used by the app
 * @param {Object} action action holding the action type and payload
 * @returns a new state
 */
function promotionReducer(state, action) {
  switch (action.type) {
    case "CREATE_PROMOTION": {
      return { ...state, promotions: [...state.promotions, action.payload] };
    }
    case "LOAD_PROMOTIONS": {
      return { ...state, promotions: action.payload };
    }
    case "DELETE_PROMOTION": {
      let promotions = state.promotions.filter(
        (promotion) => promotion.promotion_id !== action.payload
      );
      return { ...state, promotions };
    }
    case "EDIT_PROMOTION": {
      let promotions = state.promotions.map((promotion) => {
        if (promotion.promotion_id === action.payload.promotion_id) {
          return action.payload;
        } else return promotion;
      });
      return { ...state, promotions };
    }
    case "OPEN_PROMOTION": {
      let activePromotion = { ...action.payload, services: [] };
      return { ...state, activePromotion };
    }
    case "LOAD_ACTIVE_PROMOTION_SERVICES": {
      let activePromotion = {
        ...state.activePromotion,
        services: action.payload,
      };
      return { ...state, activePromotion };
    }
    case "CLOSE_ACTIVE_PROMOTION": {
      return { ...state, activePromotion: {} };
    }
    case "REMOVE_SERVICE_FROM_PROMOTION": {
      let services = state.activePromotion.services.filter((service) => {
        return service.service_id !== action.payload;
      });
      let activePromotion = { ...state.activePromotion, services };
      return { ...state, activePromotion };
    }
    case "ADD_SERVICES_TO_PROMOTION": {
      let newPromotionServices = [
        ...action.payload,
        ...state.activePromotion.services,
      ];
      let activePromotion = {
        ...state.activePromotion,
        services: newPromotionServices,
      };
      return { ...state, activePromotion };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

export default function PromotionContextProvider({ children }) {
  const initialState = {
    promotions: [],
    activePromotion: {},
    isPromotionDataLoading: true,
    isActivePromotionDataLoading: true,
  };

  const [promotionState, promotionDispatch] = useReducer(
    promotionReducer,
    initialState
  );
  const value = { promotionState, promotionDispatch };

  return (
    <PromotionContext.Provider value={value}>
      {children}
    </PromotionContext.Provider>
  );
}

function usePromotion() {
  const context = useContext(PromotionContext);
  if (context === undefined) {
    throw new Error(
      "UsePromotion must be used within a PromotionContextProvider"
    );
  }
  return context;
}

export { usePromotion };
