import { useState } from 'react';
import ActiveStructureType from './ActiveStructureType.context';

/**
 * this function provides the ActiveStructureType 
 * context to any component that needs it
 * @param {component} children any component
 * @returns {component} this component, in addition to the fact 
 * that it can already consume the ActiveStructureType context
 */

export default function ActiveStructureTypeProvider({children}) {
    let [ ActiveStructure, setActiveStructureType] = useState("Cinéma");
    let [structureType, setStructureType] = useState([]);
    
    /**
     * this function allows to change the type of structure 
     * that is active in the ActiveStructureType context
     * @param {String} structure takes as parameter a string 
     * which defines the new active structure 
     * @returns no return
     */

    const changeActiveStructureType=(structure)=>{
        setActiveStructureType(structure)
    }
    
    return (
        <ActiveStructureType.Provider value={{ActiveStructure, changeActiveStructureType, structureType, setStructureType}}>
            {children}
        </ActiveStructureType.Provider>
    )
}
