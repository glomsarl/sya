import { useContext, useReducer } from 'react';
import DemandContext from './Demand.context';

/**
 * reducer function to switch the different
 * actions and carry payload
 * @param {Object} state containing the different state data used by the app
 * @param {Object} action holding the action type and payload
 * @returns a new states
 */
function demandReducer(state, action){
    switch(action.type) {
        case "DECREMENT_PAGE_NUMBER": {
            if (state.pageNumber > 1)
                return {...state, pageNumber: state.pageNumber - 1}
            else return state
        }
        case "INCREMENT_PAGE_NUMBER": {
            let maxNumberOfPages =
                state.totalNumberOfDemands / process.env.REACT_APP_NUMBER_OF_DEMANDS_PER_PAGE;
            if (state.pageNumber < Math.ceil(maxNumberOfPages))
                return{...state, pageNumber: state.pageNumber + 1}
            else return state
        }
        case "ADD_NEW_DEMANDS": {
            if(action.payload.length <= 0) 
                return state
            else return {
                ...state,
                demands: action.payload
            }
        }
        case "DELETE_A_DEMAND": {
            let demands = state.demands.filter(demand=>demand.demand_id !== action.payload)
            return {
                ...state,
                demands
            }
        }
        case "DELETE_SELECTED_DEMANDS": {
            let demands = state.demands.filter(demand=> !state.selectedDemandIds.includes(demand.demand_id))
            return {
                ...state,
                demands
            }
        }
        case "ADD_SELECTED_DEMAND_IDS": {
            return {
                ...state, 
                selectedDemandIds:[
                    ...state.selectedDemandIds, 
                    action.payload
                ]
            }
        }
        case "REMOVE_FROM_SELECTED_DEMAND_IDS": {
            let selectedDemandIds = 
            state.selectedDemandIds.filter(demandId=>{
                return demandId !== action.payload
            })
            return {
                ...state, 
                selectedDemandIds
            }
        }
        case "SELECT_REJECTED_DEMANDS": {
            let rejectedDemands = state.demands.filter((demand) => 
              demand.rejected_at !== null
            );
            let selectedDemandIds = 
                rejectedDemands.map(demand=>demand.demand_id)
            return {
                ...state,
                selectedDemandIds
            }
        }
        case "EMPTY_REJECTED_DEMANDS": {
            return {
                ...state,
                selectedDemandIds: []
            }
        }
        case "SET_ACTIVE_DEMAND": {
            return {
                ...state,
                activeDemand: action.payload
            }
        }
        case "CLOSE_ACTIVE_DEMAND": {
            return {
                ...state,
                activeDemand: {}
            }
        }
        case "SET_TOTAL_NUMBER_OF_DEMANDS": {
            return {
                ...state,
                totalNumberOfDemands: action.payload
            }
        }
        case "MARK_DEMAND_AS_READ": {
            let demands = state.demands.map(demand => {
                if(demand.demand_id === action.payload){
                    demand.is_viewed = true;
                    return demand;
                }
                return demand;
            })

            return {
                ...state,
                demands
            }
        }
        case "REJECT_A_DEMAND": {
            let demands = state.demands.map(demand => {
                if(demand.demand_id === action.payload){
                    demand.rejected_at = Date.now();
                    return demand;
                }
                return demand;
            })

            return {
                ...state,
                demands,
                activeDemand: {...state.activeDemand, rejected_at:Date.now()},
                actionnedDemandId: ""
            }
        }
        case "VALIDATE_A_DEMAND": {
            let demands = state.demands.map(demand => {
                if(demand.demand_id === action.payload){
                    demand.is_valid = true;
                    return demand;
                }
                return demand;
            })

            return {
                ...state,
                demands,
                activeDemand: {...state.activeDemand, is_valid:true},
                actionnedDemandId: ""
            }
        }
        case "DEACTIVATE_SKELETON_SCREEN": {
            return {
                ...state,
                isDemandDataLoading: false
            }
        }
        case "LOAD_ACTIVE_DEMAND": {
            return {
                ...state,
                isActiveDemandLoading: true
            }
        }
        case "CLOSE_LOAD_ACTIVE_DEMAND": {
            return {
                ...state,
                isActiveDemandLoading: false
            }
        }
        default:{
            throw new Error(
                `Unhandled action type: ${action.type}`
            );
        }
    }
}

/**
 * Provider for the demand Context. used to send data to the children
 * needing to consume this context's data.
 * @param {JSX} children the children jsx elements passed inbetween 
 * @returns {JSX} the children loaded with the needed acced to data
 */
export default function DemandContextProvider({ children }) {
    const initialState = {
      demands: [],
      activeDemand: {},
      pageNumber: 1,
      totalNumberOfDemands: 233,
      selectedDemandIds: [],
      actionnedDemandId: "",
      isDemandDataLoading: true,
      isActiveDemandLoading: true
    };

    const [state, dispatch] = 
        useReducer( demandReducer, {...initialState})
    const value = {state, dispatch}
 
    return (
        <DemandContext.Provider value= {value}>
            { children }
        </DemandContext.Provider>
    )
}

/**
 * custom hook to avoid using useContext 
 * before specifying the data needed from the context
 * @returns Demand Context data
 */
function useDemand(){
    const context = useContext(DemandContext)
    if(context === undefined){
        throw new Error("useDemand must be used within a DemandContextProvider")
    }
    return context
}

export {useDemand}