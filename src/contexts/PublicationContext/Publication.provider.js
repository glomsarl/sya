import {useContext, useReducer } from 'react';
import PublicationContext from './Publication.context';

function publicationReducer(state, action){
    switch(action.type){
        case "DELETE_PUBLICATION":{
            let publications = state.publications.filter(publication=>publication.idPublication !== action.payload)
            return {...state, publications }
        }
        case "PUBLISH_PUBLICATIONS":{
            return {...state, publications:[...action.payload, ...state.publications ]}
        }
        case "LOAD_PUBLICATIONS":{
            return {...state, publications:action.payload}
        }
        default:{
            throw new Error(`Unhandled action type: ${action.type}`)
        }
    }
}

export default function PublicationContextProvider({children}){
    const initialState={
        publications: []
    }

    const [publicationState, publicationDispatch] = useReducer(publicationReducer, initialState);
    const value={publicationState, publicationDispatch};

    return(
        <PublicationContext.Provider value={value}>
            {children}
        </PublicationContext.Provider>
    )
}

function usePublication(){
    const context = useContext(PublicationContext);
    if(context === undefined){
        throw new Error("UsePublication must be used within a PublicationContextProvider")
    };
    return context;
}

export {usePublication}
