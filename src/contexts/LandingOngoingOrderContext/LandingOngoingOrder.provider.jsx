import { useContext, useReducer } from 'react'
import LandingOngoingOrderContext from './LandingOngoingOrder.context'

function landingOngoingOrderReducer(state, action) {
  switch (action.type) {
    case 'LOAD_ONGOING_ORDER_DETAILS': {
      return { ...state, ongoingOrder: action.payload }
    }
    case 'ADD_ONGOING_CATALOG': {
      return {
        ...state,
        ongoingOrder: {
          ...state.ongoingOrder,
          service: { ...state.ongoingOrder.service, catalogs: action.payload },
        },
      }
    }
    case 'SET_CLIENT_ORDER': {
      return { ...state, clientOrder: action.payload }
    }
    case 'ADD_PROMOTION_CODE': {
      return {
        ...state,
        clientOrder: { ...state.clientOrder, promotion_code_id: action.payload },
      }
    }
    case 'RESET_ONGOING_ORDER': {
      return {
        ...state,
        ongoingOrder: {
          service: {
            catalogs: [],
            promotions: [],
            schedules: [],
          },
        },
        clientOrder: {},
      }
    }
    default: {
      throw new Error(
        `Action with type ${action.type} is not supported by the context`,
      )
    }
  }
}

function LandingOngoingOrderContextProvider({ children }) {
  let initialState = {
    ongoingOrder: {
      service: {
        catalogs: [],
        promotions: [],
        schedules: [],
      },
    },
    clientOrder: {},
  }

  let [landingOngoingOrderState, landingOngoingOrderDispatch] = useReducer(
    landingOngoingOrderReducer,
    initialState,
  )
  let value = { landingOngoingOrderState, landingOngoingOrderDispatch }

  return (
    <LandingOngoingOrderContext.Provider value={value}>
      {children}
    </LandingOngoingOrderContext.Provider>
  )
}

export default LandingOngoingOrderContextProvider

// let exempleOngoingOrder = {
//     structureId:'',
//     serviceId:'',
//     service:{},
//     useSY:false,
//     quantity:0,
//     category:'',
//     selectedDate:'',
//     selectedTime:'',
//     catalogs:[
//         {
//             id:'boa1c664-ca3d-45e0-bc38-32b178a56f1a',
//             ddd:'2021-07-10 07:30',
//             ddf:'2021-07-15 12:45',
//         },
//         // {
//         //     id:'boa1c664-ca3d-45e0-bc38-32b178a56f1a',
//         //     ddd:'2021-07-10 12:30',
//         //     ddf:'2021-07-14 15:45',
//         // },
//         // {
//         //     id:'boa1c664-ca3d-45e0-bc38-32b178a56f1a',
//         //     ddd:'2021-07-10 07:30',
//         //     ddf:'2021-07-16 12:45',
//         // },
//         // {
//         //     id:'boa1c664-ca3d-45e0-bc38-32b178a56f1a',
//         //     ddd:'2021-07-10 07:30',
//         //     ddf:'2021-07-14 12:45',
//         // }
//     ],
// }

function useLandingOngoingOrder() {
  const context = useContext(LandingOngoingOrderContext)
  if (context === undefined) {
    throw new Error(
      'useLandingOngoingOrder must be used within a LandingOngoingOrderContextProvider',
    )
  }
  return context
}

export { useLandingOngoingOrder }
