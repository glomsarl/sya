export let AdminPageSkeleton = {
  ServicesPageTitle: "Services",
  ParametresPageTitle: "Profile",
  DemandesPageTitle: "Demands",
  StructuresPageTitle: "Structures",
  dashboardPageTitle: "Dashboard",
  demandsPageTitle: "Demands",
  mailsPageTitle: "Mails",
  configurationsPageTitle: "Settings",
  myStructuresPageTitle: "My Structures",

  ordersPageTitle: "Orders",
  catalogsPageTitle: "Catalogs",
  promotionsPageTitle: "Promotions",
  publicationPageTitle: "Publications",
};
