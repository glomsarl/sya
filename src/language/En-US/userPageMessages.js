export let userPageEnMessages = {
  myInfo: "My informations",
  myOrder: "My Orders",
  register: "Register my structure",
  signOut: "Sign Out",
  man: "Male",
  feminin: "Femalle",
  profile: "Profile",
  profileUse:
    "Your contact information will be communicated to the company when you order",
  obligated: "Compulsory Field",
  update: "Update",
  birthdate: "Birth Date",
  confirmed: "Comfirmed",
  unConfrimed: "Unconfirmed",
  cancelled: "Cancelled",
  notUpdate:
    "No changes have been made, please try again once you have modified a field.",
  gender: "Gender",
  manageStructures: "Manage My Structures",
  goToAdmin: "Manage SYA",
  category: "Category Name",
  bonusValue: "Accumulated bonus"
};