export let enMessages = {
  //common
  title: "See You Again",
  copyright: "2021 SYA SARL",
  allRightsReserved: "ALL RIGHTS RESERVED",

  // header
  aboutLinkHeader: "About",
  contactLinkHeader: "Contact",
  helpLinkHeader: "Help",
  loginBoutonHeader: "Login",
  signupButton: "Sign up",
  manageLinkHeader: "SYA Manager",
  selectStructureText: "Register your structure",
  createStructureText: "Create",

  // footer
  titlePlusSlogan: "See You Again - Leisure is not a secret anymore !",
  title1: "useful links",
  title1About: "About SYA",
  title1Help: "Do you need help ?",
  title1Contact: "Contact",
  title1HaveStructure: "You have a structure ?",
  title1HaveNotStructure: "You do not have a structure ?",
  title1Policy: "Our policy and privacy",
  title2: "Follow us here",
  title3: "NewsLetter",
  footerOffer:
    "The promotional offers are subject to conditions displayed on the detailed sheet of the different structures.",
  boutonNewsletter: "Subscribe",
  labelInputEmailAddress: "Email address",

  //  Reset password
  BoutonRecoveryLink: "Send recovery link",
  CanNotConnectTitle: " You can't connect ?",
  BackToConnectionText: " Back to the connection",
  RecoryLinkMessage: " We have sent you a recovery link to :",
  CodeInputText: " Enter the reset code",
  CodeMatchesErrorMessage: " Invalid code",
  BoutonResetText: " Renitialize",
  ResendRecoveryLink: "Resend recovery link",
  ChooseNewPassword: " Choose a new password",
  NewMdp: " New password",
  MdpConfirm: " Confirm new password",
  SaveMessage: " Save",
  BothPasswordSame: "Both password need to be the same",
  addressEmail: "Adresse email",
  CodeRequiredErrorMessage: "A code is required to continue",
  confirmPasswordRequired: "This field cannot be empty",
  SignUpMdpRequiredErrorMessage: "Password is required",
  SignUpMdpErrorMessage:
    "The password most contains at least eight(08) characters",
  password: "Mot de passe",
  notifyResetEmailLinkSent:
    "A link has been sent to the provided address. Click on the link to proceed!!!",

  //SignIn
  SignUpConnectTitle: " Log in to your account",
  ConnectionBoutonText: " connection",
  Or: " Or",
  ConnectionGoogle: " Login with Google",
  CreateAccount: " create an account",
  privacyMessagePart1: "The",
  privacyMessagePart2: "Privacy Policy",
  privacyMessagePart3: "and",
  privacyMessagePart4: "Terms of Service",
  privacyMessagePart5: "of SYA shall apply.",
  privacyMessageSignPart1: "By singing up I agree on the ",
  privacyMessageSignPart2: "and confirm that I have read and understood the ",
  signInBoutonText: "Sign Up",
  accountExist: "You already have an account on SYA ? Sign in",
  firstName: "FisrtName",
  lastName: "LastName",
  passwordField: "Password",

  //structureRequest
  demandPageTitle: "List your business on See You Again (SYA)",
  demandPageCaptiveMessage1: `Increase your revenues, increase your visibility and build customer loyalty by
              \njoining our globe of partner businesses already bookable on SYA, the leading search and
              \nbooking platform for entertainment venues in Cameroon.`,
  demandPageCaptiveMessage2: `Use our reservation, management and service preparation software,
              \nwith no time commitment. With an increased view on your finances.
              \nFree visibility and registration.`,
  //DemandeProvider
  stepOneMessage: "Structure information",
  stepTwoMessage: "Personnel informations",
  stepThirdMessage: "Confirmation",

  //InfosStructure
  structureNameInput: "Business name",
  addressInput: "Address",
  structureCategoryInput: "Business category *",
  latitudeInput: "Latitude",
  longitudeInput: "Longitude",
  getPositionButtonText: "Click here to get your position",
  particularIndications: "Particular indications",
  particularMessageHelperText: `The particular indications will allow your customers to 
              \nbetter locate your business address. Ex : village dèrrière belavie,
              \nNdokoti face tradex.`,
  nextButtonText: "Next",
  useGoogleLocalisationHeader: "Use our location service",
  useGoogleLocalisationBody1: `Let us help you determine the location of your structure,
              \nso you don't get confused about its exact position.`,
  useGoogleLocalisationBody2: `NB: Only do this if you are currently in your business facility.`,
  disagreeGoogleLocalisation: "Disagree",
  agreeGoogleLocalisation: "I agree",
  nameStringErrorMessage: "The name must be a string",
  nameRequiredErrorMessage: "The business must have a name",
  nameMinCharacterErrorMessage: "The name must be at least 3 characters long",
  addressStringErrorMessage: "The address must be a string",
  addressRequiredErrorMessage: "The business must have an address",
  structureCategoryStringErrorMessage:
    "The structure category must be a string",
  structureCategoryRequiredErrorMessage:
    "The structure must have a structure category",

  //InfosProprietaire
  ownerNameStringErrorMessage: "The first name must be a string",
  ownerNameRequiredErrorMessage: "The owner must have a name",
  ownerNameMinCharacterErrorMessage:
    "The first name must be at least 3 characters long",
  ownerLastNameStringErrorMessage: "The last name must be a string",
  ownerLastNameMinCharacterErrorMessage:
    "The last name must be at least 3 characters long",
  ownerEmailErrorMessage: "Invalid email",
  ownerEmailRequiredErrorMessage: "The owner must have an email",
  ownerPhoneErrorMessage: "Invalid phone number. Ex: 6xxxxxxxx or 2xxxxxxxx",
  ownerPhoneRequiredErrorMessage: "The owner must have a phone number",
  ownerDateOfBirthMaxDateErrorMessage: "Date of birth must be before today",
  ownerDateOfBirthRequiredErrorMessage: "The owner must have a date of birth",
  ownerNationalIdStringErrorMessage: "The national Id number must be a string",
  ownerNationalIdRequiredErrorMessage: "The owner must have a national id",
  ownerFirstName: "First name",
  ownerLastName: "Last name",
  ownerEmail: "E-mail address",
  ownerPhone: "Phone number",
  ownerDateOfBirth: "Date of birth",
  ownerGender: "Gender",
  genderMale: "Male",
  genderFemale: "Female",
  ownerNationalId: "National ID number",
  unsubscribeNewsletter: "I don't want communications from SY by e-mail/SMS",
  backButtonText: "Previous",
  sendButtonText: "Submit demand",

  //sucess structure demand
  successStructureCreation1: "Thank you for your request.",
  successStructureCreation2:
    "We have emailed the confirmation of your request, and we will send you an email when your request is confirmed",

  //SignUp client
  SignUpEmailErrorMessage: "Adresse e-mail non valide",
  SignUpEmailRequiredErrorMessage: "adresse e-mail est requis",
  SignUpFirstNameStringErrorMessage: "must be a string",
  SignUpFirstNameRequiredErrorMessage: "Name is required",
  SignUpFirstNameMinCharacterErrorMessage:
    "must be at least 3 characters long.",
  SignUpLastNameStringErrorMessage: "must be a string",
  SignUpLastNameRequiredErrorMessage: "First name is required",
  SignUpLastNameMinCharacterErrorMessage: "must be at least 3 characters long.",

  //confirmDemandHeader
  selectRejected: "Select all rejected demands",
  unSelectRejected: "Unselect all rejected demands",
  filter: "Filter",
  refresh: "Refresh",
  deleteSelected: "Delete selected demands",
  more: "More",
  previousPage: "Previous page",
  nextPage: "Next page",
  on: "of",

  //confirmDemandBody
  selectDemand: "Select demand",
  unselectDemand: "Unselect demand",
  deleteDemand: "Delete demand",
  viewed: "Viewed",
  markAsRead: "Mark as read",
  demandDetailEmailAndPhone: "Email - Phone: ",
  demandDetailNationalIdNumber: "National Id Number: ",
  demandDetailGender: "Gender: ",
  demandDetailsDateOfBirth: "Date of birth: ",
  demandDetailsNames: "First and last name: ",
  demandDetailsStructureName: "Structure name: ",
  demandDetailsStructureAddress: "Address: ",
  demandDetailsStructureCategory: "Structure category: ",
  demandDetailsSpecificIndications: "Specific indications: ",
  demandDetailsDemandDateTime: "Received at: ",
  rejectDemandButton: "Reject",
  validateDemandButton: "Validate",
  demandActionnedValidated: "VALIDATED",
  demandActionnedRejected: "REJECTED",
  demandDeletionFailedMessage: `Demand deletion failed.\nPlease try again!`,
  demandsDeletionFailedMessage: `Demands deletion failed.\nPlease try again!`,
  demandDeletionSuccessMessage: `Demand successfully deleted!`,
  demandsDeletionSuccessMessage: `Demands successfully deleted!`,
  couldNotLoadActiveDemandData:
    "Could not get the demand data. Please refresh!",
  couldNotLoadDemandBodyData: "Could not load the demands. Please refresh!",

  //RejectDemandDialog
  rejectDemandPlaceholder: "Enter rejection reason here!",
  cancelRejectionButton: "Cancel",
  rejectDemandReasonDialogHeader: "Rejection reason",
  rejectDemandDialogMessage: `NB: There is no possibility of return to this action.
                \nMake sure you really want to reject the request before proceeding!!!`,
  validateDemandDialogHeader: "Validate demand?",
  validateDemandDialogMessage: `Are you sure you want to validate this demand?
                \nNote that it is not possible to undo this action. Do you really want to continue?`,
  emptyRejectionReasonErrorMessage:
    "You cannot reject a demand without a reason",
  demandValidationSuccessMessage: "The demand was validated successfully!",
  demandValidationErrorMessage: `The demand could not be validated.\nPlease try again!`,
  demandRejectionSuccessMessage: "The demand was rejected successfully!",
  demandRejectionErrorMessage: `The demand could not be rejected.\nPlease try again!`,

  //DeleteDialog
  deleteDemandDialogHeader: "Delete demand",
  deleteDemandDialogMessage: `Are you sure you want to delete this demand?\n
                Note that this action is irreversible and you can't get the demand back later on.\n
                Do you still want to continue with the deletion?`,
  deleteSelectedDemandsDialogHeader: "Delete selected demands",
  deleteSelectedDemandsDialogMessage: `Are you sure you want to delete these selected demands?\n
                Note that this action is irreversible and you can't get the demands back later on.\n
                Do you still want to continue with the deletion?`,
  deleteDialogAgreeText: "YES",
  deleteDialogDisagreeText: "NO",

  //StructureSetting
  manageScheduleTooltip: "Manage opening and closing hours",
  personnalInformation: "Personal information",
  personnelImage: "Personnel's image",
  phoneResponsableLabel: "Phone",
  phoneResponsableStringError: "Phone must be a string",
  phoneResponsableRequiredError: "Personnel must have a phone number",
  phoneResponsableRegexFailed: "Enter a valid phone number",
  emailResponsableNotEmailError: "Enter a valid email",
  emailResponsableRequiredError: "Personnel must have an email",
  emailResponsableLabel: "Email",
  editResponsableDataButton: "Save Changes",
  changePhoto: "Change",
  professionalInformation: "Professional Information",
  structureDescriptionPlaceholder: "Structure Description *",
  structureParticularIndications: "Particular Indications *",
  structureSpecificIndicationsRequired:
    "Structure must have specific indications",
  structureDescriptionRequiredError: "Structure must have a description",
  structureNameRequiredError: "Structure must have a name",
  structureLabel: "Structure name",

  //ManageStructureBody
  structureDetailEmailAndPhone: "Email - Phone: ",
  structureDetailNationalIdNumber: "National Id Number: ",
  structureDetailGender: "Gender: ",
  structureDetailsDateOfBirth: "Date of birth: ",
  structureDetailsNames: "First and last name: ",
  structureDetailsStructureName: "Structure name: ",
  structureDetailsStructureAddress: "Address: ",
  structureDetailsStructureCategory: "Structure category: ",
  structureDetailsSpecificIndications: "Specific indications: ",
  structureDetailsStructureDateTime: "Validated on: ",
  disableStructureButton: "Deactivate Structure",
  enableStructureButton: "Activate Structure",
  validateToggleStructureAbilityButton: "Yes",
  cancelToggleStructureAbilityButton: "Cancel",
  disableStructureAbilityDialogHeader: "Disable Structure?",
  enableStructureAbilityDialogHeader: "Activate Structure?",
  disableStructureAbilityDialogMessage: `Are you sure you want to deactivate this structure?
                          \nThis will prevent the structure from posing any actions on the system!
                          \nAre you sure y00ou want to continue?`,
  enableStructureAbilityDialogMessage: `Are you sure you want to enable this structure?
                          \nThis will permit the structure to pose actions on the system!
                          \nAre you sure you want to continue?`,
  disableStructureAbilitySuccessMessage:
    "The structure was disabled successfully!",
  enableStructureAbilitySuccessMessage:
    "The structure was enabled successfully!",
  disableStructureAbilityErrorMessage:
    "Unfortunately we could not disable the structure. Please try again!",
  enableStructureAbilityErrorMessage:
    "Unfortunately we could not activate the structure. Please try again!",
  couldNotLoadActiveStructureData:
    "Could not load the demand's details. Please try again!",
  couldNotLoadStructureBodyData:
    "Could not load the structures. Please refresh to try again!",

  servicesHeaderService: "Name",
  deleteServiceMessage:
    "Deleting a service will remove it from all catalog where it has been use",
  deleteServiceDialogHeader: "Service deletion",
  serviceActionDetails: "Details",
  cancelButton: "cancel",
  agreeButton: "Agree",
  addServiceHint: "Add new service",
  serviceNameRequired: "A service name is required",
  unitPriceNumberError: "service price most be a number",
  unitPriceRequired: "service most have a unit price",
  createServiceDialogHeader: "Create new Service",
  handleServiceNameInput: "Service Name",
  handleServiceUnitPriceInput: "Service Price",
  serviceDescriptionLabel: "Service Description",
  serviceDescriptionPlaceholder: "Add description to service",
  serviceDetailsDescriptionHeader: "Service Description",
  importServiceImageLabel: "Import Service images",
  createServiceButton: "Create a service",
  servicesHeaderUnitPrice: "Price",
  servicesHeaderRating: "Rating",
  servicesHeaderNbrVotes: "Votes Number",
  servicesHeaderActions: "Actions",
  serviceDetailsImageHeader: "Service Image",
  editServiceDialogHeader: "Service Edit",
  editServiceButton: "Save",
  editServiceMessage: "Please wait a little...",
  closeActiveService: "Close description",
  editActtionService: "Edit service details",
  deleteActiveService: "Delete service",

  //StyledCatalog
  createdAt: "Created At: ",
  catalogPeriod: "Validity: ",
  openCatalogTooltip: "Open Catalog",
  editCatalogTooltip: "Edit Catalog",
  deleteCatalogTooltip: "Delete Catalog",

  //CreateCatalogDialog
  createCatalogStartInput: "Starts on",
  createCatalogEndInput: "Ends on",
  catalogStartDateRequiredError: "Catalog must have a start date",
  catalogEndDateRequiredError: "Catalog must have an end date",
  cancelCreateButton: "Cancel",
  createCatalogButton: "Create",
  createCatalogDialogHeader: "Create Catalog",
  minCatalogStartDateError: "Catalog cannot start a date before now!",
  minCatalogEndDateError: "Validity cannot end before starting!",
  editCatalogButton: "Save Changes",
  cancelEditCatalogButton: "Cancel",
  editCatalogDialogHeader: "Edit Catalog?",
  deleteCatalogDialogHeader: "Delete Catalog?",
  deleteCatalogMessage:
    "Are you sure you want to delete this catalog? Note that this action is irreversible",
  agreeDeleteCatalogButton: "Delete",
  cancelDeleteCatalogButton: "Cancel",
  editCatalogMessage: "Saving changes",
  editCatalogOverlapMessage:
    "Overlap with another catalog. Check dates and try again.",
  createCatalogOverlapMessage:
    "Overlap with another catalog. Check dates and try again.",
  properties: "Properties",
  notPayable: "Not Payable",
  revomeCatalog: `Remove from catalog`,
  editCatalog: `Edit catalog`,
  amount: "Amount",
  unitAmount: "Unit Price",
  serviceHeader: "Service",

  //Promotions
  promotionPeriod: "Validity: ",
  promotionType: "Type: ",
  reductionPromotion: "Reduction",
  bonusPromotion: "Bonus points",
  editPromotionTooltip: "Edit",
  deletePromotionTooltip: "Delete",

  //DeletePromotionDialog
  deletePromotionDialogHeader: "Delete Promotion?",
  deletePromotionMessage: `Are you sure you want to delete this promotion?
                        \nNote that this is irreversible.
                        \nDo you still want to continue?`,
  agreeDeletePromotionButton: "Yes",
  cancelDeletePromotionButton: "Cancel",

  //EditPromotionDialog
  promotionNameRequiredError: "The Promotion must have a name",
  reductionNumberError: "The reduction value must be a number",
  bonusNumberError: "The Bonus value must be a number",
  reductionValueRequiredError: "Must have a reduction percentage",
  bonusValueRequiredError: "Set the number of bonus points for this promotion",
  promotionStartDateRequiredError: "The promotion must have a start date",
  promotionEndDateRequiredError: "The promotion must have an end",
  minPromotionStartDateError: "The promotion can only start from tomorrow",
  editPromotionDialogHeader: "Edit Promotion?",
  editPromotionMessage: "Editing Promotion",
  editPromotionNameInput: "Promotion Name",
  editPromotionStartInput: "Start date",
  editPromotionEndInput: "End date",
  editReductionValueInput: "Reduction Percentage",
  editBonusValueInput: "Bonus Value",
  cancelEditButton: "Cancel",
  editPromotionButton: "Save changes",
  minPromotionEndDateError: "The promotion can end  from tomorrow",

  //PromotionDetails
  promotionDetailsServiceNameHeader: "Service",
  promotionDetailsUnitPriceHeader: "Unit Price",
  promotionDetailsDiscountedPriceHeader: "Discount Price",
  promotionDetailsActionHeader: "Actions",
  promotionDetailsHeaderRootPromotion: "Promotion",
  promotionDetailsHeaderRootPeriod: "Validity",
  promotionDetailsHeaderRootReduction: "Reduction Percentage",
  promotionDetailsHeaderRootBonus: "Defined Bonus",
  promotionDetailsAddServiceToPromotionTooltip: "Add service to promotion",
  promotionDetailsBackTooltip: "Back to promotions",
  StyledPromotionRemoveServiceFromPromotion: "Remove service from promotion",
  openPromotionTooltip: "Open promotion",

  //RemoveServiceFromPromotionDialog
  RemoveServiceFromPromotionDialogHeader: "Remove service from Promotion?",
  RemoveServiceFromPromotionMessage: `Are you sure you want to remove this service 
                        \nfrom the promotion?`,
  agreeRemoveServiceFromPromotionButton: "Remove Service",
  cancelRemoveServiceFromPromotionPromotionButton: "Cancel",

  //AddServiceToPromotion
  addServiceToPromotionDialogHeader: "Add Service to Promotion",
  addServiceToPromotionDialogMessage: `Start typing and select the services to add. Validate the "ADD SERVICE" button to add the selected services`,
  addServiceToPromotionAutocompletePlaceHolder: "Search a service",
  addServiceToPromotionCancelButton: "Cancel",
  addServiceButtonSingular: "Add Service",
  addServiceButtonPlural: "Add Services",
  addServiceToPromotionExistErrorPartOne: "The following services: ",
  addServiceToPromotionExistErrorPartTwo:
    "already exist in the promotion so were not added.",
  addServiceToPromotionNoActivePromotionError:
    "There is a problem refresh before continuing",
  addServiceToPromotionNoSelectedService: "No service selected yet",

  //CreatePromotionDialog
  promotionTypeRequiredError: "The promotion type is required",
  promotionTypeError: "The promotion type must be a text",
  valueNumberError: "Must be a number",
  valueRequiredError: "Field is required",
  createPromotionDialogHeader: "Create a Promotion",
  createPromotionButton: "Create",
  clearAutocomplete: "Clear text",
  listServicesAutocomplete: "List services",
  hideServicesAutocomplete: "Hide services",
  noServicesWithAddButton: `There are no services in this promotion. You can add one by
                clicking on the "+" icon on the bottom right of the screen`,
  noServicesWithoutAddButton: "There are no services in this promotion.",
  noPromotions: `You have no promotions. You can create one by clicking on the
                  "+" icon on the bottom right of the screen`,
  removeServiceFromList: "Remove from list",
  totalRemainingBonus:"Total Remaining: ",
  totalCashedInBonus: "Cashed In: ",
  totalCashedOutBonus: "Cashed Out: ",

  //Admins Siderbar
  Overview: "Overview",
  Demands: "Demands",
  Structures: "Structures",
  Mails: "Mails",
  Commands: "Commands",
  Services: "Services",
  Settings: "Profile Settings",
  Promotions: "Promotions",
  Publications: "Publications",
  Catalogs: "Catalogs",
  Logout: "Logout",
  //Publications
  noPublicationMessage: `You have no publications. Click on the "+" to add one`,
  publishedImageAlt: "A publication",
  unPublishedImageAlt: "To be published",
  addPublicationText: "Add photos  of your structure",
  addImageTooltip: "Add images",
  publishToBeUploadedButton: "Publish",
  publicationHeaderMessage: "Publish photos of your structure",
  alreadyPublished: "Your publications",
  deletePublication: "Delete",
  cancelDeletePublicationButton: "Cancel",
  agreeDeletePublicationButton: "Delete",
  publicationToBeDeletedAlt: "Publication to be deleted",
  deletePublicationMessage:
    "Are you sure you want to delete this publication? Note that this action is irreversible. Do you still want to continue?",
  deletePublicationDialogHeader: "Delete Publication?",
};
