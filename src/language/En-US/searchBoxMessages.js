export let searchEnMessages = {
  placeLabel: "where ?",
  mealLabel: "What ?",
  budgetLabel: "Budget",
  numberOfPersonsLabel: "Number of persons",
  searchButton: "Search",
  requiredFieldMessage: "This field is required",
  slogan: "The New Generation of Entertaintment",
};
