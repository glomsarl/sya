export let feedback = {
  feedbackDialogHeader: "Submit a feedback",
  editBonusSettingsMessage:
    "Please submit a review on your order to permit others know",
  feedbackTextLabel: "Your feedback",
  feedbackPlaceholder:
    "The structure is a good one with quick servings and good customer care",
  yourRating: "Your Rating: ",
  submitButton: "Submit",
};
