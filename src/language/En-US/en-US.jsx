import { defineMessages } from 'react-intl';
import {enMessages} from './headerMessages';
import { homeEnMessages } from './homeMessages';
import {searchEnMessages} from './searchBoxMessages';
import { userPageEnMessages } from './userPageMessages';
import { purchaseEnMessages } from './purchaseMessages';
import { landingMoreEnMessages } from './landingMoreMessages';
import { landingDetailsEnMessages } from './landingDetailsMessages';
import {purchaseWithSignInEnMessages} from './purchaseWithSignInMessages';
import { purchaseWithSignUpEnMessages } from './purchaseWithSignUpMessages';
import { en404Messages } from './en404Messages';
import { mapComponentMessages } from './mapComponentMessages';
import { configurations } from './configurations';
import { adminSubscriptions } from './adminSubcriptions';
import { subscriptionTracker } from './subscriptionTracker';
import { AdminPageSkeleton } from './adminPageSkeleton';
import { ownerStructures } from './ownerStructures';
import { feedback } from './feedback';

export default defineMessages({
    ...enMessages, 
    ...homeEnMessages,
    ...searchEnMessages,
    ...userPageEnMessages,
    ...purchaseEnMessages,
    ...landingMoreEnMessages,
    ...landingDetailsEnMessages,
    ...purchaseWithSignInEnMessages,
    ...purchaseWithSignUpEnMessages,
    ...en404Messages,
    ...mapComponentMessages,
    ...configurations,
    ...adminSubscriptions,
    ...subscriptionTracker,
    ...AdminPageSkeleton,
    ...ownerStructures,
    ...feedback
})