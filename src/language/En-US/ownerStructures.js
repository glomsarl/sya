export let ownerStructures = {
  selectStructure: "Select a structure",
  selectStructureSubtitle: `The selected structure will be taken into account by the rest of the tabs. All operations performed from then on will be referred to this structure`,
  myStructures: "My Structures",
  //orders details
  orderedByTitle: "Ordered by",
  orderedOnTitle: "Ordered",
  orderStatusTitle: "Ordered Status",
  paymentStatusTitle: "Payment Status",
  serviceStatusTitle: "Action",
  cancelledMessage: "Cancelled",
  confirmedMessage: "Confirmed",
  pendingMessage: "Pending",
  cashPaymentMessage: "Paid in Cash",
  syPaymentMessage: "Paid with sy",
  servedMessage: "Served",
  serveButton: "Serve",
  noAction: "Blocked",
  
  //order overview
  DailyStats: "Daily Statistics",
  WeeklyStats: "Weekly Statistics",
  MonthlyStats: "Monthly Statistics",
  YearlyStats: "Yearly Statistics",
  orderServingError: 'Cannot serve order before serving time selected by user',
};
