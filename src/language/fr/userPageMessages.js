export let userPageFrMessages = {
  myInfo: "Mes informatioins",
  myOrder: "Mes Commandes",
  register: "Inscrire ma structure",
  signOut: "Se déconnecter",
  man: "Homme",
  feminin: "Femme",
  profile: "Profile",
  profileUse:
    "Vos coordonnées seront communiquées à l'entreprise lors de vos commandes",
  obligated: "Champ obligatoire",
  update: "Mettre à jour",
  birthdate: "Date de Naissance",
  confirmed: "Comfirmée",
  unConfrimed: "Non confirmée",
  cancelled: "Annullée",
  notUpdate:
    "Aucun changement n'a été effectué.Veuillez réessayer une fois que vous aurez  modifié un champ.",
  gender: "Genre",
  manageStructures: "Gérer mes structures",
  goToAdmin: "Administrer SYA",
  category: "Nom de la categorie",
  bonusValue: "Bonus accumulés"
};