export let searchFrMessages = {
    placeLabel:"où ?",
    mealLabel:'Quoi ?',
    budgetLabel:'Budget',
    numberOfPersonsLabel:'Nombre de personnnes',
    searchButton:"Rechercher",
    requiredFieldMessage:'Ce champ est requis',
    slogan: "Nouvelle Génération de Divertissement"
}