export let fr404Messages = {
    message4041: "Désolé, la page à laquelle vous essayez d'accéder est introuvable. Cela se produit lorsque le lien n'existe pas sur notre serveur ou lorsque le lien a expiré.",
    statusNotChangeble: 'Une action ne peut plus etre faites sur cette commande'
}