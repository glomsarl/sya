export let purchaseWithSignUpFrMessages = {
    signUpMessage:'Entrer vos informations pour compléter votre reservation',
    firstName:'Prénom',
    lastName:'Nom',
    telephone:'Téléphone',
    code_value:'Code Promo',
    apply:'appliquer',
    agreeMessage:'We agree to receive messages concerning offer communicated by structures by email',
    profitMessage:'Par cette action vous gagnez',
    confidentiality:"Aucune publication n'est faites sans votre accord",
    checkboxRequired:'la case à cocher est requis pour continuer',
    invalidPhoneNumber:"Numéro de téléphone invailide",
    notSignUp:"je n'ai pas de compte",
}