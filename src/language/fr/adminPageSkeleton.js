export let AdminPageSkeleton = {
  ServicesPageTitle: "Services",
  ParametresPageTitle: "Profil",
  DemandesPageTitle: "Demandes",
  StructuresPageTitle: "Structures",
  dashboardPageTitle: "Tableau de bord",
  demandsPageTitle: "Demandes",
  mailsPageTitle: "Mails",
  configurationsPageTitle: "Configurations",
  myStructuresPageTitle: "Mes structures",

  ordersPageTitle: "Commandes",
  catalogsPageTitle: "Catalogues",
  promotionsPageTitle: "Promotions",
  publicationPageTitle: "Publications",
};
