export let frMessages = {
  //common
  title: "See You Again",
  copyright: "2021 SYA SARL",
  allRightsReserved: "TOUS DROITS RESERVES",

  // header
  aboutLinkHeader: "À propos",
  contactLinkHeader: "Contact",
  helpLinkHeader: "Aide",
  loginBoutonHeader: "connexion",
  signupButton: "s'inscrire",
  manageLinkHeader: "SYA Gérant",
  selectStructureText: "Inscrivez votre structure",
  createStructureText: "Creer",
  Cinéma: "Cinéma",

  // footer
  titlePlusSlogan: "See You Again - Bien yamo n'est plus un secret !",
  title1: "Liens utiles",
  title1About: "À propos de SYA",
  title1Help: "Vous avez besoin d'aide ?",
  title1Contact: "Contact",
  title1HaveStructure: "Vous possésez une structure ?",
  title1HaveNotStructure: "Vous ne possésez pas de structure ?",
  title1Policy: "Notre politique et confidentialité",
  title2: "Suivez nous ici",
  title3: "Bulletin d'information",
  footerOffer:
    "Les offres promotionnelles sont soumises à des conditions affichées sur la fiche détaillée des différentes structures.",
  boutonNewsletter: "Souscrire",
  labelInputEmailAddress: "Adresse email",

  //  Reset password
  BoutonRecoveryLink: "Envoyer le lien de récupération",
  CanNotConnectTitle: " Vous ne pouvez pas vous connecter ?",
  BackToConnectionText: " Retour à la connexion",
  RecoryLinkMessage:
    " Nous vous avons envoyé un lien de récupération à l'adresse :",
  CodeInputText: " Saisir le code de rénitialisation",
  CodeMatchesErrorMessage: " Code invalide",
  BoutonResetText: " Rénitialiser",
  ResendRecoveryLink: " Renvoyer le lien de récupération",
  ChooseNewPassword: " Choisir un nouveau mot de passe",
  NewMdp: " Nouveau mot de passe",
  MdpConfirm: " Confirmer le nouveau mot de passe",
  SaveMessage: " Enregistrer",
  BothPasswordSame: "Les deux mots de passe doivent être identiques",
  addressEmail: "Adresse email",
  CodeRequiredErrorMessage: "Un code est requis pour continuer",
  confirmPasswordRequired: "Ce champ ne peut être vide",
  password: "Mot de passe",
  notifyResetEmailLinkSent:
    "Un lien a été envoyé à l'adresse fournie. Cliquez sur le lien pour continuer !",

  //SignIn and SingUp
  SignUpConnectTitle: " Connectez-vous à votre compte",
  ConnectionBoutonText: " Connexion",
  Or: " Ou",
  ConnectionGoogle: " Se connecter avec Google",
  CreateAccount: " Créer un compte",
  privacyMessagePart1: "La",
  privacyMessagePart2: "politique de confidentialité",
  privacyMessagePart3: "et les ",
  privacyMessagePart4: "conditions d'utilisation",
  privacyMessagePart5: "de SYA s'appliquent.",
  privacyMessageSignPart1: "En m'inscrivant, j'accepte les",
  privacyMessageSignPart2: "et je confirme avoir pris connaissance de sa",
  signInBoutonText: "S'inscrire",
  accountExist: "Vous avez déjà un compte SYA ? Connectez-vous",
  firstname: "Prénom",
  lastname: "Nom",
  passwordField: "Mot de passe",

  //structureRequest
  demandPageTitle: "Inscrivez votre structure sur See You Again (SYA)",
  demandPageCaptiveMessage1: `Augmentez vos revenus, dotez-vous d’une meilleure visibilité et fidélisez
              \nvos clients en rejoignant notre globe de structures partenaires déjà
              \nréservables sur SYA, la première plateforme de recherche et de
              \nréservation en matiere de divertissement au Cameroun.`,
  demandPageCaptiveMessage2: `Utilisez, notre logiciel de réservation, gestion et preparation des services,
              \nsans engagement de durée. Avec une vue accru sur
              \nvos finances. Visibilité et inscription gratuite.`,

  //DemandeProvider
  stepOneMessage: "Informations sur la structure",
  stepTwoMessage: "Informations personnelles",
  stepThirdMessage: "Confirmation",

  //InfosStructure
  structureNameInput: "Nom de votre structure",
  addressInput: "Adresse",
  structureCategoryInput: "Catégorie de structure ",
  latitudeInput: "Latitude",
  longitudeInput: "Longitude",
  getPositionButtonText: "Cliquez ici pour avoir votre position",
  particularIndications: "Indications particulières",
  particularMessageHelperText: `Les indications particulières permettront à vos clients de situer
              \nd’avantage l’adresse de votre structure. Ex : village dèrrière
              \nbelavie, Ndokoti face tradex.`,
  nextButtonText: "Suivant",
  useGoogleLocalisationHeader: "Utilisez notre service de localisation",
  useGoogleLocalisationBody1: `Laissez nous vous aider à déterminer l'emplacement de votre
            structure, afin de ne pas vous tromper sur sa position exacte.`,
  useGoogleLocalisationBody2: `NB : Ne le faites que si vous vous trouvez actuellement dans votre
              structure.`,
  disagreeGoogleLocalisation: "Pas d'accord",
  agreeGoogleLocalisation: "D'accord",
  nameStringErrorMessage: "Le nom doit être une chaîne de caractères",
  nameRequiredErrorMessage: "La structure doit avoir un nom",
  nameMinCharacterErrorMessage:
    "Le nom doit comporter au moins de 3 caractères",
  addressStringErrorMessage: "L'adresse doit être une chaîne de caractères",
  addressRequiredErrorMessage: "La structure doit avoir une adresse",
  structureCategoryStringErrorMessage:
    "La catégorie doit être une chaîne de caractères",
  structureCategoryRequiredErrorMessage:
    "La structure doit etre d'une catégorie",

  //InfosProprietaire
  ownerNameStringErrorMessage: "Le nom doit être une chaîne de caractères",
  ownerNameRequiredErrorMessage: "Le propriétaire doit avoir un nom",
  ownerNameMinCharacterErrorMessage:
    "Le nom doit comporter au moins 3 caractères.",
  ownerLastNameStringErrorMessage:
    "Le prénom doit être une chaîne de caractères",
  ownerLastNameMinCharacterErrorMessage:
    "Le prénom doit comporter au moins 3 caractères.",
  ownerEmailErrorMessage: "Adresse e-mail invalide",
  ownerEmailRequiredErrorMessage:
    "Le propriétaire doit avoir une adresse e-mail",
  ownerPhoneErrorMessage:
    "Numéro de téléphone invalide. Ex : 6xxxxxxxx ou 2xxxxxxxx",
  ownerPhoneRequiredErrorMessage:
    "Le propriétaire doit avoir un numéro de téléphone",
  ownerDateOfBirthMaxDateErrorMessage:
    "La date de naissance doit être avant aujourd'hui",
  ownerDateOfBirthRequiredErrorMessage:
    "Le propriétaire doit avoir une date de naissance",
  ownerNationalIdStringErrorMessage:
    "Le numéro de CNI doit être une chaîne de caractères",
  ownerNationalIdRequiredErrorMessage:
    "Le propriétaire doit avoir une carte d'identité nationale",
  ownerFirstName: "Nom",
  ownerLastName: "Prénom",
  ownerEmail: "Adresse e-mail",
  ownerPhone: "Téléphone",
  ownerDateOfBirth: "Date de naissance",
  ownerGender: "Sexe",
  genderMale: "Masculin",
  genderFemale: "Feminin",
  ownerNationalId: "Numéro de CNI",
  unsubscribeNewsletter:
    "Je ne souhaite pas recevoir de communications de SY par e-mail/SMS",
  backButtonText: "Précédent",
  sendButtonText: "Soumettre la demande",

  //sucess structure demand
  successStructureCreation1: "Merci pour votre demande.",
  successStructureCreation2:
    "Nous avons envoyé par e-mail la confirmation de votre demande, et nous vous enverrons un e-mail lorsque votre demande sera confirmée",

  //SignUp client
  SignUpEmailErrorMessage: "Adresse e-mail non valide",
  SignUpEmailRequiredErrorMessage: "adresse e-mail est requis",
  SignUpMdpErrorMessage:
    "Le mot de passe doit comporter au comporter 3 caractères",
  SignUpMdpRequiredErrorMessage: "Mot de passe obligatoire",
  SignUpFirstNameStringErrorMessage: "doit être une chaîne de caractères",
  SignUpFirstNameRequiredErrorMessage: "Le nom est requis",
  SignUpFirstNameMinCharacterErrorMessage:
    "doit comporter au moins 3 caractères.",
  SignUpLastNameStringErrorMessage: "doit être une chaîne de caractères",
  SignUpLastNameRequiredErrorMessage: "Le prenom est requis",
  SignUpLastNameMinCharacterErrorMessage:
    "doit comporter au moins 3 caractères.",

  //confirmDemandHeader
  selectRejected: "Sélectionner toutes les demandes rejetées",
  unSelectRejected: "Désélectionner toutes les demandes rejetées",
  filter: "Filtrer",
  refresh: "Actualiser",
  deleteSelected: "Supprimer les demandes sélectionnées",
  more: "Plus",
  previousPage: "Page précédente",
  nextPage: "Page suivante",
  on: "sur",

  //confirmDemandBody
  selectDemand: "Sélectionner la demande",
  unselectDemand: "Désélectionner la demande",
  deleteDemand: "Supprimer la demande",
  viewed: "Lu",
  markAsRead: "Marquer comme lu",
  demandDetailEmailAndPhone: "Email - Téléphone : ",
  demandDetailNationalIdNumber: "Numéro de CNI : ",
  demandDetailGender: "Sexe : ",
  demandDetailsDateOfBirth: "Date de naissance : ",
  demandDetailsNames: "Nom et prénom : ",
  demandDetailsStructureName: "Nom de la structure : ",
  demandDetailsStructureAddress: "Adresse : ",
  demandDetailsStructureCategory: "Catégorie de structure : ",
  demandDetailsSpecificIndications: "Indications particulières : ",
  demandDetailsDemandDateTime: "Reçu à : ",
  rejectDemandButton: "Rejeter",
  validateDemandButton: "Valider",
  demandActionnedValidated: "VALIDÉE",
  demandActionnedRejected: "REJETÉE",
  demandDeletionFailedMessage: `La suppression de la demande a échoué.\nVeuillez réessayer !`,
  demandsDeletionFailedMessage: `La suppression a échoué.\nVeuillez réessayer !`,
  demandDeletionSuccessMessage: `Demande supprimée avec succès !`,
  demandsDeletionSuccessMessage: `Demandes supprimées avec succès !`,
  couldNotLoadActiveDemandData:
    "Impossible de télécharger les données de la demande. Veuillez actualiser !",
  couldNotLoadDemandBodyData:
    "Impossible de télécharger les demandes. Veuillez actualiser !",

  //RejectDemandDialog
  rejectDemandPlaceholder: "Entrez la raison du rejet ici !",
  cancelRejectionButton: "Annuler",
  rejectDemandReasonDialogHeader: "Motif du rejet",
  rejectDemandDialogMessage: `NB: il n’y a pas de retour possible sur cette action.
                \nAssurer vous de bien vouloir rejeter la demande avant de proceder !!!`,
  validateDemandDialogHeader: "Valider la demande ?",
  validateDemandDialogMessage: `Êtes-vous sûr de vouloir valider cette demande ?
                \nNotez qu'il n'est pas possible de revenir sur cette action. Voulez-vous toujours continuer ?`,
  emptyRejectionReasonErrorMessage:
    "Vous ne pouvez pas rejeter une demande sans motif.",
  demandValidationSuccessMessage: "La demande a été validée avec succès !",
  demandValidationErrorMessage: `La demande n'a pas pu être validée.\nVeuillez réessayer !`,
  demandRejectionSuccessMessage: "La demande a été rejetée avec succès !",
  demandRejectionErrorMessage: `La demande n'a pas pu être rejetée.\nVeuillez réessayer !`,

  //DeleteDialog
  deleteDemandDialogHeader: "Supprimer la demande",
  deleteDemandDialogMessage: `Êtes-vous sûr de vouloir supprimer cette demande ?
                \nNotez que cette action est irréversible et que vous ne pourrez pas récupérer la demande par la suite.
                \nVoulez-vous toujours poursuivre la suppression ?`,
  deleteSelectedDemandsDialogHeader: "Supprimer les demandes sélectionnées",
  deleteSelectedDemandsDialogMessage: `Êtes-vous sûr de vouloir supprimer les demandes sélectionnées ?
                \nNotez que cette action est irréversible et que vous ne pourrez pas récupérer les demandes par la suite.
                \nVoulez-vous toujours poursuivre la suppression ?`,
  deleteDialogAgreeText: "OUI",
  deleteDialogDisagreeText: "NON",

  //StructureSetting
  manageScheduleTooltip: "Gérer les horaires",
  personnalInformation: "Informations personnelles",
  personnelImage: "Image du personnel",
  phoneResponsableLabel: "Téléphone",
  phoneResponsableRequiredError:
    "Le personnel doit avoir un numéro de téléphone",
  phoneResponsableRegexFailed: "Entrez un numéro de téléphone valide",
  emailResponsableNotEmailError: "Entrez une adresse e-mail valide",
  emailResponsableRequiredError: "Le personnel doit avoir une adresse e-mail.",
  emailResponsableLabel: "Email",
  editResponsableDataButton: "Mettre à jour",
  changePhoto: "Changer",
  professionalInformation: "Informations Professionnelles",
  structureDescriptionPlaceholder: "Description de la structure *",
  structureParticularIndications: "Indications particulières *",
  structureSpecificIndicationsRequired:
    "La structure doit avoir des indications spécifiques.",
  structureDescriptionRequiredError: "La structure doit avoir une description",
  structureNameRequiredError: "La structure doit avoir un nom",
  structureLabel: "Nom de la structure",

  //ManageStructureBody
  structureDetailEmailAndPhone: "Email - Téléphone :",
  structureDetailNationalIdNumber: "Numéro de CNI : ",
  structureDetailGender: "Sexe : ",
  structureDetailsDateOfBirth: "Date de naissance : ",
  structureDetailsNames: "Nom et prénom : ",
  structureDetailsStructureName: "Nom de la structure : ",
  structureDetailsStructureAddress: "Adresse : ",
  structureDetailsStructureCategory: "Catégorie de structure : ",
  structureDetailsSpecificIndications: "Indications particulières : ",
  structureDetailsStructureDateTime: "Validée le :",
  disableStructureButton: "Désactiver la structure",
  enableStructureButton: "Activer la structure",
  validateToggleStructureAbilityButton: "Oui",
  cancelToggleStructureAbilityButton: "Annuler",
  disableStructureAbilityDialogHeader: "Désactiver la structure ?",
  enableStructureAbilityDialogHeader: "Activer la structure ?",
  disableStructureAbilityDialogMessage: `Êtes-vous sûr de vouloir désactiver cette structure ?
                          \nCela empêchera la structure de poser des actions sur le système !
                          \nÊtes-vous sûr de vouloir continuer ?`,
  enableStructureAbilityDialogMessage: `Etes-vous sûr de vouloir activer cette structure ?
                          \nCela permettra à la structure de poser des actions sur le système !
                          \nEtes-vous sûr de vouloir continuer ?`,
  disableStructureAbilitySuccessMessage:
    "La structure a été désactivée avec succès !",
  enableStructureAbilitySuccessMessage:
    "La structure a été activée avec succès !",
  disableStructureAbilityErrorMessage:
    "Malheureusement, nous n'avons pas pu désactiver la structure. Veuillez réessayer !",
  enableStructureAbilityErrorMessage:
    "Malheureusement, nous n'avons pas pu activer la structure. Veuillez réessayer !",
  couldNotLoadActiveStructureData:
    "Impossible de charger les détails de la demande. Veuillez réessayer !",
  couldNotLoadStructureBodyData:
    "Impossible de charger les structures. Veuillez actualiser pour réessayer !",

  servicesHeaderService: "Nom",
  deleteServiceMessage:
    "La suppression d'un service le supprimera de tous les catalogues où il a été utilisé",
  deleteServiceDialogHeader: "Suppression du service",
  serviceActionDetails: "Détails",
  cancelButton: "cancel",
  agreeButton: "Accepter",
  addServiceHint: "Ajouter un nouveau service",
  serviceNameRequired: "Un nom de service est requis",
  unitPriceNumberError: "Le prix du service doit être un nombre",
  unitPriceRequired: "Le service doit avoir un prix unitaire",
  createServiceDialogHeader: "Créer un nouveau service",
  handleServiceNameInput: "Nom du service",
  handleServiceUnitPriceInput: "Prix du service",
  serviceDescriptionLabel: "Description du service",
  serviceDescriptionPlaceholder: "Ajouter une description au service",
  serviceDetailsDescriptionHeader: "Service Description",
  importServiceImageLabel: "Importer les images du service",
  createServiceButton: "Créer un service",
  servicesHeaderUnitPrice: "Price",
  servicesHeaderRating: "Rating",
  servicesHeaderNbrVotes: "Votes Number",
  servicesHeaderActions: "Actions",
  serviceDetailsImageHeader: "Service Image",
  editServiceDialogHeader: "Service Edit",
  editServiceButton: "Enregistrer",
  editServiceMessage: "Veuillez patienter un peu...",
  closeActiveService: "Fermer la description",
  editActtionService: "Modifier les détails du service",
  deleteActiveService: "Supprimer le service",

  //StyledCatalog
  createdAt: "Créé à : ",
  catalogPeriod: "Validité : ",
  openCatalogTooltip: "Ouvrir le catalogue",
  editCatalogTooltip: "Editer le catalogue",
  deleteCatalogTooltip: "Supprimer le catalogue",

  //CreateCatalogDialog
  createCatalogStartInput: "Commence le",
  createCatalogEndInput: "Termine le",
  catalogStartDateRequiredError: "Catalogue doit avoir une date de début",
  catalogEndDateRequiredError: "Catalogue doit avoir une date de fin",
  cancelCreateButton: "Annuler",
  createCatalogButton: "Créer",
  createCatalogDialogHeader: "Créer un catalogue",
  minCatalogStartDateError:
    "Le catalogue ne peut pas commencer avant maintenant !",
  minCatalogEndDateError: "La validité ne peut pas finir avant de commencer !",
  editCatalogButton: "Enregistrer",
  cancelEditCatalogButton: "Annuler",
  editCatalogDialogHeader: "Modifier le Catalogue ?",
  deleteCatalogDialogHeader: "Supprimer Catalogue ?",
  deleteCatalogMessage:
    "Êtes-vous sûr de vouloir supprimer ce catalogue ? Notez que cette action est irréversible",
  agreeDeleteCatalogButton: "Supprimer",
  cancelDeleteCatalogButton: "Annuler",
  editCatalogMessage: "Sauvegarde des modifications",
  editCatalogOverlapMessage:
    "Chevauchement avec un autre catalogue. Vérifiez les dates et réessayez.",
  createCatalogOverlapMessage:
    "Chevauchement avec un autre catalogue. Vérifiez les dates et réessayez.",
  payable: "Payant",
  properties: "Propriétés",
  notPayable: "Non Payant",
  revomeCatalog: `Enlever du catalog`,
  editCatalog: `Editer le catalog`,
  amount: "Montant",
  unitAmount: "Prix Unitaire",
  serviceHeader: "Service",

  //Promotions
  promotionPeriod: "Validité : ",
  promotionType: "Type: ",
  reductionPromotion: "Réduction",
  bonusPromotion: "Points bonus",
  editPromotionTooltip: "Modifier",
  deletePromotionTooltip: "Supprimer",

  //DeletePromotionDialog
  deletePromotionDialogHeader: "Supprimer la promotion ?",
  deletePromotionMessage: `Êtes-vous sûr de vouloir supprimer cette promotion ? Notez que cette opération est irréversible. Voulez-vous toujours continuer ?`,
  agreeDeletePromotionButton: "Oui",
  cancelDeletePromotionButton: "Annuler",

  //EditPromotionDialog
  promotionNameRequiredError: "La promotion doit avoir un nom",
  reductionNumberError: "La valeur de la réduction doit être un nombre",
  bonusNumberError: "La valeur du bonus doit être un nombre",
  reductionValueRequiredError: "Il faut un pourcentage de réduction",
  bonusValueRequiredError: "Fixez le nombre de points bonus pour la promotion",
  promotionStartDateRequiredError: "La promotion doit avoir une date de début",
  promotionEndDateRequiredError: "La promotion doit avoir une fin",
  minPromotionStartDateError: "La promotion ne peut commencer que dès demain",
  minPromotionEndDateError: "La promotion ne peut finir avant de commencer",
  editPromotionDialogHeader: "Editer la promotion ?",
  editPromotionMessage: "Modification de la promotion",
  editPromotionNameInput: "Nom de la promotion",
  editPromotionStartInput: "Date de début",
  editPromotionEndInput: "Date de fin",
  editReductionValueInput: "Pourcentage de réduction",
  editBonusValueInput: "Valeur du bonus",
  cancelEditButton: "Annuler",
  editPromotionButton: "Sauvegarder",

  //PromotionDetails
  promotionDetailsServiceNameHeader: "Service",
  promotionDetailsUnitPriceHeader: "Prix unitaire",
  promotionDetailsDiscountedPriceHeader: "Prix escompté",
  promotionDetailsActionHeader: "Actions",
  promotionDetailsHeaderRootPromotion: "Promotion",
  promotionDetailsHeaderRootPeriod: "Validité",
  promotionDetailsHeaderRootReduction: "Taux de réduction",
  promotionDetailsHeaderRootBonus: "Bonus défini",
  promotionDetailsAddServiceToPromotionTooltip:
    "Ajouter un service à la promotion",
  promotionDetailsBackTooltip: "Retourner aux promotions",
  StyledPromotionRemoveServiceFromPromotion:
    "Retirer ce service de la promotion",
  openPromotionTooltip: "Ouvrir la promotion",

  //RemoveServiceFromPromotionDialog
  RemoveServiceFromPromotionDialogHeader:
    "Retirer le service de la promotion ?",
  RemoveServiceFromPromotionMessage: `Êtes-vous sûr de vouloir retirer ce service de la promotion ?`,
  agreeRemoveServiceFromPromotionButton: "Retirer le service",
  cancelRemoveServiceFromPromotionPromotionButton: "Annuler",

  //AddServiceToPromotion
  addServiceToPromotionDialogHeader: "Ajouter un service à la promotion",
  addServiceToPromotionDialogMessage: `Commencez à taper et sélectionnez les services à ajouter. Validez le bouton "AJOUTER LE SERVICE" pour ajouter les services sélectionnés.`,
  addServiceToPromotionAutocompletePlaceHolder: "Rechercher un service",
  addServiceToPromotionCancelButton: "Annuler",
  addServiceButtonSingular: "AJOUTER LE SERVICE",
  addServiceButtonPlural: "Ajouter les services",
  addServiceToPromotionExistErrorPartOne: "Les services suivants : ",
  addServiceToPromotionExistErrorPartTwo:
    "existent déjà dans la promotion et n'ont donc pas été ajoutés.",
  addServiceToPromotionNoActivePromotionError:
    "Il y a un problème - actualisez avant de continuer",
  addServiceToPromotionNoSelectedService:
    "Aucun service n'a encore été sélectionné",

  //CreatePromotionDialog
  promotionTypeRequiredError: "Le type de promotion est obligatoire",
  promotionTypeError: "Le type de promotion doit être un texte",
  valueNumberError: "Doit être un nombre",
  valueRequiredError: "Le champ est obligatoire",
  createPromotionDialogHeader: "Créer une promotion",
  createPromotionButton: "Créer",
  clearAutocomplete: "Vider champ",
  listServicesAutocomplete: "Lister les services",
  hideServicesAutocomplete: "Cacher les services",
  noServicesWithAddButton: `Il n'y a pas de services dans cette promotion. Vous pouvez en ajouter un en
                 cliquant sur l'icône "+" à bas droite de l'écran.`,
  noServicesWithoutAddButton: "Il n'y a pas de services dans cette promotion.",
  noPromotions: `Vous n'avez pas de promotions. Vous pouvez en créer une en cliquant sur l'icône 
                  "+" en bas à droite de l'écran`,
  removeServiceFromList: "Retirer de la liste",
  totalRemainingBonus: "Total Restant : ",
  totalCashedInBonus: "Montant encaissé : ",
  totalCashedOutBonus: "Montant décaissé : ",
  
  //Admins Siderbar
  Overview: "Vue d'ensemble",
  Demands: "Demandes",
  Structures: "Structures",
  Mails: "Mails",
  Commands: "Commandes",
  Services: "Services",
  Settings: "Paramètres profile",
  Promotions: "Promotions",
  Publications: "Publications",
  Catalogs: "Catalogues",
  Logout: "déconnexion",
  //Publications
  noPublicationMessage: `Vous n'avez aucune publication. Cliquez sur le "+" pour en ajouter une`,
  publishedImageAlt: "une publication",
  unPublishedImageAlt: "A publier",
  addPublicationText: "Ajoutez des photos de votre structure",
  addImageTooltip: "Ajouter des images",
  publishToBeUploadedButton: "Publier",
  publicationHeaderMessage: "Publier des photos de votre structure",
  alreadyPublished: "Vos publications",
  deletePublication: "Supprimer",
  cancelDeletePublicationButton: "Annuler",
  agreeDeletePublicationButton: "Supprimer",
  publicationToBeDeletedAlt: "Publication à supprimer",
  deletePublicationMessage:
    "Êtes-vous sûr de vouloir supprimer cette publication ? Notez que cette action est irréversible. Voulez-vous toujours continuer ?",
  deletePublicationDialogHeader: "Supprimer la publication ?",
};
