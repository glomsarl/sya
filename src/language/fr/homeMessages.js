export let homeFrMessages = {
  subtitle1: 'A propos de SYA',
  subtitle1Text:
    'SYA est une plateforme numérique de recensement et d’organisation des lieux de divertissement dans les villes de Douala, Yaoundé, Limbe et Kribi. L’objectif visé par cette plateforme est de s’établir dans le temps comme un outil incontournable du divertissement et du tourisme au Cameroun et de promouvoir les petites et moyennes entreprises spécialistes en la matière.',
  subtitle2: 'Sélection SY',
  structureAvgPrice: 'Prix moyen',
  structureRating: 'Note',
  reserve: 'Reserver',
  takeAway: 'Emporter',
  delivery: 'Livraison',
  SYButton: 'SY',
  allCategory: 'Toutes les categories',
  seeMore: 'VOIR PLUS',
  manege: 'Arcade | Manège',
  restaurant: 'Restaurant',
  beautySalon: 'Institut de beauté',
  cinema: 'Salle de Cinéma',
  howItWorks: 'COMMENT CA MARCHE ?',
  ownerTitle: 'Propriétaire',
  ownerContent:
    'Une fois la demande de creation de ta structure validée, deviens propriétaire et manager de ta propre structure.',
  actions: 'Achètes ou reserves',
  actionsContent:
    'Fais le choix de ta structure, ensuite achètes ou reserves les différents produit fournis par la structure.',
  evaluation: 'Evaluation et commentaires',
  evaluationContent:
    'Une fois la demande de creation de ta structure validée, deviens propriétaire et manager de ta propre structure.',
  payement: 'Mode de Paiement',
  payementContent:
    'Fais le choix de ta structure, ensuite achètes ou reserves les différents produit fournis par la structure.',
  greatQuestion:
    'Possédez-vous un Restaurant, Fast Food, Glacier, Cinéma, Arcade/Manège ou un Institut de beauté ?',
  callToSignUp: 'Inscrivez votre structure',
  callToSignUpText:
    'Donnez-nous plus de détails, et nous vous contacterons le plus rapidement possible.',
  moreInfoButton: 'VOIR PLUS D’INFORMATIONS',
  callToSignIn: 'Déjà client',
  callToSignInText: 'connectez-vous à SYA Manager et gérer votre structure',
  goToSignInButton: 'Se connecter',
  setSchedule: 'Configurer vos horaires',
  selectActiveStructure: 'Choisissez une structure comme active',
  searchResults: 'Les structures avec vos options de recherches',
  ownerPassword: "Mot de passe",
  ownerConfirmPassword: "Comfirmez votre mot de passe",
  loginNowBouton: "Connectez-vous",
  loginNowText: " et ajouer les services de votre structure.",
  addImage: "Ajouter une image",
  ownerPasswordRequiredErrorMessage: "Le mot de passe est requis",
  personnalInformationNotMatchPasswordsError: 'Mot de passse ne correspond pas',

  deleteServiceMessage: 'Etes-vous sure de vouloir supprimer',
  deleteServiceDialogHeader: 'Suppresion d\'un service',
  cancelButton: 'Annuler',
  agreeButton: "j'accepte",
}
