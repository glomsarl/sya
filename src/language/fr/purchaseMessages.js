export let purchaseFrMessages = {
  orderType: 'Type de commande: ',
  quantity: 'Quantité',
  deliveryRadio: 'Livraison à domicile',
  takeAwayRadio: 'Importation',
  reserveRadio: 'Reservation',
  today: 'Today',
  tomorrow: 'Demain',
  nextDay: 'The following day',
  date: 'Date',
  time: 'Heure',
  orderTitle: 'COMMANDER',
  goOn: 'Continuer',
  spendSY: 'Depenser mes SY',
  SYuseCondition: "SY n'est pas valide si vous n'est enregistrer",
  noPromotion: 'Standard sans promotion',
  withoutOffer: 'Action sans offres',
  specialOffer: 'Offre 1- Offre actuellement disponible',
  categoryRequired: "Le choix d'une categorie est requis",
  dateRequired: "Le choix d'une date est requis",
  timeRequired: "Le choix d'une heure est requis",
  december: 'Decembre',
  closeDialog: 'Fermer',
  summary: 'Récapitulatif',
  informations: 'Information Personnel',
  reduction: 'Réduction : ',
  total: 'Prix Total : ',
  finalPrice: 'Prix à payer :',
  price: 'Prix unique :',
  order: 'Order :',
  LessSY:
    'Vous avez pas assez de point SY pour poursuivre cette achat, veuillez essayer un autre ou continuer sans offre',
  SYTotal: 'SY Total : ',
  orderTitleSuccess: 'Commander avec Succèss',
  successBonus: 'seront credités à votre compte',
  successBonus2:
    'Ces points sy seront transférés sur votre compte après que le service ait été servi par le propriétaire de la structure.',
  successMessage:
    'Votre commande est confirmée, vous allez recevoir un email de confimation',
  cancel: 'Annuler la commande',
  traject: 'Etineraire',
  horaireNotify: 'La structure est fermé ce jour',
  failureMessage:
    'Désolé, la structure ne peut pas encaiser vos SY pour le moment. Veuillez essayer ultérieurement ou payer en cash',
  orderTitleFailure: 'Commande Echéante',
  cancelMessage: 'Votre commande a été annulé avec success',
  continueAs: 'Continuez en tant que ',
  quantityError: 'La quantité doit etre superieur 0',
  startFrom: 'Commencer à partir du',
  promotionCode: 'Entrer votre code promo si vous en avez un',
}
