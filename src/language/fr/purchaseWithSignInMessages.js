export let purchaseWithSignInFrMessages = {
    invalidEmail:'Email invalide, veuilez le corriger',
    emailHolder:'Votre adresse e-mail',
    signInMessage:'Entre votre adresse e-mail pour compléter votre réservation',
    emailAddress:'Adresse e-mail',
    orderConnection:'Se connecter avec Google',
    confidentiality:"Aucune publication n'est faites sans votre accord",
}