import { defineMessages } from "react-intl";
import {frMessages} from './headerMessages';
import {homeFrMessages} from './homeMessages';
import {searchFrMessages} from './searchBoxMessages';
import {purchaseFrMessages} from './purchaseMessages';
import { userPageFrMessages } from './userPageMessages';
import { landingMoreFrMessages } from './landingMoreMessages';
import {landingDetailsFrMessages} from './landingDetailsMessages';
import { purchaseWithSignInFrMessages } from "./purchaseWithSignInMessages";
import { purchaseWithSignUpFrMessages } from "./purchaseWithSignUpMessages";
import { fr404Messages } from "./fr404Messages";
import { mapComponentMessages } from "./mapComponentMessages";
import { configurations } from "./configurations";
import { adminSubscriptions } from "./adminSubcriptions";
import { subscriptionTracker } from "./subscriptionTracker";
import { AdminPageSkeleton } from "./adminPageSkeleton";
import { ownerStructures } from './ownerStructures';
import { feedback } from "./feedback";

export default defineMessages({
  ...frMessages,
  ...searchFrMessages,
  ...homeFrMessages,
  ...landingDetailsFrMessages,
  ...purchaseFrMessages,
  ...landingMoreFrMessages,
  ...purchaseWithSignInFrMessages,
  ...purchaseWithSignUpFrMessages,
  ...userPageFrMessages,
  ...fr404Messages,
  ...mapComponentMessages,
  ...configurations,
  ...adminSubscriptions,
  ...subscriptionTracker,
  ...AdminPageSkeleton,
  ...ownerStructures,
  ...feedback,
});
