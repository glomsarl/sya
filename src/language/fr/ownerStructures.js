export let ownerStructures = {
  selectStructure: 'Sélectionnez une structure',
  selectStructureSubtitle: `La structure sélectionnée sera prise en compte par le reste des onglets. Toutes les opérations effectuées à partir de ce moment-là feront référence à cette structure`,
  myStructures: 'Mes Structures',
  //orders details
  orderedByTitle: 'Commandé par',
  orderedOnTitle: 'Commandé',
  orderStatusTitle: ' État de la commande ',
  paymentStatusTitle: ' Etat du paiement ',
  serviceStatusTitle: 'Action',
  cancelledMessage: 'Annulé',
  confirmedMessage: 'Confirmé',
  pendingMessage: 'En attente',
  cashPaymentMessage: 'Payé en espèces',
  syPaymentMessage: 'Payé en sy',
  servedMessage: 'Servi',
  serveButton: 'Serve',
  noAction: 'Blocké',

  //order overview
  DailyStats: 'Statistiques quotidiennes',
  WeeklyStats: 'Statistiques hebdomadaires',
  MonthlyStats: 'Statistiques mensuelles',
  YearlyStats: 'Statistiques annuelles',
  orderServingError:
    "Impossible de servir la commande avant l'heure de service sélectionnée par l'utilisateur.",
}
