export let feedback = {
  feedbackDialogHeader: "Soumettre un commentaire",
  editBonusSettingsMessage:
    "Veuillez soumettre un commentaire concernant votre commande pour le faire savoir aux autres",
  feedbackTextLabel: " Votre avis ",
  feedbackPlaceholder:
    "La structure est bonne avec des services rapides et un bon service client",
  yourRating: "Votre note : ",
  submitButton : "Soumettre",
};
