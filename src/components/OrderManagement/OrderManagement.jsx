import React, { useState, useEffect } from 'react'
import { injectIntl } from 'react-intl'
import {
  makeStyles,
  Card,
  CardContent,
  Grid,
  Typography,
  Box,
  Collapse,
  Button,
  Hidden,
} from '@material-ui/core'
import Scrollbars from 'react-custom-scrollbars-2'
import { ExpandLessRounded, ExpandMoreRounded } from '@material-ui/icons'
import ServiceOrderDetails from './ServiceOrderDetails'
import OrderSkeleton from './OrderSkeleton'
import {
  getOrderStatistics,
  getOrderStatisticsByServices,
} from '../../services/statistic.service'
import { useStructure } from '../../contexts/Structures/Structure.provider'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { getAllOrders } from '../../services/order.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
const useStyles = makeStyles((theme) => ({
  numberCount: {
    fontWeight: '700',
  },
  period1: {
    backgroundColor: '#D0B17A',
  },
  period7: {
    backgroundColor: '#BC625A',
  },
  period30: {
    backgroundColor: '#718355',
  },
  period360: {
    backgroundColor: '#1E6091',
  },
  gridItem: {
    [theme.breakpoints.down('xs')]: {
      width: '86vw',
    },
  },
  clickItem: {
    cursor: 'pointer',
    '&:hover': {
      fontWeight: 'bolder',
      transition: '0.25s',
    },
  },
  Overall: {
    padding: '10px',
    display: 'grid',
    gridAutoFlow: 'column',
    alignItems: 'center',
    gap: '10px',
    backgroundColor: `${theme.palette.secondary.main}`,
    color: `${theme.palette.primary.main}`,
  },
}))

function giveStatTitle(statNumber) {
  switch (statNumber) {
    case 1:
      return 'DailyStats'
    case 7:
      return 'WeeklyStats'
    case 30:
      return 'MonthlyStats'
    case 360:
      return 'YearlyStats'
    default: {
      break
    }
  }
}

function StyledStatistic({ statistic, handleClick, formatMessage }) {
  const classes = useStyles()
  const [promotionGrow, setPromotionGrow] = useState(false)
  const {
    period,
    confirmed,
    unconfirmed,
    served,
    cancelled,
    most_ordered_service,
    service_name,
    service_id,
    main_image_ref,
  } = statistic
  return (
    <>
      <Grid
        item
        xs={12}
        sm={6}
        md={4}
        lg={3}
        xl={2}
        style={{ cursor: 'context-menu' }}
      >
        <Card
          onMouseOver={() => setPromotionGrow(true)}
          onMouseOut={() => setPromotionGrow(false)}
          elevation={promotionGrow ? 7 : 4}
          className={classes[`period${period}`]}
          style={
            period
              ? {
                  height: '100%',
                }
              : {
                  height: '100%',
                  background: `url(${process.env.REACT_APP_BASE_URL}/${main_image_ref})`,
                }
          }
        >
          <CardContent
            style={
              !period
                ? {
                    height: '100%',
                    backgroundColor: 'rgba(255,255, 255, 0.85)',
                  }
                : {
                    height: '100%',
                  }
            }
          >
            <Typography
              style={{
                fontWeight: 'Bold',
                fontSize: '1.5rem',
                marginBottom: '5px',
              }}
            >
              {period
                ? formatMessage({ id: `${giveStatTitle(period)}` })
                : `${service_name}`.toLocaleUpperCase()}
            </Typography>
            <Typography
              className={period ? null : classes.clickItem}
              onClick={() =>
                period
                  ? null
                  : handleClick(service_id, service_name, 'confirmed')
              }
            >
              {formatMessage({ id: 'Confirmed' })}
              <Typography variant='inherit' className={classes.numberCount}>
                {confirmed ?? 0}
              </Typography>
            </Typography>
            <Typography
              className={period ? null : classes.clickItem}
              onClick={() =>
                period
                  ? null
                  : handleClick(service_id, service_name, 'unconfirmed')
              }
            >
              {formatMessage({ id: 'Unconfirmed' })}
              <Typography variant='inherit' className={classes.numberCount}>
                {unconfirmed ?? 0}
              </Typography>
            </Typography>
            <Typography
              className={period ? null : classes.clickItem}
              onClick={() =>
                period ? null : handleClick(service_id, service_name, 'served')
              }
            >
              {formatMessage({ id: 'Served' })}
              <Typography variant='inherit' className={classes.numberCount}>
                {served ?? 0}
              </Typography>
            </Typography>
            <Typography
              className={period ? null : classes.clickItem}
              onClick={() =>
                period
                  ? null
                  : handleClick(service_id, service_name, 'cancelled')
              }
            >
              {formatMessage({ id: 'Cancelled' })}
              <Typography variant='inherit' className={classes.numberCount}>
                {cancelled ?? 0}
              </Typography>
            </Typography>
            {period ? (
              <Typography>
                {formatMessage({ id: 'MostOrdered' })}
                <Typography
                  noWrap
                  variant='inherit'
                  className={classes.numberCount}
                >
                  {most_ordered_service}
                </Typography>
              </Typography>
            ) : null}
          </CardContent>
        </Card>
      </Grid>
    </>
  )
}

function OrderManagement({ intl: { formatMessage } }) {
  const classes = useStyles()
  const [isOrderDataLoading, setIsOrderDataLoading] = useState(true)
  const [orders, setOrders] = useState({
    statistics: [],
    services: [],
    overall: {},
  })

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()
  const {
    state: {
      activeStructure: { structure_id },
    },
  } = useStructure()

  useEffect(() => {
    getOrderStatistics(structure_id)
      .then(({ statistics, overall }) => {
        getOrderStatisticsByServices(structure_id)
          .then((services) => {
            setOrders({ statistics, services, overall })
            setIsOrderDataLoading(false)
          })
          .catch((error) => {
            handleSessionExpiredError(
              personDispatch,
              location.pathname,
              error,
              formatMessage,
              navigate
            )
            setIsOrderDataLoading(false)
          })
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsOrderDataLoading(false)
      })
    // eslint-disable-next-line
  }, [])

  const [isServiceDetails, setIsServiceDetails] = useState(false)
  const [activeService, setActiveService] = useState({
    service_id: '',
    service_name: '',
    service_category: '',
    filter_category: '',
  })

  function handleSelectService(service_id, service_name, filter_category) {
    setIsServiceDetails(true)
    setActiveService({ service_id, service_name, filter_category })
    getAllOrders({ service_id, filter_category })
      .then((backend_orders) => {
        setOrders({
          ...orders,
          orders: backend_orders,
        })
        setIsOrderDataLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsOrderDataLoading(false)
      })
  }

  function handleUnselectService() {
    setIsServiceDetails(false)
    setActiveService({
      service_id: '',
      service_name: '',
      service_category: '',
      filter_category: '',
    })
  }

  const { statistics, services, overall } = orders
  const styledStatistics =
    statistics !== undefined
      ? statistics.map((statistic, index) => {
          return (
            <StyledStatistic
              statistic={statistic}
              handleClick={handleSelectService}
              formatMessage={formatMessage}
              key={index}
            />
          )
        })
      : null
  const styledServices =
    services !== undefined
      ? services.map((service, index) => {
          return (
            <StyledStatistic
              statistic={service}
              handleClick={handleSelectService}
              formatMessage={formatMessage}
              key={index}
            />
          )
        })
      : null

  const [isStatsVisible, setIsStatsVisible] = useState(true)

  return isOrderDataLoading ? (
    <Box
      style={{
        display: 'grid',
        gap: '20px',
        gridTemplateColumns: 'repeat(auto-fit, minmax(150px, 1fr))',
      }}
    >
      {[...new Array(12)].map((_, index) => (
        <OrderSkeleton key={index} />
      ))}
    </Box>
  ) : (
    <>
      {isServiceDetails ? (
        <ServiceOrderDetails
          service_id={activeService.service_id}
          service_name={activeService.service_name}
          filterCategory={activeService.filter_category}
          handleBackClick={handleUnselectService}
        />
      ) : (
        <Box
          style={{
            display: 'grid',
            gridTemplateRows: 'auto auto 1fr',
            minHeight: '100%',
          }}
        >
          <Box className={classes.Overall}>
            <Hidden xsDown>
              <Typography variant='body2'>
                {`${formatMessage({ id: 'allUnconfirmed' })} (${
                  overall.unconfirmed
                })`.toUpperCase()}
              </Typography>
              <Typography variant='body2'>
                {`${formatMessage({ id: 'allConfirmed' })} (${
                  overall.confirmed
                })`.toUpperCase()}
              </Typography>
              <Typography variant='body2'>
                {`${formatMessage({ id: 'allServed' })} (${
                  overall.served
                })`.toUpperCase()}
              </Typography>
            </Hidden>
            <Button
              variant='outlined'
              style={{ justifySelf: 'end', fontSize: '10px' }}
              color='primary'
              onClick={() => setIsStatsVisible(!isStatsVisible)}
            >
              {isStatsVisible ? (
                <>
                  {formatMessage({ id: 'hideStatsButton' })}
                  <ExpandLessRounded />
                </>
              ) : (
                <>
                  {formatMessage({ id: 'showStatsButton' })}
                  <ExpandMoreRounded />
                </>
              )}
            </Button>
          </Box>
          <Collapse in={isStatsVisible}>
            <Box className={classes.root} style={{ marginTop: '5px' }}>
              <Grid container spacing={2} className={classes.gridItem}>
                {statistics !== undefined &&
                statistics.length === 0 &&
                !isOrderDataLoading ? (
                  formatMessage({ id: 'noStatistics' })
                ) : statistics === undefined || isOrderDataLoading ? (
                  <OrderSkeleton />
                ) : (
                  styledStatistics
                )}
              </Grid>
            </Box>
          </Collapse>
          <Box
            className={classes.root}
            style={{ marginTop: '5px', minHeight: '100%' }}
          >
            <Scrollbars autoHide>
              <Grid container spacing={2} className={classes.gridItem}>
                {statistics !== undefined &&
                statistics.length === 0 &&
                !isOrderDataLoading ? (
                  formatMessage({ id: 'noStatistics' })
                ) : statistics === undefined || isOrderDataLoading ? (
                  <OrderSkeleton />
                ) : (
                  styledServices
                )}
              </Grid>
            </Scrollbars>
          </Box>
        </Box>
      )}
    </>
  )
}

export default injectIntl(OrderManagement)
