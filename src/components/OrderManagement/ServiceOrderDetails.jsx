import { useEffect, useState } from 'react'
import { FormattedDate, injectIntl } from 'react-intl'
import {
  Box,
  Typography,
  makeStyles,
  Grid,
  Button,
  Divider,
  Hidden,
  Fab,
  Tooltip,
  CircularProgress,
} from '@material-ui/core'
import {
  ArrowBackRounded,
  AttachMoneyRounded,
  BlockRounded,
  DoneAllRounded,
  DonutLargeRounded,
  PanToolOutlined,
} from '@material-ui/icons'
import ConfirmServiceDialog from './ConfirmServiceDialog'
import { confirmOrder, getAllOrders } from '../../services/order.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { notifySuccess } from '../../utils/toastMessages'

const useStyles = makeStyles((theme) => ({
  headerHolder: {
    display: 'grid',
    gridTemplateColumns: '1fr auto',
  },
  headerDivider: {
    borderRight: `1px solid ${theme.palette.primary.dark}`,
    margin: '5px 10px 5px 0',
  },
  orderHeader: {
    textTransform: 'uppercase',
  },
  bringOnExtraSmall: {
    display: 'none',
    fontWeight: 600,
    [theme.breakpoints.down('xs')]: {
      display: 'inline',
      paddingLeft: '10px',
    },
  },
  confirmedChip: {
    width: 'fit-content',
    padding: '5px',
    borderRadius: '5px',
    display: 'grid',
    gridAutoFlow: 'column',
    fontWeight: 'bold',
    fontSize: '0.75rem',
    color: 'white',
    backgroundColor: '#0d8b7a82',
    cursor: 'context-menu',
  },
  pendingChip: {
    backgroundColor: '#0036ff29',
    width: 'fit-content',
    padding: '5px',
    borderRadius: '5px',
    display: 'grid',
    gridAutoFlow: 'column',
    fontWeight: 'bold',
    fontSize: '0.75rem',
    cursor: 'context-menu',
  },
  cancelledChip: {
    backgroundColor: '#ff000069',
    width: 'fit-content',
    padding: '5px',
    borderRadius: '5px',
    display: 'grid',
    gridAutoFlow: 'column',
    fontWeight: 'bold',
    fontSize: '0.75rem',
    cursor: 'context-menu',
  },
  noActionChip: {
    backgroundColor: `${theme.palette.secondary.main}`,
    color: '#ffff',
    width: 'fit-content',
    padding: '5px',
    borderRadius: '5px',
    // border: '1px solid black',
    display: 'grid',
    gridAutoFlow: 'column',
    fontWeight: 'bold',
    fontSize: '0.75rem',
    cursor: 'context-menu',
  },
  tableGridItem: {
    [theme.breakpoints.down('xs')]: {
      display: 'grid',
      gridTemplateColumns: 'auto 1fr',
      gap: '5px',
    },
  },
}))
function ServiceOrderDetails({
  intl: { formatMessage },
  service_name,
  service_id,
  handleBackClick,
  filterCategory: filter_category,
}) {
  const classes = useStyles()

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  useEffect(() => {
    getAllOrders({ service_id, filter_category })
      .then((serviceOrders) => {
        setServiceOrders(serviceOrders)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })

    // eslint-disable-next-line
  }, [])
  const [serviceOrders, setServiceOrders] = useState([])

  const ServiceOrderHeader = () => (
    <>
      <Grid container spacing={2}>
        <Grid item xs={false} sm={3} classNam={classes.headerHolder}>
          <Typography noWrap variant='h4' className={classes.orderHeader}>
            {formatMessage({ id: 'orderedByTitle' })}
          </Typography>
          <Typography className={classes.headerDivider}></Typography>
        </Grid>
        <Grid item xs={false} sm={3} classNam={classes.headerHolder}>
          <Typography noWrap variant='h4' className={classes.orderHeader}>
            {formatMessage({ id: 'orderedOnTitle' })}
          </Typography>
          <Typography className={classes.headerDivider}></Typography>
        </Grid>
        <Grid item xs={false} sm={2} classNam={classes.headerHolder}>
          <Typography noWrap variant='h4' className={classes.orderHeader}>
            {formatMessage({ id: 'receitTitle' })}
          </Typography>
          <Typography className={classes.headerDivider}></Typography>
        </Grid>
        <Grid item xs={false} sm={2} classNam={classes.headerHolder}>
          <Typography noWrap variant='h4' className={classes.orderHeader}>
            {formatMessage({ id: 'orderStatusTitle' })}
          </Typography>
          <Typography className={classes.headerDivider}></Typography>
        </Grid>
        <Grid item xs={false} sm={2} classNam={classes.headerHolder}>
          <Typography noWrap variant='h4' className={classes.orderHeader}>
            {formatMessage({ id: 'paymentStatusTitle' })}
          </Typography>
          <Typography className={classes.headerDivider}></Typography>
        </Grid>
        {/* <Grid item xs={false} sm={2} classNam={classes.headerHolder}>
          <Typography noWrap variant='h4' className={classes.orderHeader}>
            {formatMessage({ id: 'serviceStatusTitle' })}
          </Typography>
        </Grid> */}
      </Grid>
      <Divider />
    </>
  )

  const StyledOrder = ({
    order: {
      quantity,
      order_id,
      is_served,
      ordered_on,
      unit_price,
      ordered_by,
      order_status,
      selected_date,
      payment_status,
    },
    formatMessage,
  }) => {
    const [isServiceDialogOpen, setIsServiceDialogOpen] = useState(false)

    const closeServiceDialog = () => {
      setIsServiceDialogOpen(false)
    }

    const location = useLocation()
    const navigate = useNavigate()
    const { personDispatch } = usePerson()

    const confirmOrderService = async () => {
      let theOrder = serviceOrders.find((order) => order.order_id === order_id)
      if (theOrder) {
        try {
          await confirmOrder(order_id)
          let newServiceOrders = serviceOrders.map((order) => {
            if (order.order_id === order_id)
              return { ...order, is_served: true }
            else return order
          })
          console.log(serviceOrders, newServiceOrders)
          setServiceOrders(newServiceOrders)
          notifySuccess(formatMessage({ id: 'serviceServedSuccessfully' }))
        } catch (error) {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
        }
      } else notifySuccess(formatMessage({ id: 'unknowMessageError' }))
    }

    return (
      <>
        <ConfirmServiceDialog
          isDialogOpen={isServiceDialogOpen}
          closeDialog={closeServiceDialog}
          confirmServingAPI={confirmOrderService}
        />

        <Grid
          container
          spacing={1}
          style={{ margin: '5px 0', alignItems: 'center' }}
        >
          <Grid item xs={12} sm={3} className={classes.tableGridItem}>
            <Typography className={classes.bringOnExtraSmall}>
              {formatMessage({ id: 'orderedByTitle' })}
            </Typography>
            <Typography id='serviceData2' noWrap style={{ display: 'inline' }}>
              {ordered_by}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3} className={classes.tableGridItem}>
            <Typography className={classes.bringOnExtraSmall}>
              {formatMessage({ id: 'orderedOnTitle' })}
            </Typography>
            <Typography id='serviceData2' noWrap style={{ display: 'inline' }}>
              {`On: `}
              <FormattedDate
                value={ordered_on}
                day='2-digit'
                month='long'
                year='numeric'
              />
              <br/>{`For: `}
              <FormattedDate
                value={selected_date}
                day='2-digit'
                month='long'
                year='numeric'
              />
            </Typography>
          </Grid>
          <Grid item xs={12} sm={2} className={classes.tableGridItem}>
            <Typography className={classes.bringOnExtraSmall}>
              {formatMessage({ id: 'receitTitle' })}
            </Typography>
            <Typography id='serviceData2' noWrap style={{ display: 'inline' }}>
              {`${quantity} x ${unit_price}`}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={2} className={classes.tableGridItem}>
            <Typography className={classes.bringOnExtraSmall}>
              {formatMessage({ id: 'orderStatusTitle' })}
            </Typography>
            <Typography id='serviceData2' noWrap style={{ display: 'inline' }}>
              {order_status === 'CONFIRMED' ? (
                <Typography className={classes.confirmedChip}>
                  {formatMessage({ id: 'confirmedMessage' })}
                  <DoneAllRounded
                    fontSize='small'
                    style={{ alignSelf: 'center', paddingLeft: '2px' }}
                  />
                </Typography>
              ) : order_status === 'UNCONFIRMED' ? (
                <Typography className={classes.pendingChip}>
                  {formatMessage({ id: 'pendingMessage' })}
                  <PanToolOutlined
                    fontSize='small'
                    style={{ alignSelf: 'center', paddingLeft: '2px' }}
                  />
                </Typography>
              ) : order_status === 'CANCELLED' ? (
                <Typography className={classes.cancelledChip}>
                  {formatMessage({ id: 'cancelledMessage' })}
                  <BlockRounded
                    fontSize='small'
                    style={{ alignSelf: 'center', paddingLeft: '2px' }}
                  />
                </Typography>
              ) : is_served ? (
                <Typography
                  style={{
                    color: 'white',
                    width: 'fit-content',
                    backgroundColor: '#28385ea8',
                    padding: '3px',
                    borderRadius: '5px',
                    display: 'grid',
                    alignContent: 'center',
                    gridAutoFlow: 'column',
                    cursor: 'context-menu',
                  }}
                >
                  {formatMessage({ id: 'servedMessage' })}{' '}
                  <DoneAllRounded fontSize='small' />{' '}
                </Typography>
              ) : null}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={2} className={classes.tableGridItem}>
            <Typography className={classes.bringOnExtraSmall}>
              {formatMessage({ id: 'paymentStatusTitle' })}
            </Typography>
            <Typography id='serviceData2' noWrap style={{ display: 'inline' }}>
              {payment_status === 'cash' ? (
                <Typography
                  style={{
                    backgroundColor: '#1f800087',
                    width: 'fit-content',
                    padding: '3px',
                    borderRadius: '5px',
                    display: 'grid',
                    gridAutoFlow: 'column',
                    fontWeight: 'bold',
                    color: 'white',
                    fontSize: '0.75rem',
                    cursor: 'context-menu',
                  }}
                >
                  {formatMessage({ id: 'cashPaymentMessage' })}
                  <AttachMoneyRounded
                    fontSize='small'
                    style={{ alignSelf: 'center' }}
                  />
                </Typography>
              ) : payment_status === 'sy' ? (
                <Typography
                  style={{
                    backgroundColor: 'rgb(248 198 0 / 61%)',
                    width: 'fit-content',
                    padding: '5px',
                    borderRadius: '5px',
                    display: 'grid',
                    gridAutoFlow: 'column',
                    fontWeight: 'bold',
                    fontSize: '0.75rem',
                    cursor: 'context-menu',
                  }}
                >
                  {formatMessage({ id: 'syPaymentMessage' })}
                  <DonutLargeRounded
                    fontSize='small'
                    style={{ alignSelf: 'center', paddingLeft: '2px' }}
                  />
                </Typography>
              ) : (
                <Typography
                  style={{
                    backgroundColor: '#0036ff29',
                    width: 'fit-content',
                    padding: '5px',
                    borderRadius: '5px',
                    display: 'grid',
                    gridAutoFlow: 'column',
                    fontWeight: 'bold',
                    fontSize: '0.75rem',
                    cursor: 'context-menu',
                  }}
                >
                  {formatMessage({ id: 'pendingMessage' })}
                  <PanToolOutlined
                    fontSize='small'
                    style={{ alignSelf: 'center', paddingLeft: '2px' }}
                  />
                </Typography>
              )}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={2} className={classes.tableGridItem}>
            <Typography id='serviceData2' noWrap style={{ display: 'inline' }}>
              {is_served ? (
                <Typography className={classes.noActionChip}>
                  {formatMessage({ id: 'noAction' })}
                  <BlockRounded
                    fontSize='small'
                    style={{ alignSelf: 'center', paddingLeft: '2px' }}
                  />
                </Typography>
              ) : order_status === 'cancelled' ? null : (
                <Button
                  variant='contained'
                  color='primary'
                  disabled={isServiceDialogOpen}
                  onClick={() => setIsServiceDialogOpen(true)}
                  size='small'
                >
                  {isServiceDialogOpen && (
                    <CircularProgress
                      color='secondary'
                      style={{
                        width: '17px',
                        height: '17px',
                        marginRight: '5px',
                      }}
                    />
                  )}
                  {formatMessage({ id: 'serveButton' })}
                </Button>
              )}
            </Typography>
          </Grid>
        </Grid>
        <Divider />
      </>
    )
  }
  return (
    <Box
      style={{
        height: '100%',
        position: 'relative',
      }}
    >
      <Typography
        style={{
          textTransform: 'uppercase',
          fontWeight: 700,
          fontSize: '1.1rem',
          paddingTop: '8px',
          paddingBottom: '16px',
        }}
      >
        {service_name}
      </Typography>
      <Hidden only='xs'>
        <ServiceOrderHeader />
      </Hidden>
      {serviceOrders.map((order) => (
        <StyledOrder order={order} formatMessage={formatMessage} />
      ))}
      <Fab
        color='primary'
        size='small'
        onClick={handleBackClick}
        style={{ position: 'fixed', bottom: '40px', right: '40px' }}
      >
        <Tooltip title={formatMessage({ id: 'back' })} arrow>
          <ArrowBackRounded fontSize='small' />
        </Tooltip>
      </Fab>
    </Box>
  )
}

export default injectIntl(ServiceOrderDetails)
