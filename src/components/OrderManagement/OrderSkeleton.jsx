import { Skeleton } from "@material-ui/lab";
import { Box } from "@material-ui/core";

export default function OrderSkeleton() {
  return (
    <Box
      style={{
        borderRadius: "5px",
        height: "100px",
        width: "100%",
        padding: "5px",
        backgroundColor: "white",
        boxShadow:
          "0px 2px 4px -1px rgb(0 0 0 / 20%), 0px 4px 5px 0px rgb(0 0 0 / 14%), 0px 1px 10px 0px rgb(0 0 0 / 12%)",
      }}
    >
      <Skeleton variant="text" width="90%" animation="wave" />
      <Skeleton variant="text" width="50%" animation="wave" />
      <Skeleton variant="text" width="20%" animation="wave" />
      <Skeleton variant="text" width="60%" animation="wave" />
    </Box>
  );
}
