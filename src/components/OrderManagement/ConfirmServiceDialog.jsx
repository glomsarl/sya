import { CircularProgress } from '@material-ui/core'
import {
  Dialog,
  Slide,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@material-ui/core'
import React, { useState } from 'react'
import { injectIntl } from 'react-intl'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />
})

function ConfirmServiceDialog({
  isDialogOpen,
  closeDialog,
  confirmServingAPI,
  intl: { formatMessage },
}) {
  const [isServing, setIsServing] = useState(false)

  async function confirmService() {
    setIsServing(true)
    await confirmServingAPI()
    setIsServing(false)
    closeDialog()
  }

  return (
    <Dialog
      open={isDialogOpen}
      TransitionComponent={Transition}
      keepMounted
      onClose={isServing ? null : closeDialog}
    >
      <DialogTitle>
        {formatMessage({ id: 'ServeOrderDialogHeader' })}
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          {formatMessage({ id: 'ServeOrderMessage' })}
        </DialogContentText>
        <DialogActions>
          <Button onClick={confirmService} disabled={isServing}>
            {isServing && (
              <CircularProgress size={24} style={{ marginRight: '8px' }} />
            )}
            {formatMessage({ id: 'agreeButton' })}
          </Button>
          <Button
            onClick={closeDialog}
            color='primary'
            disabled={isServing}
            variant='contained'
          >
            {formatMessage({ id: 'cancelButton' })}
          </Button>
        </DialogActions>
      </DialogContent>
    </Dialog>
  )
}

export default injectIntl(ConfirmServiceDialog)
