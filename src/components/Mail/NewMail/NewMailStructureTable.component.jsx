import { injectIntl } from 'react-intl'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles'
import Skeleton from '@material-ui/lab/Skeleton'
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Checkbox,
} from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  table: {
    '& .MuiTableCell-body': {
      color: theme.common.blue,
      fontWeight: 500,
      fontStyle: 'normal',
      fontSize: '0.885rem',
    },
  },
  disabledTable: {
    pointerEvents: 'none',
  },
}))

function NewMailStructureTable({
  isMailSending,
  areStructuresLoading,
  structures,
  selectStructure,
  selectedStructureEmails,
}) {
  const classes = useStyles()

  const isEmailSelected = (structure_email) =>
    selectedStructureEmails.indexOf(structure_email) !== -1

  return (
    <TableContainer>
      <Table
        className={clsx(classes.table, {
          [classes.disabledTable]: isMailSending,
        })}
        aria-labelledby='tableTitle'
        size='small'
        aria-label='enhanced table'
      >
        <TableBody>
          {areStructuresLoading
            ? [...new Array(4)].map((_, index) => (
                <TableRow key={index} disabled={true}>
                  <TableCell padding='checkbox'>
                    <Checkbox checked={false} disabled />
                  </TableCell>
                  <TableCell align='left'>
                    <Skeleton
                      variant='rect'
                      width={100}
                      height={15}
                      animation='wave'
                    />
                  </TableCell>
                  <TableCell>
                    <Skeleton
                      variant='rect'
                      width={500}
                      height={15}
                      animation='wave'
                    />
                  </TableCell>
                </TableRow>
              ))
            : structures.map(
                (
                  {
                    structure_manager,
                    structure_email,
                    structure_name,
                    category_name,
                    specific_indications,
                  },
                  index
                ) => {
                  const isItemSelected = isEmailSelected(structure_email)
                  const labelId = `structureCategoryId-list-table-checkbox-${index}`

                  return (
                    <TableRow
                      hover
                      onClick={(event) =>
                        selectStructure(event, structure_email)
                      }
                      tabIndex={-1}
                      role='checkbox'
                      aria-checked={isItemSelected}
                      key={`${structure_email}-${index}`}
                      selectedEmail={isItemSelected}
                    >
                      <TableCell padding='checkbox'>
                        <Checkbox
                          checked={isItemSelected}
                          inputProps={{ 'aria-labelledby': labelId }}
                          disabled={isMailSending}
                        />
                      </TableCell>
                      <TableCell
                        component='th'
                        id={labelId}
                        scope='row'
                        padding='none'
                      >
                        {structure_manager}
                      </TableCell>
                      <TableCell align='left'>{`${structure_name}-${specific_indications}-${category_name}`}</TableCell>
                    </TableRow>
                  )
                }
              )}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

export default injectIntl(NewMailStructureTable)
