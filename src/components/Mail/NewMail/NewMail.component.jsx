import { injectIntl } from 'react-intl'

import { useState, useEffect } from 'react'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Skeleton from '@material-ui/lab/Skeleton'
import { Button, Grid, TextField, CircularProgress } from '@material-ui/core'
import NewMailToolbar from './NewMailToolbar.component'
import NewMailStructureTable from './NewMailStructureTable.component'
import { getStructures, sendEmails } from '../../../services/mail.service'
import handleSessionExpiredError from '../../../utils/handleSessionExpiry'
import { notifySuccess } from '../../../utils/toastMessages'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../../contexts/PersonContext/Person.provider'
import { getStructureCagetories } from '../../../services/structure.service'

const useStyles = makeStyles((theme) => ({
  paper: {
    width: '100%',
    height: '100%',
    display: 'grid',
    padding: '15px',
    boxSizing: 'border-box',
    gridTemplateRows: 'auto 1fr auto',
  },
  messageTextField: {
    marginBottom: theme.spacing(3),
    '& .MuiInputBase-root': {
      color: theme.common.blue,
      fontWeight: 400,
      fontSize: '0.875rem',
      fontStyle: 'normal',
    },
    '& .MuiFilledInput-underline:before': {
      borderBottom: 'none',
    },
    '& .MuiFilledInput-underline:after': {
      borderBottom: 'none',
    },
    '& .MuiFormHelperText-root.Mui-error': {
      backgroundColor: '#fff',
      margin: 0,
      padding: '4px 14px 0px 14px',
    },
  },
  sendButton: {
    textTransform: 'none',
    fontSize: '0.9rem',
    fontStyle: 'normal',
    fontWeight: 700,
    borderColor: theme.common.blue,
  },
  circularProgress: {
    color: theme.palette.secondary.main,
    marginLeft: theme.spacing(1),
  },
}))

function NewMail({ intl: { formatMessage } }) {
  const classes = useStyles()
  const [selectedEmails, setSelectedEmails] = useState([])
  const [isMailSending, setIsMailSending] = useState(false)
  const [activeStructureCategoryId, setActiveStructureCategoryId] =
    useState(undefined)

  const [structures, setStructures] = useState([
    {
      structure_manager: '',
      structure_email: '',
      structure_name: '',
      category_name: '',
      specific_indications: '',
    },
  ])
  const [structureCategories, setStructureCategories] = useState([
    { category_id: undefined, category_name: '' },
  ])

  const handleClickStructure = (event, structure_email) => {
    const selectedStructure = selectedEmails.find((_) => _ === structure_email)

    if (selectedStructure)
      setSelectedEmails(selectedEmails.filter((_) => _ !== structure_email))
    else setSelectedEmails([...selectedEmails, structure_email])
  }
  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  const formik = useFormik({
    initialValues: {
      message: '',
    },
    validationSchema: Yup.object({
      message: Yup.string().required(
        formatMessage({ id: 'messageRequiredError' })
      ),
    }),
    onSubmit: (values, { resetForm }) => {
      const submitData = {
        message: values.message,
        emails: selectedEmails,
      }
      setIsMailSending(true)
      sendEmails(submitData)
        .then(() => {
          setSelectedEmails([])
          resetForm()
          setIsMailSending(false)
          notifySuccess('success')
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
          setIsMailSending(false)
        })
    },
  })

  const handleChangeStructureCategory = (category_id) => {
    setActiveStructureCategoryId(category_id)
  }

  const selectAllMails = (event) => {
    if (event.target.checked) {
      setSelectedEmails(
        structures.map(({ structure_email }) => structure_email)
      )
      return
    }
    setSelectedEmails([])
  }

  const [hasRefreshed, setHasRefreshed] = useState(false)
  const [areStructuresLoading, setAreStructuresLoading] = useState(true)
  const [areStructureCategoriesLoading, setAreStructureCategoriesLoading] =
    useState(true)

  useEffect(() => {
    getStructureCagetories()
      .then((categories) => {
        setStructureCategories(categories)
        setAreStructureCategoriesLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
    // eslint-disable-next-line
  }, [])

  useEffect(() => {
    getStructures({ structure_category_id: activeStructureCategoryId })
      .then((structures) => {
        setSelectedEmails([])
        setStructures(structures)
        setAreStructuresLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
    // eslint-disable-next-line
  }, [activeStructureCategoryId, hasRefreshed])

  const refreshStructureList = () => {
    getStructures({ structure_category_id: activeStructureCategoryId })
      .then((structures) => {
        setSelectedEmails([])
        setStructures(structures)
        setAreStructuresLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
  }
  const searchStructureHandler = (event) => {
    getStructures({
      structure_category_id: activeStructureCategoryId,
      search: event.target.value,
    })
      .then((structures) => {
        setSelectedEmails([])
        setStructures(structures)
        setAreStructuresLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
  }

  return (
    <Paper className={classes.paper} elevation={0}>
      <NewMailToolbar
        setHasRefreshed={setHasRefreshed}
        areStructuresLoading={areStructuresLoading}
        isMailSending={isMailSending}
        handleSelectAllMails={selectAllMails}
        changeStructureCategory={handleChangeStructureCategory}
        structureCategories={structureCategories}
        refreshStructureList={refreshStructureList}
        numberOfSelectedEmails={selectedEmails.length}
        totalNumberOfStructures={structures.length}
        activeStructureCategoryId={activeStructureCategoryId}
        searchStructureHandler={searchStructureHandler}
      />
      <NewMailStructureTable
        isMailSending={isMailSending}
        areStructuresLoading={areStructuresLoading}
        structures={structures}
        selectStructure={handleClickStructure}
        selectedStructureEmails={selectedEmails}
      />
      <Grid
        container
        direction='column'
        justifyContent='center'
        alignItems='center'
      >
        {areStructureCategoriesLoading && (
          <Skeleton
            style={{ marginBottom: 50 }}
            variant='rect'
            width='100%'
            height={220}
            animation='wave'
          />
        )}
        {!(areStructuresLoading && areStructureCategoriesLoading) && (
          <>
            <Grid item style={{ width: '100%' }}>
              <TextField
                placeholder={formatMessage({ id: 'messageLabel' })}
                className={classes.messageTextField}
                fullWidth
                multiline
                rows={10}
                maxRows={12}
                variant='filled'
                {...formik.getFieldProps('message')}
                error={Boolean(formik.errors.message && formik.touched.message)}
                helperText={formik.errors.message && formik.touched.message}
              />
            </Grid>
            <Grid item style={{ width: '100%', textAlign: 'end' }}>
              <Button
                variant='contained'
                className={classes.sendButton}
                color='secondary'
                disabled={
                  !formik.values.message ||
                  selectedEmails.length === 0 ||
                  isMailSending
                }
                onClick={formik.handleSubmit}
              >
                {formatMessage({ id: 'sendButton' })}
                {isMailSending && (
                  <CircularProgress
                    size={24}
                    className={classes.circularProgress}
                  />
                )}
              </Button>
            </Grid>
          </>
        )}
      </Grid>
    </Paper>
  )
}

export default injectIntl(NewMail)
