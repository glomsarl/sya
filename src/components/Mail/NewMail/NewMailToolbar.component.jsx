import { injectIntl } from 'react-intl'
import { alpha, makeStyles } from '@material-ui/core/styles'
import {
  Grid,
  MenuItem,
  TextField,
  Toolbar,
  Chip,
  Checkbox,
  IconButton,
  Tooltip,
} from '@material-ui/core'
import RefreshIcon from '@material-ui/icons/Refresh'

const useStyles = makeStyles((theme) => ({
  toolbar: {
    paddingLeft: theme.spacing(1),
    '& .MuiIconButton-root': {
      padding: '8px',
    },
  },
  selectTextField: {
    minWidth: '150px',
    '& .MuiInputBase-root': {
      minHeight: '24px',
      paddingLeft: theme.spacing(1),
      color: theme.common.blue,
      fontWeight: 600,
      fontSize: '0.875rem',
      fontStyle: 'normal',
    },
    marginLeft: theme.spacing(2),
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha('#E0E0E0', 0.8),
    '&:hover': {
      backgroundColor: '#E0E0E0',
    },
    paddingLeft: theme.spacing(1),
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(1),
    minWidth: '15ch',
    width: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(4),
    },
  },
}))

function NewMailToolbar({
  intl: { formatMessage },
  areStructuresLoading,
  setHasRefreshed,
  isMailSending,
  handleSelectAllMails,
  searchStructureHandler,
  changeStructureCategory,
  structureCategories,
  refreshStructureList,
  numberOfSelectedEmails,
  totalNumberOfStructures,
  activeStructureCategoryId,
}) {
  const classes = useStyles()

  return (
    <Toolbar className={classes.toolbar}>
      <Grid
        container
        direction='row'
        justifyContent='space-between'
        alignItems='center'
      >
        <Grid item>
          <Grid
            container
            direction='row'
            justifyContent='flex-start'
            alignItems='center'
          >
            <Checkbox
              indeterminate={
                numberOfSelectedEmails > 0 &&
                numberOfSelectedEmails < totalNumberOfStructures
              }
              checked={
                totalNumberOfStructures > 0 &&
                numberOfSelectedEmails === totalNumberOfStructures
              }
              onChange={handleSelectAllMails}
              inputProps={{ 'aria-label': 'select all emails' }}
              disabled={areStructuresLoading || isMailSending}
            />
            <Tooltip title={formatMessage({ id: 'refreshTooltipTitle' })}>
              <IconButton
                disabled={areStructuresLoading || isMailSending}
                onClick={refreshStructureList}
              >
                <RefreshIcon onClick={() => setHasRefreshed(true)} />
              </IconButton>
            </Tooltip>
            <TextField
              select
              variant='outlined'
              className={classes.selectTextField}
              value={activeStructureCategoryId}
              onChange={(event) => {
                changeStructureCategory(event.target.value)
              }}
              disabled={areStructuresLoading || isMailSending}
            >
              <MenuItem disabled value='#'>
                <em>{'select structure category'}</em>
              </MenuItem>
              {structureCategories.map(({ category_id, category_name }) => (
                <MenuItem key={category_id} value={category_id}>
                  {category_name ? formatMessage({ id: category_name }) : ''}
                </MenuItem>
              ))}
            </TextField>
            <TextField
              placeholder={formatMessage({ id: 'searchPlaceholder' })}
              inputProps={{ 'aria-label': 'search' }}
              className={classes.search}
              type='search'
              size='small'
              onChange={searchStructureHandler}
              disabled={areStructuresLoading || isMailSending}
            />
          </Grid>
        </Grid>
        <Grid item>
          {numberOfSelectedEmails > 0 && (
            <Chip
              label={`${numberOfSelectedEmails} ${formatMessage({
                id: 'selectedLabel',
              })}`}
              onDelete={handleSelectAllMails}
              color='secondary'
            />
          )}
        </Grid>
      </Grid>
    </Toolbar>
  )
}

export default injectIntl(NewMailToolbar)
