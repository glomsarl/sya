import {
  Box,
  Button,
  Grid,
  IconButton,
  makeStyles,
  Tooltip,
  Typography,
} from '@material-ui/core'
import { useEffect, useState } from 'react'
import { injectIntl } from 'react-intl'
import Skeleton from '@material-ui/lab/Skeleton'
import { useLocation, useNavigate } from 'react-router'
import CloseRoundedIcon from '@material-ui/icons/CloseRounded'
import EditRoundedIcon from '@material-ui/icons/EditRounded'
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded'
import { Scrollbars } from 'react-custom-scrollbars-2'

import DeleteServiceDialog from './DeleteServiceDialog'
import {
  addServiceImages,
  deleteService,
  fetchServiceData,
  getServiceImages,
} from '../../services/structure.service'
import { useService } from '../../contexts/Service/Service.provider'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { usePerson } from '../../contexts/PersonContext/Person.provider'

let useStyles = makeStyles((theme) => ({
  serviceRecord: {
    alignItems: 'center',
    borderBottom: '1px solid #f8f7f7',
    padding: '10px 0',
    '&:hover': {
      backgroundColor: 'rgba(168, 175, 213, 0.2)',
    },
  },
  activeService: {
    alignItems: 'start',
    borderBottom: '1px solid grey',
    padding: '10px 0',
    position: 'relative',
    backgroundColor: 'rgba(168, 175, 213, 0.2)',
    [theme.breakpoints.down('xs')]: {
      position: 'relative',
    },
  },
  serviceData: {
    display: 'inline',
  },
  serviceDataOthers: {
    display: 'inline',
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  serviceDataNumber: {
    paddingLeft: '10px',
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  serviceActions: {
    gridAutoFlow: 'column',
    justifyItems: 'center',
    display: 'grid',
    justifyContent: 'center',
    position: 'absolute',
    top: 9,
    right: 0,
    width: 'fit-content',
    marginTop: '-10px',
    [theme.breakpoints.down('xs')]: {
      position: 'absolute',
      top: '10px',
      right: 0,
      width: 'fit-content',
      backgroundColor: theme.palette.primary.main,
      borderRadius: '10px',
    },
  },
  serviceDetailsDescription: {
    display: 'grid',
    gridTemplateRows: 'auto 1fr',
    justifyItems: 'center',
  },
  serviceImage: {
    height: '200px',
  },
  justifyImage: {
    textAlign: 'center',
  },
  detailsHeader: {
    fontWeight: 'bolder',
  },
  removeOnSmall: {
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  descriptionData: {
    justifyContent: 'center',
  },
  serviceNamePaddingTop: {
    paddingTop: 0,
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '10px',
    },
  },
  serviceNamePaddingTopActive: {
    [theme.breakpoints.down('xs')]: {
      paddingTop: '35px',
    },
  },
  bringOnExtraSmall: {
    display: 'none',
    fontWeight: 600,
    [theme.breakpoints.down('xs')]: {
      display: 'inline',
      paddingLeft: '10px',
    },
  },
  deleteButton: {
    color: theme.palette.error.main,
  },
}))
function StyledService({ intl, service, index, openDialog }) {
  let { serviceState, serviceDispatch } = useService()
  let { isActiveServiceDataLoading, activeService } = serviceState
  let classes = useStyles()
  let [isDeleting, setIsDeleting] = useState(false)
  let [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false)
  const { formatMessage } = intl
  let { service_id, service_name, unit_price, rating, number_of_votes } =
    service

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()
  /**
   * show the details of a service.
   * @param {String} service_id Id of the service to be openned
   */
  const activateService = (service_id) => {
    fetchServiceData(service_id)
      .then((service_details) => {
        serviceDispatch({ type: 'ACTIVATE_SERVICE', payload: service_id })
        serviceDispatch({
          type: 'LOAD_ACTIVE_SERVICE_DATA',
          payload: service_details,
        })
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
  }

  /**
   * close the active service
   */
  let closeActiveService = () => {
    serviceDispatch({ type: 'CLOSE_ACTIVE_SERVICE' })
  }

  /**
   * Open the HandleService dialog
   * with edit action
   */
  let openEditDialog = () => {
    openDialog('edit')
  }

  /**
   * Open delete service dialog
   * to get confirmation of the deletion
   */
  let openConfirmDeleteDialog = () => {
    setIsDeleteDialogOpen(true)
  }

  /**
   * close the delete service dialog
   */
  let closeConfirmDeleteDialog = () => {
    setIsDeleteDialogOpen(false)
  }

  /**
   * delete a service from the list of active services.
   * @param {String} service_id service id of service to be deleted
   * actionned when the deletion of a service is confirmed.
   */
  const deleteServiceHandler = () => {
    setIsDeleting(true)
    deleteService(service_id)
      .then(() => {
        serviceDispatch({
          type: 'DELETE_SERVICE',
          payload: activeService.service_id,
        })
        setIsDeleting(false)
        closeActiveService()
        closeConfirmDeleteDialog()
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsDeleting(false)
      })
  }

  let [serviceImages, setServiceImages] = useState([])
  let [binaryServiceImages, setBinaryServiceImages] = useState([])

  let onImageUpload = (event) => {
    const files = event.target.files
    setBinaryServiceImages(files)
  }

  const [isSaveImages, setIsSavingImages] = useState(false)

  const saveServiceImages = () => {
    setIsSavingImages(true)
    addServiceImages(service_id, binaryServiceImages)
      .then((service_images) => {
        setIsSavingImages(false)
        setServiceImages(service_images)
        setBinaryServiceImages([])
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsSavingImages(false)
      })
  }

  useEffect(() => {
    getServiceImages(service_id)
      .then((service_images) => {
        setServiceImages(service_images)
        setBinaryServiceImages([])
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
    // eslint-disable-next-line
  }, [isSaveImages === false])

  return (
    <>
      <DeleteServiceDialog
        isDeleteDialogOpen={isDeleteDialogOpen}
        confirmDelete={deleteServiceHandler}
        closeDialog={closeConfirmDeleteDialog}
        isDeleting={isDeleting}
      />
      <Grid
        container
        className={
          activeService.service_id !== service_id
            ? classes.serviceRecord
            : classes.activeService
        }
      >
        <Grid item xs={12} sm={1} className={classes.serviceDataNumber}>
          <Typography id='serviceData1' noWrap className={classes.serviceData}>
            {index + 1}
          </Typography>
        </Grid>

        <Grid
          item
          xs={12}
          sm={4}
          className={
            service_id === activeService.service_id
              ? classes.serviceNamePaddingTopActive
              : classes.serviceNamePaddingTop
          }
        >
          {service_id !== activeService.service_id ? null : (
            <Typography className={classes.bringOnExtraSmall}>
              {`${formatMessage({ id: 'servicesHeaderService' })} : `}
            </Typography>
          )}
          <Typography id='serviceData2' noWrap className={classes.serviceData}>
            {service_name}
          </Typography>
        </Grid>

        <Grid
          item
          xs={12}
          sm={2}
          className={
            service_id === activeService.service_id
              ? classes.serviceData
              : classes.serviceDataOthers
          }
        >
          {service_id !== activeService.service_id ? null : (
            <Typography className={classes.bringOnExtraSmall}>
              {`${formatMessage({ id: 'servicesHeaderUnitPrice' })} : `}
            </Typography>
          )}
          <Typography id='serviceData3' noWrap className={classes.serviceData}>
            {`${unit_price} FCFA`}
          </Typography>
        </Grid>

        <Grid
          item
          xs={12}
          sm={2}
          className={
            service_id === activeService.service_id
              ? classes.serviceData
              : classes.serviceDataOthers
          }
        >
          {service_id !== activeService.service_id ? null : (
            <Typography className={classes.bringOnExtraSmall}>
              {`${formatMessage({ id: 'servicesHeaderRating' })} : `}
            </Typography>
          )}
          <Typography id='serviceData4' noWrap className={classes.serviceData}>
            {rating === 0 && number_of_votes === 0 ? 'N/A' : rating}
          </Typography>
        </Grid>

        <Grid
          item
          xs={12}
          sm={2}
          className={
            service_id === activeService.service_id
              ? classes.serviceData
              : classes.serviceDataOthers
          }
        >
          {service_id !== activeService.service_id ? null : (
            <Typography className={classes.bringOnExtraSmall}>
              {`${formatMessage({ id: 'servicesHeaderNbrVotes' })} : `}
            </Typography>
          )}
          <Typography id='serviceData5' noWrap className={classes.serviceData}>
            {number_of_votes}
          </Typography>
        </Grid>

        <Grid item xs={12} sm={1}>
          {activeService.service_id !== service_id ? (
            <Typography
              id='serviceData5'
              noWrap
              className={classes.serviceData}
            >
              <Button
                color='primary'
                onClick={() => activateService(service_id)}
              >
                {formatMessage({ id: 'serviceActionDetails' })}
              </Button>
            </Typography>
          ) : (
            <Grid container item className={classes.serviceActions}>
              <Tooltip
                arrow
                title={formatMessage({ id: 'closeActiveService' })}
              >
                <IconButton color='secondary' onClick={closeActiveService}>
                  <CloseRoundedIcon />
                </IconButton>
              </Tooltip>

              {!isActiveServiceDataLoading ? (
                <Tooltip
                  arrow
                  title={formatMessage({ id: 'editActiveService' })}
                >
                  <IconButton color='secondary' onClick={openEditDialog}>
                    <EditRoundedIcon />
                  </IconButton>
                </Tooltip>
              ) : (
                <IconButton
                  color='secondary'
                  disabled={isActiveServiceDataLoading}
                  onClick={openEditDialog}
                >
                  <EditRoundedIcon />
                </IconButton>
              )}

              {!isActiveServiceDataLoading ? (
                <Tooltip
                  arrow
                  title={formatMessage({ id: 'deleteActiveService' })}
                >
                  <IconButton
                    className={classes.deleteButton}
                    onClick={openConfirmDeleteDialog}
                  >
                    <DeleteRoundedIcon />
                  </IconButton>
                </Tooltip>
              ) : (
                <IconButton
                  color='secondary'
                  disabled={isActiveServiceDataLoading}
                  onClick={openConfirmDeleteDialog}
                >
                  <DeleteRoundedIcon />
                </IconButton>
              )}
            </Grid>
          )}
        </Grid>
        {activeService.service_id === service.service_id && (
          <Grid className={classes.serviceDetails} container item xs={12}>
            <Grid
              container
              item
              xs={12}
              sm={6}
              className={classes.serviceDetailsDescription}
            >
              <Grid
                container
                item
                className={classes.serviceDetailsDescription}
              >
                <Grid item xs={2} className={classes.removeOnSmall}></Grid>
                <Grid item xs={10}>
                  <Typography noWrap className={classes.detailsHeader}>
                    {formatMessage({ id: 'serviceDetailsDescriptionHeader' })}
                  </Typography>
                </Grid>
              </Grid>
              <Grid container item className={classes.descriptionData}>
                <Grid item xs={2} className={classes.removeOnSmall}></Grid>

                <Grid item xs={10}>
                  {isActiveServiceDataLoading ? (
                    <>
                      <Skeleton
                        height={25}
                        animation='wave'
                        width={'95%'}
                        style={{ transform: 'scale(1,0.90)' }}
                      />
                      <Skeleton
                        height={25}
                        animation='wave'
                        width={'75%'}
                        style={{ transform: 'scale(1,0.90)' }}
                      />
                      <Skeleton
                        height={25}
                        animation='wave'
                        width={'85%'}
                        style={{ transform: 'scale(1,0.90)' }}
                      />
                      <Skeleton
                        height={25}
                        animation='wave'
                        width={'90%'}
                        style={{ transform: 'scale(1,0.90)' }}
                      />
                    </>
                  ) : (
                    <>
                      <Typography>
                        {activeService.service_description}
                      </Typography>
                      <input
                        type='file'
                        id='upld-service-img-btn'
                        accept='image/png, image/jpeg'
                        hidden
                        multiple
                        onChange={onImageUpload}
                      />
                      <label
                        disabled={isSaveImages}
                        className={classes.imageLabel}
                        htmlFor='upld-service-img-btn'
                      >
                        <Button component='span' color='primary'>
                          {formatMessage({ id: 'importServiceImageLabel' })}
                        </Button>
                      </label>
                      <Scrollbars
                        autoHeight
                        autoHide
                        style={{ maxWidth: '35vw' }}
                      >
                        <Box
                          style={{
                            display: 'grid',
                            gridAutoFlow: 'column',
                          }}
                        >
                          {Object.keys(binaryServiceImages)
                            .map((key) =>
                              URL.createObjectURL(binaryServiceImages[key])
                            )
                            .map((url) => (
                              <img
                                width={200}
                                height={100}
                                src={url}
                                alt={`service images`}
                                onContextMenuCapture={() => console.log(url)}
                              />
                            ))}
                          {serviceImages.map(
                            ({ service_image_id, service_image_ref }) => (
                              <img
                                width={200}
                                height={100}
                                key={service_image_id}
                                src={`${process.env.REACT_APP_BASE_URL}/${service_image_ref}`}
                                alt={`service images`}
                              />
                            )
                          )}
                        </Box>
                      </Scrollbars>
                      <Button
                        onClick={saveServiceImages}
                        style={{ alignSelf: 'left' }}
                        disabled={
                          isSaveImages || binaryServiceImages.length === 0
                        }
                      >
                        {formatMessage({ id: 'saveButton' })}
                      </Button>
                    </>
                  )}
                </Grid>
              </Grid>
            </Grid>

            <Grid
              container
              item
              xs={12}
              sm={6}
              className={classes.justifyImage}
            >
              <Grid container item className={classes.descriptionData}>
                <Grid item xs={10}>
                  <Typography noWrap className={classes.detailsHeader}>
                    {formatMessage({ id: 'serviceDetailsImageHeader' })}
                  </Typography>
                </Grid>
                <Grid item xs={2} className={classes.removeOnSmall}></Grid>
              </Grid>
              <Grid container item className={classes.descriptionData}>
                <Grid item xs={10}>
                  {isActiveServiceDataLoading ? (
                    <Skeleton
                      animation='wave'
                      variant='rect'
                      style={{ height: '200px', width: '200px' }}
                    />
                  ) : (
                    <img
                      src={activeService.main_image_ref}
                      alt={`${activeService.service_name}`}
                      className={classes.serviceImage}
                    />
                  )}
                </Grid>
                <Grid item xs={2} className={classes.removeOnSmall}></Grid>
              </Grid>
            </Grid>
          </Grid>
        )}
      </Grid>
    </>
  )
}

export default injectIntl(StyledService)
