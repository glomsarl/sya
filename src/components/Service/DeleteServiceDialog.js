import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import { injectIntl } from "react-intl";
import { CircularProgress } from "@material-ui/core";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function DeleteServiceDialog({intl, isDeleteDialogOpen, confirmDelete, closeDialog, isDeleting}) {
  const {formatMessage} = intl

  return (
    <Dialog
    open={isDeleteDialogOpen}
    TransitionComponent={Transition}
    keepMounted
    onClose={isDeleting?null:closeDialog}
    aria-labelledby="Delete Servie Dialog"
    aria-describedby="delete the active service"
    >
    <DialogTitle>
        {formatMessage({ id:"deleteServiceDialogHeader" })}
    </DialogTitle>
    <DialogContent>
        <DialogContentText>
            {formatMessage({ id:"deleteServiceMessage" })}
        </DialogContentText>
    </DialogContent>
    <DialogActions>
        <Button onClick={confirmDelete} disabled={isDeleting}>
        { isDeleting? <CircularProgress size={24} style={{marginRight:"8px"}} />: null}
        { formatMessage({ id: "agreeButton" })}
        </Button>
        <Button onClick={closeDialog} color="primary" disabled={isDeleting} variant="contained">
        {formatMessage({ id: "cancelButton" })}
        </Button>
    </DialogActions>
    </Dialog>
  );
}

export default injectIntl(DeleteServiceDialog)
