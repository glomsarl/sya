import {
  Button,
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  makeStyles,
  DialogTitle,
  TextareaAutosize,
  TextField,
  Typography,
  CircularProgress,
} from '@material-ui/core'
import { useFormik } from 'formik'
import { useState } from 'react'
import { injectIntl } from 'react-intl'
import { useService } from '../../contexts/Service/Service.provider'
import * as yup from 'yup'
import { createService, editService } from '../../services/structure.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { notifyError, notifySuccess } from '../../utils/toastMessages'
import { useStructure } from '../../contexts/Structures/Structure.provider'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../contexts/PersonContext/Person.provider'

const useStyles = makeStyles((theme) => ({
  inputHolder: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '10px',
  },
  descriptionLabel: {
    fontWeight: 600,
    paddingTop: '20px',
    textAlign: 'left',
  },
  imageLabel: {
    display: 'block',
    width: 'fit-content',
    padding: '6px 16px',
    marginTop: '10px',
    marginBottom: '12px',
    backgroundColor: theme.palette.primary.light,
    color: 'rgba(0, 0, 0, 0.87)',
    borderRadius: '6px',
    boxShadow:
      '0px 3px 1px -2px rgb(0 0 0 / 20%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 1px 5px 0px rgb(0 0 0 / 12%)',
    transition:
      'background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,border 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
    fontSize: '0.875rem',
    fontWeight: 500,
    lineHeight: 1.75,
    textTransform: 'uppercase',
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
  },
  form: {
    textAlign: 'center',
  },
  image: {
    width: '200px',
  },
}))

function HandleService({ intl, open, action, closeDialog }) {
  const { formatMessage } = intl
  const classes = useStyles()
  let { serviceState, serviceDispatch } = useService()
  let { activeService } = serviceState
  let {
    service_id,
    service_name,
    unit_price,
    service_description,
    main_image_ref,
  } = activeService
  let [serviceImage, setServiceImage] = useState(main_image_ref)
  let [binaryServiceImage, setBinaryServiceImage] = useState()
  let [submitLoading, setSubmitLoading] = useState(false)

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()
  const {
    state: {
      activeStructure: { structure_id },
    },
  } = useStructure()

  /**
   * valid actions accepted by the dialog
   */
  const VALIDACTIONS = ['create', 'edit']

  let validationSchema = yup.object({
    service_name: yup
      .string()
      .required(formatMessage({ id: 'serviceNameRequired' })),
    unit_price: yup
      .number(formatMessage({ id: 'unitPriceNumberError' }))
      .required(formatMessage({ id: 'unitPriceRequired' })),
    service_description: yup.string(),
  })

  const formik = useFormik({
    initialValues: {
      service_name: action === 'edit' ? service_name : '',
      unit_price: action === 'edit' ? unit_price : '',
      service_description: action === 'edit' ? service_description : '',
    },
    validationSchema,
    onSubmit: (values, { resetForm }) => {
      switch (action) {
        case 'edit': {
          setSubmitLoading(true)
          let editedData = {
            ...values,
            main_image_ref:
              binaryServiceImage === undefined
                ? main_image_ref
                : binaryServiceImage,
          }
          editService(service_id, editedData)
            .then(() => {
              serviceDispatch({
                type: 'EDIT_SERVICE',
                payload: { ...editedData, service_id },
              })
              setSubmitLoading(false)
              handleCloseDialog()
            })
            .catch((error) => {
              handleSessionExpiredError(
                personDispatch,
                location.pathname,
                error,
                formatMessage,
                navigate,
              )
              setSubmitLoading(false)
            })
          break
        }
        case 'create': {
          if (binaryServiceImage !== undefined) {
            setSubmitLoading(true)
            let createData = {
              ...values,
              structure_id,
              main_image_ref: binaryServiceImage,
            }
            createService(createData)
              .then((service) => {
                serviceDispatch({
                  type: 'CREATE_SERVICE',
                  payload: service,
                })
                notifySuccess(
                  formatMessage({ id: 'serviceCreatedSuccessfully' }),
                )
                setSubmitLoading(false)
                handleCloseDialog()
                resetForm()
              })
              .catch((error) => {
                handleSessionExpiredError(
                  personDispatch,
                  location.pathname,
                  error,
                  formatMessage,
                  navigate,
                )
                setSubmitLoading(false)
              })
          } else {
            //if the image is not defined, toast a message and leave the interface open
            notifyError(formatMessage({ id: 'missingServiceImageError' }))
          }
          break
        }
        default: {
          handleCloseDialog()
          break
        }
      }
    },
  })

  /**
   * Initialize the formik form values.
   * given that the initial values in the useFormik declaration
   * already render when the page is openned, it prevents it
   * the initial values from getting updated data
   * this will execute every time the dailog opens
   * hence setting the initial values.
   */
  formik.initialValues.unit_price = action === 'edit' ? unit_price : ''
  formik.initialValues.service_name = action === 'edit' ? service_name : ''
  formik.initialValues.service_description =
    action === 'edit' ? service_description : ''

  /**
   * Get the image when loaded by the input file type
   * and store the binary version of it for server upload.
   * Also create an objectURl that will be used
   * to display the image immediately
   * @param {HTMLInputElement} event the event generated when
   * input type file (label) is clicked
   */
  let onImageUpload = (event) => {
    if (event.target.files.length === 1) {
      setBinaryServiceImage(event.target.files[0])
      setServiceImage(URL.createObjectURL(event.target.files[0]))
    }
  }

  let handleCloseDialog = () => {
    setServiceImage(undefined)
    setBinaryServiceImage(undefined)
    closeDialog()
    formik.resetForm()
  }

  return (
    <Dialog
      open={open}
      onClose={() => (VALIDACTIONS.includes(action) ? null : handleCloseDialog)}
    >
      <DialogTitle id="create_edit_dialog">
        {action === 'edit'
          ? formatMessage({ id: 'editServiceDialogHeader' })
          : action === 'create'
          ? formatMessage({ id: 'createServiceDialogHeader' })
          : null}
      </DialogTitle>
      <DialogContent>
        {submitLoading ? (
          action === 'edit' ? (
            <Typography>
              {formatMessage({ id: 'editServiceMessage' })}
            </Typography>
          ) : action === 'create' ? (
            <Typography>
              {formatMessage({ id: 'createServiceMessage' })}
            </Typography>
          ) : null
        ) : (
          <form onSubmit={formik.handleSubmit} className={classes.form}>
            <Box className={classes.inputHolder}>
              <TextField
                color="secondary"
                variant="outlined"
                required
                id="service_name"
                label={formatMessage({ id: 'handleServiceNameInput' })}
                name="service_name"
                type="text"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.service_name}
                error={
                  formik.touched.service_name && formik.errors.service_name
                    ? true
                    : false
                }
                helperText={formik.errors.service_name}
              />
              <TextField
                color="secondary"
                variant="outlined"
                required
                id="unit_price"
                label={formatMessage({ id: 'handleServiceUnitPriceInput' })}
                name="unit_price"
                type="number"
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.unit_price}
                error={
                  formik.touched.unit_price && formik.errors.unit_price
                    ? true
                    : false
                }
                helperText={formik.errors.unit_price}
              />
            </Box>
            <Typography noWrap className={classes.descriptionLabel}>
              {formatMessage({ id: 'serviceDescriptionLabel' })}
            </Typography>
            <TextareaAutosize
              rowsMax={5}
              rowsMin={5}
              style={{ width: '98.5%' }}
              id="service_description"
              placeholder={formatMessage({
                id: 'serviceDescriptionPlaceholder',
              })}
              value={formik.values.service_description}
              onChange={formik.handleChange}
              name="service_description"
            />
            <input
              type="file"
              id="upld-img-btn"
              accept="image/png, image/jpeg"
              hidden
              onChange={onImageUpload}
            />
            <label className={classes.imageLabel} htmlFor="upld-img-btn">
              {formatMessage({ id: 'importServiceImageLabel' })}
            </label>
            {action === 'create' && serviceImage === undefined ? (
              ''
            ) : action === 'edit' ||
              (action === 'create' && serviceImage !== undefined) ? (
              <img
                src={
                  serviceImage === undefined
                    ? main_image_ref
                    : serviceImage
                }
                className={classes.image}
                alt={`service main`}
              />
            ) : null}
          </form>
        )}
      </DialogContent>
      <DialogActions>
        <Button
          onClick={handleCloseDialog}
          color="default"
          disabled={submitLoading}
        >
          {formatMessage({ id: 'cancelButton' })}
        </Button>
        {action === 'create' ? (
          <Button
            onClick={formik.handleSubmit}
            color="primary"
            variant="contained"
            disabled={
              formik.values.service_name === '' ||
              formik.values.service_description === '' ||
              formik.values.unit_price === '' ||
              serviceImage === undefined ||
              submitLoading
            }
          >
            {submitLoading && (
              <CircularProgress size={24} style={{ marginRight: '10px' }} />
            )}
            {formatMessage({ id: 'createServiceButton' })}
          </Button>
        ) : action === 'edit' ? (
          <Button
            onClick={formik.handleSubmit}
            color="primary"
            variant="contained"
            disabled={
              (formik.values.service_name === service_name &&
                formik.values.service_description === service_description &&
                formik.values.unit_price === unit_price &&
                serviceImage === undefined) ||
              submitLoading
            }
          >
            {submitLoading && (
              <CircularProgress size={24} style={{ marginRight: '10px' }} />
            )}
            {formatMessage({ id: 'editServiceButton' })}
          </Button>
        ) : null}
      </DialogActions>
    </Dialog>
  )
}

export default injectIntl(HandleService)
