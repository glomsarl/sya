import {
  Box,
  Grid,
  Hidden,
  makeStyles,
  Typography,
  IconButton,
  Tooltip,
} from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { injectIntl } from 'react-intl'
import { useService } from '../../contexts/Service/Service.provider'
import MainSkeleton from '../shared/MainSkeleton/MainSkeleton'
import HandleService from './HandleService'
import StyledService from './StyledService'
import Scrollbars from 'react-custom-scrollbars-2'
import { AddCircleRounded } from '@material-ui/icons'
import { fetchStructureServives } from '../../services/structure.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import { useStructure } from '../../contexts/Structures/Structure.provider'

let useStyles = makeStyles((theme) => ({
  serviceHeaderHolder: {
    borderBottom: '1px solid #f8f7f7',
    paddingBottom: '5px',
  },
  headerHolder: {
    display: 'grid',
    gridAutoFlow: 'column',
    color: 'grey',
  },
  headerNumberHolder: {
    display: 'grid',
    gridAutoFlow: 'column',
    color: 'grey',
    paddingLeft: '10px',
  },
  headerDivider: {
    borderRight: '1px solid #f8f7f7',
    margin: '5px 10px 5px 0',
  },
  serviceHeader: {
    // fontWeight: 300,
    textTransform: 'uppercase',
  },
  root: {
    display: 'grid',
    height: '100%',
    gridTemplateRows: 'auto 1fr auto',
    [theme.breakpoints.down('xs')]: {
      gridTemplateRows: '1fr auto',
    },
  },
  servicesFooter: {
    textAlign: 'center',
    borderTop: '1px solid #f8f7f7',
    borderRadius: '7px',
    backgroundColor: theme.common.primaryTransparent,
  },
}))
function Service({ intl }) {
  const { formatMessage } = intl
  let [openDialog, setOpenDialog] = useState(false)
  let [serviceDialogAction, setServiceDialogAction] = useState('')
  let {
    serviceState: { services, isServiceDataLoading },
    serviceDispatch,
  } = useService()

  let classes = useStyles()

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()
  const {
    state: {
      activeStructure: { structure_id },
    },
  } = useStructure()

  useEffect(() => {
    fetchStructureServives(structure_id)
      .then((services) => {
        serviceDispatch({
          type: 'CLOSE_LOADING_SERVICE_DATA',
          payload: services,
        })
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
    // eslint-disable-next-line
  }, [])

  let ServicesHead = () => {
    return (
      <Hidden only='xs'>
        <Grid container className={classes.serviceHeaderHolder}>
          <Grid item xs={false} sm={1} className={classes.headerNumberHolder}>
            <Typography noWrap variant='h4' className={classes.serviceHeader}>
              #
            </Typography>
            <Typography className={classes.headerDivider}></Typography>
          </Grid>

          <Grid item xs={false} sm={4} className={classes.headerHolder}>
            <Tooltip title={formatMessage({ id: 'servicesHeaderService' })}>
              <Typography noWrap variant='h4' className={classes.serviceHeader}>
                {formatMessage({
                  id: 'servicesHeaderService',
                })}
              </Typography>
            </Tooltip>
            <Typography className={classes.headerDivider}></Typography>
          </Grid>

          <Grid item xs={false} sm={2} className={classes.headerHolder}>
            <Tooltip title={formatMessage({ id: 'servicesHeaderUnitPrice' })}>
              <Typography noWrap variant='h4' className={classes.serviceHeader}>
                {formatMessage({
                  id: 'servicesHeaderUnitPrice',
                })}
              </Typography>
            </Tooltip>
            <Typography className={classes.headerDivider}></Typography>
          </Grid>

          <Grid item xs={false} sm={2} className={classes.headerHolder}>
            <Tooltip title={formatMessage({ id: 'servicesHeaderRating' })}>
              <Typography noWrap variant='h4' className={classes.serviceHeader}>
                {formatMessage({
                  id: 'servicesHeaderRating',
                })}
              </Typography>
            </Tooltip>
            <Typography className={classes.headerDivider}></Typography>
          </Grid>

          <Grid item xs={false} sm={2} className={classes.headerHolder}>
            <Tooltip title={formatMessage({ id: 'servicesHeaderNbrVotes' })}>
              <Typography noWrap variant='h4' className={classes.serviceHeader}>
                {formatMessage({
                  id: 'servicesHeaderNbrVotes',
                })}
              </Typography>
            </Tooltip>
            <Typography className={classes.headerDivider}></Typography>
          </Grid>

          <Grid item xs={false} sm={1} className={classes.headerHolder}>
            <Tooltip title={formatMessage({ id: 'servicesHeaderActions' })}>
              <Typography noWrap variant='h4' className={classes.serviceHeader}>
                {formatMessage({
                  id: 'servicesHeaderActions',
                })}
              </Typography>
            </Tooltip>
          </Grid>
        </Grid>
      </Hidden>
    )
  }

  /**
   * open the service dialog (edit or create)
   * @param {String} action the action that wants to open the dialog
   */
  let openServiceDialog = (action) => {
    setOpenDialog(true)
    setServiceDialogAction(action)
  }

  let ServicesFooter = () => {
    return (
      <Box className={classes.servicesFooter}>
        {isServiceDataLoading ? (
          <IconButton
            color='secondary'
            onClick={() => openServiceDialog('create')}
            disabled={isServiceDataLoading}
          >
            <AddCircleRounded fontSize='large' />
          </IconButton>
        ) : (
          <Tooltip arrow title={formatMessage({ id: 'addServiceHint' })}>
            <IconButton
              color='secondary'
              onClick={() => openServiceDialog('create')}
            >
              <AddCircleRounded fontSize='large' />
            </IconButton>
          </Tooltip>
        )}
      </Box>
    )
  }

  /**
   * close the service dialog
   */
  let closeHandleServiceDialog = () => {
    setOpenDialog(false)
  }

  let StyledServices = () => {
    return services.map((service, index) => (
      <StyledService
        service={service}
        index={index}
        key={index}
        openDialog={openServiceDialog}
      />
    ))
  }

  return (
    <Box className={classes.root}>
      <HandleService
        open={openDialog}
        action={serviceDialogAction}
        closeDialog={closeHandleServiceDialog}
      />
      <ServicesHead />
      {isServiceDataLoading ? (
        <MainSkeleton />
      ) : (
        <Scrollbars autoHide style={{ flexGrow: 1 }}>
          <StyledServices />
        </Scrollbars>
      )}
      <ServicesFooter />
    </Box>
  )
}

export default injectIntl(Service)
