import {
  Box,
  Button,
  CircularProgress,
  Typography,
  makeStyles,
  IconButton,
  Tooltip,
} from '@material-ui/core'
import { SaveRounded, EditRounded, CloseRounded } from '@material-ui/icons'
import { injectIntl } from 'react-intl'
import { useState } from 'react'
import ConfirmationDialog from './ConfirmationDialog'

const useStyles = makeStyles((theme) => ({
  cardTitle: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
    fontSize: '1.2rem',
  },
  cardValue: {
    fontWeight: 'bold',
    fontSize: '1.2rem',
    color: 'rgb(0,0,0,0.4)',
  },
  editButton: {
    justifySelf: 'end',
    borderRadius: '16px',
  },
  cardHolder: {
    boxShadow: '0px 4px 4px rgb(0 0 0 / 25%)',
    padding: '5px',
    display: 'grid',
    borderRadius: '8px',
    textAlign: 'center',
  },
  saveButtonHolder: {
    display: 'grid',
    justifyContent: 'end',
    gridTemplateColumns: 'auto auto',
    columnGap: '8px',
    alignItems: 'center',
  },
}))

function BonusConfigCard({
  intl: { formatMessage },
  title,
  value,
  saveChanges,
  canSave,
  cancelEdit,
  EditComponent,
}) {
  const classes = useStyles()
  const [isEditing, setIsEditing] = useState(false)
  const [isSubmitting, setIsSubmitting] = useState(false)
  const [isConfirmDialogOpen, setIsConfirmDialogOpen] = useState(false)
  const [isDialogSubmitting, setIsDialogSubmitting] = useState(false)

  const handleSave = () => {
    setIsDialogSubmitting(true)
    saveChanges()
    setIsEditing(false)
    setIsSubmitting(false)
    setIsConfirmDialogOpen(false)
    setIsDialogSubmitting(false)
  }

  const openConfirmDialog = () => {
    setIsSubmitting(true)
    setIsConfirmDialogOpen(true)
  }

  const closeConfirmDialog = () => {
    setIsSubmitting(false)
    setIsConfirmDialogOpen(false)
  }

  const handleCloseEdit = () => {
    cancelEdit()
    setIsEditing(false)
  }

  return (
    <Box className={classes.cardHolder}>
      <Typography className={classes.cardTitle}>{title}</Typography>
      <Box style={{ margin: '16px 0' }}>
        {isEditing ? (
          EditComponent
        ) : (
          <Typography className={classes.cardValue}>{value}</Typography>
        )}
      </Box>

      {isEditing ? (
        <Box className={classes.saveButtonHolder}>
          <Tooltip arrow title={formatMessage({ id: 'cancelButton' })}>
            <IconButton
              size='small'
              color='primary'
              style={{ border: '0.5px solid yellow' }}
              disabled={isSubmitting}
              onClick={handleCloseEdit}
            >
              <CloseRounded fontSize='small' />
            </IconButton>
          </Tooltip>
          {canSave ? (
            <Button
              size='small'
              color='secondary'
              disabled={isSubmitting}
              onClick={openConfirmDialog}
              className={classes.editButton}
              variant='contained'
              startIcon={
                isSubmitting ? (
                  <CircularProgress
                    size='24px'
                    style={{ marginRight: '10px' }}
                  />
                ) : (
                  <SaveRounded />
                )
              }
            >
              {formatMessage({ id: 'saveButton' })}
            </Button>
          ) : null}
        </Box>
      ) : (
        <Button
          size='small'
          color='secondary'
          disabled={isSubmitting}
          onClick={() => setIsEditing(true)}
          variant='outlined'
          startIcon={<EditRounded />}
          className={classes.editButton}
        >
          {formatMessage({ id: 'editButton' })}
        </Button>
      )}
      <ConfirmationDialog
        isDialogOpen={isConfirmDialogOpen}
        closeDialog={closeConfirmDialog}
        confirmDialog={handleSave}
        isSubmitting={isDialogSubmitting}
      />
    </Box>
  )
}
export default injectIntl(BonusConfigCard)
