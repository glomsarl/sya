import {
  FormControl,
  InputAdornment,
  OutlinedInput,
  Switch,
  Box,
} from '@material-ui/core'
import { useState, useEffect } from 'react'
import { injectIntl } from 'react-intl'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import {
  editBonusSetting,
  getBonusSetting,
  getSubscriptions,
  updateSubscription,
} from '../../services/settings.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import BonusConfigCard from './BonusConfigCard'
import BonusConfigCardSkeleton from './BonusConfigCardSkeleton'

function BonusManagement({ intl: { formatMessage } }) {
  const [configuration, setConfiguration] = useState({
    sign_up_bonus: 0,
    referral_bonus: 0,
    structure_validation_bonus: 0,
    sy_value: 0,
    is_sign_up_bonus_on: false,
    commission_rate: 0,
  })
  const [configurationState, setConfigurationState] = useState({
    sign_up_bonus: 0,
    referral_bonus: 0,
    structure_validation_bonus: 0,
    sy_value: 0,
    is_sign_up_bonus_on: false,
    commission_rate: 0,
  })
  const { personDispatch } = usePerson()
  const location = useLocation()
  const navigate = useNavigate()

  const [isConfigurationLoading, setIsConfigurationLoading] = useState(true)
  useEffect(() => {
    getBonusSetting()
      .then((setting) => {
        getSubscriptions(process.env.REACT_APP_SUBSCRIPTION_1)
          .then(({ commission_rate }) => {
            setConfigurationState({ ...setting, commission_rate })
            setConfiguration({ ...setting, commission_rate })
            setIsConfigurationLoading(false)
          })
          .catch((error) => {
            handleSessionExpiredError(
              personDispatch,
              location.pathname,
              error,
              formatMessage,
              navigate
            )
          })
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })

    // eslint-disable-next-line
  }, [])

  useEffect(() => {
    setConfigurationState(configuration)
    // eslint-disable-next-line
  }, [configuration])

  const saveNewConfig = (setting) => {
    if (setting !== 'commission_rate')
      editBonusSetting(configurationState)
        .then(() => {
          setConfiguration(configurationState)
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
        })
    else
      updateSubscription({
        commission_rate: configurationState.commission_rate,
        subscription_type: 'MONTHLY',
      })
        .then(() => {
          setConfiguration(configurationState)
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
        })
  }

  return (
    <Box
      style={{
        display: 'grid',
        gap: '10px',
        gridTemplateColumns: 'repeat(auto-fit, minmax(250px, 1fr))',
        padding: '20px'
      }}
    >
      {isConfigurationLoading ? (
        <BonusConfigCardSkeleton />
      ) : (
        <>
          <BonusConfigCard
            title={formatMessage({ id: 'registrationGainTitle' })}
            value={`${configurationState.sign_up_bonus} SY`}
            cancelEdit={() =>
              setConfigurationState({
                ...configurationState,
                sign_up_bonus: configuration.sign_up_bonus,
              })
            }
            saveChanges={() => saveNewConfig()}
            canSave={
              configuration.sign_up_bonus !== configurationState.sign_up_bonus
            }
            EditComponent={
              <FormControl variant='outlined' size='small'>
                <OutlinedInput
                  value={configurationState.sign_up_bonus}
                  onChange={(e) =>
                    setConfigurationState({
                      ...configurationState,
                      sign_up_bonus:
                        e.target.value < 0 ? 0 : Number(e.target.value),
                    })
                  }
                  type='number'
                  endAdornment={
                    <InputAdornment position='end'>SY</InputAdornment>
                  }
                />
              </FormControl>
            }
          />
          <BonusConfigCard
            title={formatMessage({ id: 'referalGainTitle' })}
            value={`${configurationState.referral_bonus} SY`}
            cancelEdit={() =>
              setConfigurationState({
                ...configurationState,
                referral_bonus: configuration.referral_bonus,
              })
            }
            saveChanges={() => saveNewConfig()}
            canSave={
              configuration.referral_bonus !== configurationState.referral_bonus
            }
            EditComponent={
              <FormControl variant='outlined' size='small'>
                <OutlinedInput
                  value={configurationState.referral_bonus}
                  onChange={(e) =>
                    setConfigurationState({
                      ...configurationState,
                      referral_bonus:
                        e.target.value < 0 ? 0 : Number(e.target.value),
                    })
                  }
                  type='number'
                  endAdornment={
                    <InputAdornment position='end'>SY</InputAdornment>
                  }
                />
              </FormControl>
            }
          />
          <BonusConfigCard
            title={formatMessage({ id: 'structureEmissionTitle' })}
            value={`${configurationState.structure_validation_bonus} SY`}
            cancelEdit={() =>
              setConfigurationState({
                ...configurationState,
                structure_validation_bonus:
                  configuration.structure_validation_bonus,
              })
            }
            saveChanges={() => saveNewConfig()}
            canSave={
              configuration.structure_validation_bonus !==
              configurationState.structure_validation_bonus
            }
            EditComponent={
              <FormControl variant='outlined' size='small'>
                <OutlinedInput
                  value={configurationState.structure_validation_bonus}
                  onChange={(e) =>
                    setConfigurationState({
                      ...configurationState,
                      structure_validation_bonus:
                        e.target.value < 0 ? 0 : Number(e.target.value),
                    })
                  }
                  type='number'
                  endAdornment={
                    <InputAdornment position='end'>SY</InputAdornment>
                  }
                />
              </FormControl>
            }
          />
          <BonusConfigCard
            title={formatMessage({ id: 'bonusPointValueTitle' })}
            value={`${configurationState.sy_value} FCFA`}
            cancelEdit={() =>
              setConfigurationState({
                ...configurationState,
                sy_value: configuration.sy_value,
              })
            }
            saveChanges={() => saveNewConfig()}
            canSave={configuration.sy_value !== configurationState.sy_value}
            EditComponent={
              <FormControl variant='outlined' size='small'>
                <OutlinedInput
                  value={configurationState.sy_value}
                  onChange={(e) =>
                    setConfigurationState({
                      ...configurationState,
                      sy_value: e.target.value < 0 ? 0 : Number(e.target.value),
                    })
                  }
                  type='number'
                  endAdornment={
                    <InputAdornment position='end'>FCFA</InputAdornment>
                  }
                />
              </FormControl>
            }
          />
          <BonusConfigCard
            title={formatMessage({
              id: 'shouldAcquireBonusOnRegistrationTitle',
            })}
            value={formatMessage({
              id: configurationState.is_sign_up_bonus_on
                ? 'onMessage'
                : 'offMessage',
            })}
            cancelEdit={() =>
              setConfigurationState({
                ...configurationState,
                is_sign_up_bonus_on: configuration.is_sign_up_bonus_on,
              })
            }
            saveChanges={() => saveNewConfig()}
            canSave={
              configuration.is_sign_up_bonus_on !==
              configurationState.is_sign_up_bonus_on
            }
            EditComponent={
              <Switch
                checked={configurationState.is_sign_up_bonus_on}
                onChange={(e) =>
                  setConfigurationState({
                    ...configurationState,
                    is_sign_up_bonus_on: e.target.checked,
                  })
                }
              />
            }
          />
          <BonusConfigCard
            title={formatMessage({ id: 'commissionRateTitle' })}
            value={`${configurationState.commission_rate} %`}
            cancelEdit={() =>
              setConfigurationState({
                ...configurationState,
                commission_rate: configuration.commission_rate,
              })
            }
            saveChanges={() => saveNewConfig('commission_rate')}
            canSave={
              configuration.commission_rate !==
              configurationState.commission_rate
            }
            EditComponent={
              <FormControl variant='outlined' size='small'>
                <OutlinedInput
                  value={configurationState.commission_rate}
                  onChange={(e) =>
                    setConfigurationState({
                      ...configurationState,
                      commission_rate:
                        e.target.value < 0 ? 0 : Number(e.target.value),
                    })
                  }
                  type='number'
                  endAdornment={
                    <InputAdornment position='end'>%</InputAdornment>
                  }
                />
              </FormControl>
            }
          />
        </>
      )}
    </Box>
  )
}
export default injectIntl(BonusManagement)
