import { Box, Typography, makeStyles } from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";
import { injectIntl } from "react-intl";

const useStyles = makeStyles((theme) => ({
  cardHolder: {
    boxShadow: "0px 4px 4px rgb(0 0 0 / 25%)",
    padding: "5px",
    display: "grid",
    borderRadius: "8px",
    textAlign: "center",
  },
  cardTitle: {
    textTransform: "uppercase",
    fontWeight: "bold",
    fontSize: "1.2rem",
  },
  saveButtonHolder: {
    display: "grid",
    justifyContent: "end",
    gridTemplateColumns: "auto auto",
    columnGap: "8px",
    alignItems: "center",
  },
}));

function BonusConfigCardSkeleton({ intl: { formatMessage } }) {
  const classes = useStyles();
  const cardTitles = [
    formatMessage({ id: "registrationGainTitle" }),
    formatMessage({ id: "referalGainTitle" }),
    formatMessage({ id: "structureEmissionTitle" }),
    formatMessage({ id: "bonusPointValueTitle" }),
    formatMessage({
      id: "shouldAcquireBonusOnRegistrationTitle",
    }),
    formatMessage({id:"commissionRateTitle"})
  ];
  return cardTitles.map((title) => (
    <Box className={classes.cardHolder}>
      <Typography className={classes.cardTitle}>{title}</Typography>
      <Skeleton variant="text" animation="wave" height="30px" style={{ margin: "16px 35px" }} />
      <Box className={classes.saveButtonHolder}>
        <Skeleton
          animation="wave"
          variant="circle"
          height="40px"
          width="40px"
        />
        <Skeleton
          animation="wave"
          variant="circle"
          height="40px"
          width="40px"
        />
      </Box>
    </Box>
  ));
}

export default injectIntl(BonusConfigCardSkeleton);
