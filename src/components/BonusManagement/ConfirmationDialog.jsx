import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  CircularProgress,
  DialogTitle,
} from "@material-ui/core";
import { SaveRounded } from "@material-ui/icons";
import { injectIntl } from "react-intl";

function ConfirmationDialog({
  isDialogOpen,
  confirmDialog,
  closeDialog,
  isSubmitting,
  intl: { formatMessage },
}) {
  return (
    <Dialog open={isDialogOpen} onClose={isSubmitting ? null : closeDialog}>
      <DialogTitle>
        {formatMessage({ id: "bonusConfigConfirmationDialogHeader" })}
      </DialogTitle>
      <DialogContent>
        {formatMessage({ id: "editBonusSettingsMessage" })}
      </DialogContent>
      <DialogActions>
        <Button
          disabled={isSubmitting}
          onClick={confirmDialog}
          color="secondary"
          variant="outlined"
          size="small"
          startIcon={
            isSubmitting ? (
              <CircularProgress
                color="secondary"
                size="24px"
                style={{ marginRight: "10px" }}
              />
            ) : (
              <SaveRounded />
            )
          }
        >
          {formatMessage({ id: "saveButton" })}
        </Button>
        <Button
          disabled={isSubmitting}
          onClick={closeDialog}
          color="primary"
          variant="contained"
          size="small"
        >
          {formatMessage({ id: "cancelButton" })}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default injectIntl(ConfirmationDialog);
