import React from 'react';
import {Box, makeStyles} from '@material-ui/core';
import {Skeleton} from '@material-ui/lab';

let useStyles = makeStyles((theme) => ({
  skeletonRoot: {
    width: "fit-content",
    padding: "2px",
    display: "inline-block",
    margin: "0 5px"
  },
  circleVariant: {
    width: "40px",
    height: "40px",
  },
}));
export default function Publicationskeleton() {
    const classes = useStyles();
    return (
        <Box className={classes.skeletonRoot}>
            <Skeleton variant="rect" width={200} height={200} />
        </Box>
    )
}
