import {
  makeStyles,
  Box,
  Typography,
  Button,
  IconButton,
  Fab,
  Tooltip,
  CircularProgress,
} from '@material-ui/core'
import { DeleteForeverRounded } from '@material-ui/icons'
import AddRoundedIcon from '@material-ui/icons/AddRounded'
import React, { useState, useEffect } from 'react'
import { Scrollbars } from 'react-custom-scrollbars-2'
import { injectIntl } from 'react-intl'
import { useLocation, useNavigate } from 'react-router'
import { v4 as uuidv4 } from 'uuid'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { usePublication } from '../../contexts/PublicationContext/Publication.provider'
import { useStructure } from '../../contexts/Structures/Structure.provider'
import {
  addNewPublications,
  fetchAllPublications,
} from '../../services/publications.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import DeletePublicationDialog from './DeletePublicationDialog'
import Publicationskeleton from './Publicationskeleton'

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '10px',
  },
  headerBar: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridTemplateColumns: '1fr auto',
  },
  toBeUploaded: {
    display: 'grid',
    justifyItems: 'center',
    gridTemplateRows: 'auto 1fr',
    alignItems: 'center',
    height: '50vh',
    border: `1px solid grey`,
    marginTop: '20px',
    position: 'relative',
  },
  toBeUploadedEmpty: {
    display: 'grid',
    justifyItems: 'center',
    alignItems: 'center',
    height: '50vh',
    border: `1px solid grey`,
    marginTop: '20px',
    position: 'relative',
  },
  uploadFab: {
    position: 'absolute',
    right: '20px',
    bottom: '20px',
    zIndex: 2,
  },
  uploadText: {
    textAlign: 'center',
  },
  addPublicationText: {
    textAlign: 'center',
    cursor: 'context-menu',
  },
  scrollbars: {
    height: '200px',
    marginTop: '20px',
  },
  publishedHolder: {
    display: 'flex',
    flexDirection: 'row',
    gridGap: '20px',
  },
  publishedImage: {
    objectFit: 'scale-down',
    minWidth: '200px',
    width: '100%',
    height: '200px',
    padding: '2px',
  },
  unPublishedImage: {
    objectFit: 'scale-down',
    width: '200px',
    margin: '2px 5px',
    height: '200px',
    border: '1px solid grey',
    borderRadius: '5px',
    padding: '2px',
  },
  noPublicationMessage: {
    textAlign: 'center',
    marginTop: '50px',
  },
  alreadyPublished: {
    marginTop: '20px',
    marginBottom: '5px',
  },
  deletePublication: {
    position: 'absolute',
    top: 0,
    right: 0,
    color: theme.palette.error.main,
    // display: "none"
  },
  publishedImageHolder: {
    position: 'relative',
    display: 'inline-block',
    width: 'fit-content',
    '& .MuiIconButton-root': {
      display: 'none',
    },
    '&:hover .MuiIconButton-root': {
      display: 'block',
    },
  },
}))
function Publications({ intl }) {
  const classes = useStyles()
  const { formatMessage } = intl
  const [isSubmittingPublication, setIsSubmittingPublication] = useState(false)
  const [isPublicationDataLoading, setIsPublicationDataLoading] = useState(true)
  const [isDeletePublicationDialogOpen, setIsDeletePublicationDialogOpen] =
    useState(false)
  const [publicationToBeDeleted, setPublicationToBeDeleted] = useState({})
  const { publicationState, publicationDispatch } = usePublication()
  const { publications } = publicationState
  const [toBeUploadedImages, setToBeUploadedImages] = useState([])
  const [displayImages, setDisplayImages] = useState([])

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()
  const {
    state: {
      activeStructure: { structure_id },
    },
  } = useStructure()

  useEffect(() => {
    fetchAllPublications(structure_id)
      .then((publications) => {
        publicationDispatch({
          type: 'LOAD_PUBLICATIONS',
          payload: publications,
        })
        setIsPublicationDataLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
    // eslint-disable-next-line
  }, [])

  function handleFileInput(e) {
    let files = Object.keys(e.target.files)
    files = files.map((fileKey) => {
      return {
        publication_id: uuidv4(),
        publication_ref: e.target.files[fileKey],
      }
    })
    let displayImgs = files.map((file) => {
      const { publication_id, publication_ref } = file
      return {
        publication_id,
        publication_ref: URL.createObjectURL(publication_ref),
      }
    })

    setDisplayImages([...displayImages, ...displayImgs])
    setToBeUploadedImages([...toBeUploadedImages, ...files])
  }

  function deleteFromToBeUploaded(publication_id) {
    let newDisplayImgs = displayImages.filter(
      (image) => image.publication_id !== publication_id
    )
    let newToBeUplddImgs = toBeUploadedImages.filter(
      (image) => image.publication_id !== publication_id
    )
    setDisplayImages(newDisplayImgs)
    setToBeUploadedImages(newToBeUplddImgs)
  }

  function publishToBeUploaded() {
    setIsSubmittingPublication(true)
    addNewPublications(
      structure_id,
      toBeUploadedImages.map((_) => _.publication_ref)
    )
      .then((publications) => {
        publicationDispatch({
          type: 'PUBLISH_PUBLICATIONS',
          payload: publications,
        })
        setDisplayImages([])
        setToBeUploadedImages([])
        setIsSubmittingPublication(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsSubmittingPublication(false)
      })
  }

  function closeDeletePublicationDialog() {
    setPublicationToBeDeleted({})
    setIsDeletePublicationDialogOpen(false)
  }

  function openDeletePublicationDialog(publication) {
    setPublicationToBeDeleted(publication)
    setIsDeletePublicationDialogOpen(true)
  }

  function deletePublication(publication_id) {
    publicationDispatch({
      type: 'PUBLISH_PUBLICATIONS',
      payload: publications.filter(
        ({ publication_id: id }) => id !== publication_id
      ),
    })
  }

  return (
    <Box className={classes.root}>
      <DeletePublicationDialog
        isDialogOpen={isDeletePublicationDialogOpen}
        closeDialog={closeDeletePublicationDialog}
        publication={publicationToBeDeleted}
        deletePublication={deletePublication}
      />
      <Box className={classes.headerBar}>
        <Typography variant='h3'>
          {formatMessage({ id: 'publicationHeaderMessage' })}
        </Typography>
        <Button
          variant='contained'
          style={{ height: 'fit-content' }}
          color='primary'
          onClick={publishToBeUploaded}
          disabled={toBeUploadedImages.length === 0 || isSubmittingPublication}
        >
          {isSubmittingPublication && (
            <CircularProgress
              size={25}
              color='secondary'
              style={{ marginRight: '5px' }}
            />
          )}
          {formatMessage({ id: 'publishToBeUploadedButton' })}
        </Button>
      </Box>
      <Box
        className={
          toBeUploadedImages !== undefined && toBeUploadedImages.length === 0
            ? classes.toBeUploadedEmpty
            : classes.toBeUploaded
        }
      >
        {isPublicationDataLoading || publications === undefined ? null : (
          <Box className={classes.uploadText}>
            <input
              accept='image/*'
              hidden
              id='raised-button-file'
              multiple
              type='file'
              onChange={handleFileInput}
            />
            <label htmlFor='raised-button-file'>
              {toBeUploadedImages !== undefined &&
              toBeUploadedImages.length !== 0 ? (
                isSubmittingPublication ? (
                  <Fab
                    color='primary'
                    component='span'
                    size='small'
                    disabled
                    className={classes.uploadFab}
                  >
                    <AddRoundedIcon />
                  </Fab>
                ) : (
                  <Tooltip
                    arrow
                    title={formatMessage({ id: 'addImageTooltip' })}
                    className={classes.uploadFab}
                  >
                    <Fab color='primary' component='span' size='small'>
                      <AddRoundedIcon />
                    </Fab>
                  </Tooltip>
                )
              ) : (
                <>
                  <IconButton
                    variant='raised'
                    color='secondary'
                    component='span'
                  >
                    <AddRoundedIcon />
                  </IconButton>
                  <Typography
                    variant='h3'
                    className={classes.addPublicationText}
                  >
                    {formatMessage({ id: 'addPublicationText' })}
                  </Typography>
                </>
              )}
            </label>
          </Box>
        )}
        {isPublicationDataLoading || publications === undefined ? (
          <Scrollbars style={{ height: '100%' }}>
            <Box style={{ width: '100%', textAlign: 'center' }}>
              {[...new Array(12)].map((_, index) => (
                <Publicationskeleton key={index} />
              ))}
            </Box>
          </Scrollbars>
        ) : null}

        {toBeUploadedImages !== undefined && toBeUploadedImages.length > 0 && (
          <Scrollbars style={{ height: '100%' }}>
            <Box style={{ textAlign: 'center', padding: '10px 0' }}>
              {displayImages === undefined
                ? null
                : displayImages.map((publication, index) => (
                    <Box className={classes.publishedImageHolder} key={index}>
                      <img
                        className={classes.unPublishedImage}
                        src={publication.publication_ref}
                        alt={formatMessage({ id: 'unPublishedImageAlt' })}
                      />
                      {isSubmittingPublication ? (
                        <IconButton
                          size='small'
                          disabled
                          className={classes.deletePublication}
                        >
                          <DeleteForeverRounded />
                        </IconButton>
                      ) : (
                        <Tooltip
                          arrow
                          title={formatMessage({ id: 'deletePublication' })}
                          className={classes.deletePublication}
                        >
                          <IconButton
                            size='small'
                            onClick={() =>
                              deleteFromToBeUploaded(publication.publication_id)
                            }
                          >
                            <DeleteForeverRounded />
                          </IconButton>
                        </Tooltip>
                      )}
                    </Box>
                  ))}
            </Box>
          </Scrollbars>
        )}
      </Box>
      <Typography variant='h3' className={classes.alreadyPublished}>
        {formatMessage({ id: 'alreadyPublished' })}
      </Typography>
      <Scrollbars
        style={{ height: isPublicationDataLoading ? '250px' : '216px' }}
      >
        {isPublicationDataLoading || publications === undefined ? (
          <Box className={classes.publishedHolder}>
            {[...new Array(12)].map((_, index) => (
              <Publicationskeleton key={index} />
            ))}
          </Box>
        ) : (
          <Box className={classes.publishedHolder}>
            {publications !== undefined &&
              publications.map((publication, index) => (
                <Box className={classes.publishedImageHolder}>
                  <img
                    className={classes.publishedImage}
                    src={publication.publication_ref}
                    alt={formatMessage({ id: 'publishedImageAlt' })}
                    key={index}
                  />
                  {isSubmittingPublication ? (
                    <IconButton
                      size='small'
                      disabled
                      className={classes.deletePublication}
                    >
                      <DeleteForeverRounded />
                    </IconButton>
                  ) : (
                    <Tooltip
                      arrow
                      title={formatMessage({ id: 'deletePublication' })}
                      className={classes.deletePublication}
                    >
                      <IconButton
                        size='small'
                        onClick={() => openDeletePublicationDialog(publication)}
                      >
                        <DeleteForeverRounded />
                      </IconButton>
                    </Tooltip>
                  )}
                </Box>
              ))}
            {publications !== undefined && publications.length === 0 && (
              <Typography className={classes.noPublicationMessage}>
                {formatMessage({ id: 'noPublicationMessage' })}
              </Typography>
            )}
          </Box>
        )}
      </Scrollbars>
    </Box>
  )
}

export default injectIntl(Publications)
