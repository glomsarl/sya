import { CircularProgress } from '@material-ui/core'
import {
  Dialog,
  Box,
  makeStyles,
  Slide,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@material-ui/core'
import React, { useState } from 'react'
import { injectIntl } from 'react-intl'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { usePublication } from '../../contexts/PublicationContext/Publication.provider'
import { deletePublication } from '../../services/publications.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />
})

const useStyles = makeStyles((theme) => ({
  publishedImage: {
    objectFit: 'scale-down',
    width: '200px',
    margin: '2px 5px',
    height: '200px',
  },
  imageHolder: {
    display: 'grid',
    justifyContent: 'center',
  },
}))

function DeletePublicationDialog({
  intl,
  closeDialog,
  publication,
  isDialogOpen,
  deletePublication: removePublication,
}) {
  const [isDeleting, setIsDeleting] = useState(false)
  const { formatMessage } = intl
  const { publicationDispatch } = usePublication()
  const { publication_id, publication_ref } = publication
  const classes = useStyles()

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  function confirmDelete() {
    setIsDeleting(true)
    deletePublication(publication_id)
      .then(() => {
        publicationDispatch({
          type: 'DELETE_PUBLICATION',
          payload: publication_id,
        })
        removePublication(publication_id)
        setIsDeleting(false)
        closeDialog()
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsDeleting(false)
      })
  }

  return (
    <Dialog
      open={isDialogOpen}
      TransitionComponent={Transition}
      keepMounted
      onClose={isDeleting ? null : closeDialog}
    >
      <DialogTitle>
        {formatMessage({ id: 'deletePublicationDialogHeader' })}
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          {formatMessage({ id: 'deletePublicationMessage' })}
          <Box className={classes.imageHolder}>
            <img
              src={publication_ref}
              className={classes.publishedImage}
              alt={formatMessage({ id: 'publicationToBeDeletedAlt' })}
              width='200px'
              height='200px'
            />
          </Box>
        </DialogContentText>
        <DialogActions>
          <Button onClick={confirmDelete} disabled={isDeleting}>
            {isDeleting && (
              <CircularProgress size={24} style={{ marginRight: '8px' }} />
            )}
            {formatMessage({ id: 'agreeDeletePublicationButton' })}
          </Button>
          <Button
            onClick={closeDialog}
            color='primary'
            disabled={isDeleting}
            variant='contained'
          >
            {formatMessage({ id: 'cancelDeletePublicationButton' })}
          </Button>
        </DialogActions>
      </DialogContent>
    </Dialog>
  )
}

export default injectIntl(DeletePublicationDialog)
