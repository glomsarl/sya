import { Paper, makeStyles } from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";
import getRandomColor from "../../utils/getRandomColor";
import { useState } from "react";

const useStyles = makeStyles((theme) => ({
  card: {
    position: "relative",
    padding: "20px 20px 5px 20px",
    textAlign: "center",
    borderRadius: "24px",
    transition: "0.2s",
    display: "grid",
    justifyItems: "center",
  },
  structureImage: {
    borderRadius: "100%",
    position: "absolute",
    top: "0%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    height: "100px",
    width: "100px",
    objectFit: "cover",
  },
  structureName: {
    fontWeight: "bold",
    fontSize: "1.2rem",
    letterSpacing: "5px",
    marginTop: "45px",
    width: "100%",
  },
  structureCategory: {
    color: "rgb(0,0,0,0.6)",
    fontWeight: "bold",
    marginTop: "5px",
    width: "75%",
  },
  structureTown: {
    fontSize: "0.75rem",
    marginTop: "35px",
    width: "50%",
  },
}));

function StructureCardSkeleton() {
  const classes = useStyles();
  const borderColor = getRandomColor();
  let [elevation, setElevation] = useState(3);
  return (
    <Paper
      className={classes.card}
      elevation={elevation}
      onMouseEnter={() => setElevation(10)}
      onMouseLeave={() => setElevation(3)}
    >
      <Skeleton
        style={{
          border: `1px solid ${borderColor}`,
          boxShadow: `${borderColor} 0px 0px 12px`,
        }}
        className={classes.structureImage}
      />
      <Skeleton
        variant="text"
        animation="wave"
        className={classes.structureName}
      />
      <Skeleton
        variant="text"
        animation="wave"
        className={classes.structureCategory}
      />
      <Skeleton
        variant="text"
        animation="wave"
        className={classes.structureTown}
      />
    </Paper>
  );
}
export default StructureCardSkeleton;
