import { Typography, Paper, makeStyles } from '@material-ui/core'
import { injectIntl } from 'react-intl'
import getRandomColor from '../../utils/getRandomColor'
import { useState } from 'react'
import { getAddress } from '../../utils/geocodeAddress'
import { PersonAddRounded } from '@material-ui/icons'

const useStyles = makeStyles((theme) => ({
  card: {
    position: 'relative',
    padding: '20px 20px 5px 20px',
    textAlign: 'center',
    borderRadius: '24px',
    transition: '0.2s',
    cursor: 'context-menu',
  },
  structureImage: {
    borderRadius: '100%',
    position: 'absolute',
    top: '0%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    height: '100px',
    width: '100px',
    objectFit: 'cover',
  },
  structureName: {
    fontWeight: 'bold',
    fontSize: '1.2rem',
    letterSpacing: '5px',
    marginTop: '45px',
  },
  structureCategory: {
    color: 'rgb(0,0,0,0.6)',
    fontWeight: 'bold',
    marginTop: '5px',
  },
  structureTown: {
    fontSize: '0.75rem',
    marginTop: '35px',
  },
  placeholderImage: {
    width: '100px',
    height: '100px',
    size: '500px',
    borderRadius: '100%',
  },
}))

function StructureCard({
  structure: {
    logo_ref,
    specific_indications,
    name: structure_name,
    structure_category,
    longitude,
    latidute,
  },
  handleSelect,
  intl: { formatMessage },
}) {
  const classes = useStyles()
  const borderColor = getRandomColor()
  let [elevation, setElevation] = useState(3)
  const [address, setAddress] = useState('')
  getAddress(latidute, longitude).then((address) => setAddress(address))
  const categories = structure_category.split(',')
  categories.pop()
  return (
    <Paper
      className={classes.card}
      elevation={elevation}
      onMouseEnter={() => setElevation(10)}
      onMouseLeave={() => setElevation(3)}
      onClick={handleSelect}
    >
      {!logo_ref ? (
        <PersonAddRounded className={classes.placeholderImage} />
      ) : (
        <img
          src={logo_ref}
          alt={`${structure_name} main presentation`}
          style={{
            border: `1px solid ${borderColor}`,
            boxShadow: `${borderColor} 0px 0px 12px`,
          }}
          className={classes.structureImage}
        />
      )}
      <Typography className={classes.structureName}>
        {structure_name}
      </Typography>
      <Typography className={classes.structureCategory}>
        {categories.map(category => `${formatMessage({ id: category })}, `)}
      </Typography>
      <Typography className={classes.structureTown}>{address ?? specific_indications}</Typography>
    </Paper>
  )
}
export default injectIntl(StructureCard)
