import { useState, useEffect } from 'react'
import { Box, makeStyles, Typography } from '@material-ui/core'
import { injectIntl } from 'react-intl'
import StructureCard from './StructureCard'
import StructureCardSkeleton from './StructureCardSkeleton'
import {
  getOwnerStructures,
  setActiveStructure,
} from '../../services/structure.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import { useStructure } from '../../contexts/Structures/Structure.provider'

const useStyles = makeStyles((theme) => ({
  pageTitle: {
    fontWeight: 'bold',
    fontSize: '1.6rem',
    textAlign: 'center',
    letterSpacing: '2px',
    marginBottom: '30px',
  },
  pageSubtitle: {
    letterSpacing: '1px',
    marginBottom: '10px',
    textAlign: 'center',
  },
  structuresHolder: {
    display: 'grid',
    columnGap: '20px',
    padding: '20px',
    rowGap: '80px',
    gridTemplateColumns: 'repeat(auto-fit, minmax(250px, 1fr))',
    marginTop: '100px',
  },
  ownerStructuresHolder: {
    minHeight: '100%',
    display: 'grid',
    alignContent: 'center',
  },
}))

function OwnerStructures({ intl: { formatMessage } }) {
  const classes = useStyles()
  const [structures, setStructures] = useState([])
  const [isStructuresLoading, setIsStructuresLoading] = useState(true)
  const { personDispatch } = usePerson()
  const location = useLocation()
  const navigate = useNavigate()

  useEffect(() => {
    getOwnerStructures()
      .then((structures) => {
        setStructures(structures)
        setIsStructuresLoading(false)
        if (structures.length === 1) {
          dispatch({
            type: 'SET_ACTIVE_STRUCTURE',
            payload: structures[0],
          })
          structureSelectionHandler(structures[0].structure_id)
          navigate('/owner/overview')
        }
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
    // eslint-disable-next-line
  }, [])

  const { dispatch } = useStructure()

  const structureSelectionHandler = (structure_id) => {
    setActiveStructure(structure_id)
      .then(() => {
        dispatch({
          type: 'SET_ACTIVE_STRUCTURE',
          payload: {
            ...structures.find(({ structure_id: id }) => id === structure_id),
          },
        })
        navigate('/owner/overview')
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
  }

  return (
    <Box className={classes.ownerStructuresHolder}>
      <Box>
        <Typography className={classes.pageTitle}>
          {formatMessage({ id: 'selectStructure' })}
        </Typography>
        <Typography className={classes.pageSubtitle}>
          {formatMessage({ id: 'selectStructureSubtitle' })}
        </Typography>
        <Box className={classes.structuresHolder}>
          {isStructuresLoading
            ? [...new Array(5)].map((_, index) => <StructureCardSkeleton />)
            : structures.map((structure, index) => (
                <StructureCard
                  structure={structure}
                  key={index}
                  handleSelect={() =>
                    structureSelectionHandler(structure.structure_id)
                  }
                />
              ))}
        </Box>
      </Box>
    </Box>
  )
}

export default injectIntl(OwnerStructures)
