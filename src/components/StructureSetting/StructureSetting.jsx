import React, { useState, useEffect } from 'react'
import { injectIntl } from 'react-intl'
import {
  makeStyles,
  Box,
  Typography,
  Tooltip,
  IconButton,
  Grid,
  TextField,
  Button,
  TextareaAutosize,
  CircularProgress,
  MenuItem,
} from '@material-ui/core'
import {
  EditRounded,
  EventAvailableRounded,
  PersonAddRounded,
} from '@material-ui/icons'
import { useFormik } from 'formik'
import * as yup from 'yup'
import ManageScheduleDialog from './ManageScheduleDialog'
import {
  findUserInfo,
  updateProfile,
} from '../../services/authentication.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import {
  createNewSchedule,
  deleteSchedule,
  getOwnerStructures,
  getStructureCagetories,
  updateSchedule,
  updateStructure,
} from '../../services/structure.service'
import { notifySuccess } from '../../utils/toastMessages'
import { useStructure } from '../../contexts/Structures/Structure.provider'

const useStyles = makeStyles((theme) => ({
  headerHolder: {
    display: 'grid',
    gridTemplateColumns: '1fr auto',
    width: '100%',
    cursor: 'context-menu',
  },
  responsableImage: {
    width: '100%',
    objectFit: 'cover',
    borderRadius: '5%',
  },
  input: {
    width: '100%',
    backgroundColor: 'white',
  },
  textArea: {
    width: '100%',
    borderRadius: '4px 4px 0 0',
    backgroundColor: 'rgba(0, 0, 0, 0.09)',
    border: 'none',
    borderBottom: '1px solid grey',
    '&:active': {
      outline: 'none',
    },
    '&:focus': {
      outline: 'none',
      transition: '0.3s',
      borderBottom: `2px solid ${theme.palette.secondary.main}`,
    },
  },
  textAreaError: {
    width: '100%',
    borderRadius: '4px 4px 0 0',
    backgroundColor: 'rgba(0, 0, 0, 0.09)',
    border: 'none',
    borderBottom: `2px solid ${theme.palette.error.main}`,
    '&:active': {
      outline: 'none',
    },
    '&:focus': {
      outline: 'none',
      transition: '0.3s',
    },
    '&::placeholder': {
      color: theme.palette.error.main,
    },
  },
  changeImageLabel: {
    position: 'absolute',
    top: '0%',
    height: '100px',
    width: '100px',
    display: 'grid',
    gridAutoFlow: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '100%',
    backgroundColor: '#00000080',
    color: 'white',
  },
  imageHolder: {
    position: 'relative',
    height: 'fit-content',
    width: '100%',
    '& >label>p': {
      display: 'none',
    },
    '&:hover >label>p': {
      display: 'grid',
    },
  },
  headerText: {
    textDecoration: 'underline',
  },
  textAreaHelperText: {
    color: theme.palette.error.main,
    fontSize: '0.75rem',
    margin: '3px 14px',
    fontWeight: 400,
    lineHeight: '1.66',
    width: '100%',
  },
  textAreaLabelError: {
    color: theme.palette.error.main,
  },
  placeholderImage: {
    width: '100px',
    height: '100px',
    size: '500px',
    borderRadius: '100%',
  },
}))

function StructureSetting({ intl: { formatMessage } }) {
  const [isDataLoading, setIsDataLoading] = useState(true)
  const [isPersonnelDataSubmitting, setIsPersonnelDataSubmitting] =
    useState(false)
  const [isStructureDataSubmitting, setIsStructureDataSubmitting] =
    useState(false)
  const [structureInformation, setStructureInformation] = useState({})
  const [mainImageRef, setMainImageRef] = useState({
    display: undefined,
    binary: undefined,
  })
  const [imageRef, setImageRef] = useState({
    display: undefined,
    binary: undefined,
  })
  const classes = useStyles()
  const location = useLocation()
  const navigate = useNavigate()
  const {
    personDispatch,
    personState: {
      person: personnelInformation,
      person: { first_name, last_name },
    },
  } = usePerson()
  const {
    dispatch,
    state: {
      activeStructure: {
        specific_indications,
        structure_id,
        description,
        logo_ref,
        name,
      },
      activeStructure,
    },
  } = useStructure()

  const personnelValidationSchema = yup.object({
    phone: yup
      .string()
      .required(formatMessage({ id: 'phoneResponsableRequiredError' }))
      .matches(
        /^(6|2)(2|3|[5-9])[0-9]{7}$/,
        formatMessage({ id: 'phoneResponsableRegexFailed' })
      ),
    email: yup
      .string()
      .email(formatMessage({ id: 'emailResponsableNotEmailError' }))
      .required(formatMessage({ id: 'emailResponsableRequiredError' })),
  })

  const structureValidationSchema = yup.object({
    name: yup
      .string()
      .required(formatMessage({ id: 'structureNameRequiredError' })),
    category_id: yup
      .string(formatMessage({ id: 'structureCategoryStringErrorMessage' }))
      .required(formatMessage({ id: 'structureCategoryRequiredErrorMessage' })),
    description: yup
      .string()
      .required(formatMessage({ id: 'structureDescriptionRequiredError' })),
    specific_indications: yup
      .string()
      .required(formatMessage({ id: 'structureSpecificIndicationsRequired' })),
  })
  const personnelFormik = useFormik({
    initialValues: {
      phone: '',
      email: '',
    },
    validationSchema: personnelValidationSchema,
    onSubmit: (values) => {
      setIsPersonnelDataSubmitting(true)
      const { person_id } = personnelInformation
      updateProfile(person_id, { ...values, person_image_ref: imageRef.binary })
        .then(() => {
          personDispatch({
            type: 'LOAD_PERSON_DATA',
            payload: { ...personnelInformation, ...values },
          })
          setIsPersonnelDataSubmitting(false)
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
          setIsPersonnelDataSubmitting(false)
        })
    },
  })

  const structureFormik = useFormik({
    initialValues: {
      name: '',
      description: '',
      specific_indications: '',
      category_id: '',
    },
    validationSchema: structureValidationSchema,
    onSubmit: (values) => {
      setIsStructureDataSubmitting(true)
      const { structure_id } = structureInformation
      updateStructure(structure_id, {
        ...values,
        logo_ref: mainImageRef.binary,
      })
        .then(() => {
          setIsStructureDataSubmitting(false)
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
          setIsStructureDataSubmitting(false)
        })
    },
  })
  useEffect(() => {
    structureFormik.setValues({ name, description, specific_indications })
    setMainImageRef({ display: logo_ref, binary: undefined })
    setStructureInformation(activeStructure)
    setIsDataLoading(false)
    // eslint-disable-next-line
  }, [activeStructure])

  useEffect(() => {
    if (!personnelInformation || !personnelInformation.email) {
      findUserInfo()
        .then((person) => {
          personDispatch({ type: 'LOAD_PERSON_DATA', payload: person })
          const { email, phone, person_image_ref } = person
          personnelFormik.setValues({ email, phone })
          setImageRef({ display: person_image_ref, binary: undefined })
          setIsPersonnelDataSubmitting(false)
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
        })
    } else {
      personnelFormik.setValues({
        email: personnelInformation.email,
        phone: personnelInformation.phone,
      })
    }
    getOwnerStructures()
      .then((structures) => {
        setStructures(structures)
        dispatch({
          type: 'SET_ACTIVE_STRUCTURE',
          payload: structures[0],
        })
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
    // eslint-disable-next-line
  }, [])

  function handleFileInput(e) {
    if (e.target.files.length > 0) {
      let files = Object.keys(e.target.files)
      files = files.map((fileKey) => {
        return e.target.files[fileKey]
      })
      files = { display: URL.createObjectURL(files[0]), binary: files[0] }
      switch (e.target.id) {
        case 'mainImageRef':
          setMainImageRef(files)
          break
        case 'imageRef':
          setImageRef(files)
          break
        default:
          break
      }
    }
  }

  const [isScheduleDialogOpen, setIsScheduleDialogOpen] = useState(false)
  const [isScheduleDataSubmitting, setIsScheduleDataSubmitting] =
    useState(false)
  const [structureData, setStructureData] = useState({})
  const [structures, setStructures] = useState([])

  const updateStructureSchedule = (newSchedule) => {
    setIsScheduleDataSubmitting(true)
    const { schedule_id, open_at, close_at } = newSchedule
    updateSchedule(schedule_id, newSchedule)
      .then(() => {
        const { structureInformation } = structureData
        const { schedules } = structureInformation
        const newSchedules = schedules.map((schedule) => {
          if (schedule.schedule_id === schedule_id) {
            return { ...schedule, open_at, close_at }
          } else return schedule
        })

        const newStructureData = {
          ...structureData,
          structureInformation: {
            ...structureInformation,
            schedules: newSchedules,
          },
        }
        setStructureData(newStructureData)
        setIsScheduleDataSubmitting(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsScheduleDataSubmitting(false)
      })
  }

  const deleteStructureSchedule = (newSchedule) => {
    setIsScheduleDataSubmitting(true)
    const { schedule_id } = newSchedule
    deleteSchedule(schedule_id)
      .then(() => {
        const { structureInformation } = structureData
        const { schedules } = structureInformation
        const newSchedules = schedules.filter(
          (schedule) => schedule.schedule_id !== schedule_id
        )

        const newStructureData = {
          ...structureData,
          structureInformation: {
            ...structureInformation,
            schedules: newSchedules,
          },
        }
        setStructureData(newStructureData)
        setIsScheduleDataSubmitting(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsScheduleDataSubmitting(false)
      })
  }

  const createStructureSchedule = (newSchedule) => {
    setIsScheduleDataSubmitting(true)
    createNewSchedule({ ...newSchedule, structure_id })
      .then(() => {
        notifySuccess(formatMessage({ id: 'scheduleSuccess' }))
        setIsScheduleDataSubmitting(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsScheduleDataSubmitting(false)
      })
  }

  const openScheduleDialog = () => {
    setIsScheduleDialogOpen(true)
  }

  const closeScheduleDialog = () => {
    setIsScheduleDialogOpen(false)
  }
  const selectStructureHandler = (event) => {
    structureFormik.resetForm()
    dispatch({
      type: 'SET_ACTIVE_STRUCTURE',
      payload: {
        ...structures.find(({ structure_id: id }) => id === event.target.value),
      },
    })
  }

  const [categories, setCategories] = useState([])
  useEffect(() => {
    getStructureCagetories()
      .then((categories) => {
        setCategories(categories)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Box style={{ display: 'grid', gridAutoFlow: 'column' }}>
      <ManageScheduleDialog
        structure={{ structure_id, structure_name: name }}
        isDialogOpen={isScheduleDialogOpen}
        closeDialog={closeScheduleDialog}
        updateSchedule={updateStructureSchedule}
        createSchedule={createStructureSchedule}
        deleteSchedule={deleteStructureSchedule}
        isScheduleDataSubmitting={isScheduleDataSubmitting}
      />
      <Box className={classes.imageHolder}>
        {!mainImageRef.display ? (
          <PersonAddRounded className={classes.placeholderImage} />
        ) : (
          <img
            className={classes.responsableImage}
            src={mainImageRef.display}
            alt={formatMessage({ id: 'personnelImage' })}
          />
        )}
        <input
          accept='image/*'
          hidden
          id='mainImageRef'
          type='file'
          onChange={handleFileInput}
        />
        {isDataLoading || isPersonnelDataSubmitting ? null : (
          <label htmlFor='mainImageRef'>
            <Typography className={classes.changeImageLabel}>
              {formatMessage({ id: 'changePhoto' })}
              <EditRounded />
            </Typography>
          </label>
        )}{' '}
        <Typography
          onClick={openScheduleDialog}
          variant='h3'
          style={{ cursor: 'pointer', marginTop: '20px', width: '100%' }}
        >
          <Tooltip arrow title={formatMessage({ id: 'manageScheduleTooltip' })}>
            <IconButton color='secondary'>
              <EventAvailableRounded />
            </IconButton>
          </Tooltip>
          {formatMessage({ id: 'setSchedule' })}
        </Typography>
      </Box>
      <Box style={{ padding: 15 }}>
        <Grid container spacing={2}>
          <Grid item xs={12} container style={{ gap: '15px', display: 'grid' }}>
            {activeStructure?.structure_id && (
              <Typography variant='h4' color='secondary'>
                {`${formatMessage({ id: 'professionalInformation' })} `}
                <TextField
                  name='struture'
                  defaultValue={activeStructure.structure_id}
                  select
                  onChange={selectStructureHandler}
                  variant='filled'
                >
                  {structures.map(({ structure_id, name }) => (
                    <MenuItem key={structure_id} value={structure_id}>
                      {name}
                    </MenuItem>
                  ))}
                </TextField>{' '}
                <TextField
                  select
                  id='category_id'
                  name='category_id'
                  variant='filled'
                  onChange={structureFormik.handleChange}
                  value={structureFormik.values.category_id}
                  helperText={structureFormik.errors.category_id}
                  disabled={isStructureDataSubmitting}
                  defaultValue={activeStructure.categories[0]}
                  error={structureFormik.errors.category_id ? true : false}
                >
                  {categories.map(({ category_id, category_name }) => (
                    <MenuItem key={category_id} value={category_id}>
                      {category_name
                        ? formatMessage({ id: category_name })
                        : ''}
                    </MenuItem>
                  ))}
                </TextField>
              </Typography>
            )}
            <TextField
              value={structureFormik.values.name}
              color='secondary'
              size='small'
              variant='filled'
              className={classes.input}
              name='name'
              required
              id='name'
              label={formatMessage({ id: 'structureLabel' })}
              type='text'
              onChange={structureFormik.handleChange}
              onBlur={structureFormik.handleBlur}
              disabled={isStructureDataSubmitting}
              error={
                structureFormik.touched.name && structureFormik.errors.name
              }
              helperText={
                structureFormik.touched.name && structureFormik.errors.name
                  ? structureFormik.errors.name
                  : null
              }
            />
            <TextareaAutosize
              rowsMin={3}
              placeholder={formatMessage({
                id: 'structureParticularIndications',
              })}
              value={structureFormik.values.specific_indications}
              name='specific_indications'
              required
              id='specific_indications'
              onChange={structureFormik.handleChange}
              onBlur={structureFormik.onBlur}
              disabled={isStructureDataSubmitting}
              className={
                structureFormik.touched.specific_indications &&
                structureFormik.errors.specific_indications
                  ? classes.textAreaError
                  : classes.textArea
              }
            />
            <Typography className={classes.textAreaHelperText}>
              {structureFormik.touched.specific_indications &&
                structureFormik.errors.specific_indications}
            </Typography>
            <TextareaAutosize
              rowsMin={5}
              placeholder={formatMessage({
                id: 'structureDescriptionPlaceholder',
              })}
              className={
                structureFormik.touched.description &&
                structureFormik.errors.description
                  ? classes.textAreaError
                  : classes.textArea
              }
              value={structureFormik.values.description || ''}
              name='description'
              id='description'
              onChange={structureFormik.handleChange}
              onBlur={structureFormik.onBlur}
              disabled={isStructureDataSubmitting}
            />
            <Typography className={classes.textAreaHelperText}>
              {structureFormik.touched.description &&
                structureFormik.errors.description}
            </Typography>
            <Button
              variant='outlined'
              color='secondary'
              style={{ justifySelf: 'right' }}
              onClick={structureFormik.handleSubmit}
              disabled={
                isStructureDataSubmitting ||
                (structureFormik.errors.description &&
                  structureFormik.errors.name &&
                  structureFormik.errors.specific_indications) ||
                (structureInformation &&
                  structureFormik.values.name === structureInformation.name &&
                  structureFormik.values.description ===
                    structureInformation.description &&
                  structureFormik.values.specific_indications ===
                    structureInformation.specific_indications &&
                  structureFormik.values.category_id ===
                    structureInformation.category_id &&
                  mainImageRef.binary === undefined)
              }
            >
              {isStructureDataSubmitting && (
                <CircularProgress
                  size={24}
                  style={{ marginRight: 5 }}
                  color='secondary'
                />
              )}
              {formatMessage({ id: 'editResponsableDataButton' })}
            </Button>
          </Grid>
          <Grid
            item
            container
            xs={12}
            style={{ height: 'fit-content', gap: '15px', display: 'grid' }}
          >
            <Typography variant='h4' color='secondary'>
              {formatMessage({ id: 'personnalInformation' })}
              <Typography variant='body1' color='textSecondary'>
                {`${first_name} ${last_name}`}
              </Typography>
            </Typography>
            {/* <Box className={classes.imageHolder}>
              {!imageRef.display ? (
                <PersonAddRounded className={classes.placeholderImage} />
              ) : (
                <img
                  className={classes.responsableImage}
                  src={imageRef.display}
                  alt={formatMessage({ id: 'personnelImage' })}
                />
              )}
              <input
                accept='image/*'
                hidden
                id='imageRef'
                type='file'
                onChange={handleFileInput}
              />
              {isDataLoading || isPersonnelDataSubmitting ? null : (
                <label htmlFor='imageRef'>
                  <Typography className={classes.changeImageLabel}>
                    {formatMessage({ id: 'changePhoto' })}
                    <EditRounded />
                  </Typography>
                </label>
              )}
            </Box> */}
            <TextField
              value={personnelFormik.values.phone}
              color='secondary'
              variant='filled'
              className={classes.input}
              name='phone'
              required
              size='small'
              id='phone'
              label={formatMessage({ id: 'phoneResponsableLabel' })}
              type='text'
              onChange={personnelFormik.handleChange}
              onBlur={personnelFormik.handleBlur}
              disabled={isPersonnelDataSubmitting}
              error={
                personnelFormik.touched.phone && personnelFormik.errors.phone
              }
              helperText={
                personnelFormik.touched.phone && personnelFormik.errors.phone
                  ? personnelFormik.errors.phone
                  : null
              }
            />
            <TextField
              value={personnelFormik.values.email}
              color='secondary'
              variant='filled'
              className={classes.input}
              name='email'
              required
              size='small'
              id='email'
              label={formatMessage({ id: 'emailResponsableLabel' })}
              type='text'
              onChange={personnelFormik.handleChange}
              onBlur={personnelFormik.handleBlur}
              disabled={isPersonnelDataSubmitting}
              error={
                personnelFormik.touched.email && personnelFormik.errors.email
              }
              helperText={
                personnelFormik.touched.email && personnelFormik.errors.email
                  ? personnelFormik.errors.email
                  : null
              }
            />
            <Button
              variant='outlined'
              color='secondary'
              style={{ width: 'fit-content', justifySelf: 'end' }}
              onClick={personnelFormik.handleSubmit}
              disabled={
                isPersonnelDataSubmitting ||
                (personnelFormik.errors.phone &&
                  personnelFormik.errors.email) ||
                (personnelInformation &&
                  personnelFormik.values.phone === personnelInformation.phone &&
                  personnelFormik.values.email === personnelInformation.email &&
                  imageRef.binary === undefined)
              }
            >
              {isPersonnelDataSubmitting && (
                <CircularProgress
                  color='secondary'
                  size={24}
                  style={{ marginRight: 5 }}
                />
              )}
              {formatMessage({ id: 'editResponsableDataButton' })}
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Box>
  )
}

export default injectIntl(StructureSetting)
