import {
  Button,
  Box,
  Dialog,
  DialogContent,
  makeStyles,
  TextField,
  Typography,
  CircularProgress,
  Tooltip,
  IconButton,
  Paper,
  Collapse,
  MenuItem,
  Chip,
  Select,
} from '@material-ui/core'
import {
  CloseRounded,
  DeleteForeverRounded,
  SaveRounded,
} from '@material-ui/icons'
import { useEffect, useState } from 'react'
import Scrollbars from 'react-custom-scrollbars-2'
import { FormattedDate, injectIntl } from 'react-intl'
import { useFormik } from 'formik'
import MuiDialogTitle from '@material-ui/core/DialogTitle'
import * as yup from 'yup'
import { notifyError } from '../../utils/toastMessages'
import { getStructreSchedules } from '../../services/structure.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import { Skeleton } from '@material-ui/lab'

let useStyles = makeStyles(() => ({
  scheduleHolder: {
    display: 'grid',
    marginRight: '10px',
    marginTop: '10px',
    '& .save': {
      justifySelf: 'end',
    },
    '&:hover .save': {
      display: 'block',
    },
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  root: {
    margin: 0,
    padding: '5px 24px',
    display: 'grid',
    gridTemplateColumns: '1fr auto',
  },
}))

function produceDayDate(number) {
  switch (number) {
    case 2:
      return '2021/07/26'
    case 3:
      return '2021/07/27'
    case 4:
      return '2021/07/28'
    case 5:
      return '2021/07/29'
    case 6:
      return '2021/07/30'
    case 7:
      return '2021/07/31'
    case 1:
      return '2021/08/01'
    default:
      break
  }
}

function AddNewSchedule({
  createSchedule,
  formatMessage,
  isScheduleDataSubmitting,
}) {
  const classes = useStyles()
  const validationSchema = yup.object({
    date: yup
      .date()
      .min(
        yup.ref('min_date'),
        'You can only create a schedule for future dates'
      ),
    open_at: yup.string().required('Must provide an openning time'),
    close_at: yup.string().required('Must provide an closing time'),
  })
  const todaysDate = new Date()
  const formik = useFormik({
    initialValues: {
      open_at: '',
      close_at: '',
      date: todaysDate,
      min_date: new Date(),
      days: [new Date(produceDayDate(2)).getDay()],
    },
    validationSchema,
    onSubmit: (values, { resetForm }) => {
      let dateTemplate = '2022/05/01'
      if (
        new Date(`${dateTemplate} ${values.close_at}`) <
        new Date(`${dateTemplate} ${values.open_at}`)
      ) {
        notifyError(formatMessage({ id: 'timeValidation' }))
      } else {
        const { open_at, close_at, date, days } = values
        createSchedule({
          open_at,
          close_at,
          date: new Date(date).getDate() === todaysDate.getDate() ? null : date,
          days,
        })
        resetForm()
      }
    },
  })

  return (
    <Paper
      className={classes.scheduleHolder}
      style={{ padding: 10, marginBottom: 10 }}
      elevation='3'
    >
      <Typography
        style={{
          textTransform: 'capitalize',
          fontWeight: '700',
          letterSpacing: '1px',
          color: 'red',
        }}
      >
        {formatMessage({ id: 'AddSchedule' })}
      </Typography>
      <Box
        style={{
          border: '1px solid grey',
          padding: '5px',
          display: 'grid',
          gridGap: '10px',
        }}
      >
        <Select
          select
          fullWidth
          multiple
          name='days'
          variant='outlined'
          value={formik.values.days}
          disabled={isScheduleDataSubmitting}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.days && formik.errors.days}
          helperText={formik.touched.days && formik.errors.days}
          renderValue={(selected) => (
            <div className={classes.chips}>
              {selected.map((value) => (
                <Chip
                  key={value}
                  label={
                    <FormattedDate
                      value={produceDayDate(value)}
                      weekday='long'
                    />
                  }
                  className={classes.chip}
                />
              ))}
            </div>
          )}
          style={{ padding: '10px' }}
        >
          {[1, 2, 3, 4, 5, 6, 7].map((index) => (
            <MenuItem key={index} value={index}>
              <Typography
                style={{
                  textTransform: 'capitalize',
                  fontWeight: '500',
                  letterSpacing: '1px',
                  padding: 10,
                }}
              >
                <FormattedDate value={produceDayDate(index)} weekday='long' />
              </Typography>
            </MenuItem>
          ))}
        </Select>
        <TextField
          variant='outlined'
          name='date'
          type='date'
          value={formik.values.date}
          disabled={isScheduleDataSubmitting}
          fullWidth
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          error={formik.touched.date && formik.errors.date}
          helperText={formik.touched.date && formik.errors.date}
        />
        <Box
          style={{
            display: 'grid',
            gridAutoFlow: 'column',
            gridGap: '10px',
          }}
        >
          <Box>
            <Typography>{formatMessage({ id: 'OpendAt' })}</Typography>
            <TextField
              variant='outlined'
              name='open_at'
              type='time'
              value={formik.values.open_at}
              fullWidth
              disabled={isScheduleDataSubmitting}
              onChange={formik.handleChange}
            />
          </Box>
          <Box>
            <Typography>{formatMessage({ id: 'CloseAt' })}</Typography>
            <TextField
              variant='outlined'
              name='close_at'
              type='time'
              fullWidth
              value={formik.values.close_at}
              disabled={isScheduleDataSubmitting}
              onChange={formik.handleChange}
            />
          </Box>
        </Box>
      </Box>
      <Button
        size='small'
        color='secondary'
        variant='outlined'
        disabled={
          formik.values.close_at === '' ||
          formik.values.open_at === '' ||
          isScheduleDataSubmitting
        }
        style={{ marginTop: 5, justifySelf: 'end' }}
        onClick={formik.handleSubmit}
      >
        {isScheduleDataSubmitting && (
          <CircularProgress
            color='secondary'
            size={20}
            style={{ marginRight: 5, justifySelf: 'end' }}
          />
        )}
        {formatMessage({ id: 'CreateSchedule' })}
      </Button>
    </Paper>
  )
}

function StyledSchedule({
  schedule,
  formatMessage,
  updateSchedule,
  deleteSchedule,
}) {
  const classes = useStyles()
  const { day, open_at, close_at, date, schedule_id } = schedule
  const [isScheduleDataSubmitting, setIsScheduleDataSubmitting] =
    useState(false)
  const formik = useFormik({
    initialValues: {
      open_at,
      close_at,
    },
    onSubmit: (values) => {
      setIsScheduleDataSubmitting(true)
      updateSchedule({ ...values, day, date, schedule_id })
      setIsScheduleDataSubmitting(false)
    },
  })
  return (
    <Box className={classes.scheduleHolder}>
      {day !== null ? (
        <Typography
          style={{
            textTransform: 'capitalize',
            fontWeight: '700',
            letterSpacing: '1px',
          }}
        >
          <FormattedDate value={produceDayDate(day)} weekday='long' />
          {', '}
          {date && (
            <FormattedDate
              value={date}
              day='2-digit'
              month='long'
              year='numeric'
            />
          )}
        </Typography>
      ) : (
        <Typography
          style={{
            textTransform: 'capitalize',
            fontWeight: '700',
            letterSpacing: '1px',
            color: 'green',
          }}
        >
          <FormattedDate
            value={date}
            year='numeric'
            month='long'
            day='2-digit'
            weekday='short'
          />
        </Typography>
      )}
      <Box
        style={{
          border: '1px solid grey',
          display: 'grid',
          gridAutoFlow: 'column',
          padding: '2.5px',
          gridGap: '10px',
        }}
      >
        <Box>
          <Typography>{formatMessage({ id: 'OpenAt' })}</Typography>
          <TextField
            variant='outlined'
            name='open_at'
            type='time'
            defaultValue={open_at}
            disabled={
              isScheduleDataSubmitting ||
              (date !== null && new Date(date) <= new Date())
            }
            fullWidth
            onChange={formik.handleChange}
          />
        </Box>
        <Box>
          <Typography>{formatMessage({ id: 'CloseAt' })}</Typography>
          <TextField
            variant='outlined'
            name='close_at'
            type='time'
            fullWidth
            defaultValue={close_at}
            disabled={
              isScheduleDataSubmitting ||
              (date !== null && new Date(date) <= new Date())
            }
            onChange={formik.handleChange}
          />
        </Box>
      </Box>
      {(formik.values.open_at !== open_at ||
        formik.values.close_at !== close_at) &&
      !isScheduleDataSubmitting ? (
        <Box className='save'>
          {date === null || new Date(date) >= new Date() ? (
            <>
              <Tooltip arrow title='Save changes'>
                <IconButton
                  size='small'
                  color='secondary'
                  onClick={formik.handleSubmit}
                >
                  <SaveRounded fontSize='small' />
                </IconButton>
              </Tooltip>
              <Tooltip arrow title='Delete Schedule'>
                <IconButton
                  size='small'
                  // color="secondary"
                  style={{ color: 'red' }}
                  onClick={() => deleteSchedule(schedule)}
                >
                  <DeleteForeverRounded fontSize='small' />
                </IconButton>
              </Tooltip>
            </>
          ) : null}
          {date !== null && new Date(date) >= new Date() ? (
            <Tooltip arrow title='Delete Schedule'>
              <IconButton
                size='small'
                // color="secondary"
                style={{ color: 'red' }}
                onClick={() => deleteSchedule(schedule)}
              >
                <DeleteForeverRounded fontSize='small' />
              </IconButton>
            </Tooltip>
          ) : null}
        </Box>
      ) : isScheduleDataSubmitting ? (
        <CircularProgress
          color='secondary'
          size={24}
          style={{ marginTop: 5, marginRight: 5, justifySelf: 'end' }}
        />
      ) : null}
    </Box>
  )
}

const DialogTitle = ({ children, onClose }) => {
  const classes = useStyles()
  return (
    <MuiDialogTitle disableTypography className={classes.root}>
      <Typography variant='h6'>{children}</Typography>
      {onClose ? (
        <IconButton aria-label='close' onClick={onClose}>
          <CloseRounded />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  )
}

function ManageScheduleDialog({
  intl: { formatMessage },
  isDialogOpen,
  closeDialog,
  structure: { structure_id, structure_name },
  updateSchedule,
  createSchedule,
  deleteSchedule,
  isScheduleDataSubmitting,
}) {
  const [isNewScheduleOpen, setIsNewScheduleOpen] = useState(false)
  const [isScheduleDataLoading, setIsScheduleDataLoading] = useState(true)
  const [scheduleData, setScheduleData] = useState([])

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  useEffect(() => {
    if (structure_id && isDialogOpen) {
      setIsScheduleDataLoading(true)
      getStructreSchedules(structure_id)
        .then((schedules) => {
          setScheduleData(schedules)
          setIsScheduleDataLoading(false)
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
        })
    } else if (isDialogOpen)
      notifyError(formatMessage({ id: 'structureNotSelected' }))
    // eslint-disable-next-line
  }, [structure_id, isScheduleDataSubmitting, isDialogOpen])

  return (
    <Dialog open={isDialogOpen}>
      <DialogTitle id='create_edit_dialog' onClose={closeDialog}>
        {formatMessage({ id: 'ManageSchedules' })} {structure_name}
      </DialogTitle>
      <DialogContent>
        <Scrollbars
          autoHeight
          autoHide
          autoHeightMin={'70vh'}
          style={{
            paddingBottom: '2px',
            marginTop: '20px',
            boxShadow: ' 0px 0px 3px 0px rgb(0 0 0 / 12%)',
          }}
        >
          {isScheduleDataLoading ? (
            [...new Array(12)].map(() => (
              <div style={{ gap: '10px', display: 'grid', padding: '5px' }}>
                <Skeleton variant='rect' width={'100%'} height={'50px'} />
                <div style={{ display: 'flex', gap: '5px' }}>
                  <Skeleton variant='rect' width={'50%'} height={'50px'} />
                  <Skeleton variant='rect' width={'50%'} height={'50px'} />
                </div>
              </div>
            ))
          ) : scheduleData === undefined ? (
            'Something went wrong please try again'
          ) : scheduleData.length === 0 ? (
            <>
              <Button
                variant='outlined'
                onClick={() => setIsNewScheduleOpen(!isNewScheduleOpen)}
              >
                {formatMessage({ id: 'AddSchedule' })}
              </Button>
              <Collapse in={isNewScheduleOpen}>
                <AddNewSchedule
                  createSchedule={createSchedule}
                  formatMessage={formatMessage}
                  isScheduleDataSubmitting={isScheduleDataSubmitting}
                />
              </Collapse>
              <Typography>{`There are no schedules at the moment`}</Typography>
            </>
          ) : (
            <>
              <Button
                variant='outlined'
                onClick={() => setIsNewScheduleOpen(!isNewScheduleOpen)}
              >
                {formatMessage({ id: 'AddSchedule' })}
              </Button>
              <Collapse in={isNewScheduleOpen}>
                <AddNewSchedule
                  createSchedule={createSchedule}
                  formatMessage={formatMessage}
                  isScheduleDataSubmitting={isScheduleDataSubmitting}
                />
              </Collapse>
              {scheduleData.map((schedule) => {
                return (
                  <StyledSchedule
                    schedule={schedule}
                    formatMessage={formatMessage}
                    updateSchedule={updateSchedule}
                    deleteSchedule={deleteSchedule}
                    isScheduleDataSubmitting={isScheduleDataSubmitting}
                  />
                )
              })}
            </>
          )}
        </Scrollbars>
      </DialogContent>
    </Dialog>
  )
}

export default injectIntl(ManageScheduleDialog)
