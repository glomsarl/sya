import { useState } from 'react'
import {
  makeStyles,
  Card,
  CardActions,
  CardContent,
  Grid,
  Typography,
  IconButton,
  Tooltip,
  Grow,
} from '@material-ui/core'
import { injectIntl, FormattedDateTimeRange } from 'react-intl'
import DisplayDate from '../shared/DisplayDate'
import { DeleteRounded, EditRounded } from '@material-ui/icons'
// import DeletePromotionDialog from "./DeletePromotionDialog";
import { useCatalog } from '../../contexts/Catalog/Catalog.provider'
import FolderOpenRoundedIcon from '@material-ui/icons/FolderOpenRounded'
import EditCatalogDialog from './EditCatalogDialog'
import DeleteCatalogDialog from './DeleteCatalogDialog'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import { deleteCatalog, editCatalog } from '../../services/catalogs.service'

const useStyles = makeStyles((theme) => ({
  root: {
    cursor: 'pointer',
    height: '100%',
    position: 'relative',
    '& #cardOptions': {
      visibility: 'none',
    },
    '&:hover #cardOptions': {
      visibility: 'visible',
    },
  },
  colorRoot: {
    cursor: 'pointer',
    backgroundColor: theme.palette.grey[100],
    height: '100%',
    position: 'relative',
  },
  title: {
    fontSize: 14,
    display: 'inline',
  },
  promotionName: {
    borderTop: '1px solid #80808027',
    marginTop: '10px',
    paddingTop: '10px',
  },
  cardActions: {
    display: 'grid',
    gridAutoFlow: 'column',
    justifyContent: 'end',
    marginTop: '10px',
    position: 'absolute',
    bottom: 0,
    right: 0,
    backgroundColor: theme.palette.grey[100],
    borderRadius: '6px',
  },
  deleteButton: {
    color: theme.palette.error.main,
  },
}))

function StyledCatalog({ intl, catalog }) {
  const classes = useStyles()
  const [catalogGrow, setCatalogGrow] = useState(false)
  const { formatMessage } = intl
  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false)
  const [isDeleting, setIsDeleting] = useState(false)
  const { catalogDispatch } = useCatalog()
  const [catalogToDeleteId, setCatalogToDeleteId] = useState()
  const [isEditDialogOpen, setIsEditDialogOpen] = useState(false)
  const { starts_at, ends_at, created_at, catalog_id } = catalog
  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  let closeEditDialog = () => {
    setIsEditDialogOpen(false)
  }

  let openEditDialog = () => {
    setIsEditDialogOpen(true)
  }

  let closeDeleteDialog = () => {
    setIsDeleteDialogOpen(false)
    setCatalogToDeleteId(undefined)
  }

  let confirmDelete = () => {
    setIsDeleting(true)
    deleteCatalog(catalogToDeleteId)
      .then(() => {
        catalogDispatch({
          type: 'DELETE_CATALOG',
          payload: catalogToDeleteId,
        })
        closeDeleteDialog()
        setIsDeleting(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate,
        )
        setIsDeleting(false)
      })
  }

  let openDeleteDialog = () => {
    setCatalogToDeleteId(catalog_id)
    setIsDeleteDialogOpen(true)
  }

  let confirmEdit = (newCatalog) => {
    editCatalog(catalog_id, {
      starts_at: newCatalog.start,
      ends_at: newCatalog.end,
    })
      .then(() => {
        catalogDispatch({
          type: 'EDIT_CATALOG',
          payload: { ...catalog, ...newCatalog },
        })
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate,
        )
      })
  }

  let openCatalog = (event) => {
    catalogDispatch({ type: 'OPEN_CATALOG', payload: catalog })
  }

  return (
    <>
      <Grid item xs={12} sm={6} md={4} lg={3} xl={2}>
        <Card
          className={
            new Date(ends_at) < new Date() ? classes.colorRoot : classes.root
          }
          onMouseOver={() => setCatalogGrow(true)}
          onMouseOut={() => setCatalogGrow(false)}
          elevation={catalogGrow ? 7 : 4}
        >
          <CardContent>
            <DisplayDate
              dateTitle={formatMessage({ id: 'createdAt' })}
              value={new Date(created_at)}
            />
            <br />
            <Typography
              className={classes.title}
              color="textSecondary"
              gutterBottom
            >
              {formatMessage({ id: 'catalogPeriod' })}
            </Typography>
            <Typography color="textPrimary" className={classes.title}>
              <FormattedDateTimeRange
                from={new Date(starts_at)}
                to={new Date(ends_at)}
                year="numeric"
                month="short"
                day="numeric"
                weekday="short"
              />
            </Typography>
          </CardContent>
          <Grow
            in={catalogGrow}
            style={{ transformOrigin: '0 0 0' }}
            {...(catalogGrow ? { timeout: 500 } : {})}
          >
            <CardActions className={classes.cardActions} id="cardOptions">
              <Tooltip
                arrow
                title={formatMessage({ id: 'openCatalogTooltip' })}
              >
                <IconButton
                  size="small"
                  color="secondary"
                  onClick={openCatalog}
                >
                  <FolderOpenRoundedIcon fontSize="small" />
                </IconButton>
              </Tooltip>
              {new Date(ends_at) < new Date() ? null : (
                <>
                  <Tooltip
                    arrow
                    title={formatMessage({ id: 'editCatalogTooltip' })}
                  >
                    <IconButton
                      size="small"
                      color="secondary"
                      onClick={openEditDialog}
                    >
                      <EditRounded fontSize="small" />
                    </IconButton>
                  </Tooltip>
                  <Tooltip
                    arrow
                    title={formatMessage({ id: 'deleteCatalogTooltip' })}
                  >
                    <IconButton
                      size="small"
                      className={classes.deleteButton}
                      onClick={openDeleteDialog}
                    >
                      <DeleteRounded fontSize="small" />
                    </IconButton>
                  </Tooltip>
                </>
              )}
            </CardActions>
          </Grow>
        </Card>
      </Grid>
      <DeleteCatalogDialog
        isDeleteDialogOpen={isDeleteDialogOpen}
        confirmDelete={confirmDelete}
        closeDialog={closeDeleteDialog}
        isDeleting={isDeleting}
      />
      <EditCatalogDialog
        open={isEditDialogOpen}
        closeDialog={closeEditDialog}
        start={starts_at}
        end={ends_at}
        catalog_id={catalog_id}
        confirmEdit={confirmEdit}
      />
    </>
  )
}

export default injectIntl(StyledCatalog)
