import React from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Slide,
  CircularProgress,
} from '@material-ui/core'
import { injectIntl } from 'react-intl'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />
})

function RemoveServiceFromCatalogDialog({
  intl: { formatMessage },
  isRemoveDialogOpen,
  confirmRemove,
  closeDialog,
  isRemoving,
}) {
  return (
    <Dialog
      open={isRemoveDialogOpen}
      TransitionComponent={Transition}
      keepMounted
      onClose={isRemoving ? null : closeDialog}
    >
      <DialogTitle>{formatMessage({ id: 'removeService' })}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          {formatMessage({ id: 'removeServiceWarnig' })}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={confirmRemove} disabled={isRemoving}>
          {isRemoving ? (
            <CircularProgress size={24} style={{ marginRight: '8px' }} />
          ) : null}
          {formatMessage({ id: 'removeButton' })}
        </Button>
        <Button
          onClick={closeDialog}
          color='primary'
          disabled={isRemoving}
          variant='contained'
        >
          {formatMessage({ id: 'cancelButton' })}
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default injectIntl(RemoveServiceFromCatalogDialog)
