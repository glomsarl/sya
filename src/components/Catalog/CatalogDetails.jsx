import { useState, useEffect } from 'react'
import { injectIntl, FormattedDate, FormattedTime } from 'react-intl'
import {
  makeStyles,
  Typography,
  Box,
  Grid,
  Hidden,
  Fab,
  Tooltip,
} from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import AddRoundedIcon from '@material-ui/icons/AddRounded'
import KeyboardBackspaceRoundedIcon from '@material-ui/icons/KeyboardBackspaceRounded'
import Scrollbars from 'react-custom-scrollbars-2'
import { useCatalog } from '../../contexts/Catalog/Catalog.provider'
import StyledCatalogService from './StyledCatalogService'
import AddServiceToCatalogDialog from './AddServiceToCatalogDialog'
import { fetchCatalogServices } from '../../services/catalogs.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../contexts/PersonContext/Person.provider'

let useStyles = makeStyles((theme) => ({
  root: {
    display: 'grid',
    gridTemplateRows: 'auto auto 1fr',
    height: '100%',
    [theme.breakpoints.down('xs')]: {
      gridTemplateRows: 'auto 1fr',
    },
  },
  promotionValueTag: {
    backgroundColor: theme.palette.error.dark,
    borderRadius: '40%',
    color: 'white',
    padding: '2px 5px',
    fontWeight: 'bold',
  },
  headerInfo: {
    fontWeight: 'bolder',
  },
  headerRoot: {
    backgroundColor: theme.palette.primary.dark,
    padding: '10px',
  },
  headerHolder: {
    display: 'grid',
    gridAutoFlow: 'column',
    color: 'grey',
    gridTemplateColumns: 'auto 1fr',
  },
  headerNumberHolder: {
    display: 'grid',
    gridAutoFlow: 'column',
    color: 'grey',
    paddingLeft: '10px',
  },
  headerDivider: {
    borderRight: `1px solid ${theme.palette.primary.dark}`,
    margin: '5px 10px 5px 0',
  },
  serviceHeader: {
    textTransform: 'uppercase',
    justifySelf: 'center',
  },
  fab: {
    position: 'absolute',
    right: theme.spacing(2),
    bottom: theme.spacing(2),
  },
  fabBack: {
    position: 'absolute',
    left: theme.spacing(2),
    bottom: theme.spacing(2),
  },
  serviceSkeleton: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridTemplateColumns: 'auto 1fr',
    gridGap: '20px',
    borderBottom: '1px solid #f8f7f7',
  },
  noServicesText: {
    fontWeight: 100,
    color: theme.palette.secondary.light,
    height: '100%',
    display: 'grid',
    justifyContent: 'center',
    alignContent: 'center',
    textAlign: 'center',
  },
}))
function CatatogDetails({ intl: { formatMessage } }) {
  const classes = useStyles()
  const { catalogState, catalogDispatch } = useCatalog()
  const { activeCatalog } = catalogState
  const { catalog_id, starts_at, ends_at, created_at, services } = activeCatalog
  let [isAddServiceDialogOpen, setIsAddServiceDialogOpen] = useState(false)
  let [isCatalogServicesDataLoading, setIsCatalogServicesDataLoading] =
    useState(true)

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  let closeAddServiceDialog = () => {
    setIsAddServiceDialogOpen(false)
  }

  let openAddServiceDialog = () => {
    setIsAddServiceDialogOpen(true)
  }

  let CatalogDetailsHeader = () => {
    return (
      <Hidden only='xs'>
        <Grid container className={classes.serviceHeaderHolder}>
          <Grid item xs={false} sm={1} className={classes.headerNumberHolder}>
            <Typography noWrap variant='h4' className={classes.serviceHeader}>
              #
            </Typography>
            <Typography className={classes.headerDivider}></Typography>
          </Grid>

          <Grid item xs={false} sm={4} className={classes.headerHolder}>
            <Typography noWrap variant='h4' className={classes.serviceHeader}>
            {formatMessage({ id: 'service' })}
            </Typography>
            <Typography className={classes.headerDivider}></Typography>
          </Grid>

          <Grid item xs={false} sm={1} className={classes.headerHolder}>
            <Typography noWrap variant='h4' className={classes.serviceHeader}>
              {formatMessage({ id: 'quantity' })}
            </Typography>
            <Typography className={classes.headerDivider}></Typography>
          </Grid>

          <Grid item xs={false} sm={2} className={classes.headerHolder}>
            <Typography noWrap variant='h4' className={classes.serviceHeader}>
              {formatMessage({ id: 'unitAmount' })}
            </Typography>
            <Typography className={classes.headerDivider}></Typography>
          </Grid>

          <Grid item xs={false} sm={4} className={classes.headerHolder}>
            <Typography noWrap variant='h4' className={classes.serviceHeader}>
              {formatMessage({ id: 'properties' })}
            </Typography>
          </Grid>
        </Grid>
      </Hidden>
    )
  }

  let StyleCatalogServices = () => {
    return !services
      ? null
      : services.map((service, index) => (
          <StyledCatalogService key={index} service={service} number={index} />
        ))
  }

  let goToPromotions = () => {
    catalogDispatch({ type: 'CLOSE_ACTIVE_CATALOG' })
  }

  useEffect(() => {
    fetchCatalogServices({ catalog_id })
      .then((services) => {
        catalogDispatch({
          type: 'LOAD_ACTIVE_CATALOG_SERVICES',
          payload: services.catalog_services,
        })
        setIsCatalogServicesDataLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })

    // eslint-disable-next-line
  }, [])

  function ServiceSkeleton() {
    return (
      <Box className={classes.serviceSkeleton}>
        <Skeleton variant='circle' width='40px' height='40px' />
        <Skeleton variant='text' width='100%' height='43.5px' />
      </Box>
    )
  }

  return (
    <Box className={classes.root}>
      <AddServiceToCatalogDialog
        isDialogOpen={isAddServiceDialogOpen}
        closeDialog={closeAddServiceDialog}
      />

      <Box className={classes.headerRoot}>
        <Typography>
          {`Validity: `}
          <Typography component='span' className={classes.headerInfo}>
            <FormattedDate
              year='numeric'
              month='short'
              day='numeric'
              weekday='short'
              value={starts_at}
            />
            {` `}
            <FormattedTime value={starts_at} />
            {` - `}
            <FormattedDate
              year='numeric'
              month='short'
              day='numeric'
              weekday='short'
              value={ends_at}
            />
            {` `}
            <FormattedTime value={ends_at} />
          </Typography>
        </Typography>
        <Typography>
          {`Created At: `}
          <Typography component='span' className={classes.headerInfo}>
            <FormattedDate
              year='numeric'
              month='short'
              day='numeric'
              weekday='short'
              value={created_at}
            />
            {` `}
            <FormattedTime value={created_at} />
          </Typography>
        </Typography>
      </Box>
      <CatalogDetailsHeader />
      <Scrollbars>
        {services !== undefined &&
        services.length === 0 &&
        !isCatalogServicesDataLoading ? (
          new Date(ends_at) < Date.now() ? (
            <Typography className={classes.noServicesText}>
              {formatMessage({ id: 'noServicesWithoutAddButton' })}
            </Typography>
          ) : (
            <Typography className={classes.noServicesText}>
              {formatMessage({ id: 'noServicesWithAddButton' })}
            </Typography>
          )
        ) : services === undefined || isCatalogServicesDataLoading ? (
          [...new Array(12)].map((index, id) => <ServiceSkeleton key={id} />)
        ) : (
          <StyleCatalogServices />
        )}
      </Scrollbars>
      <Tooltip arrow title={`Back to catalogs`} className={classes.fabBack}>
        <Fab color='secondary' onClick={goToPromotions}>
          <KeyboardBackspaceRoundedIcon />
        </Fab>
      </Tooltip>

      <Tooltip arrow title={`Add service to catalog`} className={classes.fab}>
        <Fab color='secondary' onClick={openAddServiceDialog}>
          <AddRoundedIcon />
        </Fab>
      </Tooltip>
    </Box>
  )
}

export default injectIntl(CatatogDetails)
