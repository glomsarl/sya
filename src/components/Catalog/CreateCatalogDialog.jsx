import {
  Button,
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  makeStyles,
  DialogTitle,
  TextField,
  CircularProgress,
} from '@material-ui/core'
import { useFormik } from 'formik'
import { useState } from 'react'
import { injectIntl } from 'react-intl'
import * as yup from 'yup'
import { useCatalog } from '../../contexts/Catalog/Catalog.provider'
import Moment from 'moment'
import { extendMoment } from 'moment-range'
import { createCatalog } from '../../services/catalogs.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { notifyError } from '../../utils/toastMessages'
import { useStructure } from '../../contexts/Structures/Structure.provider'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'

const useStyles = makeStyles((theme) => ({
  inputHolder: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '10px',
    [theme.breakpoints.down('xs')]: {
      gridAutoFlow: 'row',
      gridGap: 0,
    },
  },
  form: {
    textAlign: 'center',
  },
  dialogFormInput: {
    marginTop: '15px',
  },
}))

function CreateCatalogDialog({ intl, isDialogOpen, closeDialog }) {
  const { formatMessage } = intl
  const classes = useStyles()
  let [submitLoading, setSubmitLoading] = useState(false)
  const { catalogState, catalogDispatch } = useCatalog()
  const { catalogs } = catalogState
  const moment = extendMoment(Moment)
  const {
    state: {
      activeStructure: { structure_id },
    },
  } = useStructure()

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  let validationSchema = yup.object({
    start: yup
      .date()
      .required(formatMessage({ id: 'catalogStartDateRequiredError' }))
      .min(
        yup.ref('minStartDate'),
        formatMessage({ id: 'minCatalogStartDateError' })
      ),
    end: yup
      .date()
      .required(formatMessage({ id: 'catalogEndDateRequiredError' }))
      .min(yup.ref('start'), formatMessage({ id: 'minCatalogEndDateError' })),
  })

  const formik = useFormik({
    initialValues: {
      start: '',
      end: '',
      minStartDate: new Date(), // set it to now
    },
    validationSchema,
    onSubmit: (values, { resetForm }) => {
      setSubmitLoading(true)
      let range = moment.range(values.start, values.end)
      let overlapedCatalog = catalogs.find((catalog) => {
        let range2 = moment.range(catalog.starts_at, catalog.ends_at)
        return range.overlaps(range2)
      })
      if (!overlapedCatalog) {
        createCatalog({
          structure_id,
          ends_at: values.end,
          starts_at: values.start,
        })
          .then((catalog) => {
            catalogDispatch({ type: 'CREATE_CATALOG', payload: catalog })
            resetForm()
            setSubmitLoading(false)
            closeDialog()
          })
          .catch((error) => {
            handleSessionExpiredError(
              personDispatch,
              location.pathname,
              error,
              formatMessage,
              navigate
            )
            setSubmitLoading(false)
          })
      } else {
        setSubmitLoading(false)
        notifyError(formatMessage({ id: 'createCatalogOverlapMessage' }))
      }
    },
  })

  let handleCloseDialog = () => {
    formik.resetForm()
    closeDialog()
  }

  return (
    <Dialog open={isDialogOpen}>
      <DialogTitle id='create_edit_dialog'>
        {formatMessage({ id: 'createCatalogDialogHeader' })}
      </DialogTitle>
      <DialogContent>
        <form onSubmit={formik.handleSubmit} className={classes.form}>
          <Box className={classes.inputHolder}>
            <TextField
              color='secondary'
              variant='outlined'
              required
              className={classes.dialogFormInput}
              id='start'
              label={formatMessage({ id: 'createCatalogStartInput' })}
              name='start'
              type='datetime-local'
              InputLabelProps={{ shrink: true }}
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.start}
              error={formik.touched.start && formik.errors.start ? true : false}
              helperText={formik.touched.start && formik.errors.start}
            />
            <TextField
              color='secondary'
              variant='outlined'
              required
              className={classes.dialogFormInput}
              id='end'
              label={formatMessage({ id: 'createCatalogEndInput' })}
              name='end'
              type='datetime-local'
              InputLabelProps={{ shrink: true }}
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.end}
              error={formik.touched.end && formik.errors.end ? true : false}
              helperText={formik.touched.end && formik.errors.end}
            />
          </Box>
        </form>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={handleCloseDialog}
          color='default'
          disabled={submitLoading}
        >
          {formatMessage({ id: 'cancelCreateButton' })}
        </Button>
        <Button
          onClick={formik.handleSubmit}
          color='primary'
          variant='contained'
          disabled={submitLoading}
        >
          {submitLoading && (
            <CircularProgress size={24} style={{ marginRight: '10px' }} />
          )}
          {formatMessage({ id: 'createCatalogButton' })}
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default injectIntl(CreateCatalogDialog)
