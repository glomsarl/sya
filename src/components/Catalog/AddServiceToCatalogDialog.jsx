import { useState, useEffect } from 'react'
import {
  Button,
  Box,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  CircularProgress,
  DialogContentText,
  DialogTitle,
  Divider,
  Typography,
  IconButton,
  Tooltip,
  FormControlLabel,
  Checkbox,
  makeStyles,
} from '@material-ui/core'
import { Autocomplete } from '@material-ui/lab'
import RemoveCircleRoundedIcon from '@material-ui/icons/RemoveCircleRounded'
import { Scrollbars } from 'react-custom-scrollbars-2'
import { injectIntl } from 'react-intl'
import { useCatalog } from '../../contexts/Catalog/Catalog.provider'
import { ClearRounded } from '@material-ui/icons'
import {
  addServicesToCatalog,
  fetchCatalogServices,
} from '../../services/catalogs.service'
import { notifyError } from '../../utils/toastMessages'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import { fetchStructureServives } from '../../services/structure.service'
import { useStructure } from '../../contexts/Structures/Structure.provider'

const useStyles = makeStyles((theme) => ({
  controlLabel: {
    '& .MuiFormControlLabel-label': {
      fontSize: '0.8rem',
      color: 'grey',
    },
  },
  itemHolder: {
    cursor: 'context-menu',
    position: 'relative',
  },
  itemSpecsHolder: {
    '& .removeItem': {
      visibility: 'hidden',
      position: 'absolute',
      right: 10,
      top: 0,
      color: 'red',
      borderRadius: 0,
      fontSize: '0.5rem',
    },

    '&:hover .removeItem': {
      visibility: 'visible',
    },
  },
  dialogHolder: {
    '& .MuiDialog-paperWidthSm': {
      width: '70vw',
    },
  },
}))

function AddServiceToCatalogDialog({ intl, isDialogOpen, closeDialog }) {
  let [isSubmitLoading, setIsSubmitLoading] = useState(false)
  const [selectedServices, setSelectedServices] = useState([])
  let {
    catalogState: { activeCatalog },
    catalogDispatch,
  } = useCatalog()
  const {
    state: {
      activeStructure: { structure_id },
    },
  } = useStructure()
  let { formatMessage } = intl
  let [services, setServices] = useState([])
  const classes = useStyles()

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  useEffect(() => {
    fetchCatalogServices({ catalog_id: activeCatalog.catalog_id })
      .then((services) => {
        catalogDispatch({
          type: 'LOAD_ACTIVE_CATALOG_SERVICES',
          payload: services.catalog_services,
        })
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
    // eslint-disable-next-line
  }, [isDialogOpen])

  useEffect(() => {
    fetchStructureServives(structure_id)
      .then((services) => {
        setServices(services)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })

    // eslint-disable-next-line
  }, [])

  const handleClose = () => {
    setServices([...services, ...selectedServices])
    setSelectedServices([])
    closeDialog()
  }

  let handleAutocompleteSelect = (chosenService) => {
    if (chosenService !== null) {
      let selected = selectedServices.find(
        (service) => service.service_id !== chosenService.service_id
      )
      if (selected !== null) {
        setSelectedServices([...selectedServices, chosenService])
        let newServices = services.filter(
          (service) => service.service_id !== chosenService.service_id
        )
        setServices(newServices)
      } else notifyError('service already added')
    }
  }

  let removeServiceFromSelected = (service_id) => {
    let theService = selectedServices.find(
      (service) => service.service_id === service_id
    )
    let newSelectedServices = selectedServices.filter(
      (service) => service.service_id !== service_id
    )
    setSelectedServices(newSelectedServices)
    setServices([...services, theService])
  }

  let handleAddServices = () => {
    setIsSubmitLoading(true)
    if (
      activeCatalog.catalog_id !== undefined &&
      activeCatalog.services !== undefined
    ) {
      let servicesToAdd = []
      let servicesAlreadyInPromotion = []
      selectedServices.forEach((service) => {
        let theService = activeCatalog.services.find(
          (aService) => aService.service_id === service.service_id
        )
        if (theService === undefined) servicesToAdd.push(service)
        else servicesAlreadyInPromotion.push(service.service_name)
      })
      addServicesToCatalog({
        services_to_add: servicesToAdd,
        catalog_id: activeCatalog.catalog_id,
      })
        .then(() => {
          setIsSubmitLoading(false)
          handleClose()
          if (servicesAlreadyInPromotion.length > 0)
            notifyError(
              `${formatMessage({
                id: 'addServiceToPromotionExistErrorPartOne',
              })}\n${servicesAlreadyInPromotion.join(`,\n`)}\n${formatMessage({
                id: 'addServiceToPromotionExistErrorPartTwo',
              })}`
            )
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
          setIsSubmitLoading(false)
        })
    } else notifyError('No Catalog Selected')
  }

  let handleServiceProperties = (event, service_id) => {
    const inputId = event.target.id
    let newSelectedServices = selectedServices.map((service) => {
      if (service.service_id === service_id) {
        return {
          ...service,
          [inputId]:
            inputId === 'quantity' ? event.target.value : !service[inputId],
        }
      }
      return service
    })
    setSelectedServices(newSelectedServices)
  }

  let DisplayChoosenServices = () => {
    return (
      <Box
        style={{
          border: '1px solid grey',
          padding: ' 0 15px',
          marginTop: '10px',
          borderRadius: '10px',
        }}
      >
        <Scrollbars style={{ height: '50vh' }}>
          {selectedServices.length === 0 ? (
            <Typography style={{ cursor: 'context-menu' }}>
              No Service has been selected
            </Typography>
          ) : (
            selectedServices.map((service) => (
              <Box key={service.service_id} className={classes.itemHolder}>
                <Typography
                  component='span'
                  style={{ fontWeight: 600, fontSize: '1' }}
                >
                  {service.service_name}
                </Typography>
                {isSubmitLoading ? (
                  <IconButton size='small' color='secondary' disabled>
                    <RemoveCircleRoundedIcon />
                  </IconButton>
                ) : (
                  <Box className={classes.itemSpecsHolder}>
                    <Box style={{ display: 'block' }}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={service.is_reservable}
                            onChange={(event) =>
                              handleServiceProperties(event, service.service_id)
                            }
                            name='checkedB'
                            color='secondary'
                            id='is_reservable'
                            size='small'
                          />
                        }
                        className={classes.controlLabel}
                        label={formatMessage({ id: 'reserveRadio' })}
                      />
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={service.is_take_away}
                            onChange={(event) =>
                              handleServiceProperties(event, service.service_id)
                            }
                            name='checkedB'
                            color='secondary'
                            id='is_take_away'
                            size='small'
                          />
                        }
                        className={classes.controlLabel}
                        label={formatMessage({ id: 'takeAwayRadio' })}
                      />
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={service.is_home_delivery}
                            onChange={(event) =>
                              handleServiceProperties(event, service.service_id)
                            }
                            name='checkedB'
                            color='secondary'
                            id='is_home_delivery'
                            size='small'
                          />
                        }
                        className={classes.controlLabel}
                        label={formatMessage({ id: 'deliveryRadio' })}
                      />
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={service.is_payable}
                            onChange={(event) =>
                              handleServiceProperties(event, service.service_id)
                            }
                            name='checkedB'
                            color='secondary'
                            id='is_payable'
                            size='small'
                          />
                        }
                        className={classes.controlLabel}
                        label={formatMessage({ id: 'payable' })}
                      />
                      <TextField
                        name='quantity'
                        id='quantity'
                        label={formatMessage({ id: 'quantity' })}
                        variant='filled'
                        type='number'
                        size='small'
                        value={service.quantity ?? 0}
                        className={classes.controlLabel}
                        onChange={(event) =>
                          handleServiceProperties(event, service.service_id)
                        }
                      />
                    </Box>
                    <Tooltip arrow title='Remove service from list'>
                      <IconButton
                        size='small'
                        className='removeItem'
                        onClick={() =>
                          removeServiceFromSelected(service.service_id)
                        }
                      >
                        <ClearRounded />
                      </IconButton>
                    </Tooltip>
                  </Box>
                )}
                <Divider style={{ marginBottom: '1.2rem' }} />
              </Box>
            ))
          )}
        </Scrollbars>
      </Box>
    )
  }

  return (
    <Dialog
      open={isDialogOpen}
      onClose={isSubmitLoading ? null : handleClose}
      aria-labelledby='form-dialog-title'
      className={classes.dialogHolder}
    >
      <DialogTitle id='form-dialog-title'>Add Services to Catalog</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Select services to be added to your catalog
        </DialogContentText>
        <Box>
          <Autocomplete
            id='services-auto-complete'
            options={services}
            getOptionLabel={(option) => option.service_name}
            style={{ width: '100%' }}
            clearText='Clear'
            openText='List Services'
            closeText='Hide Services'
            renderInput={(params) => (
              <TextField
                {...params}
                label='Select a service'
                variant='outlined'
              />
            )}
            onChange={(e, chosenService) =>
              handleAutocompleteSelect(chosenService)
            }
            autoHighlight
          />
          <DisplayChoosenServices />
        </Box>
      </DialogContent>
      <DialogActions>
        <Button
          disabled={isSubmitLoading}
          onClick={handleClose}
          color='primary'
        >
          Cancel
        </Button>
        <Button
          disabled={isSubmitLoading || selectedServices.length === 0}
          onClick={
            selectedServices.length < 1 || isSubmitLoading
              ? null
              : handleAddServices
          }
          color='primary'
          variant='contained'
        >
          {isSubmitLoading && (
            <CircularProgress size='24px' style={{ marginRight: '10px' }} />
          )}
          {selectedServices.length > 1 ? 'Add Services' : 'Add Service'}
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default injectIntl(AddServiceToCatalogDialog)
