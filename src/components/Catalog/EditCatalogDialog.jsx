import {
  Button,
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  makeStyles,
  DialogTitle,
  TextField,
  Typography,
  CircularProgress,
} from '@material-ui/core'
import { useFormik } from 'formik'
import { useState } from 'react'
import { injectIntl } from 'react-intl'
import * as yup from 'yup'
import { useCatalog } from '../../contexts/Catalog/Catalog.provider'
import Moment from 'moment'
import { extendMoment } from 'moment-range'
import { notifyError, notifyInfo } from '../../utils/toastMessages'

const useStyles = makeStyles((theme) => ({
  inputHolder: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '10px',
    [theme.breakpoints.down('xs')]: {
      gridAutoFlow: 'row',
      gridGap: 0,
    },
  },
  descriptionLabel: {
    fontWeight: 600,
    paddingTop: '20px',
    textAlign: 'left',
  },
  imageLabel: {
    display: 'block',
    width: 'fit-content',
    padding: '6px 16px',
    marginTop: '10px',
    marginBottom: '12px',
    backgroundColor: theme.palette.primary.light,
    color: 'rgba(0, 0, 0, 0.87)',
    borderRadius: '6px',
    boxShadow:
      '0px 3px 1px -2px rgb(0 0 0 / 20%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 1px 5px 0px rgb(0 0 0 / 12%)',
    transition:
      'background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,border 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
    fontSize: '0.875rem',
    fontWeight: 500,
    lineHeight: 1.75,
    textTransform: 'uppercase',
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
  },
  form: {
    textAlign: 'center',
  },
  image: {
    width: '200px',
  },
  dialogFormInput: {
    marginTop: '15px',
  },
}))

function EditCatalogDialog({
  intl,
  open,
  closeDialog,
  start,
  end,
  catalog_id,
  confirmEdit,
}) {
  const { formatMessage } = intl
  const classes = useStyles()
  let [submitLoading, setSubmitLoading] = useState(false)
  let { catalogState } = useCatalog()
  let { catalogs } = catalogState
  const moment = extendMoment(Moment)

  let validationSchema = yup.object({
    start: yup
      .date()
      .required(formatMessage({ id: 'catalogStartDateRequiredError' }))
      .min(
        yup.ref('maxStartDate'),
        formatMessage({ id: 'minCatalogStartDateError' }),
      ),
    end: yup
      .date()
      .required(formatMessage({ id: 'catalogEndDateRequiredError' }))
      .min(yup.ref('start'), formatMessage({ id: 'minCatalogStartDateError' })),
  })

  const validationSchema2 = yup.object({
    end: yup
      .date()
      .required(formatMessage({ id: 'catalogEndDateRequiredError' }))
      .min(
        yup.ref('minEndDate'),
        formatMessage({ id: 'minCatalogEndDateError' }),
      ),
  })

  const formik2 = useFormik({
    initialValues: {
      end: end,
      minEndDate: new Date(), // set it to the date of tomorrow
    },
    validationSchema2,
    onSubmit: (values, { resetForm }) => {
      setSubmitLoading(true)
      let range = moment.range(values.start, values.end)
      let overlapedCatalog = catalogs.find((catalog) => {
        let range2 = moment.range(catalog.start, catalog.end)
        return range.overlaps(range2) && catalog.catalog_id !== catalog_id
      })
      let isCatalogOverlap = overlapedCatalog !== undefined
      if (!isCatalogOverlap) {
        delete values.minEndDate
        confirmEdit({ ...values })
        resetForm()
        closeDialog()
      } else {
        notifyError(formatMessage({ id: 'editCatalogOverlapMessage' }))
      }
      setSubmitLoading(false)
    },
  })

  const formik = useFormik({
    initialValues: {
      start: start,
      end: end,
      maxStartDate: new Date(), // set it to the date of tomorrow
    },
    validationSchema,
    onSubmit: (values, { resetForm }) => {
      if (values.start === start && values.end === end) notifyInfo('nothing changed')
      else {
        setSubmitLoading(true)
        let range = moment.range(values.start, values.end)
        let overlapedCatalog = catalogs.find((catalog) => {
          let range2 = moment.range(catalog.start, catalog.end)
          return range.overlaps(range2) && catalog.catalog_id !== catalog_id
        })
        let isCatalogOverlap = overlapedCatalog !== undefined
        if (!isCatalogOverlap) {
          delete values['maxStartDate']
          confirmEdit({ ...values })
          resetForm()
          closeDialog()
        } else {
          notifyError(formatMessage({ id: 'editCatalogOverlapMessage' }))
        }
        setSubmitLoading(false)
      }
    },
  })

  /**
   * Initialize the formik form values.
   * given that the initial values in the useFormik declaration
   * already render when the page is openned, it prevents it
   * the initial values from getting updated data
   * this will execute every time the dailog opens
   * hence setting the initial values.
   */
  formik.initialValues.start = start
  formik.initialValues.end = end
  formik2.initialValues.end = end

  let handleCloseDialog = () => {
    closeDialog()
    formik.resetForm()
    formik2.resetForm()
  }

  return (
    <Dialog open={open} onClose={submitLoading ? null : handleCloseDialog}>
      <DialogTitle id="create_edit_dialog">
        {formatMessage({ id: 'editCatalogDialogHeader' })}
      </DialogTitle>
      <DialogContent>
        {submitLoading ? (
          <Typography>{formatMessage({ id: 'editCatalogMessage' })}</Typography>
        ) : new Date(start) < new Date() ? (
          <form onSubmit={formik2.handleSubmit} className={classes.form}>
            <TextField
              color="secondary"
              variant="outlined"
              required
              id="end"
              label={formatMessage({ id: 'createCatalogEndInput' })}
              name="end"
              fullWidth
              type="datetime-local"
              InputLabelProps={{ shrink: true }}
              onBlur={formik2.handleBlur}
              onChange={formik2.handleChange}
              value={formik2.values.end}
              error={formik2.touched.end && formik2.errors.end ? true : false}
              helperText={formik2.errors.end}
            />
          </form>
        ) : (
          <form onSubmit={formik.handleSubmit} className={classes.form}>
            <Box className={classes.inputHolder}>
              <TextField
                color="secondary"
                variant="outlined"
                required
                className={classes.dialogFormInput}
                id="start"
                label={formatMessage({ id: 'createCatalogStartInput' })}
                name="start"
                type="datetime-local"
                InputLabelProps={{ shrink: true }}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.start}
                error={
                  formik.touched.start && formik.errors.start ? true : false
                }
                helperText={formik.errors.start}
              />
              <TextField
                color="secondary"
                variant="outlined"
                required
                className={classes.dialogFormInput}
                id="end"
                label={formatMessage({ id: 'createCatalogEndInput' })}
                name="end"
                type="datetime-local"
                InputLabelProps={{ shrink: true }}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.end}
                error={formik.touched.end && formik.errors.end ? true : false}
                helperText={formik.errors.end}
              />
            </Box>
          </form>
        )}
      </DialogContent>
      <DialogActions>
        <Button
          onClick={handleCloseDialog}
          color="default"
          disabled={submitLoading}
        >
          {formatMessage({ id: 'cancelEditCatalogButton' })}
        </Button>
        <Button
          onClick={
            new Date(start) < new Date()
              ? formik2.handleSubmit
              : formik.handleSubmit
          }
          color="primary"
          variant="contained"
          disabled={
            submitLoading ||
            (formik.values.start === start && formik.values.end === end)
          }
        >
          {submitLoading && (
            <CircularProgress size={24} style={{ marginRight: '10px' }} />
          )}
          {formatMessage({ id: 'editCatalogButton' })}
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default injectIntl(EditCatalogDialog)
