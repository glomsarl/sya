import {
  Box,
  Grid,
  Fab,
  Tooltip,
  makeStyles,
  Typography,
} from '@material-ui/core'
import { useState, useEffect } from 'react'
import { useCatalog } from '../../contexts/Catalog/Catalog.provider'
import StyledCatalog from './StyledCatalog'
import AddRoundedIcon from '@material-ui/icons/AddRounded'
import CatalogSkeleton from './CatalogSkeleton'
import { injectIntl } from 'react-intl'
import CreateCatalogDialog from './CreateCatalogDialog'
import CatalogDetails from './CatalogDetails'
import { getAllCatalogs } from '../../services/catalogs.service'
import { useStructure } from '../../contexts/Structures/Structure.provider'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { useNavigate, useLocation } from 'react-router'
import { usePerson } from '../../contexts/PersonContext/Person.provider'

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    height: '100%',
    width: '100%',
    padding: 10,
    boxSizing: 'border-box',
  },
  alternateRoot: {
    position: 'relative',
    height: '100%',
    width: '100%',
    display: 'grid',
    justifyContent: 'center',
    alignContent: 'center',
    padding: 10,
  },
  fab: {
    position: 'absolute',
    right: theme.spacing(2),
    bottom: theme.spacing(2),
  },
  noPromotionsText: {
    fontWeight: 100,
    color: theme.palette.secondary.light,
    textAlign: 'center',
  },
}))

function Catalogs({ intl }) {
  const { catalogState, catalogDispatch } = useCatalog()
  const { formatMessage } = intl
  const { catalogs, activeCatalog } = catalogState
  const [isCatalogDataLoading, setIsCatalogDataLoading] = useState(true)
  const [isCreateCatalogDialogOpen, setIsCreateCatalogDialogOpen] =
    useState(false)

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  const {
    state: {
      activeStructure: { structure_id },
    },
  } = useStructure()

  useEffect(() => {
    getAllCatalogs({ structure_id })
      .then((catalogs) => {
        catalogDispatch({
          type: 'LOAD_CATALOGS',
          payload: catalogs,
        })
        setIsCatalogDataLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsCatalogDataLoading(false)
      })
    // eslint-disable-next-line
  }, [])

  function closeCatalogCreationDialog() {
    setIsCreateCatalogDialogOpen(false)
  }

  function openCatalogCreationDialog() {
    setIsCreateCatalogDialogOpen(true)
  }

  const classes = useStyles()
  let styledCatalogs =
    catalogs !== undefined
      ? catalogs.map((catalog, index) => {
          return <StyledCatalog catalog={catalog} key={index} />
        })
      : null

  return (
    <Box
      className={
        catalogs !== undefined && catalogs.length === 0 && !isCatalogDataLoading
          ? classes.alternateRoot
          : classes.root
      }
    >
      {activeCatalog.catalog_id === undefined ? (
        <>
          <Grid container spacing={2}>
            {catalogs !== undefined &&
            catalogs.length === 0 &&
            !isCatalogDataLoading ? (
              <Typography className={classes.noPromotionsText}>
                {formatMessage({ id: 'noCatalogs' })}
              </Typography>
            ) : catalogs === undefined || isCatalogDataLoading ? (
              [...new Array(12)].map((index, id) => (
                <CatalogSkeleton key={id} />
              ))
            ) : (
              styledCatalogs
            )}
          </Grid>
          <CreateCatalogDialog
            isDialogOpen={isCreateCatalogDialogOpen}
            closeDialog={closeCatalogCreationDialog}
          />
          {isCatalogDataLoading ? (
            <Fab
              color='primary'
              aria-label='create catalog'
              className={classes.fab}
              disabled
              onClick={openCatalogCreationDialog}
            >
              <AddRoundedIcon />
            </Fab>
          ) : (
            <Tooltip arrow title='Create promotion' className={classes.fab}>
              <Fab
                color='primary'
                aria-label='create promotion'
                onClick={openCatalogCreationDialog}
              >
                <AddRoundedIcon />
              </Fab>
            </Tooltip>
          )}
        </>
      ) : (
        // null
        <CatalogDetails />
        // <PromotionDetails />
      )}
    </Box>
  )
}

export default injectIntl(Catalogs)
