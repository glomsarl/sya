import {
  Grid,
  Box,
  IconButton,
  makeStyles,
  Tooltip,
  Typography,
} from '@material-ui/core'
import { useState } from 'react'
import { injectIntl } from 'react-intl'
import { useCatalog } from '../../contexts/Catalog/Catalog.provider'
import { DeleteForeverRounded } from '@material-ui/icons'
import RemoveServiceFromCatalogDialog from './RemoveServiceFromCatalogDialog'
import { removeServicesFromCatalog } from '../../services/catalogs.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'

const useStyles = makeStyles((theme) => ({
  serviceRecord: {
    // alignItems: "center",
    borderBottom: '1px solid #f8f7f7',
    padding: '10px 0',
    '& #catalogServiceActions': {
      display: 'none',
    },
    '&:hover #catalogServiceActions': {
      display: 'block',
    },
    '&:hover': {
      backgroundColor: 'rgba(168, 175, 213, 0.2)',
    },
    position: 'relative',
  },
  serviceDataNumber: {
    paddingLeft: '10px',
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  serviceData: {
    display: 'inline',
  },
  bringOnExtraSmall: {
    display: 'none',
    fontWeight: 600,
    [theme.breakpoints.down('xs')]: {
      display: 'inline',
      paddingLeft: '10px',
    },
  },
  propertiesHolder: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  simpleTags: {
    width: 'fit-content',
    fontSize: ' 0.75rem',
    margin: '2px',
    padding: '0 5px',
    borderRadius: '10px',
    backgroundColor: 'grey',
  },
  payableTag: {
    width: 'fit-content',
    fontSize: ' 0.75rem',
    margin: '2px',
    padding: '0 5px',
    borderRadius: '10px',
    color: 'white',
    backgroundColor: 'green',
  },
  reductionTag: {
    width: 'fit-content',
    fontSize: ' 0.75rem',
    margin: '2px',
    padding: '0 5px',
    borderRadius: '10px',
    color: 'white',
    backgroundColor: 'red',
  },
  bonusPointTag: {
    width: 'fit-content',
    fontSize: ' 0.75rem',
    margin: '2px',
    padding: '0 5px',
    borderRadius: '10px',
    backgroundColor: 'yellow',
  },
  catalogServiceActions: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    backgroundColor: theme.palette.grey[300],
    borderTopLeftRadius: '20px',
    paddingLeft: '5px',
  },
}))

function StyledPromotionService({ intl, service, number }) {
  const classes = useStyles()
  const { formatMessage } = intl
  let [isRemoveDialogOpen, setIsRemoveDialogOpen] = useState(false)
  let [isRemoving, setIsRemoving] = useState(false)
  const {
    catalogState: {
      activeCatalog: { catalog_id },
    },
    catalogDispatch,
  } = useCatalog()
  const {
    service_name,
    unit_price,
    is_reservable,
    is_take_away,
    is_home_delivery,
    is_payable,
    promotions,
    //   cancellation_delay,
    quantity,
    service_id,
  } = service

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  let confirmRemove = () => {
    setIsRemoving(true)
    removeServicesFromCatalog({
      catalog_id,
      remove_services: [service_id],
    })
      .then(() => {
        catalogDispatch({
          type: 'REMOVE_SERVICE_FROM_CATALOG',
          payload: service_id,
        })
        setIsRemoving(false)
        closeDialog()
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
  }

  /**
   * The following lines of code (11 lines) intend to bring out
   * only the biggest amongst the list of promotions of the same type.
   * if there are many reductions (30, 20, 50), then it will take only the biggest (50)
   */
  let sanitizedPromotions = []
  promotions.forEach((promo) => {
    let response = sanitizedPromotions.find(
      (sanitizedPromo) => sanitizedPromo.promotion_type === promo.promotion_type
    )
    if (response !== undefined) {
      if (response.value < promo.value) {
        let newPromos = sanitizedPromotions.filter(
          (newPromo) => newPromo.promotion_type !== promo.promotion_type
        )
        newPromos.push(promo)
        sanitizedPromotions = newPromos
      }
    } else sanitizedPromotions.push(promo)
  })

  let closeDialog = () => {
    setIsRemoveDialogOpen(false)
  }

  let openDialog = () => {
    setIsRemoveDialogOpen(true)
  }

  return (
    <>
      <RemoveServiceFromCatalogDialog
        isRemoveDialogOpen={isRemoveDialogOpen}
        confirmRemove={confirmRemove}
        closeDialog={closeDialog}
        isRemoving={isRemoving}
      />
      <Grid container className={classes.serviceRecord}>
        <Grid item xs={12} sm={1} className={classes.serviceDataNumber}>
          <Typography id='serviceData1' noWrap className={classes.serviceData}>
            {number + 1}
          </Typography>
        </Grid>

        <Grid item xs={12} sm={4}>
          <Typography className={classes.bringOnExtraSmall}>
            {formatMessage({ id: 'servicesHeader' })}
          </Typography>
          <Typography id='serviceData2' noWrap className={classes.serviceData}>
            {service_name}
          </Typography>
        </Grid>

        <Grid item xs={12} sm={1} className={classes.serviceDataOthers}>
          <Typography className={classes.bringOnExtraSmall}>
            {formatMessage({ id: 'quantity' })}
          </Typography>
          <Typography id='serviceData3' noWrap className={classes.serviceData}>
            {quantity}
          </Typography>
        </Grid>

        <Grid item xs={12} sm={2} className={classes.serviceDataOthers}>
          <Typography className={classes.bringOnExtraSmall}>
            {formatMessage({ id: 'unitAmount' })}
          </Typography>
          <Typography id='serviceData4' noWrap className={classes.serviceData}>
            {`${unit_price} FCFA`}
          </Typography>
        </Grid>

        <Grid item xs={12} sm={4} className={classes.serviceDataOthers}>
          <Typography className={classes.bringOnExtraSmall}>
            {formatMessage({ id: 'properties' })}
          </Typography>
          <Box className={classes.propertiesHolder}>
            {is_reservable ? (
              <Box className={classes.simpleTags}>
                {formatMessage({ id: 'reserveRadio' })}
              </Box>
            ) : null}
            {is_take_away ? (
              <Box className={classes.simpleTags}>
                {formatMessage({ id: 'takeAwayRadio' })}
              </Box>
            ) : null}
            {is_home_delivery ? (
              <Box className={classes.simpleTags}>
                {formatMessage({ id: 'deliveryRadio' })}
              </Box>
            ) : null}
            {is_payable ? (
              <Box className={classes.payableTag}>
                {formatMessage({ id: `Payant` })}
              </Box>
            ) : (
              <Box className={classes.payableTag}>
                {formatMessage({ id: `nonPayant` })}
              </Box>
            )}
            {promotions !== undefined
              ? sanitizedPromotions.map((promo) => {
                  return promo.promotion_type === 'reduction' &&
                    promo.value !== null ? (
                    <Box
                      className={classes.reductionTag}
                    >{`-${promo.value}%`}</Box>
                  ) : promo.promotion_type === 'bonus' &&
                    promo.value !== null ? (
                    <Box
                      className={classes.bonusPointTag}
                    >{`${promo.value} SY`}</Box>
                  ) : null
                })
              : null}
          </Box>
        </Grid>

        <Box
          className={classes.catalogServiceActions}
          id='catalogServiceActions'
        >
          <Tooltip arrow title={formatMessage({ id: 'revomeCatalog' })}>
            <IconButton color='secondary' onClick={openDialog} size='small'>
              <DeleteForeverRounded />
            </IconButton>
          </Tooltip>
        </Box>
      </Grid>
    </>
  )
}

export default injectIntl(StyledPromotionService)
