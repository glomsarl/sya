import { Box, Grid, makeStyles } from "@material-ui/core";
import { Skeleton } from '@material-ui/lab'

const useStyles = makeStyles((theme) => ({
  root: {
    display: "grid",
    padding: "8px",
    borderRadius: "5px",
    boxShadow:
      "0px 2px 4px -1px rgb(0 0 0 / 8%), 0px 4px 5px 0px rgb(0 0 0 / 8%), 0px 1px 10px 0px rgb(0 0 0 / 8%)",
  },
  actions: {
    width: "fit-content",
    justifySelf: "end",
    display: "grid",
    gridAutoFlow: "column",
    gridGap: "5px",
  },
}));
export default function CatalogSkeleton() {
  const classes = useStyles();
  return (
    <Grid item xs={12} sm={6} md={4} lg={3} xl={2}>
      <Box className={classes.root}>
        <Skeleton variant="text" width="40%" />
        <Skeleton variant="text" width="90%" />
        <Skeleton variant="rect" width="70%" height={35} />
        <Box className={classes.actions}>
          <Skeleton variant="circle" width={40} height={40} />
          <Skeleton variant="circle" width={40} height={40} />
          <Skeleton variant="circle" width={40} height={40} />
        </Box>
      </Box>
    </Grid>
  );
}
