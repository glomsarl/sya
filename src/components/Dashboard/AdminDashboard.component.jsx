/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react'
import AdminDashboardCard from './AdminDashboardCard.component'
import { injectIntl } from 'react-intl'
import { makeStyles } from '@material-ui/core/styles'
import { Scrollbars } from 'react-custom-scrollbars-2'
import { alpha, MenuItem, TextField, Typography } from '@material-ui/core'
import Skeleton from '@material-ui/lab/Skeleton'
import Chart from 'chart.js/auto'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import randomNumber from '../../utils/randomNumber'
import {
  getAdminOverviews,
  getClientStatistics,
  getFinanceStatistics,
  getOwnerOverviews,
  getSaleStatistics,
} from '../../services/statistic.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useStructure } from '../../contexts/Structures/Structure.provider'

const useStyles = makeStyles((theme) => ({
  scrollbar: {
    height: 230,
    width: '100%',
    display: 'grid',
    gridAutoFlow: 'column',
    alignItems: 'center',
  },
  graph: {
    display: 'grid',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  textField: {
    margin: theme.spacing(1, 2),
    '& .MuiInputBase-input.MuiOutlinedInput-input': {
      padding: theme.spacing(1, 4),
      backgroundColor: alpha('#B1B1B1', 0.21),
      fontSize: '0.938rem',
      fontWeight: 500,
      letterSpacing: 1.23,
      borderRadius: '20px',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderRadius: '20px',
      },
    },
    '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
      color: theme.palette.primary.main,
    },
  },
  selectTextField: {
    margin: theme.spacing(1, 2),
    '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
      color: theme.palette.primary.main,
    },
    '& .MuiInputBase-root': {
      minWidth: 120,
      minHeight: 34,
      paddingLeft: theme.spacing(3),
      backgroundColor: alpha('#B1B1B1', 0.21),
      fontSize: '0.938rem',
      fontWeight: 500,
      letterSpacing: 1.23,
      borderRadius: '20px',
    },
  },
  skeletonContainer: {
    display: 'grid',
    gridAutoFlow: 'column',
    alignItems: 'end',
    gap: '10px',
    margin: theme.spacing(4, 2),
    marginBottom: 0,
  },
}))

function AdminDashboard({
  intl: { formatMessage, formatDate, formatNumber },
  user,
}) {
  const classes = useStyles()

  const [statistics, setStatistics] = useState({
    number_of_catalogs: 0,
    number_of_services: 0,
    number_of_reductions: 0,
    number_of_bonuses: 0,
  })
  const [adminStats, setAdminStats] = useState({
    number_of_clients: 0,
    number_of_structures: 0,
    number_of_served_orders: 0,
    total_money_generated: 0,
  })
  const statisticsTitle = {
    number_of_catalogs: formatMessage({ id: 'catalogs' }),
    number_of_services: formatMessage({ id: 'services' }),
    number_of_reductions: formatMessage({ id: 'reductions' }),
    number_of_bonuses: formatMessage({ id: 'bonuses' }),
    number_of_clients: formatMessage({ id: 'totalAdheredClients' }),
    number_of_structures: formatMessage({ id: 'totalActiveStructures' }),
    number_of_served_orders: formatMessage({ id: 'totalServedOrders' }),
    total_money_generated: formatMessage({ id: 'totalMoneyGenerated' }),
  }
  const statsValues = Object.keys(user === 'owner' ? statistics : adminStats)

  const [salesGraphData, setSalesGraphData] = useState([
    { date: '', quantity: 0 },
  ])
  const [financeGraphData, setFinanceGraphData] = useState([
    { date: '', quantity: 0 },
  ])
  const [clientGraphData, setClientGraphData] = useState([
    { date: '', quantity: 0 },
  ])
  const [isGraphDataLoading, setIsGraphDataLoading] = useState(false)

  const config = (data) => {
    return {
      type: 'line',
      data,
      options: {
        maintainAspectRatio: false,
        tension: 0.4,
        scales: {
          y: {
            stacked: true,
            grid: {
              display: true,
              color: 'rgba(255,99,132,0.2)',
            },
          },
          x: {
            grid: {
              display: false,
            },
          },
        },
        parsing: {
          xAxisKey: 'date',
          yAxisKey: 'quantity',
        },
      },
    }
  }

  const updateGraph = () => {
    const dataChart = Chart.getChart('dataChart')
    if (dataChart !== undefined) dataChart.destroy()

    const secondGraphData =
      user === 'owner' ? financeGraphData : clientGraphData
    new Chart(
      document.getElementById('dataChart'),
      config({
        datasets: [
          {
            label:
              user === 'owner'
                ? formatMessage({ id: 'financeGraphHeader' })
                : formatMessage({ id: 'clientsGraphHeader' }),
            data: secondGraphData.map((data) => ({
              ...data,
              date: formatDate(new Date(data.date), {
                year: 'numeric',
                month: 'long',
                day: '2-digit',
              }),
            })),
            borderColor: 'rgba(255,99,132,1)',
            backgroundColor: 'rgba(255,99,132,0.2)',
            fill: true,
            order: 2,
          },
          {
            label: formatMessage({ id: 'salesGraphHeader' }),
            data: salesGraphData.map((data) => ({
              ...data,
              date: formatDate(new Date(data.date), {
                year: 'numeric',
                month: 'long',
                day: '2-digit',
              }),
              order: 1,
            })),
            borderColor: '#0099FF',
            backgroundColor: '#00990052',
            fill: true,
          },
        ],
      })
    )
  }
  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  const {
    state: {
      activeStructure: { structure_id },
    },
  } = useStructure()

  const financialStateUpdate = (updateObject, salesStatistics) => {
    getFinanceStatistics(updateObject)
      .then((finances) => {
        setFinanceGraphData(finances)
        setSalesGraphData(salesStatistics)
        setIsGraphDataLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
  }

  const clientStateUpdate = (updateObject, salesStatistics) => {
    getClientStatistics(updateObject)
      .then((clientData) => {
        setSalesGraphData(salesStatistics)
        setClientGraphData(clientData)
        setIsGraphDataLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
  }

  useEffect(() => {
    setIsGraphDataLoading(true)
    getSaleStatistics({
      structure_id,
      ...formik.values,
    })
      .then((salesStatistics) => {
        if (user === 'owner') {
          getOwnerOverviews(structure_id)
            .then((overviews) => {
              setStatistics({ ...statistics, ...overviews })
            })
            .catch((error) => {
              handleSessionExpiredError(
                personDispatch,
                location.pathname,
                error,
                formatMessage,
                navigate
              )
            })
          financialStateUpdate(
            {
              ...formik.values,
              structure_id,
            },
            salesStatistics
          )
        } else {
          getAdminOverviews(structure_id)
            .then((overviews) => {
              setAdminStats({ ...adminStats, ...overviews })
            })
            .catch((error) => {
              handleSessionExpiredError(
                personDispatch,
                location.pathname,
                error,
                formatMessage,
                navigate
              )
            })
          clientStateUpdate({ ...formik.values }, salesStatistics)
        }
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })

    // eslint-disable-next-line
  }, [])

  useEffect(() => {
    updateGraph()
    // eslint-disable-next-line
  }, [salesGraphData, financeGraphData, clientGraphData])

  const getTodaysDate = () => {
    const date = new Date()
    let dd = date.getDate()
    let mm = date.getMonth() + 1 //January is 0!

    let yyyy = date.getFullYear()
    if (dd < 10) {
      dd = `0${dd}`
    }
    if (mm < 10) {
      mm = `0${mm}`
    }
    const today = `${yyyy}-${mm}-${dd}`

    return today
  }

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      starts_at:
        Array.isArray(salesGraphData) && salesGraphData.length > 0
          ? salesGraphData[0].date
          : null,
      ends_at:
        Array.isArray(salesGraphData) && salesGraphData.length > 0
          ? salesGraphData[salesGraphData.length - 1].date
          : null,
      interval: 1,
    },
    validationSchema: Yup.object({
      starts_at: Yup.date(),
      ends_at: Yup.date(),
      interval: Yup.string(),
    }),
  })

  const intervalList = [
    { interval_name: 'daily', interval_duration: 1 },
    { interval_name: 'weekly', interval_duration: 7 },
    { interval_name: 'monthly', interval_duration: 30 },
    { interval_name: 'yearly', interval_duration: 365 },
  ]

  const handleChangeFilter = (value) => {
    setIsGraphDataLoading(true)
    getSaleStatistics({
      structure_id,
      ...formik.values,
      ...value,
    })
      .then((salesStatistics) => {
        if (user === 'owner') {
          financialStateUpdate(
            {
              ...formik.values,
              ...value,
              structure_id,
            },
            salesStatistics
          )
        } else {
          clientStateUpdate({
            ...formik.values,
            ...value,
          }, salesStatistics)
        }
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
  }

  return (
    <div
      style={{ display: 'grid', gridTemplateRows: 'auto 1fr', height: '100%' }}
    >
      <Scrollbars
        style={{
          height: 230,
          width: '100%',
        }}
      >
        <div className={classes.scrollbar}>
          {statsValues.map((title, index) => {
            const value =
              user === 'owner' ? statistics[title] : adminStats[title]
            return (
              <AdminDashboardCard
                key={index}
                title={statisticsTitle[title]}
                value={
                  user === 'owner' || title !== 'total_money_generated'
                    ? value
                    : formatNumber(value, {
                        style: 'currency',
                        currency: 'XAF',
                      })
                }
              />
            )
          })}
        </div>
      </Scrollbars>

      <div
        style={{
          height: '100%',
          display: 'grid',
          gridTemplateRows: 'auto 1fr',
        }}
      >
        <div
          style={{
            gridAutoFlow: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            display: 'grid',
          }}
        >
          <TextField
            type='date'
            variant='outlined'
            className={classes.textField}
            InputProps={{
              inputProps: {
                max: getTodaysDate(),
              },
            }}
            {...formik.getFieldProps('starts_at')}
            onChange={(event) => {
              formik.handleChange(event)
              handleChangeFilter({ starts_at: event.target.value })
            }}
            disabled={isGraphDataLoading}
          />
          <Typography>{formatMessage({ id: 'to' })}</Typography>
          <TextField
            type='date'
            variant='outlined'
            className={classes.textField}
            InputProps={{
              inputProps: {
                max: getTodaysDate(),
              },
            }}
            {...formik.getFieldProps('ends_at')}
            onChange={(event) => {
              formik.handleChange(event)
              handleChangeFilter({ ends_at: event.target.value })
            }}
            disabled={isGraphDataLoading}
          />
          <TextField
            select
            variant='outlined'
            className={classes.selectTextField}
            {...formik.getFieldProps('interval')}
            onChange={(event) => {
              formik.handleChange(event)
              handleChangeFilter({ interval: event.target.value })
            }}
            disabled={isGraphDataLoading}
          >
            {intervalList.map(({ interval_name, interval_duration }) => (
              <MenuItem key={interval_duration} value={interval_duration}>
                {interval_name}
              </MenuItem>
            ))}
          </TextField>
        </div>
        <div
          style={{
            height: '100%',
            width: '100%',
            display: isGraphDataLoading ? 'none' : 'grid',
          }}
        >
          <canvas id='dataChart'></canvas>
        </div>
        {isGraphDataLoading && (
          <div className={classes.skeletonContainer}>
            {[...new Array(9)].map((_, index) => (
              <Skeleton
                key={index}
                variant='rect'
                animation='wave'
                height={`${randomNumber(0, 100)}%`}
              />
            ))}
          </div>
        )}
      </div>
    </div>
  )
}

export default injectIntl(AdminDashboard)
