import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Grid,
  Card,
  CardActionArea,
  CardContent,
  Typography,
  Tooltip,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 150,
    maxWidth: 450,
    borderRadius: 10,
    flexShrink: 0,
    height: "165px",
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  title: {
    fontSize: "1.125rem",
    fontWeight: 500,
    fontStyle: "normal",
    letterSpacing: "0.1rem",
  },
  value: {
    fontSize: "2.5rem",
    fontWeight: 500,
    fontStyle: "normal",
    letterSpacing: 1.23,
    color: theme.common.black,
  },
  content: {
    paddingBottom: theme.spacing(1),
  },
}));

function AdminDashboardCard({ title, value }) {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardContent className={classes.content}>
          <Grid
            container
            direction="column"
            justifyContent="center"
            alignItems="center"
          >
            <Grid item style={{ width: "100%" }}>
              <Tooltip arrow title={title}>
                <Typography
                  gutterBottom
                  style={{
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                    textAlign: "center",
                  }}
                  className={classes.title}
                >
                  {title}
                </Typography>
              </Tooltip>
            </Grid>
            <Grid item style={{ width: "100%" }}>
              <Tooltip arrow title={value}>
                <Typography
                  className={classes.value}
                  style={{
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                    textAlign: "center",
                  }}
                >
                  {value}
                </Typography>
              </Tooltip>
            </Grid>
          </Grid>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

export default AdminDashboardCard;
