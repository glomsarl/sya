import {
  Box,
  Grid,
  Fab,
  Tooltip,
  makeStyles,
  Typography,
} from '@material-ui/core'
import { useState, useEffect } from 'react'
import { usePromotion } from '../../contexts/Promotion/Promotion.provider'
import StyledPromotion from './StyledPromotion'
import AddRoundedIcon from '@material-ui/icons/AddRounded'
import PromotionDetails from './PromotionDetails'
import CreatePromotionDialog from './CreatePromotionDialog'
import PromotionsSkeleton from './PromotionsSkeleton'
import { injectIntl } from 'react-intl'
import { fetchAllPromotions } from '../../services/promotions.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import { useStructure } from '../../contexts/Structures/Structure.provider'

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    height: '100%',
    width: '100%',
    padding: 10,
    boxSizing: 'border-box',
  },
  alternateRoot: {
    position: 'relative',
    height: '100%',
    width: '100%',
    display: 'grid',
    justifyContent: 'center',
    alignContent: 'center',
    padding: 10,
    boxSizing: 'border-box',
  },
  fab: {
    position: 'absolute',
    right: theme.spacing(2),
    bottom: theme.spacing(2),
  },
  noPromotionsText: {
    fontWeight: 100,
    color: theme.palette.secondary.light,
    textAlign: 'center',
  },
}))

function Promotions({ intl: { formatMessage } }) {
  const {
    promotionState: { promotions, activePromotion },
    promotionDispatch,
  } = usePromotion()
  const [isPromotionDataLoading, setIsPromotionDataLoading] = useState(true)
  const [isCreatePromotionDialogOpen, setIsCreatePromotionDialogOpen] =
    useState(false)

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()
  const {
    state: {
      activeStructure: { structure_id },
    },
  } = useStructure()

  useEffect(() => {
    fetchAllPromotions(structure_id)
      .then((promotions) => {
        promotionDispatch({
          type: 'LOAD_PROMOTIONS',
          payload: promotions,
        })
        setIsPromotionDataLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsPromotionDataLoading(false)
      })
    // eslint-disable-next-line
  }, [])

  function closePromotionCreationDialog() {
    setIsCreatePromotionDialogOpen(false)
  }

  function openPromotionCreationDialog() {
    setIsCreatePromotionDialogOpen(true)
  }

  const classes = useStyles()
  let styledPromotions =
    promotions !== undefined
      ? promotions.map((promotion, index) => {
          return <StyledPromotion promotion={promotion} key={index} />
        })
      : null

  return (
    <Box
      className={
        promotions !== undefined &&
        promotions.length === 0 &&
        !isPromotionDataLoading
          ? classes.alternateRoot
          : classes.root
      }
    >
      {activePromotion.promotion_id === undefined ? (
        <>
          <Grid container spacing={2}>
            {promotions !== undefined &&
            promotions.length === 0 &&
            !isPromotionDataLoading ? (
              <Typography className={classes.noPromotionsText}>
                {formatMessage({ id: 'noPromotions' })}
              </Typography>
            ) : promotions === undefined || isPromotionDataLoading ? (
              [...new Array(12)].map((index, id) => (
                <PromotionsSkeleton key={id} />
              ))
            ) : (
              styledPromotions
            )}
          </Grid>
          <CreatePromotionDialog
            isDialogOpen={isCreatePromotionDialogOpen}
            closeDialog={closePromotionCreationDialog}
            structureId={structure_id}
          />
          {isPromotionDataLoading ? (
            <Fab
              color='primary'
              aria-label='create promotion'
              className={classes.fab}
              disabled
              onClick={openPromotionCreationDialog}
            >
              <AddRoundedIcon />
            </Fab>
          ) : (
            <Tooltip arrow title='Create promotion' className={classes.fab}>
              <Fab
                color='primary'
                aria-label='create promotion'
                onClick={openPromotionCreationDialog}
              >
                <AddRoundedIcon />
              </Fab>
            </Tooltip>
          )}
        </>
      ) : (
        <PromotionDetails />
      )}
    </Box>
  )
}

export default injectIntl(Promotions)
