import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import { injectIntl } from "react-intl";
import { CircularProgress } from "@material-ui/core";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function RemoveServiceFromPromotionDialog({
  intl,
  isRemoveDialogOpen,
  confirmRemove,
  closeDialog,
  isRemoving,
}) {
  const { formatMessage } = intl;

  return (
    <Dialog
      open={isRemoveDialogOpen}
      TransitionComponent={Transition}
      keepMounted
      onClose={isRemoving ? null : closeDialog}
    >
      <DialogTitle>
        {formatMessage({ id: "RemoveServiceFromPromotionDialogHeader" })}
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          {formatMessage({ id: "RemoveServiceFromPromotionMessage" })}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={confirmRemove} disabled={isRemoving}>
          {isRemoving ? (
            <CircularProgress size={24} style={{ marginRight: "8px" }} />
          ) : null}
          {formatMessage({ id: "agreeRemoveServiceFromPromotionButton" })}
        </Button>
        <Button
          onClick={closeDialog}
          color="primary"
          disabled={isRemoving}
          variant="contained"
        >
          {formatMessage({
            id: "cancelRemoveServiceFromPromotionPromotionButton",
          })}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default injectIntl(RemoveServiceFromPromotionDialog);
