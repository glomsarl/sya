import { useEffect, useState } from 'react'
import {
  Button,
  Box,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  CircularProgress,
  DialogContentText,
  DialogTitle,
  Divider,
  Typography,
  IconButton,
  Tooltip,
} from '@material-ui/core'
import { Autocomplete } from '@material-ui/lab'
import RemoveCircleRoundedIcon from '@material-ui/icons/RemoveCircleRounded'
import { Scrollbars } from 'react-custom-scrollbars-2'
import { usePromotion } from '../../contexts/Promotion/Promotion.provider'
import { injectIntl } from 'react-intl'
import { fetchStructureServives } from '../../services/structure.service'
import { useStructure } from '../../contexts/Structures/Structure.provider'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import { notifyError } from '../../utils/toastMessages'
import { addServiceToPromotion } from '../../services/promotions.service'

function AddServiceToPromotionDialog({ intl, isDialogOpen, closeDialog }) {
  let [isSubmitLoading, setIsSubmitLoading] = useState(false)
  const [selectedServices, setSelectedServices] = useState([])
  let { promotionState, promotionDispatch } = usePromotion()
  let { activePromotion } = promotionState
  let { formatMessage } = intl
  const {
    state: {
      activeStructure: { structure_id },
    },
  } = useStructure()

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()
  let [services, setServices] = useState([])

  useEffect(() => {
    fetchStructureServives(structure_id)
      .then((services) => {
        setServices(services)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
  
 // eslint-disable-next-line
}, [])

  const handleClose = () => {
    setSelectedServices([])
    closeDialog()
  }

  let handleAutocompleteSelect = (chosenService) => {
    if (chosenService !== null) {
      setSelectedServices([...selectedServices, chosenService])
      let newServices = services.filter(
        (service) => service.service_id !== chosenService.service_id
      )
      setServices(newServices)
    }
  }

  let removeServiceFromSelected = (service_id) => {
    let theService = selectedServices.find(
      (service) => service.service_id === service_id
    )
    let newSelectedServices = selectedServices.filter(
      (service) => service.service_id !== service_id
    )
    setSelectedServices(newSelectedServices)
    setServices([...services, theService])
  }

  let handleAddServices = () => {
    setIsSubmitLoading(true)
    setTimeout(() => {
      if (
        activePromotion.promotion_id !== undefined &&
        activePromotion.services !== undefined
      ) {
        let servicesToAdd = []
        let servicesAlreadyInPromotion = []
        selectedServices.forEach((service) => {
          let theService = activePromotion.services.find(
            (aService) => aService.service_id === service.service_id
          )
          if (theService === undefined) servicesToAdd.push(service.service_id)
          else servicesAlreadyInPromotion.push(service.service_name)
        })

        addServiceToPromotion({
          promotion_id: activePromotion.promotion_id,
          service_to_add: servicesToAdd,
        })
          .then((services) => {
            promotionDispatch({
              type: 'ADD_SERVICES_TO_PROMOTION',
              payload: services,
            })
            if (servicesAlreadyInPromotion.length > 0)
              notifyError(
                `${formatMessage({
                  id: 'addServiceToPromotionExistErrorPartOne',
                })}\n${servicesAlreadyInPromotion.join(`,\n`)}\n${formatMessage(
                  {
                    id: 'addServiceToPromotionExistErrorPartTwo',
                  }
                )}`
              )
          })
          .catch((error) => {
            handleSessionExpiredError(
              personDispatch,
              location.pathname,
              error,
              formatMessage,
              navigate
            )
          })
      } else
        notifyError(
          formatMessage({ id: 'addServiceToPromotionNoActivePromotionError' })
        )
      setIsSubmitLoading(false)
      handleClose()
    }, 3000)
  }

  let DisplayChoosenServices = () => {
    return (
      <Box
        style={{
          border: '1px solid grey',
          padding: ' 0 15px',
          marginTop: '10px',
          borderRadius: '10px',
        }}
      >
        <Scrollbars style={{ height: '20vh' }}>
          {selectedServices.length === 0 ? (
            <Typography style={{ cursor: 'context-menu' }}>
              {formatMessage({
                id: 'addServiceToPromotionNoSelectedService',
              })}
            </Typography>
          ) : (
            selectedServices.map((service) => (
              <Box key={service.service_id} style={{ cursor: 'context-menu' }}>
                <Typography component='span'>{service.service_name}</Typography>
                {isSubmitLoading ? (
                  <IconButton size='small' color='secondary' disabled>
                    <RemoveCircleRoundedIcon />
                  </IconButton>
                ) : (
                  <Tooltip
                    arrow
                    title={formatMessage({ id: 'removeServiceFromList' })}
                  >
                    <IconButton
                      size='small'
                      color='secondary'
                      onClick={() =>
                        removeServiceFromSelected(service.service_id)
                      }
                    >
                      <RemoveCircleRoundedIcon />
                    </IconButton>
                  </Tooltip>
                )}
                <Divider />
              </Box>
            ))
          )}
        </Scrollbars>
      </Box>
    )
  }

  return (
    <Dialog
      open={isDialogOpen}
      onClose={isSubmitLoading ? null : handleClose}
      aria-labelledby='form-dialog-title'
    >
      <DialogTitle id='form-dialog-title'>
        {formatMessage({ id: 'addServiceToPromotionDialogHeader' })}
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          {formatMessage({ id: 'addServiceToPromotionDialogMessage' })}
        </DialogContentText>
        <Box>
          <Autocomplete
            id='services-auto-complete'
            options={services}
            getOptionLabel={(option) => option.service_name}
            style={{ width: '100%' }}
            clearText={formatMessage({ id: 'clearAutocomplete' })}
            openText={formatMessage({ id: 'listServicesAutocomplete' })}
            closeText={formatMessage({ id: 'hideServicesAutocomplete' })}
            renderInput={(params) => (
              <TextField
                {...params}
                label={formatMessage({
                  id: 'addServiceToPromotionAutocompletePlaceHolder',
                })}
                variant='outlined'
              />
            )}
            onChange={(e, chosenService) =>
              handleAutocompleteSelect(chosenService)
            }
            autoHighlight
          />
          <DisplayChoosenServices />
        </Box>
      </DialogContent>
      <DialogActions>
        <Button
          disabled={isSubmitLoading}
          onClick={handleClose}
          color='primary'
        >
          {formatMessage({ id: 'addServiceToPromotionCancelButton' })}
        </Button>
        <Button
          disabled={isSubmitLoading || selectedServices.length === 0}
          onClick={
            selectedServices.length < 1 || isSubmitLoading
              ? null
              : handleAddServices
          }
          color='primary'
          variant='contained'
        >
          {isSubmitLoading && (
            <CircularProgress size='24px' style={{ marginRight: '10px' }} />
          )}
          {selectedServices.length > 1
            ? formatMessage({ id: 'addServiceButtonPlural' })
            : formatMessage({ id: 'addServiceButtonSingular' })}
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default injectIntl(AddServiceToPromotionDialog)
