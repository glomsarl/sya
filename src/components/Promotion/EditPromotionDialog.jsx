import {
  Button,
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  makeStyles,
  DialogTitle,
  TextField,
  Typography,
  CircularProgress,
} from '@material-ui/core'
import { useFormik } from 'formik'
import { injectIntl } from 'react-intl'
import * as yup from 'yup'
import { notifyInfo } from '../../utils/toastMessages'

const useStyles = makeStyles((theme) => ({
  inputHolder: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '10px',
    [theme.breakpoints.down('xs')]: {
      gridAutoFlow: 'row',
      gridGap: 0,
    },
  },
  descriptionLabel: {
    fontWeight: 600,
    paddingTop: '20px',
    textAlign: 'left',
  },
  imageLabel: {
    display: 'block',
    width: 'fit-content',
    padding: '6px 16px',
    marginTop: '10px',
    marginBottom: '12px',
    backgroundColor: theme.palette.primary.light,
    color: 'rgba(0, 0, 0, 0.87)',
    borderRadius: '6px',
    boxShadow:
      '0px 3px 1px -2px rgb(0 0 0 / 20%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 1px 5px 0px rgb(0 0 0 / 12%)',
    transition:
      'background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,border 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
    fontSize: '0.875rem',
    fontWeight: 500,
    lineHeight: 1.75,
    textTransform: 'uppercase',
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
  },
  form: {
    textAlign: 'center',
  },
  image: {
    width: '200px',
  },
  dialogFormInput: {
    marginTop: '15px',
  },
}))

function EditPromotionDialog({
  intl,
  open,
  closeDialog,
  start,
  end,
  value,
  name,
  type,
  isEditting,
  confirmEdit,
  purchasing_bonus,
}) {
  const { formatMessage } = intl
  const classes = useStyles()

  let validationSchema = yup.object({
    name: yup
      .string()
      .required(formatMessage({ id: 'promotionNameRequiredError' })),
    value: yup
      .number(
        type === 'Bonus points'
          ? formatMessage({ id: 'reductionNumberError' })
          : formatMessage({ id: 'bonusNumberError' })
      )
      .required(
        type === 'Bonus points'
          ? formatMessage({ id: 'reductionValueRequiredError' })
          : formatMessage({ id: 'bonusValueRequiredError' })
      ),
    purchasing_bonus: yup
      .number(formatMessage({ id: 'valueNumberError' }))
      .required(formatMessage({ id: 'valueRequiredError' })),
    start: yup
      .date()
      .required(formatMessage({ id: 'promotionStartDateRequiredError' }))
      .min(
        yup.ref('maxStartDate'),
        formatMessage({ id: 'minPromotionStartDateError' })
      ),
    end: yup
      .date()
      .required(formatMessage({ id: 'promotionEndDateRequiredError' }))
      .min(
        yup.ref('start'),
        formatMessage({ id: 'minPromotionStartDateError' })
      ),
  })

  const validationSchema2 = yup.object({
    end: yup
      .date()
      .required(formatMessage({ id: 'promotionEndDateRequiredError' }))
      .min(
        yup.ref('minEndDate'),
        formatMessage({ id: 'minPromotionEndDateError' })
      ),
    purchasing_bonus: yup
      .number(formatMessage({ id: 'valueNumberError' }))
      .required(formatMessage({ id: 'valueRequiredError' })),
  })

  const formik2 = useFormik({
    initialValues: {
      end: end,
      purchasing_bonus: purchasing_bonus,
      minEndDate: new Date(new Date().setDate(new Date().getDate() + 1)), // set it to the date of tomorrow
    },
    validationSchema2,
    onSubmit: (values, { resetForm }) => {
      delete values.minEndDate
      alert(JSON.stringify(values))
      confirmEdit({ ...values })
      resetForm()
      closeDialog()
    },
  })

  const formik = useFormik({
    initialValues: {
      start: start,
      end: end,
      value: value,
      name: name,
      purchasing_bonus,
      maxStartDate: new Date(new Date().setDate(new Date().getDate() + 1)), // set it to the date of tomorrow
    },
    validationSchema,
    onSubmit: (values, { resetForm }) => {
      if (
        values.start === start &&
        values.end === end &&
        values.value === value &&
        values.name === name &&
        values.purchasing_bonus === purchasing_bonus
      )
        notifyInfo('nothing changed')
      else {
        alert(JSON.stringify(values))
        if (delete values['maxStartDate']) confirmEdit({ ...values })
        resetForm()
        closeDialog()
      }
    },
  })

  /**
   * Initialize the formik form values.
   * given that the initial values in the useFormik declaration
   * already render when the page is openned, it prevents it
   * the initial values from getting updated data
   * this will execute every time the dailog opens
   * hence setting the initial values.
   */
  formik.initialValues.start = start
  formik.initialValues.end = end
  formik.initialValues.value = value
  formik.initialValues.name = name
  formik.initialValues.purchasing_bonus = purchasing_bonus
  formik2.initialValues.end = end
  formik2.initialValues.purchasing_bonus = purchasing_bonus

  let handleCloseDialog = () => {
    closeDialog()
    formik.resetForm()
    formik2.resetForm()
  }

  return (
    <Dialog open={open} onClose={isEditting ? null : handleCloseDialog}>
      <DialogTitle id='create_edit_dialog'>
        {formatMessage({ id: 'editPromotionDialogHeader' })}
      </DialogTitle>
      <DialogContent>
        {isEditting ? (
          <Typography>
            {formatMessage({ id: 'editPromotionMessage' })}
          </Typography>
        ) : new Date(start) < new Date() ? (
          <form onSubmit={formik2.handleSubmit} className={classes.form}>
            <Box className={classes.inputHolder}>
              <TextField
                color='secondary'
                variant='outlined'
                required
                id='end'
                label={formatMessage({ id: 'editPromotionEndInput' })}
                name='end'
                fullWidth
                type='datetime-local'
                InputLabelProps={{ shrink: true }}
                onBlur={formik2.handleBlur}
                onChange={formik2.handleChange}
                value={formik2.values.end}
                error={formik2.touched.end && formik2.errors.end ? true : false}
                helperText={formik2.errors.end}
              />
              <TextField
                color='secondary'
                variant='outlined'
                required
                id='purchasing_bonus'
                label={formatMessage({ id: 'editPurchasingBonusInput' })}
                name='purchasing_bonus'
                fullWidth
                type='number'
                className={classes.dialogFormInput}
                onChange={formik2.handleChange}
                onBlur={formik2.handleBlur}
                disabled={formik2.values.type === '0'}
                value={formik2.values.purchasing_bonus}
                error={
                  formik2.touched.purchasing_bonus &&
                  formik2.errors.purchasing_bonus
                    ? true
                    : false
                }
                helperText={
                  formik.touched.purchasing_bonus &&
                  formik.errors.purchasing_bonus
                }
              />
            </Box>
          </form>
        ) : (
          <form onSubmit={formik.handleSubmit} className={classes.form}>
            <TextField
              color='secondary'
              variant='outlined'
              required
              id='name'
              label={formatMessage({ id: 'editPromotionNameInput' })}
              name='name'
              type='text'
              fullWidth
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.name}
              error={formik.touched.name && formik.errors.name ? true : false}
              helperText={formik.errors.name}
            />
            <Box className={classes.inputHolder}>
              <TextField
                color='secondary'
                variant='outlined'
                required
                id='value'
                label={
                  formik.values.type === '1'
                    ? formatMessage({ id: 'editBonusValueInput' })
                    : formatMessage({ id: 'editReductionValueInput' })
                }
                name='value'
                type='number'
                fullWidth
                className={classes.dialogFormInput}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.value}
                error={
                  formik.touched.value && formik.errors.value ? true : false
                }
                helperText={formik.touched.value && formik.errors.value}
              />
              <TextField
                color='secondary'
                variant='outlined'
                required
                id='purchasing_bonus'
                label={formatMessage({ id: 'editPurchasingBonusInput' })}
                name='purchasing_bonus'
                fullWidth
                type='number'
                className={classes.dialogFormInput}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                disabled={formik.values.type === '0'}
                value={formik.values.purchasing_bonus}
                error={
                  formik.touched.purchasing_bonus &&
                  formik.errors.purchasing_bonus
                    ? true
                    : false
                }
                helperText={
                  formik.touched.purchasing_bonus &&
                  formik.errors.purchasing_bonus
                }
              />
            </Box>
            <Box className={classes.inputHolder}>
              <TextField
                color='secondary'
                variant='outlined'
                required
                className={classes.dialogFormInput}
                id='start'
                label={formatMessage({ id: 'editPromotionStartInput' })}
                name='start'
                type='datetime-local'
                InputLabelProps={{ shrink: true }}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.start}
                error={
                  formik.touched.start && formik.errors.start ? true : false
                }
                helperText={formik.errors.start}
              />
              <TextField
                color='secondary'
                variant='outlined'
                required
                className={classes.dialogFormInput}
                id='end'
                label={formatMessage({ id: 'editPromotionEndInput' })}
                name='end'
                type='datetime-local'
                InputLabelProps={{ shrink: true }}
                onBlur={formik.handleBlur}
                onChange={formik.handleChange}
                value={formik.values.end}
                error={formik.touched.end && formik.errors.end ? true : false}
                helperText={formik.errors.end}
              />
            </Box>
          </form>
        )}
      </DialogContent>
      <DialogActions>
        <Button
          onClick={handleCloseDialog}
          color='default'
          disabled={isEditting}
        >
          {formatMessage({ id: 'cancelEditButton' })}
        </Button>
        <Button
          onClick={
            new Date(start) < new Date()
              ? formik2.handleSubmit
              : formik.handleSubmit
          }
          color='primary'
          variant='contained'
          disabled={isEditting}
        >
          {isEditting && (
            <CircularProgress size={24} style={{ marginRight: '10px' }} />
          )}
          {formatMessage({ id: 'editPromotionButton' })}
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default injectIntl(EditPromotionDialog)
