import { useState } from 'react'
import {
  makeStyles,
  Card,
  CardActions,
  CardContent,
  Grid,
  Typography,
  IconButton,
  Tooltip,
  Grow,
} from '@material-ui/core'
import { injectIntl, FormattedDateTimeRange } from 'react-intl'
import DisplayDate from '../shared/DisplayDate'
import { DeleteRounded, EditRounded } from '@material-ui/icons'
import DeletePromotionDialog from './DeletePromotionDialog'
import { usePromotion } from '../../contexts/Promotion/Promotion.provider'
import EditPromotionDialog from './EditPromotionDialog'
import FolderOpenRoundedIcon from '@material-ui/icons/FolderOpenRounded'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import {
  deletePromotion,
  editPromotion,
  getPromotionStatistics,
} from '../../services/promotions.service'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import { useStructure } from '../../contexts/Structures/Structure.provider'

const useStyles = makeStyles((theme) => ({
  root: {
    cursor: 'pointer',
    height: '100%',
    position: 'relative',
    '& #cardOptions': {
      visibility: 'none',
    },
    '&:hover #cardOptions': {
      visibility: 'visible',
    },
  },
  colorRoot: {
    cursor: 'pointer',
    backgroundColor: theme.palette.grey[100],
    height: '100%',
    position: 'relative',
  },
  title: {
    fontSize: 14,
    display: 'inline',
  },
  promotionName: {
    borderTop: '1px solid #80808027',
    marginTop: '10px',
    paddingTop: '10px',
  },
  cardActions: {
    display: 'grid',
    gridAutoFlow: 'column',
    justifyContent: 'end',
    marginTop: '10px',
    position: 'absolute',
    bottom: 0,
    right: 0,
    backgroundColor: theme.palette.grey[100],
    borderRadius: '6px',
  },
  deleteButton: {
    color: theme.palette.error.main,
  },
}))

function StyledPromotion({
  intl,
  promotion,
  promotion: {
    purchasing_bonus,
    promotion_name,
    starts_at,
    ends_at,
    promotion_type,
    created_at,
    promotion_id,
    value,
  },
}) {
  const classes = useStyles()
  const [promotionGrow, setPromotionGrow] = useState(false)
  const { formatMessage } = intl
  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false)
  const [isDeleting, setIsDeleting] = useState(false)
  const { promotionDispatch } = usePromotion()
  const [promotionToDeleteId, setPromotionToDeleteId] = useState()
  const [isEditDialogOpen, setIsEditDialogOpen] = useState(false)
  const [isEditting, setIsEditting] = useState(false)
  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  const {
    state: {
      activeStructure: { structure_id },
    },
  } = useStructure()

  let closeEditDialog = () => {
    setIsEditDialogOpen(false)
  }

  let openEditDialog = () => {
    setIsEditDialogOpen(true)
  }

  let closeDeleteDialog = () => {
    setIsDeleteDialogOpen(false)
    setPromotionToDeleteId(undefined)
  }

  let confirmDelete = () => {
    setIsDeleting(true)
    deletePromotion(promotionToDeleteId)
      .then(() => {
        promotionDispatch({
          type: 'DELETE_PROMOTION',
          payload: promotionToDeleteId,
        })
        closeDeleteDialog()
        setIsDeleting(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsDeleting(false)
      })
  }

  let openDeleteDialog = () => {
    setPromotionToDeleteId(promotion_id)
    setIsDeleteDialogOpen(true)
  }

  let confirmEdit = (newPromotion) => {
    editPromotion(promotion_id, {
      ends_at: newPromotion.end,
      value: newPromotion.value,
      promotion_name: newPromotion.name,
      purchasing_bonus: newPromotion.purchasing_bonus
    })
      .then((promotion) => {
        promotionDispatch({
          type: 'EDIT_PROMOTION',
          payload: promotion,
        })
        setIsEditting(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
  }

  let openPromotion = (event) => {
    getPromotionStatistics({ promotion_id, structure_id })
      .then((bonus_statistics) => {
        promotionDispatch({
          type: 'OPEN_PROMOTION',
          payload: { ...promotion, ...bonus_statistics },
        })
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
  }

  return (
    <>
      <Grid item xs={12} sm={6} md={4} lg={3} xl={2}>
        <Card
          className={
            new Date(ends_at) < new Date() ? classes.colorRoot : classes.root
          }
          onMouseOver={() => setPromotionGrow(true)}
          onMouseOut={() => setPromotionGrow(false)}
          elevation={promotionGrow ? 7 : 4}
        >
          <CardContent>
            <DisplayDate
              dateTitle={formatMessage({ id: 'createdAt' })}
              value={new Date(created_at)}
            />
            <Typography
              variant='h5'
              component='h2'
              noWrap
              className={classes.promotionName}
            >
              {promotion_name}
            </Typography>
            <Typography
              className={classes.title}
              color='textSecondary'
              gutterBottom
            >
              {formatMessage({ id: 'promotionType' })}
            </Typography>
            <Typography color='textPrimary' className={classes.title}>
              {promotion_type === 'Reduction'
                ? formatMessage({ id: 'reductionPromotion' })
                : formatMessage({ id: 'bonusPromotion' })}
            </Typography>
            <br />
            <Typography
              className={classes.title}
              color='textSecondary'
              gutterBottom
            >
              {formatMessage({ id: 'promotionPeriod' })}
            </Typography>
            <Typography color='textPrimary' className={classes.title}>
              <FormattedDateTimeRange
                from={new Date(starts_at)}
                to={new Date(ends_at)}
                year='numeric'
                month='short'
                day='numeric'
                weekday='short'
              />
            </Typography>
          </CardContent>
          <Grow
            in={promotionGrow}
            style={{ transformOrigin: '0 0 0' }}
            {...(promotionGrow ? { timeout: 500 } : {})}
          >
            <CardActions className={classes.cardActions} id='cardOptions'>
              <Tooltip
                arrow
                title={formatMessage({ id: 'openPromotionTooltip' })}
              >
                <IconButton
                  size='small'
                  color='secondary'
                  onClick={openPromotion}
                >
                  <FolderOpenRoundedIcon fontSize='small' />
                </IconButton>
              </Tooltip>
              {new Date(ends_at) < new Date() ? null : (
                <>
                  <Tooltip
                    arrow
                    title={formatMessage({ id: 'editPromotionTooltip' })}
                  >
                    <IconButton
                      size='small'
                      color='secondary'
                      onClick={openEditDialog}
                    >
                      <EditRounded fontSize='small' />
                    </IconButton>
                  </Tooltip>
                  <Tooltip
                    arrow
                    title={formatMessage({ id: 'deletePromotionTooltip' })}
                  >
                    <IconButton
                      size='small'
                      className={classes.deleteButton}
                      onClick={openDeleteDialog}
                    >
                      <DeleteRounded fontSize='small' />
                    </IconButton>
                  </Tooltip>
                </>
              )}
            </CardActions>
          </Grow>
        </Card>
      </Grid>
      <DeletePromotionDialog
        isDeleteDialogOpen={isDeleteDialogOpen}
        confirmDelete={confirmDelete}
        closeDialog={closeDeleteDialog}
        isDeleting={isDeleting}
      />
      <EditPromotionDialog
        open={isEditDialogOpen}
        closeDialog={closeEditDialog}
        start={starts_at}
        end={ends_at}
        value={value}
        name={promotion_name}
        type={promotion_type}
        isEditting={isEditting}
        confirmEdit={confirmEdit}
        purchasing_bonus={purchasing_bonus}
      />
    </>
  )
}

export default injectIntl(StyledPromotion)
