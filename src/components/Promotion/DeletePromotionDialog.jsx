import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import { injectIntl } from "react-intl";
import { CircularProgress } from "@material-ui/core";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function DeletePromotionDialog({
  intl,
  isDeleteDialogOpen,
  confirmDelete,
  closeDialog,
  isDeleting,
}) {
  const { formatMessage } = intl;

  return (
    <Dialog
      open={isDeleteDialogOpen}
      TransitionComponent={Transition}
      keepMounted
      onClose={isDeleting ? null : closeDialog}
    >
      <DialogTitle>
        {formatMessage({ id: "deletePromotionDialogHeader" })}
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          {formatMessage({ id: "deletePromotionMessage" })}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={confirmDelete} disabled={isDeleting}>
          {isDeleting ? (
            <CircularProgress size={24} style={{ marginRight: "8px" }} />
          ) : null}
          {formatMessage({ id: "agreeDeletePromotionButton" })}
        </Button>
        <Button
          onClick={closeDialog}
          color="primary"
          disabled={isDeleting}
          variant="contained"
        >
          {formatMessage({ id: "cancelDeletePromotionButton" })}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default injectIntl(DeletePromotionDialog);
