import {
  Grid,
  IconButton,
  makeStyles,
  Tooltip,
  Typography,
} from '@material-ui/core'
import { useState } from 'react'
import { injectIntl } from 'react-intl'
import RemoveCircleRoundedIcon from '@material-ui/icons/RemoveCircleRounded'
import RemoveServiceFromPromotionDialog from './RemoveServiceFromPromotionDialog'
import { usePromotion } from '../../contexts/Promotion/Promotion.provider'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import { removeServiceFromPromotions } from '../../services/promotions.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'

const useStyles = makeStyles((theme) => ({
  serviceRecord: {
    alignItems: 'center',
    borderBottom: '1px solid #f8f7f7',
    padding: '10px 0',
    '&:hover': {
      backgroundColor: 'rgba(168, 175, 213, 0.2)',
    },
  },
  serviceDataNumber: {
    paddingLeft: '10px',
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  serviceData: {
    display: 'inline',
  },
  bringOnExtraSmall: {
    display: 'none',
    fontWeight: 600,
    [theme.breakpoints.down('xs')]: {
      display: 'inline',
      paddingLeft: '10px',
    },
  },
}))
function StyledPromotionService({
  intl: { formatMessage },
  service: { service_name, unit_price, service_id, consequent_price },
  number,
  type,
}) {
  const classes = useStyles()
  let [isRemoveDialogOpen, setIsRemoveDialogOpen] = useState(false)
  let [isRemoving, setIsRemoving] = useState(false)
  const {
    promotionState: {
      activePromotion: { start: starts_at, promotion_id },
    },
    promotionDispatch,
  } = usePromotion()

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  let confirmRemove = () => {
    setIsRemoving(true)
    removeServiceFromPromotions(service_id, promotion_id)
      .then(() => {
        promotionDispatch({
          type: 'REMOVE_SERVICE_FROM_PROMOTION',
          payload: service_id,
        })
        setIsRemoving(false)
        closeDialog()
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate,
        )
        setIsRemoving(false)
      })
  }

  let closeDialog = () => {
    setIsRemoveDialogOpen(false)
  }

  let openDialog = () => {
    setIsRemoveDialogOpen(true)
  }
  let isPromotionReduction = type === 'Reduction'
  let isPromotionStarted = new Date(starts_at) < new Date()

  return (
    <>
      <RemoveServiceFromPromotionDialog
        isRemoveDialogOpen={isRemoveDialogOpen}
        confirmRemove={confirmRemove}
        closeDialog={closeDialog}
        isRemoving={isRemoving}
      />
      <Grid container className={classes.serviceRecord}>
        <Grid item xs={12} sm={1} className={classes.serviceDataNumber}>
          <Typography id="serviceData1" noWrap className={classes.serviceData}>
            {number + 1}
          </Typography>
        </Grid>

        <Grid
          item
          xs={12}
          sm={
            isPromotionReduction && isPromotionStarted
              ? 5
              : !isPromotionReduction && isPromotionStarted
              ? 7
              : !isPromotionReduction && !isPromotionStarted
              ? 6
              : 5
          }
        >
          <Typography className={classes.bringOnExtraSmall}>
            {formatMessage({ id: 'promotionDetailsServiceNameHeader' })}
          </Typography>
          <Typography id="serviceData2" noWrap className={classes.serviceData}>
            {service_name}
          </Typography>
        </Grid>

        <Grid
          item
          xs={12}
          sm={
            isPromotionReduction && isPromotionStarted
              ? 3
              : !isPromotionReduction && isPromotionStarted
              ? 4
              : !isPromotionReduction && !isPromotionStarted
              ? 3
              : 2
          }
          className={classes.serviceDataOthers}
        >
          <Typography className={classes.bringOnExtraSmall}>
            {formatMessage({ id: 'promotionDetailsUnitPriceHeader' })}
          </Typography>
          <Typography id="serviceData3" noWrap className={classes.serviceData}>
            {`${unit_price} FCFA`}
          </Typography>
        </Grid>

        {type === 'Reduction' ? (
          <Grid item xs={12} sm={3} className={classes.serviceDataOthers}>
            <Typography className={classes.bringOnExtraSmall}>
              {formatMessage({ id: 'promotionDetailsDiscountedPriceHeader' })}
            </Typography>
            <Typography
              id="serviceData4"
              noWrap
              className={classes.serviceData}
            >
              {`${consequent_price} FCFA`}
            </Typography>
          </Grid>
        ) : null}

        {new Date(starts_at) < new Date() ? null : (
          <Grid item xs={12} sm={type === 'Reduction' ? 1 : 2}>
            <Grid container item className={classes.serviceActions}>
              <Tooltip
                arrow
                title={formatMessage({
                  id: 'StyledPromotionRemoveServiceFromPromotion',
                })}
              >
                <IconButton color="secondary" onClick={openDialog}>
                  <RemoveCircleRoundedIcon />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        )}
      </Grid>
    </>
  )
}

export default injectIntl(StyledPromotionService)
