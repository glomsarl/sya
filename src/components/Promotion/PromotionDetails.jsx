import { useState, useEffect } from 'react'
import { injectIntl, FormattedDateTimeRange } from 'react-intl'
import { usePromotion } from '../../contexts/Promotion/Promotion.provider'
import {
  makeStyles,
  Typography,
  Box,
  Grid,
  Hidden,
  Fab,
  Tooltip,
} from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import StyledPromotionService from './StyledPromotionService'
import AddRoundedIcon from '@material-ui/icons/AddRounded'
import KeyboardBackspaceRoundedIcon from '@material-ui/icons/KeyboardBackspaceRounded'
import AddServiceToPromotionDialog from './AddServiceToPromotion'
import Scrollbars from 'react-custom-scrollbars-2'
import { fetchPromotionServices } from '../../services/promotions.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'

let useStyles = makeStyles((theme) => ({
  root: {
    display: 'grid',
    gridTemplateRows: 'auto auto 1fr',
    height: '100%',
    [theme.breakpoints.down('xs')]: {
      gridTemplateRows: 'auto 1fr',
    },
  },
  promotionValueTag: {
    backgroundColor: theme.palette.error.dark,
    borderRadius: '40%',
    color: 'white',
    padding: '2px 5px',
    fontWeight: 'bold',
  },
  headerInfo: {
    fontWeight: 'bolder',
  },
  headerRoot: {
    backgroundColor: theme.palette.primary.dark,
    padding: '10px',
  },
  headerHolder: {
    display: 'grid',
    gridAutoFlow: 'column',
    color: 'grey',
  },
  headerNumberHolder: {
    display: 'grid',
    gridAutoFlow: 'column',
    color: 'grey',
    paddingLeft: '10px',
  },
  headerDivider: {
    borderRight: `1px solid ${theme.palette.primary.dark}`,
    margin: '5px 10px 5px 0',
  },
  serviceHeader: {
    textTransform: 'uppercase',
  },
  fab: {
    position: 'absolute',
    right: theme.spacing(2),
    bottom: theme.spacing(2),
  },
  fabBack: {
    position: 'absolute',
    left: theme.spacing(2),
    bottom: theme.spacing(2),
  },
  serviceSkeleton: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridTemplateColumns: 'auto 1fr',
    gridGap: '20px',
    borderBottom: '1px solid #f8f7f7',
  },
  noServicesText: {
    fontWeight: 100,
    color: theme.palette.secondary.light,
    height: '100%',
    display: 'grid',
    justifyContent: 'center',
    alignContent: 'center',
    textAlign: 'center',
  },
}))
function PromotionDetails({ intl }) {
  const classes = useStyles()
  const { formatMessage } = intl
  const { promotionState, promotionDispatch } = usePromotion()
  const { activePromotion } = promotionState
  const {
    promotion_id,
    promotion_type,
    starts_at,
    ends_at,
    promotion_name,
    value,
    services,
    purchasing_bonus,
    total_cashed_in_bonus,
    total_cashed_out_bonus,
    total_remaining_bonus
  } = activePromotion
  let [isAddServiceDialogOpen, setIsAddServiceDialogOpen] = useState(false)
  let [
    isPromotionServicesDataLoading,
    setIsPromotionServicesDataLoading,
  ] = useState(true)

  let closeAddServiceDialog = () => {
    setIsAddServiceDialogOpen(false)
  }

  let openAddServiceDialog = () => {
    setIsAddServiceDialogOpen(true)
  }

  let isPromotionReduction = promotion_type === 'Reduction'
  let isPromotionStarted = new Date(starts_at) < new Date()

  let PromotionDetailsHeader = () => {
    return (
      <Hidden only="xs">
        <Grid container className={classes.serviceHeaderHolder}>
          <Grid item xs={false} sm={1} className={classes.headerNumberHolder}>
            <Typography noWrap variant="h4" className={classes.serviceHeader}>
              #
            </Typography>
            <Typography className={classes.headerDivider}></Typography>
          </Grid>

          <Grid
            item
            xs={false}
            sm={
              isPromotionReduction && isPromotionStarted
                ? 5
                : !isPromotionReduction && isPromotionStarted
                ? 7
                : !isPromotionReduction && !isPromotionStarted
                ? 6
                : 5
            }
            className={classes.headerHolder}
          >
            <Typography noWrap variant="h4" className={classes.serviceHeader}>
              {formatMessage({ id: 'promotionDetailsServiceNameHeader' })}
            </Typography>
            <Typography className={classes.headerDivider}></Typography>
          </Grid>

          <Grid
            item
            xs={false}
            sm={
              isPromotionReduction && isPromotionStarted
                ? 3
                : !isPromotionReduction && isPromotionStarted
                ? 4
                : !isPromotionReduction && !isPromotionStarted
                ? 3
                : 2
            }
            className={classes.headerHolder}
          >
            <Typography noWrap variant="h4" className={classes.serviceHeader}>
              {formatMessage({ id: 'promotionDetailsUnitPriceHeader' })}
            </Typography>
            <Typography className={classes.headerDivider}></Typography>
          </Grid>

          {promotion_type === 'Reduction' ? (
            <Grid item xs={false} sm={3} className={classes.headerHolder}>
              <Typography noWrap variant="h4" className={classes.serviceHeader}>
                {formatMessage({
                  id: 'promotionDetailsDiscountedPriceHeader',
                })}
              </Typography>
              <Typography className={classes.headerDivider}></Typography>
            </Grid>
          ) : null}

          {new Date(starts_at) < new Date() ? null : (
            <Grid
              item
              xs={false}
              sm={promotion_type === 'Reduction' ? 1 : 2}
              className={classes.headerHolder}
            >
              <Typography noWrap variant="h4" className={classes.serviceHeader}>
                {formatMessage({ id: 'promotionDetailsActionHeader' })}
              </Typography>
            </Grid>
          )}
        </Grid>
      </Hidden>
    )
  }

  let StylePromotionServices = () => {
    return services === undefined
      ? null
      : services.map((service, index) => (
          <StyledPromotionService
            key={index}
            service={service}
            type={promotion_type}
            number={index}
          />
        ))
  }

  let goToPromotions = () => {
    promotionDispatch({ type: 'CLOSE_ACTIVE_PROMOTION' })
  }

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  useEffect(() => {
    fetchPromotionServices(promotion_id)
      .then((services) => {
        promotionDispatch({
          type: 'LOAD_ACTIVE_PROMOTION_SERVICES',
          payload: services,
        })
        setIsPromotionServicesDataLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate,
        )
        setIsPromotionServicesDataLoading(false)
      })
  
 // eslint-disable-next-line
}, [])

  function ServiceSkeleton() {
    return (
      <Box className={classes.serviceSkeleton}>
        <Skeleton variant="circle" width="40px" height="40px" />
        <Skeleton variant="text" width="100%" height="43.5px" />
      </Box>
    )
  }

  return (
    <Box className={classes.root}>
      <AddServiceToPromotionDialog
        isDialogOpen={isAddServiceDialogOpen}
        closeDialog={closeAddServiceDialog}
      />
      <Box className={classes.headerRoot}>
        <Typography>
          {`${formatMessage({ id: "promotionDetailsHeaderRootPromotion" })} `}
          <Typography component="span" className={classes.headerInfo}>
            {promotion_name}
          </Typography>
        </Typography>
        <Typography>
          {`${formatMessage({ id: "promotionDetailsHeaderRootPeriod" })} `}
          <Typography component="span" className={classes.headerInfo}>
            {
              <FormattedDateTimeRange
                from={new Date(starts_at)}
                to={new Date(ends_at)}
                year="numeric"
                month="short"
                day="numeric"
                weekday="short"
              />
            }
          </Typography>
        </Typography>
        <Typography>
          {promotion_type === "Reduction"
            ? `${formatMessage({
                id: "promotionDetailsHeaderRootReduction",
              })} `
            : `${formatMessage({ id: "promotionDetailsHeaderRootBonus" })} `}
          <Typography component="span" className={classes.headerInfo}>
            {promotion_type === "Reduction" ? `-${value}%` : `${value} SY`}
          </Typography>
        </Typography>
        {promotion_type.toLowerCase() !== "reduction" ? (
          <>
            <Typography>
              {`${formatMessage({
                id: "totalRemainingBonus",
              })} `}
              <Typography component="span" className={classes.headerInfo}>
                {total_remaining_bonus} SY
              </Typography>
            </Typography>
            <Typography>
              {`${formatMessage({
                id: "totalCashedInBonus",
              })} `}
              <Typography component="span" className={classes.headerInfo}>
                {total_cashed_in_bonus} SY
              </Typography>
            </Typography>
            <Typography>
              {`${formatMessage({
                id: "totalCashedOutBonus",
              })} `}
              <Typography component="span" className={classes.headerInfo}>
                {total_cashed_out_bonus} SY
              </Typography>
            </Typography>
            <Typography>
              {`${formatMessage({
                id: "editPurchasingBonusInput",
              })} `}
              <Typography component="span" className={classes.headerInfo}>
                {purchasing_bonus} SY
              </Typography>
            </Typography>
          </>
        ) : null}
      </Box>
      <PromotionDetailsHeader />
      <Scrollbars>
        {services !== undefined &&
        services.length === 0 &&
        !isPromotionServicesDataLoading ? (
          new Date(ends_at) < Date.now() ? (
            <Typography className={classes.noServicesText}>
              {formatMessage({ id: "noServicesWithoutAddButton" })}
            </Typography>
          ) : (
            <Typography className={classes.noServicesText}>
              {formatMessage({ id: "noServicesWithAddButton" })}
            </Typography>
          )
        ) : services === undefined || isPromotionServicesDataLoading ? (
          [...new Array(12)].map((index, id) => <ServiceSkeleton key={id} />)
        ) : (
          <StylePromotionServices />
        )}
      </Scrollbars>
      <Tooltip
        arrow
        title={formatMessage({
          id: "promotionDetailsBackTooltip",
        })}
        className={classes.fabBack}
      >
        <Fab
          color="secondary"
          aria-label={formatMessage({
            id: "promotionDetailsBackTooltip",
          })}
          onClick={goToPromotions}
        >
          <KeyboardBackspaceRoundedIcon />
        </Fab>
      </Tooltip>

      {new Date(ends_at) < new Date() ? null : (
        <Tooltip
          arrow
          title={formatMessage({
            id: "promotionDetailsAddServiceToPromotionTooltip",
          })}
          className={classes.fab}
        >
          <Fab
            color="secondary"
            aria-label={formatMessage({
              id: "promotionDetailsAddServiceToPromotionTooltip",
            })}
            onClick={openAddServiceDialog}
          >
            <AddRoundedIcon />
          </Fab>
        </Tooltip>
      )}
    </Box>
  );
}

export default injectIntl(PromotionDetails)
