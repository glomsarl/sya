import {
  Button,
  Box,
  Dialog,
  DialogActions,
  DialogContent,
  makeStyles,
  DialogTitle,
  TextField,
  MenuItem,
  CircularProgress,
} from '@material-ui/core'
import { useFormik } from 'formik'
import { useState } from 'react'
import { injectIntl } from 'react-intl'
import * as yup from 'yup'
import { usePromotion } from '../../contexts/Promotion/Promotion.provider'
import { createPromotion } from '../../services/promotions.service'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { notifySuccess } from '../../utils/toastMessages'

const useStyles = makeStyles((theme) => ({
  inputHolder: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '10px',
    [theme.breakpoints.down('xs')]: {
      gridAutoFlow: 'row',
      gridGap: 0,
    },
  },
  descriptionLabel: {
    fontWeight: 600,
    paddingTop: '20px',
    textAlign: 'left',
  },
  imageLabel: {
    display: 'block',
    width: 'fit-content',
    padding: '6px 16px',
    marginTop: '10px',
    marginBottom: '12px',
    backgroundColor: theme.palette.primary.light,
    color: 'rgba(0, 0, 0, 0.87)',
    borderRadius: '6px',
    boxShadow:
      '0px 3px 1px -2px rgb(0 0 0 / 20%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 1px 5px 0px rgb(0 0 0 / 12%)',
    transition:
      'background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,border 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
    fontSize: '0.875rem',
    fontWeight: 500,
    lineHeight: 1.75,
    textTransform: 'uppercase',
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
  },
  form: {
    textAlign: 'center',
  },
  image: {
    width: '200px',
  },
  dialogFormInput: {
    marginTop: '15px',
  },
  select: {
    '&>.MuiInputBase-root': {
      padding: '18.5px 14px',
      textAlign: 'left',
    },
  },
}))

function CreatePromotionDialog({
  intl,
  isDialogOpen,
  closeDialog,
  structureId,
}) {
  const { formatMessage } = intl
  const classes = useStyles()
  let [submitLoading, setSubmitLoading] = useState(false)
  const { promotionDispatch } = usePromotion()

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  let validationSchema = yup.object({
    name: yup
      .string()
      .required(formatMessage({ id: 'promotionNameRequiredError' })),
    value: yup
      .number(formatMessage({ id: 'valueNumberError' }))
      .required(formatMessage({ id: 'valueRequiredError' })),
    purchasing_bonus: yup
      .number(formatMessage({ id: 'valueNumberError' }))
      .required(formatMessage({ id: 'valueRequiredError' })),
    start: yup
      .date()
      .required(formatMessage({ id: 'promotionStartDateRequiredError' }))
      .min(
        yup.ref('minStartDate'),
        formatMessage({ id: 'minPromotionStartDateError' })
      ),
    end: yup
      .date()
      .required(formatMessage({ id: 'promotionEndDateRequiredError' }))
      .min(yup.ref('start'), formatMessage({ id: 'minPromotionEndDateError' })),
    type: yup
      .string()
      .required(formatMessage({ id: 'promotionTypeRequiredError' }))
      .matches(/1|0/, formatMessage({ id: 'promotionTypeError' })),
  })

  const formik = useFormik({
    initialValues: {
      start: '',
      end: '',
      value: '',
      name: '',
      type: '0',
      purchasing_bonus: 0,
      minStartDate: new Date(new Date().setDate(new Date().getDate() + 1)), // set it to the date of tomorrow
    },
    validationSchema,
    onSubmit: (values, { resetForm }) => {
      setSubmitLoading(true)
      createPromotion({
        value: values.value,
        ends_at: values.end,
        starts_at: values.start,
        structure_id: structureId,
        promotion_name: values.name,
        promotion_type: values.type === '1' ? 'Bonus' : 'Reduction',
        purchasing_bonus: values.type === '1' ? values.purchasing_bonus : null,
      })
        .then((promotion) => {
          promotionDispatch({ type: 'CREATE_PROMOTION', payload: promotion })
          notifySuccess(formatMessage({ id: 'promotionCreationSuccess' }))
          setSubmitLoading(false)
          closeDialog()
          resetForm()
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
          setSubmitLoading(false)
        })
    },
  })

  let handleCloseDialog = () => {
    formik.resetForm()
    closeDialog()
  }

  return (
    <Dialog open={isDialogOpen}>
      <DialogTitle id='create_edit_dialog'>
        {formatMessage({ id: 'createPromotionDialogHeader' })}
      </DialogTitle>
      <DialogContent>
        <form onSubmit={formik.handleSubmit} className={classes.form}>
          <TextField
            color='secondary'
            variant='outlined'
            required
            id='type'
            label='promotion type'
            name='type'
            select
            fullWidth
            className={classes.select}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.type}
            error={formik.touched.type && formik.errors.type ? true : false}
            helperText={formik.touched.type && formik.errors.type}
          >
            <MenuItem value='1'>Bonus points</MenuItem>
            <MenuItem value='0'>Reduction</MenuItem>
          </TextField>
          <TextField
            color='secondary'
            variant='outlined'
            required
            id='name'
            label={formatMessage({ id: 'editPromotionNameInput' })}
            name='name'
            type='text'
            fullWidth
            className={classes.dialogFormInput}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.name}
            error={formik.touched.name && formik.errors.name ? true : false}
            helperText={formik.touched.name && formik.errors.name}
          />
          <Box className={classes.inputHolder}>
            <TextField
              color='secondary'
              variant='outlined'
              required
              id='value'
              label={
                formik.values.type === '1'
                  ? formatMessage({ id: 'editBonusValueInput' })
                  : formatMessage({ id: 'editReductionValueInput' })
              }
              name='value'
              type='number'
              fullWidth
              className={classes.dialogFormInput}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.value}
              error={formik.touched.value && formik.errors.value ? true : false}
              helperText={formik.touched.value && formik.errors.value}
            />
            <TextField
              color='secondary'
              variant='outlined'
              required
              id='purchasing_bonus'
              label={formatMessage({ id: 'editPurchasingBonusInput' })}
              name='purchasing_bonus'
              fullWidth
              type='number'
              className={classes.dialogFormInput}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              disabled={formik.values.type === '0'}
              value={formik.values.purchasing_bonus}
              error={
                formik.touched.purchasing_bonus &&
                formik.errors.purchasing_bonus
                  ? true
                  : false
              }
              helperText={
                formik.touched.purchasing_bonus &&
                formik.errors.purchasing_bonus
              }
            />
          </Box>
          <Box className={classes.inputHolder}>
            <TextField
              color='secondary'
              variant='outlined'
              required
              className={classes.dialogFormInput}
              id='start'
              label={formatMessage({ id: 'editPromotionStartInput' })}
              name='start'
              type='datetime-local'
              InputLabelProps={{ shrink: true }}
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.start}
              error={formik.touched.start && formik.errors.start ? true : false}
              helperText={formik.touched.start && formik.errors.start}
            />
            <TextField
              color='secondary'
              variant='outlined'
              required
              className={classes.dialogFormInput}
              id='end'
              label={formatMessage({ id: 'editPromotionEndInput' })}
              name='end'
              type='datetime-local'
              InputLabelProps={{ shrink: true }}
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              value={formik.values.end}
              error={formik.touched.end && formik.errors.end ? true : false}
              helperText={formik.touched.end && formik.errors.end}
            />
          </Box>
        </form>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={handleCloseDialog}
          color='default'
          disabled={submitLoading}
        >
          {formatMessage({ id: 'cancelEditButton' })}
        </Button>
        <Button
          onClick={formik.handleSubmit}
          color='primary'
          variant='contained'
          disabled={submitLoading}
        >
          {submitLoading && (
            <CircularProgress size={24} style={{ marginRight: '10px' }} />
          )}
          {formatMessage({ id: 'createPromotionButton' })}
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default injectIntl(CreatePromotionDialog)
