import MaterialTable from 'material-table'
import { useState, forwardRef, useEffect } from 'react'
import { injectIntl } from 'react-intl'
import SubscriptionDashboardCard from './SubscriptionDashboardCard'
import StructureReceiptDialog from './StructureReceiptDialog'
import moment from 'moment'
import {
  Box,
  makeStyles,
  Typography,
  IconButton,
  Tooltip,
  TextField,
} from '@material-ui/core'
import {
  AddBoxRounded,
  ArrowDownwardRounded,
  CheckRounded,
  ChevronLeftRounded,
  ChevronRightRounded,
  ClearRounded,
  DeleteOutlineRounded,
  EditRounded,
  FilterListRounded,
  FirstPageRounded,
  LastPageRounded,
  RemoveRounded,
  SaveAltRounded,
  SearchRounded,
  ViewColumnRounded,
  FiberManualRecordRounded,
  PaymentRounded,
  AccountBalanceRounded,
  BlockRounded,
  PauseRounded,
  ArrowBackIosRounded,
  ArrowForwardIosRounded,
  DoneAllRounded,
} from '@material-ui/icons'
import {
  getActiveSubscription,
  getAllSubscriptions,
  subscriptionStatistics,
} from '../../services/subscription.services'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'

const useStyles = makeStyles((theme) => ({
  pageHolder: { padding: '0 20px', height: '100%' },
  pageHeader: {
    fontWeight: 'bold',
    fontSize: '1.3rem',
    textTransform: 'uppercase',
  },
  cardsHolder: {
    display: 'grid',
    gridTemplateColumns: 'repeat(auto-fit, minmax(150px, 1fr))',
    gap: '20px',
  },
  filterHolder: {
    display: 'grid',
    gap: '15px',
    gridTemplateColumns: 'auto auto 1fr',
    alignItems: 'center',
    marginTop: '10px',
  },
  icons: {
    height: '17px',
    width: '17px',
  },
  monthSelector: {
    justifySelf: 'end',
    '& .MuiInputBase-formControl::before': {
      borderBottom: 'none',
    },
    '& .MuiInputBase-root': {
      borderRadius: '6px',
    },
    '& .MuiInputBase-input': {
      padding: '10px 12px',
    },
  },
}))

function AdminSubscriptionDashboard({
  intl: { formatMessage, formatDate, formatTime, formatNumber },
}) {
  const classes = useStyles()
  const [isCardsDataLoading, setIsCardsDataLoading] = useState(true)
  const [cards, setCards] = useState([
    {
      key: 'cleared_accounts',
      Icon: PaymentRounded,
      title: formatMessage({ id: 'clearedAccounts' }),
      value: 0,
    },
    {
      key: 'active_accounts',
      Icon: AccountBalanceRounded,
      title: formatMessage({ id: 'activeAccounts' }),
      value: 0,
    },
    {
      key: 'blocked_accounts',
      Icon: BlockRounded,
      title: formatMessage({ id: 'blockedAccounts' }),
      value: 0,
    },
  ])

  const { personDispatch } = usePerson()
  const location = useLocation()
  const navigate = useNavigate()

  useEffect(() => {
    subscriptionStatistics()
      .then((statistics) => {
        const newCards = cards.map((card) => {
          let new_card = { ...card, value: statistics[card.key] }
          return new_card
        })
        setCards(newCards)
        setIsCardsDataLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
    // eslint-disable-next-line
  }, [])

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBoxRounded {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <CheckRounded {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <ClearRounded {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => (
      <DeleteOutlineRounded {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRightRounded {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => <EditRounded {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAltRounded {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => (
      <FilterListRounded {...props} ref={ref} />
    )),
    FirstPage: forwardRef((props, ref) => (
      <FirstPageRounded {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPageRounded {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRightRounded {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeftRounded {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <ClearRounded {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => <SearchRounded {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownwardRounded {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <RemoveRounded {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => (
      <ViewColumnRounded {...props} ref={ref} />
    )),
  }
  const [activeDate, setActiveDate] = useState(moment().format('YYYY-MM-DD'))
  const [isReceiptDialogOpen, setIsReceiptDialogOpen] = useState(false)
  const [isActiveStructureLoading, setIsActiveStructureLoading] =
    useState(false)
  const [activeStructure, setActiveStructure] = useState({
    structure_id: undefined,
    structure_name: '',
    paid_at: undefined,
    commission: 0,
    is_paid: false,
    is_blocked: false,
    paid_from_number: undefined,
    paid_to_number: undefined,
    payment_method: '',
    reference: '',
  })

  const viewStructureDetails = (structure_details) => {
    setActiveStructure(structure_details)
    setIsReceiptDialogOpen(true)
  }

  const closeActiveStructure = () => {
    setIsReceiptDialogOpen(false)
    setActiveStructure({
      structure_id: undefined,
      structure_name: '',
      paid_at: undefined,
      commission: 0,
      is_paid: false,
      is_blocked: false,
    })
  }

  useEffect(() => {
    if (isReceiptDialogOpen) {
      setIsActiveStructureLoading(true)
      getActiveSubscription({
        structure_id: activeStructure.structure_id,
        subscription_payment_id: activeStructure.subscription_payment_id,
      })
        .then((subscriptions) => {
          setActiveStructure(subscriptions)
          setIsActiveStructureLoading(false)
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
        })
    }
    // eslint-disable-next-line
  }, [isReceiptDialogOpen])

  const [subscriptions, setSubscriptions] = useState([])
  const [isSubscriptionsLoading, setIsSubscriptionsLoading] = useState(true)

  useEffect(() => {
    setIsSubscriptionsLoading(true)
    getAllSubscriptions({ selected_date: activeDate ? new Date() : null })
      .then(({ subscriptions }) => {
        setSubscriptions(subscriptions)
        setIsSubscriptionsLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
    // eslint-disable-next-line
  }, [activeDate])

  return (
    <>
      <StructureReceiptDialog
        structure={activeStructure}
        isDialogOpen={isReceiptDialogOpen}
        closeDialog={closeActiveStructure}
        isActiveStructureLoading={isActiveStructureLoading}
      />
      <Box className={classes.pageHolder}>
        <Typography className={classes.pageHeader}>
          {formatMessage({ id: 'subscriptions' })}
        </Typography>
        <Box className={classes.cardsHolder}>
          {cards.map((card, index) => (
            <SubscriptionDashboardCard
              key={index}
              card={card}
              isCardsLoading={isCardsDataLoading}
            />
          ))}
        </Box>
        <Box className={classes.filterHolder}>
          <Typography style={{ fontWeight: 500 }}>
            {formatDate(new Date(activeDate), {
              year: 'numeric',
              month: 'long',
              day: 'numeric',
              weekday: 'short',
            })}
          </Typography>
          <Box>
            <Tooltip arrow title={formatMessage({ id: 'yesterday' })}>
              <IconButton
                size='small'
                onClick={() =>
                  setActiveDate(
                    moment(activeDate).subtract(1, 'days').format('YYYY-MM-DD')
                  )
                }
              >
                <ArrowBackIosRounded className={classes.icons} />
              </IconButton>
            </Tooltip>
            <Tooltip arrow title={formatMessage({ id: 'tomorrow' })}>
              <IconButton
                size='small'
                onClick={() =>
                  setActiveDate(
                    moment(activeDate).add(1, 'days').format('YYYY-MM-DD')
                  )
                }
              >
                <ArrowForwardIosRounded className={classes.icons} />
              </IconButton>
            </Tooltip>
          </Box>
          <TextField
            type='date'
            variant='filled'
            className={classes.monthSelector}
            value={activeDate}
            onChange={(e) => setActiveDate(e.target.value)}
          />
        </Box>
        <Box>
          <MaterialTable
            icons={tableIcons}
            isLoading={isSubscriptionsLoading}
            style={{ marginTop: '20px', boxShadow: 'none' }}
            onRowClick={(event, rowData) => viewStructureDetails(rowData)}
            columns={[
              {
                title: formatMessage({ id: 'structureName' }),
                field: 'structure_name',
                render: ({ structure_name, is_blocked }) =>
                  is_blocked ? (
                    <Box
                      style={{
                        display: 'grid',
                        gridTemplateColumns: '1fr auto',
                        alignItems: 'center',
                      }}
                    >
                      <Typography component='span'>{structure_name}</Typography>
                      <Tooltip
                        arrow
                        title={formatMessage({ id: 'blockedStructure' })}
                      >
                        <FiberManualRecordRounded
                          fontSize='small'
                          style={{ color: 'red' }}
                        />
                      </Tooltip>
                    </Box>
                  ) : (
                    structure_name
                  ),
              },
              {
                title: formatMessage({ id: 'paidOn' }),
                field: 'paid_on',
                render: ({ paid_at, is_paid }) =>
                  is_paid ? (
                    formatDate(new Date(paid_at), {
                      year: 'numeric',
                      month: 'long',
                      day: 'numeric',
                    })
                  ) : (
                    <Tooltip
                      arrow
                      title={formatMessage({ id: 'waitingPayment' })}
                    >
                      <PauseRounded fontSize='small' style={{ color: 'red' }} />
                    </Tooltip>
                  ),
              },
              {
                title: formatMessage({ id: 'resolvedAt' }),
                field: 'paid_at',
                render: ({ paid_at, is_paid }) =>
                  is_paid ? (
                    formatTime(new Date(paid_at))
                  ) : (
                    <Tooltip
                      arrow
                      title={formatMessage({ id: 'waitingPayment' })}
                    >
                      <PauseRounded fontSize='small' style={{ color: 'red' }} />
                    </Tooltip>
                  ),
              },
              {
                title: formatMessage({ id: 'transactionAmount' }),
                field: 'transaction_amount',
                render: ({ transaction_amount }) =>
                  formatNumber(transaction_amount, {
                    style: 'currency',
                    currency: 'XAF',
                  }),
              },
              {
                title: formatMessage({ id: 'transactionStatut' }),
                field: 'is_paid',
                render: ({ is_paid }) =>
                  is_paid ? (
                    <Tooltip
                      arrow
                      title={formatMessage({ id: 'receiptSettled' })}
                    >
                      <DoneAllRounded style={{ color: 'green' }} />
                    </Tooltip>
                  ) : (
                    <Tooltip
                      arrow
                      title={formatMessage({ id: 'waitingPayment' })}
                    >
                      <PauseRounded fontSize='small' style={{ color: 'red' }} />
                    </Tooltip>
                  ),
              },
            ]}
            data={subscriptions}
            title={formatMessage({ id: 'lastTransactions' })}
          />
        </Box>
      </Box>
    </>
  )
}

export default injectIntl(AdminSubscriptionDashboard)
