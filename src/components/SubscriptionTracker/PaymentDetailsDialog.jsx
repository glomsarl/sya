import {
  Box,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  CircularProgress,
  useTheme,
  Tooltip,
} from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import { CheckCircleRounded } from '@material-ui/icons'
import { injectIntl } from 'react-intl'
import { PauseRounded } from '@material-ui/icons'
import momo from '../../assets/momo.png'
import om from '../../assets/om.png'
import logo from '../../assets/emoji.png'
import randomNumber from '../../utils/randomNumber'

const ReceiptLine = ({ title, value, type, isLoading }) => {
  const theme = useTheme()
  return (
    <Box style={{ display: 'grid', gridAutoFlow: 'column', gap: '5px' }}>
      <Typography component='span'>{title}</Typography>
      {!isLoading ? (
        <Typography
          component='span'
          style={{ justifySelf: 'end', color: theme.palette.grey[600] }}
        >
          {value}
        </Typography>
      ) : type === 'image' ? (
        <Skeleton
          variant='circle'
          width='20px'
          height='20px'
          animation='wave'
          style={{ justifySelf: 'end' }}
        />
      ) : (
        <Skeleton
          variant='text'
          animation='wave'
          style={{ justifySelf: 'end', width: '100%' }}
        />
      )}
    </Box>
  )
}

function PaymentDetailsDialog({
  intl: {
    formatMessage,
    formatDate,
    formatTime,
    formatNumber,
    formatDateTimeRange,
  },
  paymentInfo,
  paymentInfo: {
    structure_name,
    reference,
    started_at,
    paid_at,
    expired_at,
    total_period_revenue,
    commission,
    commission_rate,
    is_paid,
    payment_method,
  },
  isDialogOpen,
  closeDialog,
  isSubmitting,
  confirmDialog,
  isActiveStructureLoading,
}) {
  const theme = useTheme()

  const paymentImage = {
    momo: <img src={momo} alt='mtn mobile money' />,
    om: <img src={om} width='34px' alt='orange mobile money' />,
  }

  let receiptDetails = [
    {
      title: formatMessage({ id: 'paymentReference' }),
      value: is_paid ? (
        reference
      ) : (
        <Tooltip arrow title={formatMessage({ id: 'waitingPayment' })}>
          <PauseRounded fontSize='small' color='secondary' />
        </Tooltip>
      ),
      type: 'string',
    },
    {
      title: formatMessage({ id: 'description' }),
      value: formatMessage({ id: 'commissionFeePayment' }),
      type: 'string',
    },
    {
      title: formatMessage({ id: 'period' }),
      value: formatDateTimeRange(new Date(started_at), new Date(expired_at), {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
      }),
    },
    {
      title: formatMessage({ id: 'revenue' }),
      value: formatNumber(total_period_revenue, {
        style: 'currency',
        currency: 'XAF',
      }),
      type: 'string',
    },
    {
      title: formatMessage({ id: 'commissionRate' }),
      value: formatNumber(commission_rate / 100, { style: 'percent' }),
      type: 'string',
    },
    {
      title: formatMessage({ id: 'totalPayable' }),
      value: formatNumber(commission, {
        style: 'currency',
        currency: 'XAF',
      }),
      type: 'string',
    },
    {
      title: formatMessage({ id: 'paymentMethod' }),
      value: is_paid ? (
        paymentImage[payment_method]
      ) : (
        <Tooltip arrow title={formatMessage({ id: 'waitingPayment' })}>
          <PauseRounded fontSize='small' color='secondary' />
        </Tooltip>
      ),
      type: 'image',
    },
  ]
  return (
    <Dialog open={isDialogOpen} onClose={isSubmitting ? null : closeDialog}>
      <DialogTitle
        style={{
          backgroundColor: theme.palette.secondary.main,
          color: theme.palette.secondary.main,
        }}
      >
        <Box
          style={{
            display: 'grid',
            gridTemplateColumns: '1fr 1fr',
            gap: '10px',
            marginBottom: '8px',
            alignItems: 'center',
          }}
        >
          <Box
            style={{
              color: theme.palette.grey[400],
              display: 'grid',
              gridAutoFlow: 'row',
              gap: '5px',
            }}
          >
            <Typography component='span'>
              {isActiveStructureLoading ? (
                <Skeleton
                  variant='text'
                  animation='wave'
                  width={`${randomNumber(70, 95)}%`}
                  style={{ color: 'yellow' }}
                />
              ) : is_paid ? (
                formatDate(new Date(paid_at), {
                  year: 'numeric',
                  month: 'long',
                  day: 'numeric',
                })
              ) : null}
            </Typography>
            {/* <Typography component='span'>
              {isActiveStructureLoading ? (
                <Skeleton
                  variant='text'
                  animation='wave'
                  width={`${randomNumber(30, 60)}%`}
                />
              ) : is_paid(
                formatTime(new Date(paid_at))
              )}
            </Typography> */}
            <Typography component='span' style={{ fontWeight: 'bolder' }}>
              {isActiveStructureLoading ? (
                <Skeleton
                  variant='text'
                  animation='wave'
                  width={`${randomNumber(70, 100)}%`}
                />
              ) : (
                structure_name
              )}
            </Typography>
          </Box>

          <Box
            style={{
              display: 'grid',
              alignItems: 'center',
              justifyContent: 'end',
              gridTemplateColumns: 'auto auto',
              gap: '10px',
            }}
          >
            <img src={logo} alt='SYA' height='30px' />
            <Typography
              style={{
                fontSize: '1.2rem',
                color: theme.palette.primary.main,
                fontWeight: 'bolder',
              }}
            >
              See You Again
            </Typography>
          </Box>
        </Box>
      </DialogTitle>
      <DialogContent>
        <Typography style={{ fontWeight: 'bold', textAlign: 'center' }}>
          {formatMessage({ id: 'paymentReceipt' })}
        </Typography>
        <Box style={{ display: 'grid', gap: '3px', marginTop: '12px' }}>
          {receiptDetails.map(({ title, value, type }) => (
            <ReceiptLine
              title={title}
              value={value}
              type={type}
              isLoading={isActiveStructureLoading}
            />
          ))}
        </Box>
      </DialogContent>
      <DialogActions style={{ display: 'grid', justifyContent: 'center' }}>
        {is_paid ? (
          <CheckCircleRounded />
        ) : (
          <Button
            disabled={isSubmitting || isActiveStructureLoading}
            onClick={() => confirmDialog(paymentInfo)}
            color='secondary'
            variant='outlined'
            size='small'
            startIcon={
              isSubmitting ? (
                <CircularProgress
                  color='secondary'
                  size='24px'
                  style={{ marginRight: '10px' }}
                />
              ) : null
            }
          >
            {formatMessage({ id: 'payNow' })}
          </Button>
        )}
      </DialogActions>
    </Dialog>
  )
}

export default injectIntl(PaymentDetailsDialog)
