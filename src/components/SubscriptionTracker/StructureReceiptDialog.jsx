import {
  Box,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  useTheme,
} from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";
import {
  CheckCircleRounded,
  BlockRounded,
} from "@material-ui/icons";
import { injectIntl } from "react-intl";
import { PauseRounded } from "@material-ui/icons";
import momo from "../../assets/momo.png";
import om from "../../assets/om.png";

const ReceiptLine = ({ title, value, type, isLoading }) => {
  const theme = useTheme();
  return (
    <Box style={{ display: "grid", gridAutoFlow: "column", gap: "5px" }}>
      <Typography component="span">{title}</Typography>
      {!isLoading ? (
        <Typography
          component="span"
          style={{ justifySelf: "end", color: theme.palette.grey[600] }}
        >
          {value}
        </Typography>
      ) : type === "image" ? (
        <Skeleton
          variant="circle"
          width="20px"
          height="20px"
          animation="wave"
          style={{ justifySelf: "end" }}
        />
      ) : (
        <Skeleton
          variant="text"
          animation="wave"
          style={{ justifySelf: "end", width: "100%" }}
        />
      )}
    </Box>
  );
};

function StructureReceiptDialog({
  intl: { formatMessage, formatDate, formatTime, formatNumber },
  structure: {
    paid_at,
    structure_name,
    is_paid,
    transaction_amount,
    paid_from_number,
    paid_to_number,
    payment_method,
    payment_reference,
  },
  isDialogOpen,
  closeDialog,
  isActiveStructureLoading,
}) {
  const theme = useTheme();

  const paymentImage = {
    momo: <img src={momo} alt="mtn mobile money" />,
    om: <img src={om} width="34px" alt="orange mobile money" />,
  };

  let receiptDetails = [
    {
      title: formatMessage({ id: "depositingAccount" }),
      value: is_paid ? (
        formatNumber(paid_from_number, { type: "string" })
      ) : (
        <PauseRounded fontSize="small" color="secondary" />
      ),
      type: "string",
    },
    {
      title: formatMessage({ id: "benefittingAccount" }),
      value: is_paid ? (
        formatNumber(paid_to_number, { type: "string" })
      ) : (
        <PauseRounded fontSize="small" color="secondary" />
      ),
      type: "string",
    },
    {
      title: formatMessage({ id: "paymentMethod" }),
      value: is_paid ? (
        paymentImage[payment_method]
      ) : (
        <PauseRounded fontSize="small" color="secondary" />
      ),
      type: "image",
    },
    {
      title: formatMessage({ id: "paymentReference" }),
      value: is_paid ? (
        payment_reference
      ) : (
        <PauseRounded fontSize="small" color="secondary" />
      ),
      type: "string",
    },
    {
      title: is_paid
        ? formatMessage({ id: "paymentAmount" })
        : formatMessage({ id: "amountDue" }),
      value: formatNumber(transaction_amount, {
        style: "currency",
        currency: "XAF",
      }),
      type: "string",
    },
  ];
  return (
    <Dialog open={isDialogOpen} onClose={closeDialog}>
      <DialogTitle
        style={{
          backgroundColor: theme.palette.primary.main,
          color: theme.palette.secondary.main,
        }}
      >
        {formatMessage({ id: "transactionDetailsHeader" })}
      </DialogTitle>
      <DialogContent>
        {is_paid ? (
          <Box
            style={{
              color: theme.palette.grey[400],
              display: "grid",
              gridAutoFlow: "column",
              gap: "5px",
            }}
          >
            <Typography component="span">
              {formatDate(new Date(paid_at), {
                year: "numeric",
                month: "long",
                day: "numeric",
              })}
            </Typography>
            <Typography component="span">
              {formatTime(new Date(paid_at))}
            </Typography>
            <Typography component="span">{structure_name}</Typography>
          </Box>
        ) : (
          <Typography
            style={{ color: theme.palette.grey[400], textAlign: "center" }}
          >
            {structure_name}
          </Typography>
        )}
        <Box style={{ display: "grid", gap: "3px", marginTop: "12px" }}>
          {receiptDetails.map(({ title, value, type }) => (
            <ReceiptLine
              title={title}
              value={value}
              type={type}
              isLoading={isActiveStructureLoading}
            />
          ))}
        </Box>
      </DialogContent>
      <DialogActions style={{ display: "grid", justifyContent: "center" }}>
        {is_paid ? (
          <Box
            style={{
              display: "grid",
              justifyItems: "center",
              color: theme.palette.success.main,
            }}
          >
            <CheckCircleRounded style={{ color: theme.palette.success.main }} />
            <Typography>
              {formatMessage({ id: "successfullTransaction" })}
            </Typography>
          </Box>
        ) : (
          <Box
            style={{
              display: "grid",
              justifyItems: "center",
              color: theme.palette.error.main,
            }}
          >
            <BlockRounded style={{ color: theme.palette.error.main }} />
            <Typography>
              {formatMessage({ id: "pendingTransaction" })}
            </Typography>
          </Box>
        )}
      </DialogActions>
    </Dialog>
  );
}

export default injectIntl(StructureReceiptDialog);
