import MaterialTable from 'material-table'
import { useState, forwardRef, useEffect } from 'react'
import { injectIntl } from 'react-intl'
import { Skeleton } from '@material-ui/lab'
import Moment from 'moment'
import { extendMoment } from 'moment-range'
import { Box, makeStyles, Tooltip, Typography } from '@material-ui/core'
import {
  AddBoxRounded,
  ArrowDownwardRounded,
  CheckRounded,
  ChevronLeftRounded,
  ChevronRightRounded,
  ClearRounded,
  DeleteOutlineRounded,
  EditRounded,
  FilterListRounded,
  FirstPageRounded,
  LastPageRounded,
  RemoveRounded,
  SaveAltRounded,
  SearchRounded,
  ViewColumnRounded,
  LoopRounded,
  PauseRounded,
  DoneAllRounded,
} from '@material-ui/icons'
import randomNumber from '../../utils/randomNumber'
import PaymentDetailsDialog from './PaymentDetailsDialog'
import {
  getActiveSubscription,
  getAllSubscriptions,
  paySubscription,
} from '../../services/subscription.services'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import { useStructure } from '../../contexts/Structures/Structure.provider'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'

const useStyles = makeStyles((theme) => ({
  pageHolder: { padding: '0 20px', height: '100%' },
  pageHeader: {
    display: 'grid',
    gridTemplateColumns: '1fr 2fr',
    gap: '10px',
    padding: '30px 10px',
  },
  icons: {
    height: '17px',
    width: '17px',
  },
  statTitle: {
    fontWeight: theme.palette.grey[500],
  },
  statValue: {
    fontWeight: 600,
    fontSize: '1.2rem',
  },
}))

function OwnerSubscriptionHistory({
  intl: { formatMessage, formatNumber, formatDateTimeRange },
}) {
  const classes = useStyles()
  const moment = extendMoment(Moment)

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBoxRounded {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <CheckRounded {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <ClearRounded {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => (
      <DeleteOutlineRounded {...props} ref={ref} />
    )),
    DetailPanel: forwardRef((props, ref) => (
      <ChevronRightRounded {...props} ref={ref} />
    )),
    Edit: forwardRef((props, ref) => <EditRounded {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAltRounded {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => (
      <FilterListRounded {...props} ref={ref} />
    )),
    FirstPage: forwardRef((props, ref) => (
      <FirstPageRounded {...props} ref={ref} />
    )),
    LastPage: forwardRef((props, ref) => (
      <LastPageRounded {...props} ref={ref} />
    )),
    NextPage: forwardRef((props, ref) => (
      <ChevronRightRounded {...props} ref={ref} />
    )),
    PreviousPage: forwardRef((props, ref) => (
      <ChevronLeftRounded {...props} ref={ref} />
    )),
    ResetSearch: forwardRef((props, ref) => (
      <ClearRounded {...props} ref={ref} />
    )),
    Search: forwardRef((props, ref) => <SearchRounded {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => (
      <ArrowDownwardRounded {...props} ref={ref} />
    )),
    ThirdStateCheck: forwardRef((props, ref) => (
      <RemoveRounded {...props} ref={ref} />
    )),
    ViewColumn: forwardRef((props, ref) => (
      <ViewColumnRounded {...props} ref={ref} />
    )),
  }
  const [isReceiptDialogOpen, setIsReceiptDialogOpen] = useState(false)
  const [isPaymentSubmitting, setIsPaymentSubmitting] = useState(false)
  const [isActivePaymentLoading, setIsActivePaymentLoading] = useState(false)
  const [activePayment, setActivePayment] = useState({
    subscription_payment_id: undefined,
    structure_name: '',
    structure_id: undefined,
    reference: undefined,
    started_at: undefined,
    paid_at: undefined,
    expired_at: undefined,
    total_period_revenue: 0,
    commission: 0,
    commission_rate: 0,
    is_paid: false,
    payment_method: undefined,
  })

  const openPaymentReceipt = (payment_details) => {
    setActivePayment(payment_details)
    setIsReceiptDialogOpen(true)
  }

  const closeActivePayment = () => {
    setIsReceiptDialogOpen(false)
    setActivePayment({
      subscription_payment_id: undefined,
      structure_name: '',
      structure_id: undefined,
      reference: undefined,
      started_at: undefined,
      paid_at: undefined,
      expired_at: undefined,
      total_period_revenue: 0,
      commission: 0,
      commission_rate: 0,
      is_paid: false,
      payment_method: undefined,
    })
  }

  const { personDispatch } = usePerson()
  const location = useLocation()
  const navigate = useNavigate()

  const payDue = (paymentDetails) => {
    setIsPaymentSubmitting(true)
    paySubscription(paymentDetails)
      .then(() => {
        setIsPaymentSubmitting(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsPaymentSubmitting(false)
      })
  }

  useEffect(() => {
    if (isReceiptDialogOpen) {
      setIsActivePaymentLoading(true)
      getActiveSubscription({
        structure_id: activePayment.structure_id,
        subscription_payment_id: activePayment.subscription_payment_id,
      })
        .then((subscriptions) => {
          setActivePayment(subscriptions)
          setIsActivePaymentLoading(false)
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
        })
    }
   
   // eslint-disable-next-line
}, [isReceiptDialogOpen])

  const [totalStats, setTotalStats] = useState({
    total_system_revenue: 0,
    total_commission_paid: 0,
  })
  const [isTotalStatsLoading, setIsTotalStatsLoading] = useState(true)

  const [payments, setPayments] = useState([])
  const [isPaymentsLoading, setIsPaymentsLoading] = useState(true)

  const {
    state: {
      activeStructure: { structure_id },
    },
  } = useStructure()

  useEffect(() => {
    setIsTotalStatsLoading(true)
    setIsPaymentsLoading(true)
    getAllSubscriptions({ structure_id })
      .then(({ subscriptions, statistics }) => {
        setPayments(subscriptions)
        setIsPaymentsLoading(false)
        setTotalStats(statistics)
        setIsTotalStatsLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
   
  
 // eslint-disable-next-line
}, [])

  return (
    <>
      <PaymentDetailsDialog
        paymentInfo={activePayment}
        isDialogOpen={isReceiptDialogOpen}
        closeDialog={closeActivePayment}
        isActiveStructureLoading={isActivePaymentLoading}
        isSubmitting={isPaymentSubmitting}
        confirmDialog={payDue}
      />
      <Box className={classes.pageHolder}>
        <Box className={classes.pageHeader}>
          <Box
            style={{
              justifySelf: 'start',
              marginLeft: '50px',
              alignSelf: 'center',
            }}
          ></Box>
          <Box style={{ justifySelf: 'end' }}>
            <Box
              style={{
                display: 'grid',
                gridTemplateColumns: 'auto 1fr',
                gap: '5px',
                alignItems: 'center',
              }}
            >
              <Typography className={classes.statTitle}>
                {formatMessage({ id: 'totalRevenue' })}
              </Typography>
              {isTotalStatsLoading ? (
                <Skeleton
                  variant='text'
                  animation='wave'
                  width={`${randomNumber(60, 95)}px`}
                />
              ) : (
                <Typography className={classes.statValue}>
                  {formatNumber(totalStats.total_system_revenue, {
                    style: 'currency',
                    currency: 'XAF',
                  })}
                </Typography>
              )}
            </Box>
            <Box
              style={{
                display: 'grid',
                gridTemplateColumns: 'auto 1fr',
                gap: '5px',
              }}
            >
              <Typography className={classes.statTitle}>
                {formatMessage({ id: 'totalCommission' })}
              </Typography>
              {isTotalStatsLoading ? (
                <Skeleton
                  variant='text'
                  animation='wave'
                  width={`${randomNumber(60, 95)}px`}
                />
              ) : (
                <Typography className={classes.statValue}>
                  {formatNumber(totalStats.total_commission_paid, {
                    style: 'currency',
                    currency: 'XAF',
                  })}
                </Typography>
              )}
            </Box>
          </Box>
        </Box>
        <MaterialTable
          icons={tableIcons}
          isLoading={isPaymentsLoading}
          style={{ marginTop: '20px', boxShadow: 'none' }}
          onRowClick={(event, rowData) => openPaymentReceipt(rowData)}
          columns={[
            {
              title: formatMessage({ id: 'paymentReference' }),
              field: 'reference',
              render: ({ reference, is_paid }) =>
                is_paid ? (
                  reference
                ) : (
                  <Tooltip
                    arrow
                    title={formatMessage({ id: 'waitingPayment' })}
                  >
                    <PauseRounded fontSize='small' style={{ color: 'red' }} />
                  </Tooltip>
                ),
            },
            {
              title: formatMessage({ id: 'period' }),
              field: 'started_at',
              render: ({ started_at, expired_at }) =>
                formatDateTimeRange(
                  new Date(started_at),
                  new Date(expired_at),
                  {
                    year: 'numeric',
                    month: 'short',
                    day: 'numeric',
                  }
                ),
            },
            {
              title: formatMessage({ id: 'revenue' }),
              field: 'total_period_revenue',
              render: ({ total_period_revenue }) =>
                formatNumber(total_period_revenue, {
                  style: 'currency',
                  currency: 'XAF',
                }),
            },
            {
              title: formatMessage({ id: 'commission' }),
              field: 'commission',
              render: ({ commission }) =>
                formatNumber(commission, {
                  style: 'currency',
                  currency: 'XAF',
                }),
            },
            {
              title: formatMessage({ id: 'transactionStatut' }),
              field: 'is_paid',
              render: ({ is_paid, started_at, expired_at }) => {
                const range = moment.range(started_at, expired_at)
                return !range.contains(new Date()) ? (
                  <Tooltip arrow title={formatMessage({ id: 'ongoingPeriod' })}>
                    <LoopRounded color='secondary' />
                  </Tooltip>
                ) : is_paid ? (
                  <Tooltip
                    arrow
                    title={formatMessage({ id: 'receiptSettled' })}
                  >
                    <DoneAllRounded style={{ color: 'green' }} />
                  </Tooltip>
                ) : (
                  <Tooltip
                    arrow
                    title={formatMessage({ id: 'waitingPayment' })}
                  >
                    <PauseRounded fontSize='small' style={{ color: 'red' }} />
                  </Tooltip>
                )
              },
            },
          ]}
          data={payments}
          title={formatMessage({ id: 'paymentHistory' })}
        />
      </Box>
    </>
  )
}

export default injectIntl(OwnerSubscriptionHistory)
