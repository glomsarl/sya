import React from "react";
import { Box, Typography, makeStyles } from "@material-ui/core";
import { MoreVertRounded } from "@material-ui/icons";
import {Skeleton} from "@material-ui/lab";
import randomNumber from "../../utils/randomNumber";

const useStyles = makeStyles((theme) => ({
  cardHolder: {
    display: "grid",
    backgroundColor: "#eff0f6",
    padding: "10px",
    borderRadius: "10px"
  },
  iconHolder: {
    display: "grid",
    gridTemplateColumns: "1fr auto",
  },
  title: {
    color: "rgb(0,0,0,0.5)",
    fontSize: "0.80rem",
  },
  value: {
    fontWeight: "bold",
    fontSize: "1.2rem",
  },
}));

export default function SubscriptionDashboardCard({card:{ Icon, title, value}, isCardsLoading}) {
  const classes = useStyles();
  return (
    <Box className={classes.cardHolder}>
      <Box className={classes.iconHolder}>
        <Icon />
        <MoreVertRounded />
      </Box>
      <Typography className={classes.title}>{title}</Typography>
      {isCardsLoading? <Skeleton animation="wave" variant="text" style={{width: `${randomNumber(50,89)}%`, borderRadius: "5px"}} />:<Typography className={classes.value}>{value}</Typography>}
    </Box>
  );
}
