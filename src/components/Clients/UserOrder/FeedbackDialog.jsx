import { useState } from 'react'
import {
  Dialog,
  DialogTitle,
  DialogContent,
  Button,
  CircularProgress,
  DialogActions,
  TextField,
  DialogContentText,
  Box,
  Typography,
} from '@material-ui/core'
import { SaveRounded } from '@material-ui/icons'
import { injectIntl } from 'react-intl'
import { Rating } from 'react-simple-star-rating'
import { sendNewFeedback } from '../../../services/feedback.service'
import handleSessionExpiredError from '../../../utils/handleSessionExpiry'
import { useLocation, useNavigate } from 'react-router'

function FeedbackDialog({
  intl: { formatMessage },
  order_content_id,
  isDialogOpen,
  closeDialog,
}) {
  const [isSubmitting, setIsSubmitting] = useState(false)
  const [feedback, setFeedback] = useState('')
  const [rating, setRating] = useState(3)

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = useLocation()

  const confirmDialog = () => {
    setIsSubmitting(true)
    sendNewFeedback({ order_content_id, feedback, rating })
      .then(() => {
        setIsSubmitting(false)
        closeDialog()
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate,
        )
        setIsSubmitting(false)
      })
  }

  return (
    <Dialog open={isDialogOpen} onClose={null}>
      <DialogTitle>{formatMessage({ id: 'feedbackDialogHeader' })}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          {formatMessage({ id: 'editBonusSettingsMessage' })}
        </DialogContentText>
        <TextField
          autoFocus
          multiline
          disabled={isSubmitting}
          color="secondary"
          rows={4}
          label={formatMessage({ id: 'feedbackTextLabel' })}
          onChange={(event) => setFeedback(event.target.value)}
          value={feedback}
          variant="outlined"
          placeholder={formatMessage({ id: 'feedbackPlaceholder' })}
          fullWidth
        />
        <Box>
          <Typography
            component="span"
            style={{
              fontSize: '1.2rem',
              fontWeight: 'bold',
              color: 'rgb(0,0,0,0.6)',
            }}
          >
            {formatMessage({ id: 'yourRating' })}
          </Typography>
          <Rating
            onClick={(rate) => setRating((rate / 100) * 5)}
            ratingValue={(rating / 5) * 100}
            allowHalfIcon={true}
            tooltipDefaultText="send a rating"
            allowHover={!isSubmitting}
          />
        </Box>
      </DialogContent>
      <DialogActions>
      <Button
          disabled={isSubmitting}
          onClick={closeDialog}
          color="secondary"
          variant="outlined"
          size="small"
        >
          {formatMessage({ id: 'cancelButton' })}
        </Button>
        <Button
          disabled={isSubmitting}
          onClick={confirmDialog}
          color="secondary"
          variant="outlined"
          size="small"
          startIcon={
            isSubmitting ? (
              <CircularProgress
                color="secondary"
                size="24px"
                style={{ marginRight: '10px' }}
              />
            ) : (
              <SaveRounded />
            )
          }
        >
          {formatMessage({ id: 'submitButton' })}
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default injectIntl(FeedbackDialog)
