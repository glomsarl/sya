import {
  Box,
  Dialog,
  DialogContent,
  IconButton,
  makeStyles,
  Tooltip,
  Typography,
} from '@material-ui/core'
import { Close } from '@material-ui/icons'
import GoogleMapReact from 'google-map-react'
import { injectIntl } from 'react-intl'
import locationIcon from '../../../assets/emoji.png'

const LocationPin = ({ structureName, formatMessage }) => {
  return (
    <Tooltip
      open
      arrow
      title={
        <Box
          style={{ display: 'grid', justifyItems: 'center', padding: '5px' }}
        >
          <img
            style={{ width: '40px' }}
            alt='sya location icon'
            src={locationIcon}
          />
          <Typography
            style={{
              fontWeight: 'bold',
              letterSpacing: '1px',
              textAlign: 'center',
            }}
          >
            {structureName ?? formatMessage({ id: 'thisPlaceText' })}
          </Typography>
        </Box>
      }
    >
      <Typography style={{ fontWeight: 'bold' }}>ICI</Typography>
    </Tooltip>
  )
}
const useStyles = makeStyles((theme) => ({
  DialogTitle: {
    color: '#FFFF',
    padding: '10px',
    marginLeft: '-4px',
    display: 'grid',
    gridAutoFlow: 'column',
  },
}))

const MapComponent = ({
  open,
  location,
  zoomLevel,
  closeHandler,
  structureName,
  logoRef,
  intl: { formatMessage },
}) => {
  const classes = useStyles()
  return (
    <Dialog open={open} fullScreen keepMounted onClose={closeHandler}>
      <Box
        id='alert-dialog-slide-title'
        style={{
          background: `url(${process.env.REACT_APP_BASE_URL}/${logoRef}`,
          backgroundPosition: 'center',
        }}
        className={classes.DialogTitle}
      >
        <Typography variant='h6'>{formatMessage({ id: 'traject' })}</Typography>
        <IconButton
          variant='outlined'
          onClick={closeHandler}
          style={{ justifySelf: 'right', background: '#011638' }}
          color='secondary'
        >
          <Close color='primary' />
        </IconButton>
      </Box>
      <DialogContent>
        <GoogleMapReact
          bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_MAP_API_KEY }}
          defaultCenter={location || { lat: 4.059955, lng: 9.715712 }}
          defaultZoom={zoomLevel || 16}
        >
          <LocationPin
            formatMessage={formatMessage}
            lat={(location && location.lat) || 4.059955}
            lng={(location && location.lng) || 9.715712}
            structureName={structureName}
          />
        </GoogleMapReact>
      </DialogContent>
    </Dialog>
  )
}

export default injectIntl(MapComponent)
