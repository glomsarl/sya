import { useState } from 'react'
import { Delete, DoneAll, Feedback, LocationOn } from '@material-ui/icons'
import { injectIntl, FormattedDate, FormattedTime } from 'react-intl'
import { usePerson } from '../../../contexts/PersonContext/Person.provider'
import {
  Button,
  Typography,
  makeStyles,
  Card,
  CardActionArea,
  CardContent,
  CardActions,
  Box,
  LinearProgress,
} from '@material-ui/core'
import { changeOrderStatus } from '../../../services/order.service'
import { useLocation, useNavigate } from 'react-router'
import handleSessionExpiredError from '../../../utils/handleSessionExpiry'
import { getAddress } from '../../../utils/geocodeAddress'
import { useEffect } from 'react'
import FeedbackDialog from './FeedbackDialog'

const useStyles = makeStyles((theme) => ({
  Area: {
    display: 'grid',
    gridAutoFlow: 'column',
    width: '330px',
    height: 'fit-content',
  },
  Status: {
    justifySelf: 'right',
    padding: '5px',
    borderRadius: '5px 0px',
    height: 'fit-content',
    position: 'absolute',
  },
  Header: {
    display: 'grid',
    gridAutoFlow: 'column',
  },
  Schedule: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '25px',
  },
  Home: {
    backgroundColor: '#38B000',
    borderRadius: '5px',
    padding: '0 8px',
    fontSize: '0.6rem',
    width: 'fit-content',
    height: 'fit-content',
  },
  Deliver: {
    backgroundColor: '#1A759F',
    borderRadius: '5px',
    padding: '0 8px',
    fontSize: '0.6rem',
    width: 'fit-content',
    height: 'fit-content',
  },
  Reserve: {
    backgroundColor: '#38B000',
    borderRadius: '5px',
    padding: '0 8px',
    fontSize: '10px',
    width: 'fit-content',
    height: 'fit-content',
  },
  TagHolder: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '10px',
    paddingTop: '5px',
    height: 'fit-content',
    width: 'fit-content',
  },
  Method: {
    width: '40px',
    height: 'fit-content',
    borderRadius: '10px',
    textAlign: 'center',
    padding: '0px 5px',
    fontSize: '10px',
    backgroundColor: theme.palette.primary.main,
  },
  Cancel: {
    color: theme.palette.error.main,
    fontSize: '10px',
  },
  Confirm: {
    color: theme.palette.success.main,
    fontSize: '10px',
  },
  Content: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
    width: '200px',
    padding: '0px',
  },
  Actions: {
    padding: '3px 0px',
    width: '330px',
  },
}))

function UserOrder({
  order: {
    order_id,
    status,
    method,
    ordered_at,
    order_type,
    order_content_id,
    service: { unit_price, service_name, quantity, main_image_ref },
    structure: { structure_name, longitude: lng, latitude: lat, logo_ref },
  },
  openMapHandler,
  intl: { formatMessage },
}) {
  let classes = useStyles()

  let [isPending, setIsPending] = useState(false)
  let {
    personState: { orderHistory },
    personDispatch,
  } = usePerson()
  const location = useLocation()
  const navigate = useNavigate()
  const [address, setAddress] = useState('')
  useEffect(() => {
    getAddress(lat, lng).then((address) => setAddress(address))
  })

  const confirmOrderHandler = (order_id) => {
    setIsPending(true)
    changeOrderStatus({ order_id, status: 'CONFIRMED' })
      .then(() => {
        let newPurchase = orderHistory.map((order) => {
          if (order.order_id === order_id)
            return { ...order, status: 'CONFIRMED' }
          else return order
        })
        personDispatch({ type: 'LOAD_ORDER_HISTORY', payload: newPurchase })
        setIsPending(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
  }

  const cancelOrderHandler = (order_id) => {
    setIsPending(true)
    changeOrderStatus({ order_id, status: 'CANCELLED' })
      .then(() => {
        let newPurchase = orderHistory.map((order) => {
          if (order.order_id === order_id)
            return { ...order, status: 'CANCELLED' }
          else return order
        })
        personDispatch({ type: 'LOAD_ORDER_HISTORY', payload: newPurchase })
        setIsPending(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
  }
  const myBackground = {
    CONFIRMED: '#38B000',
    CANCELLED: '#D00000',
    UNCONFIRMED: '#1A759F',
    SERVED: '#F8C600',
  }
  const [isDialogOpen, setIsDialogOpen] = useState(false)

  return (
    <Card style={{ width: 'fit-content' }}>
      <CardActionArea className={classes.Area}>
        <img src={main_image_ref} alt='Ndole' width='130' height='130' />
        <CardContent className={classes.Content}>
          <Typography
            variant='caption'
            className={classes.Status}
            style={{
              backgroundColor: myBackground[status],
            }}
          >
            {formatMessage({ id: `${status}` })}
          </Typography>
          <Box style={{ paddingInline: '10px' }}>
            <Box className={classes.Header}>
              <Typography
                variant='h4'
                color='secondary'
                style={{ fontWeight: 'bold' }}
              >
                {structure_name}
                <Typography variant='body2' color='textSecondary'>
                  {address}
                </Typography>
              </Typography>
            </Box>
            <Typography variant='caption' color='secondary'>
              {service_name}
            </Typography>
            <br />
            <Typography
              variant='caption'
              color='textSecondary'
              style={{ fontWeight: 'bold' }}
            >
              {`${formatMessage({ id: 'price' })} ${quantity} * ${unit_price} FCFA`}
            </Typography>
            <Box className={classes.Schedule}>
              <Typography variant='caption' color='secondary'>
                {formatMessage({ id: 'date' })}
                <br />
                <FormattedDate
                  value={ordered_at}
                  day='2-digit'
                  date='2-digit'
                  month='long'
                  year='numeric'
                />
              </Typography>
              <Typography variant='caption' color='secondary'>
                {formatMessage({ id: 'time' })}
                <br />
                <FormattedTime value={ordered_at} />
              </Typography>
            </Box>
            <Box className={classes.TagHolder}>
              {order_type === 'T' ? (
                <Typography className={classes.Home}>
                  {formatMessage({ id: 'takeAwayRadio' })}
                </Typography>
              ) : order_type === 'R' ? (
                <Typography className={classes.Reserve}>
                  {formatMessage({ id: 'reserveRadio' })}
                </Typography>
              ) : order_type === 'H' ? (
                <Typography className={classes.Deliver}>
                  {formatMessage({ id: 'deliveryRadio' })}
                </Typography>
              ) : null}
              <Typography
                variant='body2'
                color='textSecondary'
                className={classes.Method}
              >
                {method}
              </Typography>
            </Box>
          </Box>
        </CardContent>
      </CardActionArea>
      <CardActions className={classes.Actions}>
        <Button
          color='secondary'
          style={{ fontSize: '10px' }}
          onClick={() =>
            openMapHandler({
              lng,
              lat,
              logoRef: logo_ref,
              structureName: structure_name,
            })
          }
          disabled={isPending}
        >
          <LocationOn style={{ fontSize: '15px' }} />
          {formatMessage({ id: 'traject' })}
        </Button>
        {status === 'UNCONFIRMED' ? (
          <>
            <Button
              className={classes.Confirm}
              onClick={() => confirmOrderHandler(order_id)}
              disabled={isPending}
            >
              {!isPending ? (
                <>
                  <DoneAll style={{ fontSize: '15px' }} />
                  {formatMessage({ id: 'confirm' })}
                </>
              ) : (
                <LinearProgress color='secondary' />
              )}
            </Button>
            <Button
              className={classes.Cancel}
              onClick={() => cancelOrderHandler(order_id)}
              disabled={isPending}
            >
              {!isPending ? (
                <>
                  <Delete style={{ fontSize: '15px' }} />
                  {formatMessage({ id: 'cancel' })}
                </>
              ) : (
                <LinearProgress color='secondary' />
              )}
            </Button>
          </>
        ) : status === 'CONFIRMED' ? (
          <Button
            className={classes.Cancel}
            onClick={() => cancelOrderHandler(order_id)}
            disabled={isPending}
          >
            <Delete style={{ fontSize: '15px' }} />
            {formatMessage({ id: 'cancel' })}
          </Button>
        ) : status === 'SERVED' ? (
          <>
            <FeedbackDialog
              isDialogOpen={isDialogOpen}
              order_content_id={order_content_id}
              closeDialog={() => setIsDialogOpen(false)}
            />
            <Button
              color='secondary'
              style={{ fontSize: '10px' }}
              onClick={() => setIsDialogOpen(true)}
              disabled={isPending}
            >
              <Feedback style={{ fontSize: '15px' }} />
              {formatMessage({ id: 'feedbackTextLabel' })}
            </Button>
          </>
        ) : null}
      </CardActions>
    </Card>
  )
}

export default injectIntl(UserOrder)
