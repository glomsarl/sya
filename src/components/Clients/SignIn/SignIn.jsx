import React from 'react'
import * as yup from 'yup'
import { useState } from 'react'
import { useFormik } from 'formik'
import { injectIntl } from 'react-intl'
import {
  makeStyles,
  Grid,
  Link,
  TextField,
  Button,
  Typography,
  Container,
  CircularProgress,
} from '@material-ui/core'

import GoogleIcon from '../../../assets/icons/Google'
import womanCartImage from '../../../assets/undraw_shopping_app_flsj.svg'
import { usePerson } from '../../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import { signIn } from '../../../services/authentication.service'
import handleSessionExpiredError from '../../../utils/handleSessionExpiry'

const useStyles = makeStyles((theme) => ({
  paper: {
    [theme.breakpoints.up('md')]: {
      marginTop: theme.spacing(4),
      padding: theme.spacing(4),
      alignItems: 'center',
      display: 'flex',
      flexDirection: 'column',
      boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .2)',
    },
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  title: {
    [theme.breakpoints.up('md')]: {
      marginTop: theme.spacing(5),
    },
    [theme.breakpoints.down('sm')]: {
      marginTop: theme.spacing(1),
    },
  },
  titleLink: {
    fontWeight: theme.common.fontWeightRegular,
    cursor: 'pointer',
    '&:hover': {
      textDecoration: 'none',
    },
  },
  subtitle: {
    color: '#012a4a',
    fontWeight: theme.common.fontWeightRegular2,
    marginBottom: theme.spacing(2),
  },
  link: {
    color: '#0466c8',
  },
  signup: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(1),
  },
  foot: {
    marginTop: theme.spacing(3),
    padding: theme.spacing(3),
  },
  boutonIcon: {
    marginTop: theme.spacing(2),
    background: '-webkit-linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
  },
  image: {
    [theme.breakpoints.up('md')]: {
      width: '20%',
      position: 'absolute',
      bottom: theme.spacing(6),
      left: theme.spacing(0),
    },
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
}))

function SignUp({ intl }) {
  const { formatMessage } = intl
  const classes = useStyles()

  const { personDispatch } = usePerson()
  const location = useLocation()
  const navigate = useNavigate()

  let validationSchema = yup.object({
    email: yup
      .string()
      .email(formatMessage({ id: 'SignUpEmailErrorMessage' }))
      .required(formatMessage({ id: 'SignUpEmailRequiredErrorMessage' })),
    password: yup
      .string()
      .required(formatMessage({ id: 'SignUpMdpRequiredErrorMessage' }))
  })

  /**
   * prevent the default functionning of click on the element
   * @param {HTMLInputElement} event event generated from the click of the visibility icon
   */
  const [isSubmitting, setIsSubmitting] = useState(false)

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema,
    onSubmit: (values, { resetForm }) => {
      setIsSubmitting(true)
      signIn(values)
        .then((person) => {
          personDispatch({
            type: 'LOAD_PERSON_DATA',
            payload: { ...person, isConnected: true },
          })
          resetForm()
          setIsSubmitting(false)
          navigate(person.is_admin ? '/admin' : person.is_owner ? '/owner' : '/')
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
          setIsSubmitting(false)
        })
    },
  })

  return (
    <Container maxWidth='xs'>
      <div className={classes.paper}>
        <Typography align='center' className={classes.subtitle}>
          {formatMessage({ id: 'SignUpConnectTitle' })}
        </Typography>
        <form className={classes.form} onSubmit={formik.handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                color='secondary'
                className={classes.TextField}
                variant='outlined'
                required
                fullWidth
                id='email'
                label={formatMessage({ id: 'ownerEmail' })}
                name='email'
                type='email'
                autoComplete='email'
                onChange={formik.handleChange}
                value={formik.values.email}
                error={formik.touched.email && formik.errors.email}
                helperText={formik.touched.email && formik.errors.email}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id='password'
                variant='outlined'
                label={formatMessage({ id: 'password' })}
                type='password'
                color='secondary'
                value={formik.values.password}
                onChange={formik.handleChange}
                error={formik.touched.password && formik.errors.password}
                helperText={formik.touched.password && formik.errors.password}
                required
                fullWidth
                onBlur={formik.handleBlur}
              />
            </Grid>
          </Grid>
          <Button
            type='submit'
            fullWidth
            variant='contained'
            color='primary'
            className={classes.submit}
          >
            {formatMessage({ id: 'ConnectionBoutonText' })}
            {isSubmitting && (
              <CircularProgress
                color='secondary'
                style={{ height: '25px', width: '25px', marginInline: '25px' }}
              />
            )}
          </Button>
        </form>
        <Typography align='center' variant='h4'>
          {formatMessage({ id: 'Or' })}
        </Typography>
        <Button
          className={classes.boutonIcon}
          color='secondary'
          fullWidth
          startIcon={<GoogleIcon />}
          size='medium'
          variant='contained'
        >
          {formatMessage({ id: 'ConnectionGoogle' })}
        </Button>
        <Grid container justify='center' className={classes.signup}>
          <Grid item md={6} align='center'>
            <Link
              href='/reset-password'
              style={{ fontSize: '0.8em' }}
              className={classes.link}
            >
              {formatMessage({ id: 'CanNotConnectTitle' })}
            </Link>
          </Grid>
          <Grid item md={6} align='center'>
            <Link
              href='/sign-up'
              style={{ fontSize: '0.8em' }}
              className={classes.link}
            >
              {formatMessage({ id: 'CreateAccount' })}
            </Link>
          </Grid>
        </Grid>
      </div>
      <Typography variant='body2' align='center' className={classes.foot}>
        {formatMessage({ id: 'privacyMessagePart1' })}
        <Link href='#' className={classes.link}>
          {' '}
          {formatMessage({ id: 'privacyMessagePart2' })}{' '}
        </Link>
        {formatMessage({ id: 'privacyMessagePart3' })}
        <Link href='#' className={classes.link}>
          {' '}
          {formatMessage({ id: 'privacyMessagePart4' })}{' '}
        </Link>
        {formatMessage({ id: 'privacyMessagePart5' })}
      </Typography>
      <Grid container>
        <img
          src={womanCartImage}
          alt='ImageManager'
          className={classes.image}
        />
      </Grid>
    </Container>
  )
}

export default injectIntl(SignUp)
