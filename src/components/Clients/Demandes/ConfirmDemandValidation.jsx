import React, { useContext, useState } from 'react'
import { injectIntl } from 'react-intl'
import {
  Button,
  Grid,
  makeStyles,
  Typography,
  Paper,
  CircularProgress,
} from '@material-ui/core'

import Demandes from '../../../contexts/Demandes/Demande.context'
import { usePerson } from '../../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import { sendStructureDemand } from '../../../services/authentication.service'
import handleSessionExpiredError from '../../../utils/handleSessionExpiry'
import ActiveStructureType from '../../../contexts/ActiveStructureType/ActiveStructureType.context'

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: '6px 16px',
    paddingBottom: theme.spacing(3),
  },
  title: {
    color: theme.palette.secondary.main,
    paddingBottom: theme.spacing(1),
    fontSize: theme.typography.h4.fontSize,
  },
  cle: {
    color: theme.common.fontWeightBold,
    paddingBottom: theme.spacing(1),
  },
  valeur: {
    color: theme.common.fontWeightBold,
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
}))

function AddressForm({ intl }) {
  const { formatMessage } = intl

  const {
    activeStep,
    setActiveStep,
    steps,
    ownerInfos,
    infosStructure,
  } = useContext(Demandes)

  const classes = useStyles()

  const { personDispatch } = usePerson()
  const location = useLocation()
  const navigate = useNavigate()
  const [isSubmitting, setIsSubmitting] = useState(false)
  const { structureType } = useContext(ActiveStructureType)

  const handleNext = () => {
    setIsSubmitting(true)
    if (activeStep === steps.length - 1) {
      sendStructureDemand({
        structure_info: {...infosStructure, logo_ref: infosStructure.logo_ref.binary},
        owner_info: { ...ownerInfos, ...infosStructure, logo_ref: infosStructure.logo_ref.binary },
      })
        .then(() => {
          const anchor = document.getElementById('#demandValidation')
          anchor.scrollIntoView();
          setActiveStep(activeStep + 1)
          setIsSubmitting(false)
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate,
            )
            setIsSubmitting(false)
        })
    } else {
      setActiveStep(activeStep + 1)
    }
  }

  const handleBack = () => {
    setActiveStep(activeStep - 1)
  }

  return (
    <React.Fragment>
      <Paper elevation={0} className={classes.paper} id="#demandValidation">
        <Typography className={classes.title}>
          {formatMessage({ id: 'stepOneMessage' })}
        </Typography>

        <Grid md={12} container direction="row">
          <Grid md={5} xs={5}>
            <Typography className={classes.cle}>
              {formatMessage({ id: 'structureNameInput' })} :
            </Typography>
          </Grid>
          <Grid md={7} xs={5}>
            <Typography className={classes.valeur}>
              {infosStructure.name}
            </Typography>
          </Grid>
        </Grid>

        <Grid md={12} container direction="row">
          <Grid md={5} xs={5}>
            <Typography className={classes.cle}>
              {formatMessage({ id: 'addressInput' })} :
            </Typography>
          </Grid>
          <Grid md={7} xs={5}>
            <Typography className={classes.valeur}>
              {infosStructure.place_id}
            </Typography>
          </Grid>
        </Grid>

        <Grid md={12} container direction="row">
          <Grid md={5} xs={5}>
            <Typography className={classes.cle}>
              {formatMessage({ id: 'structureCategoryInput' })}:
            </Typography>
          </Grid>
          <Grid md={7} xs={5}>
            <Typography className={classes.valeur}>
              {structureType.find((_) => _.category_id === infosStructure.category_id)?.category_name}
            </Typography>
          </Grid>
        </Grid>

        <Grid md={12} container direction="row">
          <Grid md={5} xs={5}>
            <Typography className={classes.cle}>
              {formatMessage({ id: 'latitudeInput' })} :
            </Typography>
          </Grid>
          <Grid md={7} xs={5}>
            <Typography className={classes.valeur}>
              {infosStructure.latitude}
            </Typography>
          </Grid>
        </Grid>

        <Grid md={12} container direction="row">
          <Grid md={5} xs={5}>
            <Typography className={classes.cle}>
              {formatMessage({ id: 'longitudeInput' })} :
            </Typography>
          </Grid>
          <Grid md={7} xs={5}>
            <Typography className={classes.valeur}>
              {infosStructure.longitude}
            </Typography>
          </Grid>
        </Grid>

        <Grid md={12} container direction="row">
          <Grid md={5} xs={5}>
            <Typography className={classes.cle}>
              {formatMessage({ id: 'particularIndications' })} :
            </Typography>
          </Grid>
          <Grid md={7} xs={5}>
            <Typography className={classes.valeur}>
              {infosStructure.specific_indications}
            </Typography>
          </Grid>
        </Grid>
      </Paper>

      <Paper elevation={0} className={classes.paper}>
        <Typography className={classes.title}>
          {formatMessage({ id: 'stepTwoMessage' })}
        </Typography>

        <Grid md={12} container direction="row">
          <Grid md={5} xs={5}>
            <Typography className={classes.cle}>
              {formatMessage({ id: 'ownerFirstName' })} :
            </Typography>
          </Grid>
          <Grid md={7} xs={5}>
            <Typography className={classes.valeur}>
              {ownerInfos.last_name}
            </Typography>
          </Grid>
        </Grid>

        <Grid md={12} container direction="row">
          <Grid md={5} xs={5}>
            <Typography className={classes.cle}>
              {formatMessage({ id: 'ownerLastName' })} :
            </Typography>
          </Grid>
          <Grid md={7} xs={5}>
            <Typography className={classes.valeur}>
              {ownerInfos.first_name}
            </Typography>
          </Grid>
        </Grid>

        <Grid md={12} container direction="row">
          <Grid md={5} xs={5}>
            <Typography className={classes.cle}>
              {formatMessage({ id: 'ownerEmail' })} :
            </Typography>
          </Grid>
          <Grid md={7} xs={5}>
            <Typography className={classes.valeur}>
              {ownerInfos.email}
            </Typography>
          </Grid>
        </Grid>

        <Grid md={12} container direction="row">
          <Grid md={5} xs={5}>
            <Typography className={classes.cle}>
              {formatMessage({ id: 'ownerPhone' })} :
            </Typography>
          </Grid>
          <Grid md={7} xs={5}>
            <Typography className={classes.valeur}>
              {ownerInfos.phone}
            </Typography>
          </Grid>
        </Grid>

        <Grid md={12} container direction="row">
          <Grid md={5} xs={5}>
            <Typography className={classes.cle}>
              {formatMessage({ id: 'ownerDateOfBirth' })} :
            </Typography>
          </Grid>
          <Grid md={7} xs={5}>
            <Typography className={classes.valeur}>
              {ownerInfos.date_of_birth}
            </Typography>
          </Grid>
        </Grid>

        <Grid md={12} container direction="row">
          <Grid md={5} xs={5}>
            <Typography className={classes.cle}>
              {formatMessage({ id: 'ownerGender' })} :
            </Typography>
          </Grid>
          <Grid md={7} xs={5}>
            <Typography className={classes.valeur}>
              {ownerInfos.gender}
            </Typography>
          </Grid>
        </Grid>

        <Grid md={12} container direction="row">
          <Grid md={5} xs={5}>
            <Typography className={classes.cle}>
              {formatMessage({ id: 'ownerNationalId' })} :
            </Typography>
          </Grid>
          <Grid md={7} xs={5}>
            <Typography className={classes.valeur}>
              {ownerInfos.national_id_number}
            </Typography>
          </Grid>
        </Grid>
      </Paper>
      <Grid item md={12} sm={12} xs={12}>
        <div className={classes.buttons}>
          {activeStep !== 0 && (
            <Button onClick={handleBack} className={classes.button}>
              {formatMessage({ id: 'backButtonText' })}
            </Button>
          )}
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={handleNext}
          >
            {activeStep === steps.length - 1 ? (
              <>
                {formatMessage({ id: 'sendButtonText' })}
                {isSubmitting && (
                  <CircularProgress style={{ marginLeft: "10px", height: '20px', width: "20px" }} color="secondary" />
                )}
              </>
            ) : (
              formatMessage({ id: 'nextButtonText' })
            )}
          </Button>
        </div>
      </Grid>
    </React.Fragment>
  )
}

export default injectIntl(AddressForm)
