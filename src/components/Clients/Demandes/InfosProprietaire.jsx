import * as yup from 'yup'
import { useFormik } from 'formik'
import { injectIntl } from 'react-intl'
import React, { useContext, useState } from 'react'
import {
  Button,
  FormControl,
  FormLabel,
  makeStyles,
  Radio,
  RadioGroup,
  Checkbox,
  FormControlLabel,
  TextField,
  Grid,
} from '@material-ui/core'

import Demandes from '../../../contexts/Demandes/Demande.context'
import { findUserInfo } from '../../../services/authentication.service'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../../contexts/PersonContext/Person.provider'
import handleSessionExpiredError from '../../../utils/handleSessionExpiry'
import { useEffect } from 'react'

const useStyles = makeStyles((theme) => ({
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
}))

function AddressForm({ intl }) {
  const { formatMessage } = intl
  const classes = useStyles()
  const { changeOwnerInfos, activeStep, setActiveStep, steps, ownerInfos } =
    useContext(Demandes)

  let validationSchema = yup.object({
    first_name: yup
      .string(formatMessage({ id: 'ownerNameStringErrorMessage' }))
      .required(formatMessage({ id: 'ownerNameRequiredErrorMessage' }))
      .min(3, formatMessage({ id: 'ownerNameMinCharacterErrorMessage' })),

    last_name: yup
      .string(formatMessage({ id: 'ownerLastNameStringErrorMessage' }))
      .min(3, formatMessage({ id: 'ownerLastNameMinCharacterErrorMessage' })),

    email: yup
      .string()
      .email(formatMessage({ id: 'ownerEmailErrorMessage' }))
      .required(formatMessage({ id: 'ownerEmailRequiredErrorMessage' })),
    password: yup
      .string()
      .required(formatMessage({ id: 'ownerPasswordRequiredErrorMessage' })),
    confirm_password: yup
      .string()
      .oneOf(
        [yup.ref('password'), null],
        formatMessage({ id: 'personnalInformationNotMatchPasswordsError' })
      )
      .required(formatMessage({ id: 'ownerPasswordRequiredErrorMessage' })),

    phone: yup
      .string()
      .matches(
        /^(6|2)(2|3|[5-9])[0-9]{7}$/,
        formatMessage({ id: 'ownerPhoneErrorMessage' })
      )
      .required(formatMessage({ id: 'ownerPhoneRequiredErrorMessage' })),

    date_of_birth: yup
      .date()
      .max(
        yup.ref('EndDate'),
        formatMessage({ id: 'ownerDateOfBirthMaxDateErrorMessage' })
      )
      .required(formatMessage({ id: 'ownerDateOfBirthRequiredErrorMessage' })),

    national_id_number: yup
      .string(formatMessage({ id: 'ownerNationalIdStringErrorMessage' }))
      .required(formatMessage({ id: 'ownerNationalIdRequiredErrorMessage' })),
  })

  const {
    personDispatch,
    personState: { person },
  } = usePerson()
  const location = useLocation()
  const navigate = useNavigate()

  const formik = useFormik({
    initialValues: {
      first_name: (ownerInfos !== undefined ? ownerInfos.first_name : '') || '',
      last_name: (ownerInfos !== undefined ? ownerInfos.last_name : '') || '',
      email: (ownerInfos !== undefined ? ownerInfos.email : '') || '',
      phone: (ownerInfos !== undefined ? ownerInfos.phone : '') || '',
      date_of_birth:
        (ownerInfos !== undefined ? ownerInfos.date_of_birth : '2021-01-01') ||
        '',
      gender: (ownerInfos !== undefined ? ownerInfos.gender : '') || '',
      national_id_number:
        (ownerInfos !== undefined ? ownerInfos.national_id_number : '') || '',
      EndDate: Date.now(),
      password: '',
      confirm_password: '',
    },
    validationSchema,
    onSubmit: (values) => {
      changeOwnerInfos({ ...person, ...values })
      handleNext()
    },
  })
  let [checkEmailSms, setCheckEmailSms] = useState(person.isConnected)

  let rempli =
    Object.keys(formik.values)
      .map((key) => formik.values[key] === '')
      .includes(true) || !checkEmailSms
  let err = Object.keys(formik.errors).length !== 0
  console.log({ rempli, err, values: formik.values, errors: formik.errors })

  const handleNext = () => {
    setActiveStep(activeStep + 1)
  }

  const handleBack = () => {
    changeOwnerInfos(formik.values)
    setActiveStep(activeStep - 1)
  }

  useEffect(() => {
    findUserInfo()
      .then((person) => {
        personDispatch({ type: 'LOAD_PERSON_DATA', payload: person })
        let values = {}
        Object.keys(person).forEach((key) => {
          if (Object.keys(formik.initialValues).includes(key))
            values = { ...values, [key]: person[key] }
        })
        formik.setValues({
          ...formik.values,
          ...values,
          password: person.email,
          confirm_password: person.email,
        })
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })

    // eslint-disable-next-line
  }, [])

  return (
    <React.Fragment>
      <form className={classes.form} onSubmit={formik.handleSubmit}>
        <Grid container spacing={3}>
          <Grid item md={6} sm={12} xs={12}>
            <TextField
              autoComplete='given-name'
              name='first_name'
              variant='outlined'
              fullWidth
              id='first_name'
              label={formatMessage({ id: 'ownerFirstName' })}
              type='text'
              color='secondary'
              onChange={formik.handleChange}
              value={formik.values.first_name}
              error={formik.errors.first_name ? true : false}
              helperText={formik.errors.first_name}
              disabled={person.isConnected}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextField
              variant='outlined'
              fullWidth
              id='last_name'
              label={`${formatMessage({ id: 'ownerLastName' })}*`}
              name='last_name'
              autoComplete='last_name'
              type='text'
              color='secondary'
              onChange={formik.handleChange}
              value={formik.values.last_name}
              error={formik.errors.last_name ? true : false}
              helperText={formik.errors.last_name}
              disabled={person.isConnected}
            />
          </Grid>
          <Grid item md={12} sm={12} xs={12}>
            <TextField
              name='email'
              variant='outlined'
              fullWidth
              id='email'
              label={`${formatMessage({ id: 'ownerEmail' })}*`}
              type='email'
              color='secondary'
              onChange={formik.handleChange}
              value={formik.values.email}
              error={formik.errors.email ? true : false}
              helperText={formik.errors.email}
              disabled={person.isConnected}
            />
          </Grid>
          {!person.isConnected && (
            <>
              <Grid item md={6} sm={6} xs={6}>
                <TextField
                  name='password'
                  variant='outlined'
                  fullWidth
                  id='password'
                  label={`${formatMessage({ id: 'ownerPassword' })}*`}
                  type='password'
                  color='secondary'
                  onChange={formik.handleChange}
                  value={formik.values.password}
                  error={formik.errors.password ? true : false}
                  helperText={formik.errors.password}
                  disabled={person.isConnected}
                />
              </Grid>
              <Grid item md={6} sm={6} xs={6}>
                <TextField
                  name='confirm_password'
                  variant='outlined'
                  fullWidth
                  id='confirm_password'
                  label={`${formatMessage({ id: 'ownerConfirmPassword' })}*`}
                  type='password'
                  color='secondary'
                  onChange={formik.handleChange}
                  value={formik.values.confirm_password}
                  error={formik.errors.confirm_password ? true : false}
                  helperText={formik.errors.confirm_password}
                  disabled={person.isConnected}
                />
              </Grid>
            </>
          )}
          <Grid item md={6} sm={12} xs={12}>
            <TextField
              name='phone'
              variant='outlined'
              fullWidth
              id='phone'
              label={`${formatMessage({ id: 'ownerPhone' })}*`}
              type='text'
              color='secondary'
              onChange={formik.handleChange}
              value={formik.values.phone}
              error={formik.errors.phone ? true : false}
              helperText={formik.errors.phone}
              disabled={person.isConnected && person.phone !== ''}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextField
              name='date_of_birth'
              id='date_of_birth'
              fullWidth
              variant='outlined'
              label={`${formatMessage({ id: 'ownerDateOfBirth' })}*`}
              type='date'
              color='secondary'
              onChange={formik.handleChange}
              value={formik.values.date_of_birth}
              error={formik.errors.date_of_birth ? true : false}
              helperText={formik.errors.date_of_birth}
              disabled={person.isConnected && person.date_of_birth}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextField
              name='national_id_number'
              variant='outlined'
              fullWidth
              id='national_id_number'
              label={`${formatMessage({ id: 'ownerNationalId' })}*`}
              type='text'
              color='secondary'
              onChange={formik.handleChange}
              value={formik.values.national_id_number}
              error={formik.errors.national_id_number ? true : false}
              helperText={formik.errors.national_id_number}
              disabled={person.isConnected && person.national_id_number}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <FormControl component='fieldset'>
              <FormLabel component='legend' color='secondary'>{`${formatMessage(
                { id: 'ownerGender' }
              )}*`}</FormLabel>
              <RadioGroup
                row
                aria-label='position'
                name='gender'
                id='gender'
                onChange={formik.handleChange}
                value={formik.values.gender}
                disabled={person.isConnected && person.gender}
              >
                <FormControlLabel
                  value='M'
                  control={<Radio color='secondary' size='small' />}
                  label={`${formatMessage({ id: 'genderMale' })}`}
                  disabled={person.isConnected && person.gender}
                />
                <FormControlLabel
                  value='F'
                  control={<Radio color='secondary' size='small' />}
                  label={`${formatMessage({ id: 'genderFemale' })}`}
                  disabled={person.isConnected && person.gender}
                />
              </RadioGroup>
            </FormControl>
          </Grid>
          <Grid item md={12} sm={12} xs={12}>
            <FormControlLabel
              onChange={() => setCheckEmailSms(!checkEmailSms)}
              control={<Checkbox checked={checkEmailSms} color='secondary' />}
              label={`${formatMessage({ id: 'unsubscribeNewsletter' })}`}
              labelPlacement='end'
            />
          </Grid>
          <Grid item md={12} sm={12} xs={12}>
            <div className={classes.buttons}>
              {activeStep !== 0 && (
                <Button
                  onClick={handleBack}
                  className={classes.button}
                  type='submit'
                >
                  {formatMessage({ id: 'backButtonText' })}
                </Button>
              )}
              <Button
                variant='contained'
                color='primary'
                className={classes.button}
                type='submit'
                disabled={rempli || err}
              >
                {activeStep === steps.length - 1
                  ? formatMessage({ id: 'sendButtonText' })
                  : formatMessage({ id: 'nextButtonText' })}
              </Button>
            </div>
          </Grid>
        </Grid>
      </form>
    </React.Fragment>
  )
}

export default injectIntl(AddressForm)
