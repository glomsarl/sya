import React from 'react';
import { injectIntl } from 'react-intl';
import { Grid, Typography, makeStyles } from '@material-ui/core';

import DemandeForm from './DemandeForm';
import ImageManager from '../../../assets/undraw_personal_finance_tqcd.svg';

const useStyles = makeStyles((theme) => ({
    toolbarTitle: {
        // padding: "15px",
        fontSize: theme.typography.h2.fontSize,
        fontWeight:theme.common.fontWeightRegular,
        padding: theme.spacing(1),
    },
    paper: {
        backgroundColor: theme.palette.primary.main,
    },
    grid: {
        paddingTop: theme.spacing(1),
    },
    grid1: {
        padding: "15px",
        [theme.breakpoints.up('md')]: {
            paddingRight: theme.spacing(5),
        },  
    },
    text: {
        fontSize: theme.typography.h4.fontSize,
        color: theme.common.blue,
    },
    imageManager: {
        paddingTop:'30px',
        paddingBottom:'30px',
        width: '50%',
        textAlign: 'center',
        display: 'block',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 'auto',
    },
}));

function CustomSeparator({intl}) {
  const classes = useStyles();
  const { formatMessage } = intl

  return (
    <React.Fragment>
        <Typography variant="h2" color="secondary" className={classes.toolbarTitle}>
            <p>{formatMessage({ id: "demandPageTitle" })}</p>
        </Typography>
        <Grid container className={classes.grid}>
            <Grid item sm={12} md={6} xs={12} className={classes.grid1} >
                <Typography align="justify" component="p" className={classes.text}>
                    {formatMessage({ id:"demandPageCaptiveMessage1" })}
                    <p>
                        {formatMessage({ id:"demandPageCaptiveMessage2" })}
                    </p>
                </Typography>
                <img src={ImageManager} alt="ImageManager" className={classes.imageManager}/>
            </Grid>
            <Grid item sm={12} md={6} xs={12} className={classes.paper}>
                <DemandeForm />
            </Grid>
        </Grid>
    </React.Fragment>
  );
}

export default injectIntl(CustomSeparator)