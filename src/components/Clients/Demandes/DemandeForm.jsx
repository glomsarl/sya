import React, { useContext, useEffect } from 'react'
import { injectIntl } from 'react-intl'
import {
  CssBaseline,
  Paper,
  Stepper,
  Step,
  StepLabel,
  Typography,
  makeStyles,
  Box,
} from '@material-ui/core'

import InfosStructure from '../Demandes/InfosStructure'
import InfosProprietaire from '../Demandes/InfosProprietaire'
import ConfirmDemandValidation from '../Demandes/ConfirmDemandValidation'

import Demandes from '../../../contexts/Demandes/Demande.context'

const useStyles = makeStyles((theme) => ({
  layout: {
    width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
  },
  stepper: {
    padding: theme.spacing(3, 0, 5),
  },
}))

function getStepContent(step) {
  switch (step) {
    case 0:
      return <InfosStructure />
    case 1:
      return <InfosProprietaire />
    case 2:
      return <ConfirmDemandValidation />
    default:
      throw new Error('Unknown step')
  }
}

function Checkout({ intl }) {
  let { formatMessage } = intl
  const classes = useStyles()
  const { activeStep, steps } = useContext(Demandes)

  useEffect(() => {
    const displayer = document.querySelector('#back-to-top-anchor')
    displayer.scrollIntoView()
    // eslint-disable-next-line
  }, [activeStep])

  return (
    <React.Fragment>
      <CssBaseline />
      <main className={classes.layout}>
        <Paper elevation={5} className={classes.paper}>
          <Stepper
            alternativeLabel
            activeStep={activeStep}
            className={classes.stepper}
          >
            {steps.map((label) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          <React.Fragment>
            {activeStep === steps.length ? (
              <React.Fragment>
                <Typography variant='h3'>
                  {formatMessage({ id: 'successStructureCreation1' })}
                </Typography>
                <Box
                  style={{
                    display: 'grid',
                    gridAutoFlow: 'column',
                    gap: '15px',
                  }}
                >
                  <Typography variant='subtitle1'>
                    {formatMessage({ id: 'successStructureCreation2' })}{' '}
                    <a href='/sign-in'>
                      {formatMessage({ id: 'loginNowBouton' })}
                    </a>
                    {formatMessage({ id: 'loginNowText' })}
                  </Typography>
                </Box>
              </React.Fragment>
            ) : (
              <React.Fragment>{getStepContent(activeStep)}</React.Fragment>
            )}
          </React.Fragment>
        </Paper>
      </main>
    </React.Fragment>
  )
}

export default injectIntl(Checkout)
