import * as yup from 'yup'
import { useFormik } from 'formik'
import { injectIntl } from 'react-intl'
import React, { useContext, useState, useEffect } from 'react'
import LocationOnIcon from '@material-ui/icons/LocationOn'
import Autocomplete from '@material-ui/lab/Autocomplete'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  Typography,
  TextField,
  NativeSelect,
  Grid,
  DialogContentText,
  DialogTitle,
  FormControl,
  InputBase,
  InputLabel,
  makeStyles,
  Slide,
  withStyles,
} from '@material-ui/core'

import Demandes from '../../../contexts/Demandes/Demande.context'
import StructureTypes from '../../../contexts/ActiveStructureType/ActiveStructureType.context'
import parse from 'autosuggest-highlight/parse'
import throttle from 'lodash/throttle'
import { getStructureCagetories } from '../../../services/structure.service'
import handleSessionExpiredError from '../../../utils/handleSessionExpiry'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../../contexts/PersonContext/Person.provider'
import { AddRounded } from '@material-ui/icons'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='down' ref={ref} {...props} />
})

const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '15px 26px 15px 15px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
  },
}))(InputBase)

// function for address generation

function loadScript(src, position, id) {
  if (!position) {
    return
  }

  const script = document.createElement('script')
  script.setAttribute('async', '')
  script.setAttribute('id', id)
  script.src = src
  position.appendChild(script)
}

const autocompleteService = { current: null }

// end

const useStyles = makeStyles((theme) => ({
  dialogText: {
    color: theme.common.black,
    fontWeight: theme.common.fontWeightLight,
  },
  icon: {
    color: theme.palette.text.secondary,
    marginRight: theme.spacing(2),
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    marginTop: theme.spacing(1),
    marginLeft: theme.spacing(1),
  },
}))

function AddressForm({ intl }) {
  const { formatMessage } = intl
  const classes = useStyles()
  const {
    ActiveStructure,
    structureType: structureCategories,
    setStructureType,
  } = useContext(StructureTypes)
  const [open, setOpen] = useState(false)
  const { changeInfosStructure, activeStep, setActiveStep, infosStructure } =
    useContext(Demandes)

  // generation of addresses with Google
  const [value, setValue] = useState(null)
  const [inputValue, setInputValue] = useState('')
  const [options, setOptions] = useState([])
  const loaded = React.useRef(false)

  if (typeof window !== 'undefined' && !loaded.current) {
    if (!document.querySelector('#google-maps')) {
      loadScript(
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyDYFQ9bbgFmqBdn_llTxm4gfooXajGYOuE&libraries=places',
        document.querySelector('head'),
        'google-maps'
      )
    }

    loaded.current = true
  }

  const fetch = React.useMemo(
    () =>
      throttle((request, callback) => {
        autocompleteService.current.getPlacePredictions(request, callback)
      }, 200),
    []
  )
  const [isCategoriesLoading, setIsCategoriesLoading] = useState(true)

  const { personDispatch } = usePerson()
  const location = useLocation()
  const navigate = useNavigate()

  useEffect(() => {
    getStructureCagetories()
      .then((categories) => {
        setStructureType(categories)
        setIsCategoriesLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })

    let active = true

    if (!autocompleteService.current && window.google) {
      autocompleteService.current =
        new window.google.maps.places.AutocompleteService()
    }
    if (!autocompleteService.current) {
      return undefined
    }

    if (inputValue === '') {
      setOptions(value ? [value] : [])
      return undefined
    }

    fetch({ input: inputValue }, (results) => {
      if (active) {
        let newOptions = []

        if (value) {
          newOptions = [value]
        }

        if (results) {
          newOptions = [...newOptions, ...results]
        }

        setOptions(newOptions)
      }
    })

    return () => {
      active = false
    }
    // eslint-disable-next-line
  }, [value, inputValue, fetch])

  // end

  let validationSchema = yup.object({
    name: yup
      .string(formatMessage({ id: 'nameStringErrorMessage' }))
      .required(formatMessage({ id: 'nameRequiredErrorMessage' }))
      .min(3, formatMessage({ id: 'nameMinCharacterErrorMessage' })),

    place_id: yup
      .string(formatMessage({ id: 'addressStringErrorMessage' }))
      .required(formatMessage({ id: 'addressRequiredErrorMessage' })),

    category_id: yup
      .string(formatMessage({ id: 'structureCategoryStringErrorMessage' }))
      .required(formatMessage({ id: 'structureCategoryRequiredErrorMessage' })),
  })

  const [structureImage, setStructureImge] = useState(infosStructure?.logo_ref)
  const formik = useFormik({
    initialValues: {
      name: infosStructure?.name || '',
      place_id: infosStructure?.place_id || 'Douala',
      category_id: infosStructure?.category_id || ActiveStructure,
      longitude: infosStructure?.longitude || '',
      latitude: infosStructure?.latitude || '',
      specific_indications: infosStructure?.specific_indications || '',
    },
    validationSchema,
    onSubmit: (values) => {
      changeInfosStructure({ ...values, logo_ref: structureImage })
      handleNext()
    },
  })

  let rempli = Object.keys(formik.values)
    .map((key) => formik.values[key] === '')
    .includes(true)
  let err = Object.keys(formik.errors).length !== 0

  const handleClickOpen = () => {
    setOpen(true)
  }

  let getGPSLocation = async (position) => {
    formik.setFieldValue('longitude', position.coords.longitude)
    formik.setFieldValue('latitude', position.coords.latitude)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleAccept = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(getGPSLocation)
    }
    handleClose()
  }

  const handleNext = () => {
    setActiveStep(activeStep + 1)
  }

  function handleFileInput(e) {
    if (e.target.files.length > 0) {
      let files = Object.keys(e.target.files)
      files = files.map((fileKey) => {
        return e.target.files[fileKey]
      })
      files = { display: URL.createObjectURL(files[0]), binary: files[0] }
      setStructureImge(files)
    }
  }

  return (
    <React.Fragment>
      <form className={classes.form} onSubmit={formik.handleSubmit}>
        <Grid container spacing={3}>
          <Grid item md={7} sm={12} xs={12}>
            <TextField
              disabled={isCategoriesLoading}
              autoComplete='given-name'
              name='name'
              variant='outlined'
              fullWidth
              id='name'
              label={`${formatMessage({ id: 'structureNameInput' })}`}
              required
              type='text'
              color='secondary'
              onChange={formik.handleChange}
              value={formik.values.name}
              error={formik.errors.name ? true : false}
              helperText={formik.errors.name}
            />
          </Grid>
          <Grid item md={5} sm={12} xs={12}>
            <Autocomplete
              disabled={isCategoriesLoading}
              name='place_id'
              id='google-map-demo'
              getOptionLabel={(option) =>
                typeof option === 'string' ? option : option.description
              }
              filterOptions={(x) => x}
              options={options}
              autoComplete
              includeInputInList
              filterSelectedOptions
              value={formik.values.place_id}
              error={formik.errors.place_id ? true : false}
              helperText={formik.errors.place_id}
              onChange={(event, newValue) => {
                setOptions(newValue ? [newValue, ...options] : options)
                setValue(newValue)
                if (newValue === null) formik.setFieldValue('place_id', '')
                else formik.setFieldValue('place_id', newValue.description)
              }}
              onInputChange={(event, newInputValue) => {
                setInputValue(newInputValue)
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label={`${formatMessage({ id: 'addressInput' })}`}
                  required
                  color='secondary'
                  variant='outlined'
                  fullWidth
                />
              )}
              renderOption={(option) => {
                const matches =
                  option.structured_formatting.main_text_matched_substrings
                const parts = parse(
                  option.structured_formatting.main_text,
                  matches.map((match) => [
                    match.offset,
                    match.offset + match.length,
                  ])
                )
                return (
                  <Grid container alignItems='center'>
                    <Grid item>
                      <LocationOnIcon className={classes.icon} />
                    </Grid>
                    <Grid item xs>
                      {parts !== undefined
                        ? parts.map((part, index) => (
                            <span
                              key={index}
                              style={{ fontWeight: part.highlight ? 700 : 400 }}
                            >
                              {part.text}
                            </span>
                          ))
                        : null}

                      <Typography variant='body2' color='textSecondary'>
                        {option.structured_formatting.secondary_text}
                      </Typography>
                    </Grid>
                  </Grid>
                )
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <FormControl className={classes.margin} color='secondary' fullWidth>
              <InputLabel htmlFor='demo-customized-select-native'>
                {formatMessage({ id: 'structureCategoryInput' })}
              </InputLabel>
              <NativeSelect
                disabled={isCategoriesLoading}
                id='category_id'
                name='category_id'
                onChange={formik.handleChange}
                value={formik.values.category_id}
                helperText={formik.errors.category_id}
                defaultValue={structureCategories[0]?.category_id}
                error={formik.errors.category_id ? true : false}
                input={<BootstrapInput />}
              >
                {structureCategories.map(({ category_id, category_name }) => (
                  <option key={category_id} value={category_id}>
                    {category_name ? formatMessage({ id: category_name }) : ''}
                  </option>
                ))}
              </NativeSelect>
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <TextField
              disabled={isCategoriesLoading}
              variant='outlined'
              fullWidth
              name='specific_indications'
              label={formatMessage({ id: 'specific_indications' })}
              required
              type='text'
              id='specific_indications'
              multiline
              rows={4}
              color='secondary'
              onChange={formik.handleChange}
              value={formik.values.specific_indications}
            />
          </Grid>
          <Grid item md={12} sm={12} xs={12}>
            <Button
              variant='outlined'
              color='secondary'
              onClick={handleClickOpen}
            >
              {formatMessage({ id: 'getPositionButtonText' })}
            </Button>
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextField
              disabled={!open}
              name='latitude'
              variant='outlined'
              fullWidth
              id='latitude'
              label={`${formatMessage({ id: 'latitudeInput' })}`}
              required
              type='text'
              color='secondary'
              value={formik.values.latitude}
              error={formik.errors.latitude ? true : false}
              helperText={formik.errors.latitude}
            />
          </Grid>
          <Grid item md={6} sm={12} xs={12}>
            <TextField
              required
              fullWidth
              type='text'
              id='longitude'
              name='longitude'
              color='secondary'
              variant='outlined'
              disabled={!open}
              value={formik.values.longitude}
              helperText={formik.errors.longitude}
              error={formik.errors.longitude ? true : false}
              label={formatMessage({ id: 'longitudeInput' })}
            />
          </Grid>

          {structureImage?.display ? (
            <Grid item>
              <img
                src={structureImage?.display}
                alt='structureImage'
                style={{ height: '200px', objectFit: 'scale-down' }}
              />
            </Grid>
          ) : null}

          <Grid item>
            <input
              accept='image/*'
              hidden
              id='structure-image'
              type='file'
              onChange={handleFileInput}
            />
            <label htmlFor='structure-image'>
              <Button component='span' variant='outlined' color='secondary'>
                <AddRounded />
                {formatMessage({
                  id: structureImage?.display ? 'changeImage' : 'addImage',
                })}
              </Button>
            </label>
          </Grid>

          <Grid item md={12} sm={12} xs={12}>
            <Typography className={classes.foot}>
              {formatMessage({ id: 'particularMessageHelperText' })}
            </Typography>
          </Grid>
          <Grid item md={12} sm={12} xs={12} className={classes.buttons}>
            <Button
              variant='contained'
              color='primary'
              className={classes.button}
              type='submit'
              disabled={rempli || err || !structureImage}
            >
              {formatMessage({ id: 'nextButtonText' })}
            </Button>
          </Grid>
        </Grid>
      </form>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby='alert-dialog-slide-title'
        aria-describedby='alert-dialog-slide-description'
      >
        <DialogTitle id='alert-dialog-slide-title'>
          {formatMessage({ id: 'useGoogleLocalisationHeader' })}
        </DialogTitle>
        <DialogContent>
          <DialogContentText
            id='alert-dialog-slide-description'
            align='justify'
            className={classes.dialogText}
          >
            <Typography variant='subtitle2' color='textSecondary'>
              <Typography>
                {formatMessage({ id: 'useGoogleLocalisationBody1' })}
              </Typography>
              <Typography style={{ color: 'red', marginTop: '10px' }}>
                {formatMessage({ id: 'useGoogleLocalisationBody2' })}
              </Typography>
            </Typography>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color='secondary'>
            {formatMessage({ id: 'disagreeGoogleLocalisation' })}
          </Button>
          <Button onClick={handleAccept} color='secondary'>
            {formatMessage({ id: 'agreeGoogleLocalisation' })}
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  )
}

export default injectIntl(AddressForm)
