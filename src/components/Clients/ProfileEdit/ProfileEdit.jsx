import * as yup from 'yup'
import React, { useEffect, useState } from 'react'
import { useFormik } from 'formik'
import { injectIntl } from 'react-intl'
import { usePerson } from '../../../contexts/PersonContext/Person.provider'
import {
  Typography,
  Box,
  makeStyles,
  FormControlLabel,
  TextField,
  RadioGroup,
  Radio,
  Button,
  CircularProgress,
  Tooltip,
  IconButton,
  Table,
  TableHead,
  TableCell,
  TableBody,
  TableRow,
} from '@material-ui/core'
import { Close } from '@material-ui/icons'
import {
  updateProfile,
  getSyPoints,
} from '../../../services/authentication.service'
import handleSessionExpiredError from '../../../utils/handleSessionExpiry'
import { useLocation, useNavigate } from 'react-router'
import { Skeleton } from '@material-ui/lab'

const useStyles = makeStyles((theme) => ({
  Main: {
    maxWidth: '350px',
  },
  Profile: {
    display: 'grid',
    gridAutoFlow: 'row',
    gap: '10px',
    marginInline: theme.spacing(2),
    padding: theme.spacing(2),
    maxWidth: '100%',
    border: `1px solid ${theme.palette.divider}`,
  },
  Close: {
    justifySelf: 'right',
    width: '50px',
    height: '50px',
  },
  Header: {
    paddingInline: theme.spacing(2),
    display: 'grid',
    gridAutoFlow: 'column',
    alignItems: 'center',
  },
  HeadTitle: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  RadioGroup: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
  },
  GridBox: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
  },
  TextField: {
    backgroundColor: '#FFFF',
    borderBottom: '1px solid grey',
  },
  Spinner: {
    width: 'fit-content',
    justifySelf: 'right',
    marginTop: '10px',
  },
}))

function ProfileEdit({ intl: { formatMessage }, closeHandler }) {
  let classes = useStyles()
  const navigate = useNavigate()
  const location = useLocation()
  const {
    personState: {
      person: {
        first_name,
        last_name,
        email,
        phone,
        gender,
        date_of_birth,
        person_id,
        isConnected,
      },
    },
    personDispatch,
  } = usePerson()

  let [isSubmitting, setIsSubmitting] = useState(false)

  const validationSchema = yup.object({
    last_name: yup
      .string('')
      .required(formatMessage({ id: 'requiredFieldMessage' })),
    first_name: yup
      .string('')
      .required(formatMessage({ id: 'requiredFieldMessage' })),
    email: yup
      .string('')
      .email(formatMessage({ id: 'invalidEmail' }))
      .required(formatMessage({ id: 'requiredFieldMessage' })),
    phone: yup
      .string()
      .matches(
        /^(6|2)(2|3|[5-9])[0-9]{7}$/,
        formatMessage({ id: 'invalidPhoneNumber' })
      )
      .required(formatMessage({ id: 'requiredFieldMessage' })),
    date_of_birth: yup.string(''),
  })
  const formik = useFormik({
    initialValues: {
      first_name,
      last_name,
      email,
      phone,
      gender,
      date_of_birth,
    },
    validationSchema,
    onSubmit: (values) => {
      setIsSubmitting(true)
      updateProfile(person_id, {
        ...values,
        // person_image_ref: 'imageRef.binary',
      })
        .then(() => {
          formik.setValues(values)
          personDispatch({
            type: 'LOAD_PERSON_DATA',
            payload: values,
          })
          setIsSubmitting(false)
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
          setIsSubmitting(false)
        })
    },
  })

  const [isBonusLoading, setIsBonusLoading] = useState(false)
  const [syPoints, setSyPoints] = useState([])
  useEffect(() => {
    setIsBonusLoading(true)
    getSyPoints(person_id)
      .then((syPoints) => {
        setSyPoints(syPoints)
        setIsBonusLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsBonusLoading(false)
      })
    // eslint-disable-next-line
  }, [])

  return (
    <Box className={classes.Main}>
      <Box className={classes.Header}>
        <Typography
          variant='h4'
          color='secondary'
          className={classes.HeadTitle}
        >
          {formatMessage({ id: 'profile' })}
        </Typography>
        <Tooltip arrow title='Close'>
          <IconButton className={classes.Close} onClick={closeHandler}>
            <Close />
          </IconButton>
        </Tooltip>
      </Box>
      <form className={classes.Profile} onSubmit={formik.handleSubmit}>
        <Typography variant='subtitle1'>
          {formatMessage({ id: 'profileUse' })}
        </Typography>
        <Typography variant='body2' style={{ fontStyle: 'italic' }}>
          * {formatMessage({ id: 'obligated' })}
        </Typography>
        <Typography variant='body2'>
          {formatMessage({ id: 'gender' })}
        </Typography>
        <RadioGroup className={classes.RadioGroup}>
          <FormControlLabel
            value='M'
            control={
              <Radio name='gender' checked={formik.values.gender === 'M'} />
            }
            color='textSecondary'
            onChange={formik.handleChange}
            label={formatMessage({ id: 'man' })}
          />
          <FormControlLabel
            value='F'
            control={
              <Radio name='gender' checked={formik.values.gender === 'F'} />
            }
            color='textSecondary'
            onChange={formik.handleChange}
            label={formatMessage({ id: 'feminin' })}
          />
        </RadioGroup>
        <Box className={classes.GridBox}>
          <TextField
            name='first_name'
            variant='standard'
            className={classes.TextField}
            onChange={formik.handleChange}
            value={formik.values.first_name || ''}
            label={`*${formatMessage({ id: 'firstName' })}`}
            error={
              formik.touched.first_name && Boolean(formik.errors.first_name)
            }
            helperText={formik.touched.first_name && formik.errors.first_name}
          />
          <TextField
            name='last_name'
            variant='standard'
            className={classes.TextField}
            onChange={formik.handleChange}
            value={formik.values.last_name || ''}
            label={`*${formatMessage({ id: 'firstName' })}`}
            error={formik.touched.last_name && Boolean(formik.errors.last_name)}
            helperText={formik.touched.last_name && formik.errors.last_name}
          />
        </Box>
        <Box className={classes.GridBox}>
          <TextField
            name='phone'
            variant='standard'
            className={classes.TextField}
            value={formik.values.phone}
            onChange={formik.handleChange}
            label={`*${formatMessage({ id: 'phone' })}`}
            error={formik.touched.phone && Boolean(formik.errors.phone)}
            helperText={formik.touched.phone && formik.errors.phone}
          />
          <TextField
            type='date'
            name='date_of_birth'
            variant='standard'
            className={classes.TextField}
            value={formik.values.date_of_birth}
            onChange={formik.handleChange}
            error={
              formik.touched.date_of_birth &&
              Boolean(formik.errors.date_of_birth)
            }
            helperText={
              formik.touched.date_of_birth && formik.errors.date_of_birth
            }
          />
        </Box>
        <TextField
          name='email'
          variant='standard'
          className={classes.TextField}
          disabled={isConnected}
          value={formik.values.email}
          onChange={formik.handleChange}
          label={`*${formatMessage({ id: 'emailAddress' })}`}
          error={formik.touched.email && Boolean(formik.errors.email)}
          helperText={formik.touched.email && formik.errors.email}
        />
        <Button
          variant='contained'
          color='secondary'
          type='submit'
          disabled={
            isSubmitting ||
            (formik.values.first_name === first_name &&
              formik.values.last_name === last_name &&
              formik.values.email === email &&
              formik.values.phone === phone &&
              formik.values.gender === gender &&
              formik.values.date_of_birth === date_of_birth)
          }
          className={classes.Spinner}
        >
          {formatMessage({ id: 'update' })}
          {isSubmitting && (
            <CircularProgress
              style={{ width: '25px', height: '25px', margin: '0px 10px' }}
            />
          )}
        </Button>
      </form>
      <Table>
        <TableHead>
          <TableCell>{formatMessage({ id: 'category' })}</TableCell>
          <TableCell>{formatMessage({ id: 'bonusValue' })}</TableCell>
        </TableHead>
        <TableBody>
          {isBonusLoading
            ? new Array(4).map((elt, index) => (
                <React.Fragment key={index}>
                  <Skeleton variant='rect' height={5} />
                  <Skeleton variant='rect' height={5} />
                </React.Fragment>
              ))
            : syPoints.map(({ person_bonus_id, category_name, value }) => (
                <TableRow key={person_bonus_id}>
                  <TableCell>{formatMessage({ id: category_name })}</TableCell>
                  <TableCell>{value}</TableCell>
                </TableRow>
              ))}
        </TableBody>
      </Table>
    </Box>
  )
}

export default injectIntl(ProfileEdit)
