import { injectIntl } from 'react-intl'
import {
  Box,
  Typography,
  Divider,
  makeStyles,
  Tooltip,
  IconButton,
} from '@material-ui/core'
import {
  Home,
  ExitToApp,
  AccountCircle,
  LibraryBooksTwoTone,
  Dashboard,
  Link,
} from '@material-ui/icons'
import { useNavigate } from 'react-router'
import { usePerson } from '../../../contexts/PersonContext/Person.provider'

const useStyles = makeStyles((theme) => ({
  Profile: {
    width: '250px',
    display: 'grid',
    height: '100%',
    gridTemplateRows: '1fr auto',
  },
  Account: {
    width: '100px',
    height: '100px',
    justifySelf: 'center',
    color: theme.palette.secondary.main,
    [theme.breakpoints.down('xs')]: {
      marginLeft: '10px',
    },
  },
  AccountBox: {
    justifySelf: 'center',
    textAlign: 'center',
    padding: '20px 0px',
    display: 'grid',
  },
  Option: {
    display: 'flex',
    padding: '10px',
    cursor: 'pointer',
    '&:hover': {
      transition: '0.2s ease-in',
      transform: 'translate(25px)',
    },
  },
  Label: {
    marginInline: '10px',
    fontSize: 'large',
  },
  Active: {
    backgroundColor: theme.palette.secondary.main,
  },
  CloseIcon: {
    justifySelf: 'right',
    width: 'fit-content',
    height: 'fit-content',
  },
  Names: {
    fontWeight: 'bold',
    textAlign: 'center',
    [theme.breakpoints.down('xs')]: {
      fontSize: 'large',
    },
  },
}))

function UserProfile({
  person: { first_name, last_name, sya_amount: bonus_acquired, referral_link },
  intl: { formatMessage },
  handleDetails,
  handleLogOut,
}) {
  let classes = useStyles()
  const navigate = useNavigate()
  const {
    personState: {
      person: { is_admin, is_owner },
    },
  } = usePerson()

  return (
    <Box className={classes.Profile}>
      <Box>
        <Box className={classes.AccountBox}>
          <AccountCircle className={classes.Account} />
          <Typography variant='h5' color='secondary' className={classes.Names}>
            {first_name} {last_name}
            <br />
            {bonus_acquired} SY
          </Typography>
          <Typography variant='caption' color='primary'>
            {referral_link}
            <Tooltip title={formatMessage({ id: 'clickToCopy' })}>
              <IconButton
                onClick={() => navigator.clipboard.writeText(referral_link)}
              >
                <Link />
              </IconButton>
            </Tooltip>
          </Typography>
        </Box>
        <Divider />
        <Box className={classes.Option} onClick={() => handleDetails('info')}>
          <AccountCircle color='secondary' style={{ fontSize: 35 }} />
          <Typography className={classes.Label} variant='h5'>
            {formatMessage({ id: 'myInfo' })}
          </Typography>
        </Box>
        <Divider />
        <Box className={classes.Option} onClick={() => handleDetails('order')}>
          <LibraryBooksTwoTone color='secondary' style={{ fontSize: 35 }} />
          <Typography className={classes.Label} variant='h5'>
            {formatMessage({ id: 'myOrder' })}
          </Typography>
        </Box>
        <Divider />
        <Box className={classes.Option} onClick={() => navigate('/demand')}>
          <Home color='secondary' style={{ fontSize: 35 }} />
          <Typography className={classes.Label} variant='h5'>
            {formatMessage({ id: 'register' })}
          </Typography>
        </Box>
        <Divider />
        {is_admin || is_owner ? (
          <Box className={classes.Option}>
            <Dashboard color='secondary' style={{ fontSize: 35 }} />
            <Typography
              size='small'
              variant='text'
              onClick={() => navigate(is_owner ? '/owner' : '/admin')}
              className={classes.Label}
            >
              {formatMessage({
                id: is_owner ? 'manageStructures' : 'goToAdmin',
              })}
            </Typography>
          </Box>
        ) : null}
      </Box>
      <Divider />
      <Box className={classes.Option} onClick={() => handleLogOut()}>
        <ExitToApp color='secondary' style={{ fontSize: 35 }} />
        <Typography className={classes.Label} variant='h5'>
          {formatMessage({ id: 'signOut' })}
        </Typography>
      </Box>
    </Box>
  )
}

export default injectIntl(UserProfile)
