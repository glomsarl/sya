import * as yup from "yup";
import { useFormik } from "formik";
import { injectIntl } from "react-intl";
import React, { useState } from "react";
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import {
  makeStyles,
  CircularProgress,
  Typography,
  Grid,
  TextField,
  Button,
  Container,
} from "@material-ui/core";

import GoogleIcon from "../../../assets/icons/Google";
import Image from "../../../assets/undraw_shopping_app_flsj.svg";

import { usePerson } from "../../../contexts/PersonContext/Person.provider";
import { register } from "../../../services/authentication.service";
import handleSessionExpiredError from "../../../utils/handleSessionExpiry";

const useStyles = makeStyles((theme) => ({
  paper: {
    [theme.breakpoints.up("md")]: {
      marginTop: theme.spacing(5),
      padding: theme.spacing(4),
      alignItems: "center",
      display: "flex",
      flexDirection: "column",
      boxShadow: "0 3px 5px 2px rgba(0, 0, 0, .2)",
    },
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  title: {
    marginTop: theme.spacing(3),
    fontWeight: theme.common.fontWeightRegular2,
    cursor: "pointer",
    "&:hover": {
      textDecoration: "none",
    },
  },
  title2: {
    color: "#012a4a",
    fontWeight: theme.common.fontWeightRegular2,
    marginBottom: theme.spacing(2),
  },
  link: {
    color: "#0466c8",
  },
  signin: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
  },
  foot: {
    marginTop: theme.spacing(3),
    padding: theme.spacing(3),
  },
  boutonIcon: {
    marginTop: theme.spacing(2),
    background: "-webkit-linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
  },
  Image: {
    position: "absolute",
    top: theme.spacing(60),
    left: theme.spacing(0),
    [theme.breakpoints.up("md")]: {
      width: "20%",
    },
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
  },
}));

function SignUp({ intl }) {
  const { formatMessage } = intl;

  const classes = useStyles();
  const [isLoading, setIsLoading] = useState(false);

  const { personDispatch } = usePerson();
  const location = useLocation();
  const params = useParams();
  const navigate = useNavigate();

  let validationSchema = yup.object({
    first_name: yup
      .string(formatMessage({ id: "SignUpFirstNameStringErrorMessage" }))
      .required(formatMessage({ id: "SignUpFirstNameRequiredErrorMessage" }))
      .min(3, formatMessage({ id: "SignUpFirstNameMinCharacterErrorMessage" })),

    last_name: yup
      .string(formatMessage({ id: "SignUpLastNameStringErrorMessage" }))
      .required(formatMessage({ id: "SignUpLastNameRequiredErrorMessage" }))
      .min(3, formatMessage({ id: "SignUpLastNameMinCharacterErrorMessage" })),

    email: yup
      .string()
      .email(formatMessage({ id: "SignUpEmailErrorMessage" }))
      .required(formatMessage({ id: "SignUpEmailRequiredErrorMessage" })),

    password: yup
      .string()
      .required(formatMessage({ id: "SignUpMdpRequiredErrorMessage" }))
      .min(3, formatMessage({ id: "SignUpMdpErrorMessage" })),
  });

  const formik = useFormik({
    initialValues: {
      first_name: "",
      last_name: "",
      email: "",
      password: "",
    },
    validationSchema,
    onSubmit: (values, { resetForm }) => {
      setIsLoading(true);
        register({
          ...values,
          referral_id: params.person_id
            ? params.person_id.length !== 36
              ? null
              : params.person_id
            : null,
        })
          .then((logPerson) => {
            personDispatch({ type: "LOAD_PERSON_DATA", payload: logPerson });
            setIsLoading(false);
            resetForm();
            navigate("/");
          })
          .catch((error) => {
            handleSessionExpiredError(
              personDispatch,
              location.pathname,
              error,
              formatMessage,
              navigate
            );
            setIsLoading(false);
          });
    },
  });

  let rempli = Object.keys(formik.values)
    .map((key) => formik.values[key] === "")
    .includes(true);
  let err = Object.keys(formik.errors).length !== 0;

  return (
    <Container maxWidth="xs">
      <div className={classes.paper}>
        <Typography align="center" className={classes.title2}>
          {formatMessage({ id: "CreateAccount" })}
        </Typography>
        <form className={classes.form} onSubmit={formik.handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                color="secondary"
                autoComplete="given-name"
                name="first_name"
                variant="outlined"
                fullWidth
                required
                id="first_name"
                label={formatMessage({ id: "first_name" })}
                autoFocus
                type="text"
                onChange={formik.handleChange}
                value={formik.values.first_name}
                error={formik.touched.first_name && formik.errors.first_name}
                helperText={
                  formik.touched.first_name && formik.errors.first_name
                }
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                color="secondary"
                className={classes.TextField}
                autoComplete="given-name"
                variant="outlined"
                fullWidth
                required
                id="last_name"
                label={formatMessage({ id: "last_name" })}
                name="last_name"
                type="text"
                onChange={formik.handleChange}
                value={formik.values.last_name}
                error={formik.touched.last_name && formik.errors.last_name}
                helperText={formik.touched.last_name && formik.errors.last_name}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                color="secondary"
                className={classes.TextField}
                variant="outlined"
                required
                fullWidth
                id="email"
                label={formatMessage({ id: "addressEmail" })}
                name="email"
                type="email"
                autoComplete="email"
                onChange={formik.handleChange}
                value={formik.values.email}
                error={formik.touched.email && formik.errors.email}
                helperText={formik.touched.email && formik.errors.email}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                color="secondary"
                variant="outlined"
                required
                fullWidth
                name="password"
                label={formatMessage({ id: "passwordField" })}
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={formik.handleChange}
                value={formik.values.password}
                error={formik.touched.password && formik.errors.password}
                helperText={formik.touched.password && formik.errors.password}
              />
            </Grid>
            <Grid item xs={12}>
              <Typography variant="body2">
                {formatMessage({ id: "privacyMessageSignPart1" })}
                <Link to="#" className={classes.link}>
                  {" "}
                  {formatMessage({ id: "privacyMessagePart4" })} SYA{" "}
                </Link>
                {formatMessage({ id: "privacyMessageSignPart2" })}
                <Link to="#" className={classes.link}>
                  {" "}
                  {formatMessage({ id: "privacyMessagePart2" })}
                </Link>
              </Typography>
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={rempli || err || isLoading}
          >
            {isLoading && (
              <CircularProgress
                color="secondary"
                style={{ width: "25px", height: "25px", marginInline: 10 }}
              />
            )}
            {formatMessage({ id: "signInBoutonText" })}
          </Button>
        </form>
        <Typography align="center" variant="h4">
          {formatMessage({ id: "Or" })}
        </Typography>
        <Button
          className={classes.boutonIcon}
          color="secondary"
          fullWidth
          startIcon={<GoogleIcon />}
          size="medium"
          variant="contained"
        >
          {formatMessage({ id: "ConnectionGoogle" })}
        </Button>
        <Grid container justify="center" className={classes.signin}>
          <Grid item>
            <Link to="#" style={{ fontSize: "0.8em" }} className={classes.link}>
              {formatMessage({ id: "accountExist" })}
            </Link>
          </Grid>
        </Grid>
      </div>
      <Typography variant="body2" align="center" className={classes.foot}>
        {formatMessage({ id: "privacyMessagePart1" })}
        <Link to="#" className={classes.link}>
          {" "}
          {formatMessage({ id: "privacyMessagePart2" })}{" "}
        </Link>
        {formatMessage({ id: "privacyMessagePart3" })}
        <Link to="#" className={classes.link}>
          {" "}
          {formatMessage({ id: "privacyMessagePart4" })}{" "}
        </Link>
        {formatMessage({ id: "privacyMessagePart5" })}
      </Typography>
      <Grid container>
        <img src={Image} alt="ImageManager" className={classes.Image} />
      </Grid>
    </Container>
  );
}

export default injectIntl(SignUp);
