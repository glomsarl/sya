import React, { useState } from 'react'
import Button from '@material-ui/core/Button'
import Link from '@material-ui/core/Link'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import { useFormik } from 'formik'
import { injectIntl } from 'react-intl'
import * as yup from 'yup'
import { CircularProgress, IconButton, TextField } from '@material-ui/core'
import { Visibility, VisibilityOff } from '@material-ui/icons'
import ResetPassword from './ResetPassword'
import { useLocation, useNavigate, useParams } from 'react-router-dom'
import { setNewPassword } from '../../../services/authentication.service'
import handleSessionExpiredError from '../../../utils/handleSessionExpiry'
import { usePerson } from '../../../contexts/PersonContext/Person.provider'

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
    display: 'grid',
    gridGap: '5px',
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  title2: {
    color: '#012a4a',
    fontWeight: theme.common.fontWeightRegular2,
    marginBottom: theme.spacing(2),
  },
  link: {
    color: '#0466c8',
  },
  button: {
    boxShadow: 'none',
  },
}))

function EmailForm({ intl }) {
  const classes = useStyles()
  const { formatMessage } = intl
  const [isLoading, setLoading] = React.useState(false)
  const [isPasswordVisible, setIsPasswordVisible] = useState(false)
  const [
    isPasswordVisibleConfirmMdp,
    setIsPasswordVisibleConfirmMdp,
  ] = useState(false)

  /**
   * Toggle the visibility of the password
   */
  let handleTogglePasswordView = () => {
    setIsPasswordVisible(!isPasswordVisible)
  }

  let handleToggleConfirmPasswordView = () => {
    setIsPasswordVisibleConfirmMdp(!isPasswordVisibleConfirmMdp)
  }

  let validationSchema = yup.object().shape({
    password: yup
      .string()
      .required(formatMessage({ id: 'SignUpMdpRequiredErrorMessage' }))
      .min(8, formatMessage({ id: 'SignUpMdpErrorMessage' })),
    confirm_password: yup
      .string()
      .required(formatMessage({ id: 'confirmPasswordRequired' }))
      .oneOf(
        [yup.ref('password'), null],
        formatMessage({ id: 'BothPasswordSame' }),
      ),
  })
  
  const params = useParams()
  const location = useLocation();
  const navigate = useNavigate();
  const { personDispatch } = usePerson();

  const formik = useFormik({
    initialValues: {
      password: '',
      confirm_password: '',
    },
    validationSchema,
    onSubmit: (values) => {
      setLoading(true)
      const { link_id, email } = params

      setNewPassword({
        email,
        link: link_id,
        new_password: values.password,
      })
        .then(() => {
          setLoading(false)
          formik.resetForm();
          navigate("/sign-in")
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate,
            )
            setLoading(false)
        })
    },
  })

  return (
    <ResetPassword>
      <Typography align="center" className={classes.title2}>
        {formatMessage({ id: 'ChooseNewPassword' })}
      </Typography>
      <form className={classes.form} onSubmit={formik.handleSubmit}>
        <TextField
          id="password"
          type={isPasswordVisible ? 'text' : 'password'}
          label={formatMessage({ id: 'NewMdp' })}
          variant="outlined"
          onChange={formik.handleChange}
          value={formik.values.password}
          error={formik.touched.password && formik.errors.password}
          helperText={formik.touched.password && formik.errors.password}
          required
          fullWidth
          onBlur={formik.handleBlur}
          InputProps={{
            endAdornment: (
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleTogglePasswordView}
              >
                {isPasswordVisible ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            ),
          }}
        />
        <TextField
          id="confirm_password"
          fullWidth
          type={isPasswordVisibleConfirmMdp ? 'text' : 'password'}
          color="secondary"
          variant="outlined"
          label={formatMessage({ id: 'MdpConfirm' })}
          onChange={formik.handleChange}
          value={formik.values.confirm_password}
          error={
            formik.touched.confirm_password && formik.errors.confirm_password
          }
          helperText={
            formik.touched.confirm_password && formik.errors.confirm_password
          }
          required
          onBlur={formik.handleBlur}
          InputProps={{
            endAdornment: (
              <IconButton
                aria-label="toggle confirm password visibility"
                onClick={handleToggleConfirmPasswordView}
              >
                {isPasswordVisibleConfirmMdp ? (
                  <Visibility />
                ) : (
                  <VisibilityOff />
                )}
              </IconButton>
            ),
          }}
        />
        <div className={classes.submit}>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            fullWidth
            className={classes.button}
            disabled={isLoading}
          >
            {isLoading && (
              <CircularProgress style={{ width: 25, height: 25 }} />
            )}
            {formatMessage({ id: 'SaveMessage' })}
          </Button>
        </div>
      </form>
      <Grid container justify="center" className={classes.signup}>
        <Grid item align="center">
          <Link href="#" style={{ fontSize: '0.8em' }} className={classes.link}>
            {formatMessage({ id: 'BackToConnectionText' })}
          </Link>
        </Grid>
      </Grid>
    </ResetPassword>
  )
}

export default injectIntl(EmailForm)
