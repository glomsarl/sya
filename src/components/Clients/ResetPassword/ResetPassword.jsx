import React from 'react';
import { Link, } from 'react-router-dom';
import { Typography, Box, makeStyles, Container, Grid } from '@material-ui/core';
import { injectIntl } from "react-intl";
import Image from '../../../assets/undraw_shopping_app_flsj.svg';

const useStyles = makeStyles((theme) => ({
  paper: {
    [theme.breakpoints.up('sm')]: {
      marginTop: theme.spacing(4),
      padding: theme.spacing(4),
      alignItems: 'center',
      display: 'flex',
      flexDirection: 'column',
      boxShadow: '0 3px 5px 2px rgba(0, 0, 0, .2)',
    },
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  title: {
    [theme.breakpoints.up('md')]: {
      marginTop: theme.spacing(5),
    },
    [theme.breakpoints.down('sm')]: {
      marginTop: theme.spacing(1),
    },
    fontWeight:theme.common.fontWeightRegular,
    cursor:"pointer",
    '&:hover':{
      textDecoration:"none",
    }
  },
  title2: {
    color:"#012a4a",
    fontWeight:theme.common.fontWeightRegular2,
    marginBottom:theme.spacing(2),
  },
  link:{
    color:"#0466c8",
  },
  signup:{
    marginTop:theme.spacing(3),
    marginBottom:theme.spacing(1),
  },
  foot:{
    marginTop:theme.spacing(1),
    paddingLeft:theme.spacing(3),
    paddingRight:theme.spacing(3),
    paddingBottom:theme.spacing(2),
  },
  boutonIcon:{
    marginTop:theme.spacing(2),
    background: "-webkit-linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
  },
  image:{
    [theme.breakpoints.up('sm')]: {
      width:"20%",
    },
    [theme.breakpoints.down('xs')]: {
      display:"none",
    },
  }
}));

function ResetPassword({ intl, children }) {

  const classes = useStyles();

  let { formatMessage } = intl;

  return (
    <Box style={{ position: 'relative' }}>
      <Container maxWidth="xs" style={{textAlign: "center"}}>
        <Grid container spacing={12}
          direction="row"
          justify="center"
          alignItems="center"
        >
          <Grid item >
            <Typography variant="h2" color="primary" className={classes.title}>
                See You Again
            </Typography>
          </Grid>
        </Grid>
        <div className={classes.paper}>
          {children}
        </div>
        <Typography variant="caption" align="center" className={classes.foot}>
          {formatMessage({id:'privacyMessagePart1'})}
          <Link to="#" className={classes.link}> {formatMessage({id:'privacyMessagePart2'})}</Link>
          {` ${formatMessage({id:'privacyMessagePart3'})}`}
          <Link to="Hello" className={classes.link}> {formatMessage({id:'privacyMessagePart4'})}</Link>
          {` ${formatMessage({id:'privacyMessagePart5'})}`}
        </Typography>
        {/* <Grid container > */}
          <img src={Image} alt="ImageManager" className={classes.image} style={{position: "absolute", bottom: 0, left: 0}}/>
        {/* </Grid> */}
      </Container>
      </Box>

  );
}

export default injectIntl(ResetPassword)
