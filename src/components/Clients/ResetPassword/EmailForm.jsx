import React from 'react'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import { useFormik } from 'formik'
import { injectIntl } from 'react-intl'
import * as yup from 'yup'
import ResetPassword from './ResetPassword'
import { toast } from 'react-toastify'
import { sendResetPasswordLink } from '../../../services/authentication.service'
import handleSessionExpiredError from '../../../utils/handleSessionExpiry'
import { useState } from 'react'
import { CircularProgress } from '@material-ui/core'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../../contexts/PersonContext/Person.provider'

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  title2: {
    color: '#012a4a',
    fontWeight: theme.common.fontWeightRegular2,
    marginBottom: theme.spacing(2),
  },
  link: {
    color: '#0466c8',
  },
  button: {
    boxShadow: 'none',
  },
}))

function EmailForm({ intl }) {
  const classes = useStyles()
  const { formatMessage } = intl
  const [isSendingLink, setIsSendingLink] = useState(false)

  let validationSchema = yup.object({
    email: yup
      .string()
      .email(formatMessage({ id: 'SignUpEmailErrorMessage' }))
      .required(formatMessage({ id: 'SignUpEmailRequiredErrorMessage' })),
  })

  const { personDispatch } = usePerson()
  const location = useLocation()
  const navigate = useNavigate()

  const formik = useFormik({
    initialValues: {
      email: '',
    },
    validationSchema,
    onSubmit: (values) => {
      setIsSendingLink(true)
      sendResetPasswordLink(values)
        .then(() => {
          setIsSendingLink(false)
          toast.info(formatMessage({ id: 'notifyResetEmailLinkSent' }), {
            position: 'top-right',
            autoClose: false,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          })
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate,
          )
          setIsSendingLink(false)
        })
    },
  })

  return (
    <ResetPassword>
      <Typography align="center" className={classes.title2}>
        {formatMessage({ id: 'CanNotConnectTitle' })}
      </Typography>
      <form className={classes.form} onSubmit={formik.handleSubmit}>
        <TextField
          color="secondary"
          variant="outlined"
          required
          fullWidth
          id="email"
          label={formatMessage({ id: 'ownerEmail' })}
          name="email"
          type="email"
          autoComplete="email"
          onChange={formik.handleChange}
          value={formik.values.email}
          error={formik.errors.email ? true : false}
          helperText={formik.errors.email}
        />
        <div className={classes.submit}>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            fullWidth
            className={classes.button}
            disabled={isSendingLink}
          >
            {formatMessage({ id: 'BoutonRecoveryLink' })}
            {isSendingLink && <CircularProgress style={{fontSize: "25px"}} />}
          </Button>
        </div>
      </form>
    </ResetPassword>
  )
}

export default injectIntl(EmailForm)
