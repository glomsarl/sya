import React from 'react'
import { injectIntl } from 'react-intl'
import {
  makeStyles,
  Box,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
} from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  Structure: {
    width: 250,
    marginInline: 20,
    height: 'fit-content',
  },
  StructureImage: {
    height: 110,
  },
  Home: {
    backgroundColor: '#38B000',
    borderRadius: '5px',
    padding: '0 8px',
    fontSize: '0.8rem',
    width: 'fit-content',
  },
  Deliver: {
    backgroundColor: '#1A759F',
    borderRadius: '5px',
    padding: '0 8px',
    fontSize: '0.8rem',
    width: 'fit-content',
  },
  Reserve: {
    backgroundColor: '#38B000',
    borderRadius: '5px',
    padding: '0 8px',
    fontSize: '0.8rem',
    width: 'fit-content',
  },
  TagHolder: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
    paddingTop: '8px',
    height: '20px',
  },
  Promotion: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
  },
  PromotionEtiquet: {
    width: '40px',
    height: '17px',
    borderRadius: '10px',
    textAlign: 'center',
  },
}))

function Structure({ structure, intl, myId, handleSelect }) {
  const classes = useStyles()

  let { formatMessage } = intl
  const {
    logo_ref: mainImageRef,
    name,
    location,
    average_price,
    rating,
    is_on_promotion,
    promotion_percentage,
    is_structure_sy,
    is_reservable,
    is_home_delivery,
    is_take_away,
  } = structure

  return (
    <Card className={classes.Structure} id={myId} onClick={handleSelect}>
      <CardActionArea>
        <CardMedia
          className={classes.StructureImage}
          image={`${process.env.REACT_APP_BASE_URL}/${mainImageRef}`}
          title={name}
        />
        <CardContent>
          <Typography gutterBottom variant='h6' component='h2'>
            {name}
          </Typography>
          <Typography variant='body2' color='textSecondary' component='p'>
            {location}
          </Typography>
          <Typography variant='body2' color='textSecondary' component='p'>
            {formatMessage({ id: 'structureAvgPrice' })} : {average_price} FCFA
          </Typography>
          <Typography
            variant='body2'
            color='textSecondary'
            component='p'
            className={classes.Promotion}
          >
            <span>
              {formatMessage({ id: 'structureRating' })} :{' '}
              {rating !== 0 ? <b>{`${rating}/10`}</b> : 'No rate'}
            </span>
            {is_on_promotion ? (
              <Typography
                variant='caption'
                style={{ backgroundColor: 'red' }}
                className={classes.PromotionEtiquet}
              >
                {promotion_percentage}%
              </Typography>
            ) : null}
            {is_structure_sy ? (
              <Typography
                variant='caption'
                style={{ backgroundColor: 'yellow' }}
                className={classes.PromotionEtiquet}
              >
                {formatMessage({ id: 'SYButton' })}
              </Typography>
            ) : null}
          </Typography>
          <Box className={classes.TagHolder}>
            {is_take_away ? (
              <Typography className={classes.Home}>
                {formatMessage({ id: 'takeAway' })}
              </Typography>
            ) : null}
            {is_reservable ? (
              <Typography className={classes.Reserve}>
                {formatMessage({ id: 'reserve' })}
              </Typography>
            ) : null}
            {is_home_delivery ? (
              <Typography className={classes.Deliver}>
                {formatMessage({ id: 'delivery' })}
              </Typography>
            ) : null}
          </Box>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}

export default injectIntl(Structure)
