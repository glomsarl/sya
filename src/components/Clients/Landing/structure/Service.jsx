import React, { useEffect, useState } from 'react'
import { injectIntl } from 'react-intl'
import {
  makeStyles,
  Box,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
} from '@material-ui/core'
import maxPromotion from '../../../../utils/maxPromotion'

const useStyles = makeStyles((theme) => ({
  Structure: {
    width: 250,
    marginInline: 20,
    height: 'fit-content',
  },
  StructureImage: {
    height: 110,
  },
  Home: {
    backgroundColor: '#38B000',
    borderRadius: '5px',
    padding: '0 8px',
    fontSize: '0.8rem',
    width: 'fit-content',
  },
  Deliver: {
    backgroundColor: '#1A759F',
    borderRadius: '5px',
    padding: '0 8px',
    fontSize: '0.8rem',
    width: 'fit-content',
  },
  Reserve: {
    backgroundColor: '#38B000',
    borderRadius: '5px',
    padding: '0 8px',
    fontSize: '0.8rem',
    width: 'fit-content',
  },
  TagHolder: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
    paddingTop: '8px',
    height: '20px',
  },
  Promotion: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
  },
  PromotionEtiquet: {
    width: '40px',
    height: '17px',
    borderRadius: '10px',
    textAlign: 'center',
  },
}))

function Service({ service, intl, handleSelect }) {
  const classes = useStyles()

  let { formatMessage } = intl
  const {
    main_image_ref,
    service_name,
    unit_price,
    rating,
    promotions,
    description,
    is_reservable,
    is_home_delivery,
    is_take_away,
  } = service
  let [reduction, setReduction] = useState(0)
  let [promotion, setPromotion] = useState(0)

  useEffect(() => {
    setReduction(maxPromotion(promotions, 'Reduction').max_promotion_value)
    setPromotion(maxPromotion(promotions, 'Bonus').max_promotion_value)
    // eslint-disable-next-line
  }, [])

  return (
    <Card className={classes.Structure} onClick={handleSelect}>
      <CardActionArea>
        <CardMedia
          className={classes.StructureImage}
          image={`${process.env.REACT_APP_BASE_URL}/${main_image_ref}`}
          title={service_name}
        />
        <CardContent>
          <Typography gutterBottom variant='h6' component='h2'>
            {service_name}
          </Typography>
          <Typography variant='body2' color='textSecondary' component='p'>
            {description}
          </Typography>
          <Typography variant='body2' color='textSecondary' component='p'>
            {formatMessage({ id: 'structureAvgPrice' })} : {unit_price} FCFA
          </Typography>
          <Typography
            variant='body2'
            color='textSecondary'
            component='p'
            className={classes.Promotion}
          >
            {rating !== 0 && (
              <span>
                {formatMessage({ id: 'structureRating' })}
                <b>{`: ${rating}/10`}</b>
              </span>
            )}
            {promotion !== 0 && (
              <Typography
                variant='caption'
                color='primary'
                className={classes.PromotionEtiquet}
              >
                {promotion !== 0 ? `${promotion}SY` : 'SY'}
              </Typography>
            )}
            {reduction !== 0 && (
              <Typography
                variant='caption'
                style={{ backgroundColor: 'red' }}
                className={classes.PromotionEtiquet}
              >
                {reduction}%
              </Typography>
            )}
          </Typography>
          <Box className={classes.TagHolder}>
            {is_take_away ? (
              <Typography className={classes.Home}>
                {formatMessage({ id: 'takeAway' })}
              </Typography>
            ) : null}
            {is_reservable ? (
              <Typography className={classes.Reserve}>
                {formatMessage({ id: 'reserve' })}
              </Typography>
            ) : null}
            {is_home_delivery ? (
              <Typography className={classes.Deliver}>
                {formatMessage({ id: 'delivery' })}
              </Typography>
            ) : null}
          </Box>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}

export default injectIntl(Service)
