import { Skeleton } from "@material-ui/lab";
import {Box, makeStyles} from '@material-ui/core';

const useStyles = makeStyles ((theme)=>({
    root:{
        width: "fit-content",
        marginLeft: "10px"
    },
    noteSkeleton:{
        display: "grid",
        gridTemplateColumns:"1fr auto auto",
        gridGap: "10px"
    },
    tagsSkeleton: {
        display: "grid",
        gridAutoFlow: "column",
        gridGap: `${10/3}px`
    }
}))
function StructureSkeleton(){
    const classes = useStyles();
    return(
        <Box className={classes.root}>
            <Skeleton variant="rect" width={250} height={110} />
            <Skeleton variant="text" width="95%" />
            <Skeleton variant="text" width="25%" />
            <Skeleton variant="text" width="45%" />
            <Box className={classes.noteSkeleton}>
                <Skeleton variant="text" width="100%" />
                <Skeleton variant="circle" width={40} height={40} style={{justifySelf:"end"}}/>
                <Skeleton variant="circle" width={40} height={40} style={{justifySelf:"end"}}/>
            </Box>
            <Box className={classes.tagsSkeleton} >
                <Skeleton variant="text" width="100%" />
                <Skeleton variant="text" width="100%" />
                <Skeleton variant="text" width="100%" />
            </Box>
        </Box>
    )
}

export default StructureSkeleton