import { Box, Divider, makeStyles, Typography } from '@material-ui/core'
import { useEffect, useState } from 'react'
import { injectIntl } from 'react-intl'

import maxPromotion from '../../../../utils/maxPromotion'

const useStyles = makeStyles((theme) => ({
  MoreStructure: {
    display: 'grid',
    padding: '10px',
    gridAutoFlow: 'column',
    gridTemplateColumns: 'auto 1fr',
    [theme.breakpoints.down('xs')]: {
      gridAutoFlow: 'row',
    },
  },
  Details: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
    padding: '10px',
    cursor: 'pointer',
  },
  Home: {
    backgroundColor: '#38B000',
    borderRadius: '5px',
    padding: '0 5px',
    fontSize: '0.8rem',
    width: 'fit-content',
  },
  Deliver: {
    backgroundColor: '#1A759F',
    borderRadius: '5px',
    padding: '0 5px',
    fontSize: '0.8rem',
    width: 'fit-content',
  },
  Reserve: {
    backgroundColor: '#38B000',
    borderRadius: '5px',
    padding: '0 5px',
    fontSize: '0.8rem',
    width: 'fit-content',
  },
  TagHolder: {
    display: 'grid',
    gridAutoFlow: 'column',
    height: '20px',
    gridGap: '5px',
    width: 'fit-content',
    padding: '0 10px',
  },
  PromotionEtiquet: {
    width: '40px',
    height: 'fit-content',
    borderRadius: '10px',
    textAlign: 'center',
  },
  Information: {
    display: 'grid',
  },
  BoxImage: {
    width: '200px',
    height: '170px',
    cursor: 'pointer',
  },
  Rate: {
    fontWeight: 'bold',
    justifySelf: 'right',
  },
  Location: {
    padding: '10px',
    width: '70%',
  },
  Image: {
    width: '100%',
    height: '100%',
    borderRadius: '25px 25px 30px 30px',
  },
}))

function MoreStructure({ intl, service, handleSelect }) {
  let classes = useStyles()
  let { formatMessage } = intl
  const {
    main_image_ref,
    service_name,
    unit_price,
    rating,
    promotions,
    description,
    is_reservable,
    is_home_delivery,
    is_take_away,
  } = service
  let [reduction, setReduction] = useState(0)
  let [promotion, setPromotion] = useState(0)

  useEffect(() => {
    setReduction(maxPromotion(promotions, 'Reduction').max_promotion_value)
    setPromotion(maxPromotion(promotions, 'Bonus').max_promotion_value)
    // eslint-disable-next-line
  }, [])

  return (
    <>
      <Box className={classes.MoreStructure} onClick={handleSelect}>
        <Box className={classes.BoxImage}>
          <img
            alt={`${service_name}'s logo`}
            src={`${process.env.REACT_APP_BASE_URL}/${main_image_ref}`}
            className={classes.Image}
          />
        </Box>
        <Box className={classes.Information}>
          <Box className={classes.Details}>
            <Typography
              variant='h3'
              color='secondary'
              style={{ fontWeight: 'bold' }}
            >
              {service_name}
            </Typography>
            {rating !== 0 && (
              <Typography
                variant='h3'
                color='secondary'
                className={classes.Rate}
              >
                {rating}
                <span style={{ fontSize: '15px' }}>/10</span>
              </Typography>
            )}
          </Box>
          <Typography
            variant='body1'
            className={classes.Location}
            color='textSecondary'
          >
            {description}
            <Typography variant='h4' color='secondary'>
              {formatMessage({ id: 'structureAvgPrice' })} :{' '}
              <b>{unit_price} FCFA </b>
            </Typography>
          </Typography>
          <Box className={classes.TagHolder}>
            {is_take_away ? (
              <Typography className={classes.Home}>
                {formatMessage({ id: 'takeAway' })}
              </Typography>
            ) : null}
            {is_reservable ? (
              <Typography className={classes.Reserve}>
                {formatMessage({ id: 'reserve' })}
              </Typography>
            ) : null}
            {is_home_delivery ? (
              <Typography className={classes.Deliver}>
                {formatMessage({ id: 'delivery' })}
              </Typography>
            ) : null}
            {reduction !== 0 && (
              <Typography
                variant='caption'
                style={{ backgroundColor: 'red' }}
                className={classes.PromotionEtiquet}
              >
                {reduction}%
              </Typography>
            )}
            {promotion !== 0 && (
              <Typography
                variant='caption'
                style={{ backgroundColor: '#FFDF00' }}
                className={classes.PromotionEtiquet}
              >
                SY
              </Typography>
            )}
          </Box>
        </Box>
      </Box>
      <Divider />
    </>
  )
}

export default injectIntl(MoreStructure)
