import resto from '../../assets/resto.png'
import { useState } from 'react';
import {Close, ArrowBack } from '@material-ui/icons';
import {makeStyles, Typography, Box, IconButton, } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    DialogTitle:{
        color:'#FFFF',
        padding: '10px',
        marginLeft:'-4px',
        display:'grid',
        gridAutoFlow:'column',
    },
}))

function purchaseHeader({intl, ongoingOrder, handleClose}){
    let classes = useStyles()
    let {formatMessage } = intl;

     

    const stepBackHandler = () =>{

    }

     return (
        <Box
            id="alert-dialog-slide-title" 
            style={{ background:`url(${resto}`}} 
            className={classes.DialogTitle}
        >
            <Typography variant='h6' className={classes.Title}>
                {purchaseLevel ? 
                    <IconButton
                        style={{justifySelf:'left', color:'white'}} 
                        onClick={stepBackHandler}
                    >
                        <ArrowBack />
                    </IconButton>
                : null } 
                <Typography variant='h6' style={{width:'90%', textAlign:'center'}}>
                    {formatMessage({id:'orderTitle'})} : {service ? service.name: null}
                </Typography>
                <IconButton
                    variant='outlined'
                    onClick={closeHandler} 
                    className={classes.IconButton}
                >
                    <Close />
                </IconButton>
            </Typography>
            {selectedDate ? 
                <Typography variant='body1' color='textSecondary' className={classes.Breffing}>
                    {quantity} {
                    category === 'reserve'? formatMessage({id:'reserveRadio'})
                    : category === 'delivery' ? formatMessage({id:'deliveryRadio'})
                    : category === 'takeAway' ? formatMessage({id:'takeAwayRadio'})
                    : null} - 
                    {/* <FormattedDate value={selectedDate} day="long" month="numeric" year="numeric" /> */}
                    {daysOfTheWeek[selectedDate.getDay()]} {selectedDate.getDate()}/{selectedDate.getMonth()+1}/{selectedDate.getFullYear()}
                        - 
                        {/* <FormattedTime value={selectedTime} /> */}
                        {selectedTime}
                </Typography>
            : null }
        </Box>
     )
}

export default injectIntl(PurchaseHeader)