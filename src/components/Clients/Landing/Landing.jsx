import { injectIntl } from 'react-intl'
import { useLocation, useNavigate } from 'react-router'
import { useEffect, useState } from 'react'
import { Skeleton } from '@material-ui/lab'
import Scrollbars from 'react-custom-scrollbars-2'
import { Box, Typography, makeStyles, Grid, Button } from '@material-ui/core'

import Hoc from '../../../hoc/hoc'
import Subtitle from './subTitles/subTitle'
import Structure from './structure/Structure'
import randomNumber from '../../../utils/randomNumber'
import reviewsImage from '../../../assets/undraw_reviews.png'
import StructureSkeleton from './structure/StructureSkeleton'
import creditCardImage from '../../../assets/undraw_Credit_card.png'
import ownwerImage from '../../../assets/undraw_Operating_system.png'
import purchaseImage from '../../../assets/undraw_successful_purchase.png'

import { usePerson } from '../../../contexts/PersonContext/Person.provider'
import { useLandingStructure } from '../../../contexts/LandingStructureContext/LandingStructure.provider'
import { useLandingCategory } from '../../../contexts/LandingCategoryContext/LandingCategory.provider'

import {
  getStructureCagetories,
  getUserStructures,
} from '../../../services/structure.service'
import handleSessionExpiredError from '../../../utils/handleSessionExpiry'
import { findUserInfo } from '../../../services/authentication.service'
import QuestionBox from './QuestionBox/QuestionBox'
import { fetchCatalogServices } from '../../../services/catalogs.service'
import Service from './structure/Service'
import { useLandingOngoingOrder } from '../../../contexts/LandingOngoingOrderContext/LandingOngoingOrder.provider'
import Purchasing from './popUps/Purchasing'

const useStyles = makeStyles((theme) => ({
  Body: {
    display: 'grid',
    justifyContent: 'center',
    overflow: 'hidden',
    marginTop: '25px',
  },
  Structures: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridTemplateColumns: 'auto auto auto auto 1fr',
    height: '100%',
  },
  Category: {
    backgroundColor: theme.palette.secondary.main,
    fontWeight: 'bold',
    fontSize: '24px',
    lineHeight: '77px',
    textAlign: 'center',
    color: '#FFFF',
    marginTop: '30px',
  },
  SeeMoreBox: {
    diplay: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
    justifySelf: 'right',
  },
  HowItWorks: {
    backgroundColor: 'rgb(248 198 0 / 20%)',
    fontWeight: 'bold',
    fontSize: '24px',
    lineHeight: '77px',
    textAlign: 'center',
    color: theme.palette.secondary.main,
    marginTop: '30px',
  },
  Box: {
    display: 'grid',
    marginBottom: '20px',
    width: '80%',
    padding: '20px',
    gridAutoFlow: 'column',
    justifySelf: 'center',
    alignItems: 'center',
    [theme.breakpoints.down('xs')]: {
      width: 'fit-content',
    },
  },
  AboutBox: {
    display: 'grid',
    gridGap: '20px',
    gridAutoFlow: 'row',
    justifyContent: 'center',
    backgroundColor: '#F7F7FC',
  },
  AboutSubBox: {
    padding: '50px',
    display: 'grid',
    gridGap: '20px',
    gridAutoFlow: 'column',
    [theme.breakpoints.down('xs')]: {
      gridAutoFlow: 'row',
    },
  },
  LastGrid: {
    width: '65%',
    justifySelf: 'center',
    border: '1px solid ' + theme.palette.secondary.main,
    padding: '15px',
    [theme.breakpoints.down('xs')]: {
      width: '85%',
    },
  },
  SYStructures: {
    display: 'grid',
    justifySelf: 'center',
  },
  HtmlDiv: {
    display: 'flex',
    alignItems: 'center',
    backgroundColor: 'rgb(248 198 0 / 20%)',
    opacity: 0.8,
    height: '330px',
    width: '90%',
    paddingLeft: '20px',
    [theme.breakpoints.down('sm')]: {
      width: '70%',
    },
    [theme.breakpoints.down('xs')]: {
      width: '90%',
    },
  },
  GreatQuestion: {
    fontWeight: 'bold',
    textAlign: 'center',
    width: '80%',
    justifySelf: 'center',
  },
  CallToSignBox: {
    display: 'grid',
    padding: '20px',
    marginBottom: '20px',
    gridAutoFlow: 'column',
    justifySelf: 'center',
  },
  SubTitle1Text: {
    width: '75%',
    textAlign: 'center',
    justifySelf: 'center',
  },
  HtmlDivCatalog: {
    width: '80%',
    height: '450px',
    [theme.breakpoints.down('xs')]: {
      width: '70%',
    },
  },
}))

function HowItWorksStep(props) {
  let { title, illustratingIcon, content } = props

  return (
    <>
      <Box style={{ display: 'flex', flexDirection: 'column', gap: '20px' }}>
        <Typography variant='h3' component='h2' color='Secondary'>
          {title}
        </Typography>
        <Typography variant='body1' component='p'>
          {content}
        </Typography>
        <img
          src={illustratingIcon}
          alt='illustrating icon'
          width='300'
          height='165'
        />
      </Box>
    </>
  )
}

function Landing({
  intl: { formatMessage },
  isSearchActive,
  isSearching,
  searchStructures,
}) {
  let classes = useStyles()

  const {
    landingStructureState: { structures },
    landingStructureDispatch,
  } = useLandingStructure()
  const {
    landingCategoryState: { categories },
    landingCategoryDispatch,
  } = useLandingCategory()

  const location = useLocation()
  const navigate = useNavigate()
  let { personDispatch } = usePerson()

  const moreStucturesHandler = ({
    category_name,
    category_id,
    longitude,
    latitude,
  }) => {
    navigate(
      `/structures?${category_id ?? ''}&${category_name ?? ''}&${
        longitude ?? ''
      }&${latitude ?? ''}`
    )
  }

  const structureSelectHandler = (structure) => {
    landingStructureDispatch({
      type: 'LOAD_SELECTED_STRUCTURE',
      payload: structure,
    })
    navigate(`/structure/${structure.structure_id}`)
  }
  const [isStructuresLoading, setIsStructuresLoading] = useState(true)
  const [isCategoriesLoading, setIsCategoriesLoading] = useState(true)
  const [services, setServices] = useState([])

  const anchor = document.querySelector('#result-info')
  if (anchor && isSearchActive) {
    anchor.scrollIntoView({ behavior: 'smooth', block: 'center' })
  }

  useEffect(() => {
    landingStructureDispatch({
      type: 'LOAD_STRUCTURES_DATA',
      payload: searchStructures ?? [],
    })
    // eslint-disable-next-line
  }, [searchStructures])

  useEffect(() => {
    if (!isSearchActive) {
      findUserInfo()
        .then((person) => {
          personDispatch({ type: 'LOAD_PERSON_DATA', payload: person })
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
        })
      getStructureCagetories()
        .then((categories) => {
          landingCategoryDispatch({
            type: 'LOAD_CATEGORIES_DATA',
            payload: categories,
          })
          setIsCategoriesLoading(false)
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
        })
      getUserStructures()
        .then(({ structures }) => {
          landingStructureDispatch({
            type: 'LOAD_STRUCTURES_DATA',
            payload: structures,
          })
          setIsStructuresLoading(false)
          fetchCatalogServices()
            .then(({ catalog_services }) => {
              setServices(catalog_services)
            })
            .catch((error) => {
              handleSessionExpiredError(
                personDispatch,
                location.pathname,
                error,
                formatMessage,
                navigate
              )
            })
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
        })
    }
    // eslint-disable-next-line
  }, [isSearchActive])

  const [logoRef, setLogRef] = useState()
  const [structureId, setStructureId] = useState()
  const [isDialogOpen, setIsDialogOpen] = useState()
  const [strLocation, setStrLocation] = useState({ lat: 0, lng: 0 })

  let { landingOngoingOrderDispatch } = useLandingOngoingOrder()

  const ongoingOrderHandler = (service) => {
    setIsDialogOpen(true)
    setStructureId(service.structure_id)
    setLogRef(service.main_image_ref)
    const { longitude, latitude } = structures.find(
      ({ structure_id: id }) => id === service.structure_id
    )
    setStrLocation({
      lat: latitude,
      lng: longitude,
    })
    let order = { service, structure_id: service.structure_id }
    landingOngoingOrderDispatch({
      type: 'LOAD_ONGOING_ORDER_DETAILS',
      payload: order,
    })
  }

  const closePurchaseHandler = () => {
    setIsDialogOpen(false)
    landingOngoingOrderDispatch({
      type: 'RESET_ONGOING_ORDER',
    })
  }

  return (
    <Box className={classes.Body}>
      <Grid container direction='column' justify='center' alignItems='center'>
        <Subtitle subTitle={formatMessage({ id: 'subtitle1' })} />
        <Typography variant='h6' className={classes.SubTitle1Text}>
          {formatMessage({ id: 'subtitle1Text' })}
        </Typography>
        {isStructuresLoading ||
          (structures &&
            structures.length > 0 &&
            structures.find(({ is_structure_sy }) => is_structure_sy) &&
            !isSearchActive && (
              <>
                <Box className={classes.Box}>
                  <div style={{ justifySelf: 'left' }}>
                    <Subtitle subTitle={formatMessage({ id: 'subtitle2' })} />
                  </div>
                  {isCategoriesLoading ? (
                    <Skeleton
                      variant='rect'
                      height='20px'
                      width='100%'
                      style={{ alignSelf: 'center' }}
                    />
                  ) : (
                    <Box className={classes.SeeMoreBox}>
                      <Button
                        color='secondary'
                        onClick={() =>
                          moreStucturesHandler({
                            category_id: 'sy-structures',
                            category_name: 'sy-structures',
                          })
                        }
                      >
                        {formatMessage({ id: 'seeMore' })}
                      </Button>
                      {/* <Navigables /> */}
                    </Box>
                  )}
                </Box>
                <div className={classes.HtmlDiv}>
                  <Scrollbars
                    style={{ width: '98%', height: '80%' }}
                    className={classes.SYStructures}
                  >
                    <Box className={classes.Structures} id='sy'>
                      {isStructuresLoading
                        ? [...new Array(randomNumber(3, 10))].map(() => (
                            <StructureSkeleton />
                          ))
                        : structures.map((structure, index) =>
                            structure.is_structure_sy ? (
                              <Structure
                                key={structure.id}
                                myId={`sy${index}`}
                                structure={structure}
                                handleSelect={() =>
                                  structureSelectHandler(structure)
                                }
                              />
                            ) : null
                          )}
                    </Box>
                  </Scrollbars>
                </div>
              </>
            ))}
      </Grid>
      {isStructuresLoading ||
      isCategoriesLoading ||
      isSearching ||
      (structures &&
        structures.length > 0 &&
        categories &&
        categories.length > 0) ? (
        <Box className={classes.Category}>
          {isSearchActive
            ? formatMessage({ id: 'searchResults' })
            : formatMessage({ id: 'allCategory' })}
        </Box>
      ) : null}
      <Grid
        id='result-info'
        container
        direction='column'
        justify='center'
        alignItems='center'
      >
        {isCategoriesLoading || isStructuresLoading || isSearching ? (
          <Skeleton
            variant='rect'
            height='150px'
            width='80%'
            style={{ marginTop: '20px' }}
          />
        ) : isSearchActive ? (
          <div className={classes.HtmlDivCatalog} style={{ marginTop: '20px' }}>
            <Scrollbars
              style={{
                width: '100%',
                height: '100%',
                overflow: 'hidden',
              }}
            >
              <Box className={classes.Structures} id='cat'>
                {isSearching ? (
                  [...new Array(randomNumber(3, 10))].map(() => (
                    <StructureSkeleton />
                  ))
                ) : structures && structures.length === 0 ? (
                  <Typography>
                    {formatMessage({ id: 'noContentMessage' })}
                  </Typography>
                ) : (
                  structures.map((structure) => (
                    <Structure
                      key={structure.category_id}
                      myId={structure.structure_id}
                      structure={structure}
                      handleSelect={() => structureSelectHandler(structure)}
                    />
                  ))
                )}
                <span id='searchResults'></span>
              </Box>
            </Scrollbars>
          </div>
        ) : categories && categories.length === 0 ? (
          <Typography>{formatMessage({ id: 'noContentMessage' })}</Typography>
        ) : (
          categories.map(
            // Find if there is data least a structure with
            //the category id before displaying else don't display
            ({ category_id, category_name }) =>
              services.find(
                ({ category_id: cat_id }) => cat_id === category_id
              ) && (
                <Hoc key={category_id}>
                  <Box className={classes.Box}>
                    <div style={{ justifySelf: 'left' }}>
                      <Subtitle
                        subTitle={formatMessage({ id: category_name })}
                      />
                    </div>
                    <Box className={classes.SeeMoreBox}>
                      <Button
                        color='secondary'
                        onClick={() =>
                          moreStucturesHandler({ category_id, category_name })
                        }
                      >
                        {formatMessage({ id: 'seeMore' })}
                      </Button>
                    </Box>
                  </Box>
                  <div className={classes.HtmlDivCatalog}>
                    <Scrollbars
                      style={{
                        width: '100%',
                        height: '100%',
                        overflow: 'hidden',
                      }}
                    >
                      <Box className={classes.Structures} id='cat'>
                        {isStructuresLoading ? (
                          [...new Array(randomNumber(3, 10))].map(() => (
                            <StructureSkeleton />
                          ))
                        ) : services.length === 0 ? (
                          <Typography style={{ justifySelf: 'end' }}>
                            {formatMessage({ id: 'noContentMessage' })}
                          </Typography>
                        ) : (
                          <>
                            {services.map(
                              (service) =>
                                service.category_id === category_id && (
                                  <Service
                                    key={service.service_id}
                                    service={service}
                                    handleSelect={() =>
                                      ongoingOrderHandler(service)
                                    }
                                  />
                                )
                            )}
                            <Purchasing
                              logoRef={logoRef}
                              show={isDialogOpen}
                              structureId={structureId}
                              formatMessage={formatMessage}
                              handleClose={closePurchaseHandler}
                              location={strLocation}
                            />
                          </>
                        )}
                      </Box>
                    </Scrollbars>
                  </div>
                </Hoc>
              )
          )
        )}
      </Grid>
      <Box
        style={{
          display: 'grid',
          gridAutoFlow: 'column',
          justifyContent: 'center',
        }}
      >
        <Box style={{ width: '85vw' }}>
          <Box className={classes.HowItWorks}>
            {formatMessage({ id: 'howItWorks' })}
          </Box>
          <Box className={classes.AboutBox}>
            <Box className={classes.AboutSubBox}>
              <HowItWorksStep
                title={formatMessage({ id: 'ownerTitle' })}
                content={formatMessage({ id: 'ownerContent' })}
                illustratingIcon={ownwerImage}
              />
              <HowItWorksStep
                title={formatMessage({ id: 'actions' })}
                content={formatMessage({ id: 'actionsContent' })}
                illustratingIcon={purchaseImage}
              />
            </Box>
            <Box className={classes.AboutSubBox}>
              <HowItWorksStep
                title={formatMessage({ id: 'evaluation' })}
                content={formatMessage({ id: 'evaluationContent' })}
                illustratingIcon={reviewsImage}
              />
              <HowItWorksStep
                title={formatMessage({ id: 'payement' })}
                content={formatMessage({ id: 'payementContent' })}
                illustratingIcon={creditCardImage}
              />
            </Box>
          </Box>
        </Box>
      </Box>
      <QuestionBox />
    </Box>
  )
}

export default injectIntl(Landing)
