import * as yup from 'yup'
import { useFormik } from 'formik'
import { injectIntl } from 'react-intl'
import { useState, useEffect } from 'react'
import {
  makeStyles,
  Box,
  Typography,
  TextField,
  Button,
  Divider,
  CircularProgress,
} from '@material-ui/core'

import { useLandingOngoingOrder } from '../../../../contexts/LandingOngoingOrderContext/LandingOngoingOrder.provider'
import { getCodeValuePercentage } from '../../../../services/promotions.service'
import handleSessionExpiredError from '../../../../utils/handleSessionExpiry'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../../../contexts/PersonContext/Person.provider'
import { notifyInfo } from '../../../../utils/toastMessages'
import maxPromotion from '../../../../utils/maxPromotion'

const useStyles = makeStyles((theme) => ({
  StandardTextField: {
    borderBottom: '1px solid gray',
  },
  Apply: {
    width: 'fit-content',
    height: 'fit-content',
    alignSelf: 'end',
  },
  Form: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
  },
  Element: {
    margin: '10px',
  },
  Benefit: {
    color: theme.palette.grey[400],
    textAlign: 'center',
  },
}))

function OrderSummary({ intl: { formatMessage }, handleNext }) {
  let classes = useStyles()

  const [isSubmitting, setIsSubmitting] = useState(false)
  const [reduction, setReduction] = useState(0)
  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  const {
    landingOngoingOrderState: {
      clientOrder: {
        quantity,
        order_type,
        unit_price,
        service_name,
        sy_price,
        promotions,
      },
    },
    landingOngoingOrderDispatch,
  } = useLandingOngoingOrder()

  const validationSchema = yup.object({
    code_value: yup.string(''),
  })

  const formik = useFormik({
    initialValues: {
      code_value: '',
    },
    validationSchema,
    onSubmit: (values) => {
      setIsSubmitting(true)
      getCodeValuePercentage(values.code_value)
        .then((promotion_code) => {
          if (promotion_code) {
            const { promotion_code_id, percentage } = promotion_code
            setReduction((percentage * unit_price * quantity) / 100)
            landingOngoingOrderDispatch({
              type: 'ADD_PROMOTION_CODE',
              payload: promotion_code_id,
            })
          } else notifyInfo(formatMessage({ id: 'NotAvidCode' }))
          setIsSubmitting(false)
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
          setIsSubmitting(false)
        })
    },
  })

  useEffect(() => {
    if (promotions.length > 0) {
      const { max_promotion_value } = maxPromotion(promotions, 'Reduction')
      setReduction((quantity * unit_price * max_promotion_value) / 100)
    }
    // eslint-disable-next-line
  }, [quantity])

  return (
    <>
      <Box display={{ display: 'grid', gridGap: '25px' }}>
        <Typography
          variant='body1'
          color='textSecondary'
          className={classes.Element}
        >
          {formatMessage({ id: 'order' })} {service_name}
        </Typography>
        <Divider />
        <Typography
          variant='body1'
          color='textSecondary'
          className={classes.Element}
        >
          {formatMessage({ id: 'orderType' })}{' '}
          {order_type === 'H'
            ? formatMessage({ id: 'deliveryRadio' })
            : order_type === 'R'
            ? formatMessage({ id: 'reserveRadio' })
            : order_type === 'T'
            ? formatMessage({ id: 'takeAwayRadio' })
            : null}
        </Typography>
        <Divider />
        <Typography
          variant='body1'
          color='textSecondary'
          className={classes.Element}
        >
          {`${formatMessage({ id: 'price' })} ${unit_price} FCFA ${
            sy_price ? `(${sy_price} SY)` : ''
          }`}
        </Typography>
        <Divider />
        <Typography
          variant='body1'
          color='textSecondary'
          className={classes.Element}
        >
          {`${formatMessage({ id: 'quantity' })}: `}
          <span style={{ fontWeight: 'bold' }}>{quantity}</span>
        </Typography>
        <Divider />
        <Typography
          variant='body1'
          color='textSecondary'
          className={classes.Element}
        >
          {formatMessage({ id: 'total' })}
          <span style={{ fontWeight: 'bold' }}>
            {Number(quantity) * Number(unit_price)}
            FCFA
          </span>
        </Typography>
        <Divider />
        {promotions.length > 0 ? (
          <Typography
            variant='h5'
            color='textSecondary'
            className={classes.Element}
          >
            {formatMessage({ id: 'reduction' })}
            <span style={{ fontWeight: 'bold' }}>
              {maxPromotion(promotions, 'Reduction').max_promotion_value}%
            </span>
          </Typography>
        ) : reduction !== 0 ? (
          <Typography
            variant='h5'
            color='textSecondary'
            className={classes.Element}
          >
            {formatMessage({ id: 'reduction' })}
            <span style={{ fontWeight: 'bold' }}>{reduction}%</span>
          </Typography>
        ) : null}
        <Divider />
        <Typography
          variant='body1'
          color='secondary'
          className={classes.Element}
          style={{ fontWeight: 'bold' }}
        >
          {formatMessage({ id: 'finalPrice' })}
          {` ${Number(quantity) * Number(unit_price) - reduction} FCFA 
                ${
                  sy_price
                    ? `(${sy_price * (quantity - reduction / unit_price)} SY )`
                    : ''
                }`}
        </Typography>
        <Typography variant='body1' color='textSecondary'>
          {formatMessage({ id: 'promotionCode' })}
        </Typography>
        <form className={classes.Form} onSubmit={formik.handleSubmit}>
          <TextField
            name='code_value'
            variant='standard'
            className={classes.StandardTextField}
            value={formik.values.code_value}
            onChange={formik.handleChange}
            label={formatMessage({ id: 'code_value' })}
            disabled={promotions.length > 0}
          />
          <Button
            size='small'
            type='submit'
            color='secondary'
            variant='outlined'
            onClick={formik.handleSubmit}
            className={classes.Apply}
            disabled={
              promotions.length > 0 ||
              isSubmitting ||
              formik.values.code_value === ''
            }
          >
            {isSubmitting && <CircularProgress color='primary' fontSize={24} />}
            {formatMessage({ id: 'apply' })}
          </Button>
          <Box style={{ justifySelf: 'end', alignSelf: 'end' }}>
            <Button
              type='submit'
              color='primary'
              size='small'
              variant='contained'
              onClick={handleNext}
            >
              {formatMessage({ id: 'nextButtonText' })}
            </Button>
          </Box>
        </form>

        {promotions.length > 0 && !sy_price ? (
          <Typography variant='caption' className={classes.Benefit}>
            {formatMessage({ id: 'profitMessage' })}{' '}
            {maxPromotion(promotions, 'Bonus').max_promotion_value} SY
          </Typography>
        ) : null}
      </Box>
    </>
  )
}

export default injectIntl(OrderSummary)
