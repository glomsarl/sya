import { injectIntl } from 'react-intl'
import { useState, useEffect } from 'react'
import { Skeleton } from '@material-ui/lab'
import Carousel from 'react-material-ui-carousel'
import Scrollbars from 'react-custom-scrollbars-2'
import { Box, Typography, makeStyles, Button } from '@material-ui/core'

import Hoc from '../../../hoc/hoc'
import Structure from './structure/Structure'
import randomNumber from '../../../utils/randomNumber'
import StructureSkeleton from './structure/StructureSkeleton'
import OtherStructure from './otherStructures/OtherStructures'
import StructureDetail from './structureDetail/StructureDetail'
import SearchCatalogMenu from './SearchCatalogMenu/SearchCatalogMenu'
import LandingCatalogMenu from './LandingCatalogMenu/LandingCatalogMenu'
import LandingDetailAbout from './landingDetailAbout/LandingDetailAbout'
import DetailAboutSkeleton from './landingDetailAbout/DetailAboutSkeleton'
import StructureDetailSkeleton from './structureDetail/StructureDetailSkeleton'

import { useLandingStructure } from '../../../contexts/LandingStructureContext/LandingStructure.provider'
import {
  getStructreSchedules,
  getStructureInfo,
  getUserStructures,
} from '../../../services/structure.service'
import handleSessionExpiredError from '../../../utils/handleSessionExpiry'
import { usePerson } from '../../../contexts/PersonContext/Person.provider'
import { fetchAllPublications } from '../../../services/publications.service'
import { fetchCatalogServices } from '../../../services/catalogs.service'
import { useLocation, useNavigate } from 'react-router'
import QuestionBox from './QuestionBox/QuestionBox'

const useStyles = makeStyles((theme) => ({
  Body: {
    display: 'grid',
    justifyContent: 'center',
    marginTop: 50,
  },
  Structures: {
    display: 'grid',
    gridAutoFlow: 'column',
    height: '100%',
  },
  SeeMoreBox: {
    diplay: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
    justifySelf: 'right',
  },
  LastGrid: {
    width: '65%',
    justifySelf: 'center',
    border: '1px solid ' + theme.palette.secondary.main,
    padding: '15px',
    [theme.breakpoints.down('xs')]: {
      width: 'fit-content',
    },
  },
  GreatQuestion: {
    fontWeight: 'bold',
    textAlign: 'center',
    width: '90%',
    justifySelf: 'center',
    marginTop: '30px',
  },
  CallToSignBox: {
    display: 'grid',
    marginBottom: '20px',
    width: '100%',
    padding: '20px',
    gridAutoFlow: 'column',
  },
  CarouselInfo: {
    borderLeft: `5px solid ${theme.palette.secondary.main}`,
    backgroundColor: 'rgb(248 198 0 / 50%)',
    height: 'fit-content',
    paddingLeft: '5px',
  },
  ActiveLink: {
    fontWeight: 'bold',
    borderBottom: `3px solid ${theme.palette.secondary.main}`,
    height: 'fit-content',
    [theme.breakpoints.down('xs')]: {
      fontSize: '12px',
    },
  },
  Links: {
    borderBottom: `1px solid gray`,
    height: 'fit-content',
    color: '#000000',
    [theme.breakpoints.down('xs')]: {
      fontSize: '12px',
    },
  },
  ScrollControl: {
    borderRadius: '5px',
    border: '1px',
  },
  OtherCatalogs: {
    background: '#ECF0F6',
    marginTop: '30px',
    padding: '20px',
  },
  ChildCatalog: {
    display: 'grid',
    gridAutoFlow: 'column',
  },
  ScrollbarHeard: {
    display: 'grid',
    gridAutoFlow: 'column',
  },
  HtmlDiv: {
    display: 'flex',
    // paddingLeft: '7%',
    alignItems: 'center',
  },
  OtherSubtitles: {
    justifySelf: 'left',
    fontWeight: 'bold',
  },
  Main: {
    display: 'grid',
    gridAutoFlow: 'column',
    justifyContent: 'center',
    gridGap: '15px',
    padding: '10px',
    [theme.breakpoints.down('xs')]: {
      display: 'flex',
      flexDirection: 'column-reverse',
      alignItems: 'center',
    },
  },
  CarouselBox: {
    maxWidth: '460px',
    minWidth: '50vw',
    display: 'grid',
    gridAutoFlow: 'row',
    gridGap: '5px',
    [theme.breakpoints.down('xs')]: {
      width: '350px',
    },
  },
  ServiceDisplay: {
    display: 'grid',
    gridAutoFlow: 'row',
    height: 'fit-content',
  },
  BodyNavigation: {
    display: 'grid',
    gridAutoFlow: 'column',
    textAlign: 'center',
  },
}))

function SelectedStructure({ intl: { formatMessage } }) {
  let classes = useStyles()

  const {
    landingStructureState: {
      selectedStructure,
      selectedService,
      closestStructures,
      sameCategoryStructures,
    },
    landingStructureDispatch,
  } = useLandingStructure()
  const { is_home_delivery, is_take_away, is_reservable, longitude, latitude } =
    selectedStructure

  const [isSeletedStructureDataLoading, setIsSeletedStructureDataLoading] =
    useState(true)
  let [activeContent, setActiveContent] = useState('About')
  const [isSameCategoryStructuresLoading, setIsSameTCategoryStructuresLoading] =
    useState(true)
  const [isClosestStructuresLoading, setIsClosestStructureLoading] =
    useState(true)

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()
  const structure_id = location.pathname.split('/')[2]

  const structureSelectHandler = (structure_id) => {
    navigate(`/structure/${structure_id}`)
    window.location.reload()
  }

  const changeContentHandler = (contentTitle) => {
    setActiveContent(contentTitle)
    if (contentTitle === 'About')
      landingStructureDispatch({
        type: 'SELECT_SERVICE_FORM_TODAY_CATALOG',
        payload: {},
      })
    else if (contentTitle === 'todayCatalog') {
      const catalogId = document.querySelector('#catalogs')
      catalogId.scrollIntoView()
    }
  }

  const serviceSelectHandler = (service) => {
    const displayer = document.querySelector('#back-to-top-anchor')
    displayer.scrollIntoView()
    landingStructureDispatch({
      type: 'SELECT_SERVICE_FORM_TODAY_CATALOG',
      payload: {
        ...service,
        is_on_promotion: service.promotions.length > 0,
        schedules: selectedStructure.schedules,
      },
    })
  }

  const moreStucturesHandler = ({
    category_name,
    category_id,
    longitude,
    latitude,
  }) => {
    navigate(
      `/structures?${category_id ?? ''}&${category_name ?? ''}&${
        longitude ?? ''
      }&${latitude ?? ''}`
    )
  }

  useEffect(() => {
    if (
      isClosestStructuresLoading &&
      isSameCategoryStructuresLoading &&
      isSeletedStructureDataLoading
    ) {
      getStructureInfo(structure_id)
        .then((structure) => {
          getStructreSchedules(structure_id)
            .then((schedules) => {
              fetchAllPublications(structure_id)
                .then((publications) => {
                  fetchCatalogServices({ structure_id })
                    .then(({ catalog_services: services, catalog }) => {
                      landingStructureDispatch({
                        type: 'LOAD_SELECTED_STRUCTURE',
                        payload: {
                          ...structure,
                          publications,
                          schedules,
                          today_catalog: { ...catalog, services },
                        },
                      })
                      setIsSeletedStructureDataLoading(false)

                      const { category_id, longitude, latitude } = structure
                      getUserStructures({
                        category_id,
                        structure_id,
                      })
                        .then(({ structures }) => {
                          landingStructureDispatch({
                            type: 'LOAD_SAME_CATEGORY_STRUCTURES_DATA',
                            payload: structures,
                          })
                          setIsSameTCategoryStructuresLoading(false)
                        })
                        .catch((error) => {
                          handleSessionExpiredError(
                            personDispatch,
                            location.pathname,
                            error,
                            formatMessage,
                            navigate
                          )
                        })
                      getUserStructures({ longitude, latitude, structure_id })
                        .then(({ structures }) => {
                          landingStructureDispatch({
                            type: 'LOAD_CLOSEST_STRUCTURES_DATA',
                            payload: structures,
                          })
                          setIsClosestStructureLoading(false)
                        })
                        .catch((error) => {
                          handleSessionExpiredError(
                            personDispatch,
                            location.pathname,
                            error,
                            formatMessage,
                            navigate
                          )
                        })
                    })
                    .catch((error) => {
                      handleSessionExpiredError(
                        personDispatch,
                        location.pathname,
                        error,
                        formatMessage,
                        navigate
                      )
                    })
                })
                .catch((error) => {
                  handleSessionExpiredError(
                    personDispatch,
                    location.pathname,
                    error,
                    formatMessage,
                    navigate
                  )
                })
            })
            .catch((error) => {
              handleSessionExpiredError(
                personDispatch,
                location.pathname,
                error,
                formatMessage,
                navigate
              )
            })
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
        })
    }
    // eslint-disable-next-line
  }, [structure_id])

  return (
    <Box className={classes.Body}>
      <Box className={classes.Main}>
        <Box className={classes.CarouselBox}>
          {isSeletedStructureDataLoading ? (
            <Skeleton variant='rect' width='450' style={{ height: '260px' }} />
          ) : (
            <Carousel>
              {selectedStructure.publications &&
                selectedStructure.publications.map(
                  ({ publication_id, publication_ref }) => (
                    <div
                      key={publication_id}
                      style={{
                        height: '495px',
                        backgroundSize: 'contain',
                        background: `url(${publication_ref}) center center fixed`,
                      }}
                    ></div>
                  )
                )}
            </Carousel>
          )}
          <Typography
            variant='body1'
            color='textSecondary'
            className={classes.CarouselInfo}
          >
            {formatMessage({ id: 'carourelInTitule' })}
            <Typography
              variant='body1'
              color='textSecondary'
              style={{ textAlign: 'center', fontWeight: 'bold' }}
            >
              {is_home_delivery ? formatMessage({ id: 'delivery' }) : null}*{' '}
              {is_reservable ? formatMessage({ id: 'reserve' }) : null}*{' '}
              {is_take_away ? formatMessage({ id: 'takeAway' }) : null}
            </Typography>
          </Typography>
          <Typography
            variant='body1'
            color='textSecondary'
            className={classes.BodyNavigation}
          >
            <Button
              onClick={() => changeContentHandler('About')}
              className={
                activeContent === 'About' ? classes.ActiveLink : classes.Links
              }
            >
              {formatMessage({ id: 'aboutLink' })}
            </Button>
            <Button
              disabled={isSeletedStructureDataLoading}
              onClick={() => changeContentHandler('dayCatalog')}
              className={
                activeContent === 'dayCatalog'
                  ? classes.ActiveLink
                  : classes.Links
              }
            >
              {formatMessage({ id: 'dayCatalogLink' })}
            </Button>
            <Button
              disabled={isSeletedStructureDataLoading}
              onClick={() => changeContentHandler('moreCatalog')}
              className={
                activeContent === 'moreCatalog'
                  ? classes.ActiveLink
                  : classes.Links
              }
            >
              {formatMessage({ id: 'moreCatalogLink' })}
            </Button>
          </Typography>
          {isSeletedStructureDataLoading ? (
            <DetailAboutSkeleton />
          ) : activeContent === 'About' ? (
            <LandingDetailAbout structure={selectedStructure} />
          ) : activeContent === 'dayCatalog' ? (
            <Hoc>
              <Typography
                variant='h4'
                style={{ fontWeight: 'bold' }}
                color='secondary'
                id='catalogs'
              >
                {formatMessage({ id: 'dayMenu' })}
              </Typography>
              {selectedStructure.today_catalog.services
                ? selectedStructure.today_catalog.services.map((service) => (
                    <LandingCatalogMenu
                      service={service}
                      handleSelect={() => serviceSelectHandler(service)}
                    />
                  ))
                : null}
            </Hoc>
          ) : (
            <SearchCatalogMenu
              structureId={structure_id}
              serviceSelectHandler={serviceSelectHandler}
            />
          )}
        </Box>
        <Box className={classes.ServiceDisplay}>
          {isSeletedStructureDataLoading ? (
            <StructureDetailSkeleton />
          ) : selectedService.service_id ? (
            <StructureDetail
              structure={{ ...selectedStructure, structure_id }}
              selectedService={selectedService}
              gotoCatalog={() => changeContentHandler('dayCatalog')}
            />
          ) : (
            <StructureDetail
              structure={{ ...selectedStructure, structure_id }}
              gotoCatalog={() => changeContentHandler('dayCatalog')}
            />
          )}
        </Box>
      </Box>
      {!isClosestStructuresLoading &&
      closestStructures.length === 0 &&
      !isSameCategoryStructuresLoading &&
      sameCategoryStructures.length === 0 ? null : (
        <Box className={classes.OtherCatalogs}>
          {!isClosestStructuresLoading &&
          closestStructures.length === 0 ? null : (
            <>
              <Box className={classes.ScrollbarHeard}>
                <Typography
                  variant='body1'
                  color='secondary'
                  className={classes.OtherSubtitles}
                >
                  {formatMessage({ id: 'otherCatalog1' })}{' '}
                  {`<<${selectedStructure.name}>>`}
                </Typography>
                {isSeletedStructureDataLoading ? null : (
                  <Box className={classes.SeeMoreBox}>
                    <Button
                      color='secondary'
                      onClick={() =>
                        moreStucturesHandler({ longitude, latitude })
                      }
                    >
                      {formatMessage({ id: 'seeMore' })}
                    </Button>
                  </Box>
                )}
              </Box>
              <div className={classes.HtmlDiv}>
                <Scrollbars style={{ width: '95%', height: '270px' }}>
                  <Box className={classes.ChildCatalog}>
                    {isClosestStructuresLoading
                      ? [...new Array(randomNumber(3, 10))].map(
                          (rand, index) => <StructureSkeleton key={index} />
                        )
                      : closestStructures.map((structure, index) => (
                          <OtherStructure
                            key={structure.structure_id}
                            myId={`Tsy${index}`}
                            structure={structure}
                            handleSelect={() =>
                              structureSelectHandler(structure.structure_id)
                            }
                          />
                        ))}
                  </Box>
                </Scrollbars>
              </div>
            </>
          )}
          {!isSameCategoryStructuresLoading &&
          sameCategoryStructures.length === 0 ? null : (
            <>
              <Box className={classes.ScrollbarHeard}>
                <Typography
                  variant='body1'
                  color='secondary'
                  className={classes.OtherSubtitles}
                >
                  {formatMessage({ id: 'otherCatalog2' })}{' '}
                  {selectedStructure.location}{' '}
                  {formatMessage({ id: 'otherCatalog2End' })}
                </Typography>
                {isSameCategoryStructuresLoading ? null : (
                  <Box className={classes.SeeMoreBox}>
                    <Button
                      color='secondary'
                      onClick={() =>
                        moreStucturesHandler({
                          category_id: sameCategoryStructures[0].category_id,
                          category_name:
                            sameCategoryStructures[0].category_name,
                        })
                      }
                    >
                      {formatMessage({ id: 'seeMore' })}
                    </Button>
                  </Box>
                )}
              </Box>
              <Scrollbars style={{ width: '95%', height: '275px' }}>
                <Box className={classes.ChildCatalog}>
                  {isSameCategoryStructuresLoading
                    ? [...new Array(randomNumber(3, 10))].map(() => (
                        <StructureSkeleton />
                      ))
                    : sameCategoryStructures.map((structure) => (
                        <Structure
                          key={structure.structure_id}
                          myId={`sy${structure.structure_id}`}
                          structure={structure}
                          handleSelect={() =>
                            structureSelectHandler(structure.structure_id)
                          }
                        />
                      ))}
                </Box>
              </Scrollbars>
            </>
          )}
        </Box>
      )}
      <QuestionBox />
    </Box>
  )
}

export default injectIntl(SelectedStructure)
