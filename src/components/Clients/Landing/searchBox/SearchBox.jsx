import * as yup from 'yup'
import { useFormik } from 'formik'
import { injectIntl } from 'react-intl'
import WcIcon from '@material-ui/icons/Wc'
import PlaceIcon from '@material-ui/icons/Place'
import LocalDiningIcon from '@material-ui/icons/LocalDining'
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet'
import {
  makeStyles,
  TextField,
  InputAdornment,
  Grid,
  Button,
  CircularProgress,
  Box,
} from '@material-ui/core'
import { getUserStructures } from '../../../../services/structure.service'
import { usePerson } from '../../../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import handleSessionExpiredError from '../../../../utils/handleSessionExpiry'

const useStyles = makeStyles((theme) => ({
  SearchBox: {
    display: 'grid',
    boxShadow:
      '0px 4px 4px rgba(0, 0, 0, 0.25), 0px 4px 4px rgba(0, 0, 0, 0.25)',
    padding: '15px',
    backgroundColor: 'white',
    maxWidth: "500px",
    borderRadius: 15
  },
  TextField: {
    padding: '5px',
    width: "100%"
  },
  Button: {
    border: '2px solid ' + theme.palette.primary.main,
    boxSizing: 'border-box',
    borderRadius: '10px',
    marginTop: '15px',
    letterSpacing: '1.5px',
  },
  cancelButton: {
    border: '2px solid ' + theme.palette.primary.secondary,
    boxSizing: 'border-box',
    borderRadius: '10px',
    marginTop: '15px',
    letterSpacing: '1.5px',
  },
}))

function SearchBox({
  intl: { formatMessage },
  isSearchActive,
  setIsSearchActive,
  setIsSearching,
  isSearching,
  handleSearch,
}) {
  let classes = useStyles()

  // let [isSearching, setIsSearching] = useState(false);

  const validationSchema = yup.object({
    meal: yup
      .string('')
      .required(formatMessage({ id: 'requiredFieldMessage' })),
    budget: yup
      .string('')
      .required(formatMessage({ id: 'requiredFieldMessage' })),
    place: yup
      .string('')
      .required(formatMessage({ id: 'requiredFieldMessage' })),
    numberOfPersons: yup
      .string('')
      .required(formatMessage({ id: 'requiredFieldMessage' })),
  })

  const { personDispatch } = usePerson()
  const location = useLocation()
  const navigate = useNavigate()

  const formik = useFormik({
    initialValues: {
      meal: '',
      place: '',
      budget: '',
      numberOfPersons: 1,
    },
    validationSchema: validationSchema,
    onSubmit: (values, { resetForm }) => {
      setIsSearching(true)
      getUserStructures({
        meal: values.meal,
        number_of_persons: values.numberOfPersons,
        budget: values.budget,
      })
        .then(({ structures }) => {
          handleSearch(structures)
          setIsSearching(false)
          setIsSearchActive(true)
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate,
          )
          setIsSearching(false)
        })
    },
  })

  const cancelSearch = () => {
    formik.resetForm()
    setIsSearching(false)
    setIsSearchActive(false)
  }

  return (
    <Box className={classes.SearchBox}>
      <Grid container>
        <Grid items xs={12} sm={6}>
          <TextField
            id="meal"
            name="meal"
            variant="filled"
            onChange={formik.handleChange}
            value={formik.values.meal || ''}
            className={classes.TextField}
            label={formatMessage({ id: 'mealLabel' })}
            error={formik.touched.meal && Boolean(formik.errors.meal)}
            helperText={formik.touched.meal && formik.errors.meal}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <LocalDiningIcon />
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid items xs={12} sm={6}>
          <TextField
            id="place"
            name="place"
            variant="filled"
            onChange={formik.handleChange}
            value={formik.values.place || ''}
            className={classes.TextField}
            label={formatMessage({ id: 'placeLabel' })}
            error={formik.touched.place && Boolean(formik.errors.place)}
            helperText={formik.touched.place && formik.errors.place}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <PlaceIcon />
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid items xs={6}>
          <TextField
            id="budget"
            name="budget"
            variant="filled"
            onChange={formik.handleChange}
            value={formik.values.budget || ''}
            className={classes.TextField}
            label={formatMessage({ id: 'budgetLabel' })}
            error={formik.touched.budget && Boolean(formik.errors.budget)}
            helperText={formik.touched.budget && formik.errors.budget}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <AccountBalanceWalletIcon />
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid items xs={6}>
          <TextField
            id="numberOfPersons"
            name="numberOfPersons"
            variant="filled"
            type="number"
            className={classes.TextField}
            onChange={formik.handleChange}
            value={formik.values.numberOfPersons || ''}
            label={formatMessage({ id: 'numberOfPersonsLabel' })}
            error={
              formik.touched.numberOfPersons &&
              Boolean(formik.errors.numberOfPersons)
            }
            helperText={
              formik.touched.numberOfPersons && formik.errors.numberOfPersons
            }
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <WcIcon />
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid items xs={6} sm={12}>
          {isSearchActive ? (
            <Button
              className={classes.cancelButton}
              color="secondary"
              variant="contained"
              disabled={isSearching}
              onClick={cancelSearch}
            >
              {formatMessage({ id: 'cancelSearchButton' })}
            </Button>
          ) : (
            <Button
              className={classes.Button}
              type="submit"
              color="primary"
              variant="outlined"
              disabled={isSearching}
            >
              {isSearching && (
                <CircularProgress
                  color="secondary"
                  fontSize={15}
                  style={{ marginInline: '15px' }}
                />
              )}
              {formatMessage({ id: 'searchButton' })}
            </Button>
          )}
        </Grid>
      </Grid>
    </Box>
  )
}

export default injectIntl(SearchBox)
