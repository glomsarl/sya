import * as yup from 'yup'
import { useState } from 'react'
import { useFormik } from 'formik'
import { injectIntl } from 'react-intl'
import {
  TextField,
  makeStyles,
  Button,
  CircularProgress,
  Typography,
} from '@material-ui/core'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../../../contexts/PersonContext/Person.provider'
import { fetchCatalogServices } from '../../../../services/catalogs.service'
import handleSessionExpiredError from '../../../../utils/handleSessionExpiry'
import LandingCatalogMenu from '../LandingCatalogMenu/LandingCatalogMenu'

const useStyles = makeStyles((theme) => ({
  SearchBox: {
    display: 'grid',
    gridAutoFlow: 'column',
    gap: '10px',
    padding: '10px',
    [theme.breakpoints.down('xs')]: {
      gridAutoFlow: 'row',
    },
  },
  Button: {
    width: 'fit-content',
    height: '40px'
  },
  Inputs: {
    display: 'grid',
    gridAutoFlow: 'column',
  },
}))

function SearchCatalogMenu({
  intl: { formatMessage },
  structureId,
  serviceSelectHandler,
}) {
  let classes = useStyles()
  let [isSubmitting, setIsSubmitting] = useState(false)
  let [isSearch, setIsSearch] = useState(false)
  const navigate = useNavigate()
  const location = useLocation()
  const { personDispatch } = usePerson()

  const [services, setServices] = useState([])

  const validationSchema = yup.object({
    date: yup
      .string('date ')
      .required(formatMessage({ id: 'requiredFieldMessage' })),
    time: yup
      .string('time')
      .required(formatMessage({ id: 'requiredFieldMessage' })),
  })

  const formik = useFormik({
    initialValues: {
      date: null,
      time: null,
    },
    validationSchema: validationSchema,
    onSubmit: (values, { resetForm }) => {
      setIsSubmitting(true)
      fetchCatalogServices({
        structure_id: structureId,
        date: new Date(`${values.date} ${values.time}`),
      })
        .then(({ catalog_services: services }) => {
          setServices(services)
          setIsSubmitting(false)
          setIsSearch(true)
          resetForm()
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
        })
    },
  })

  return (
    <>
      <form onSubmit={formik.handleSubmit} className={classes.SearchBox}>
        <TextField
          name='date'
          type='date'
          size='small'
          labelId='date'
          variant='outlined'
          onChange={formik.handleChange}
          value={formik.values.date || ''}
          error={formik.touched.date && Boolean(formik.errors.date)}
          helperText={formik.touched.date && formik.errors.date}
        />
        <TextField
          name='time'
          type='time'
          size='small'
          labelId='time'
          variant='outlined'
          onChange={formik.handleChange}
          value={formik.values.time || ''}
          error={formik.touched.time && Boolean(formik.errors.time)}
          helperText={formik.touched.time && formik.errors.time}
        />
        <Button
          type='submit'
          size='small'
          color='secondary'
          variant='contained'
          disabled={isSubmitting}
          className={classes.Button}
        >
          {isSubmitting && (
            <CircularProgress
              color='primary'
              style={{ width: '20px', height: '20px' }}
            />
          )}
          {formatMessage({ id: 'searchButton' })}
        </Button>
      </form>
      {isSearch ? (
        <>
          <Typography
            variant='h4'
            color='secondary'
            style={{ fontWeight: 'bold', textAlign: 'center' }}
          >
            {formatMessage({ id: 'resultInfo' })}
          </Typography>
          {services.map((service) => (
            <LandingCatalogMenu
              service={service}
              handleSelect={() => serviceSelectHandler(service)}
            />
          ))}
        </>
      ) : null}
    </>
  )
}

export default injectIntl(SearchCatalogMenu)
