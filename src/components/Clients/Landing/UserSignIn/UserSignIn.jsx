import * as yup from 'yup'
import { useEffect, useState } from 'react'
import { useFormik } from 'formik'
import { injectIntl } from 'react-intl'
import {
  makeStyles,
  Typography,
  Button,
  TextField,
  Divider,
  CircularProgress,
} from '@material-ui/core'

import { usePerson } from '../../../../contexts/PersonContext/Person.provider'
import { useLandingOngoingOrder } from '../../../../contexts/LandingOngoingOrderContext/LandingOngoingOrder.provider'
import {
  purchase,
  unAuthenticatedPurchase,
} from '../../../../services/order.service'
import handleSessionExpiredError from '../../../../utils/handleSessionExpiry'
import { useLocation, useNavigate } from 'react-router'
import { findUserInfo } from '../../../../services/authentication.service'

const useStyles = makeStyles((theme) => ({
  Form: {
    display: 'grid',
    gridGap: '10px',
    minHeight: '300px',
    alignItems: 'center',
  },
  SubTitle: {
    fontWeight: 'bold',
    padding: '5px 5px',
    justifySelf: 'center',
    textAlign: 'center',
    width: '70%',
  },
  NoAccount: {
    justifySelf: 'right',
    width: 'fit-content',
  },
}))

function UserSignIn({ intl, setCurrentDialog, setErrorMessage }) {
  let classes = useStyles()
  let { formatMessage } = intl

  let [isSubmitting, setIsSubmitting] = useState({
    formik: false,
    google: false,
  })

  const {
    landingOngoingOrderState: { clientOrder },
  } = useLandingOngoingOrder()
  const [isUserInfoLoading, setIsUserInfoLoading] = useState(true)

  let {
    personState: { person },
    personDispatch,
  } = usePerson()
  const navigate = useNavigate()
  const location = useLocation()

  const validationSchema = yup.object({
    email: yup
      .string(formatMessage({ id: 'emailHolder' }))
      .email(formatMessage({ id: 'invalidEmail' }))
      .required(formatMessage({ id: 'requiredFieldMessage' })),
    password: yup.string(formatMessage({ id: 'passwordHolder' })),
  })

  useEffect(() => {
    findUserInfo()
      .then((person) => {
        personDispatch({ type: 'LOAD_PERSON_DATA', payload: person })
        formik.setFieldValue('email', person.email)
        setIsUserInfoLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate,
        )
        setIsUserInfoLoading(false)
      })
  
 // eslint-disable-next-line
}, [])

  const formik = useFormik({
    initialValues: {
      email: person.email || '',
      password: '',
    },
    validationSchema,
    onSubmit: (values, { resetForm }) => {
      setIsSubmitting({ formik: true })
      unAuthenticatedPurchase('sign-in', {
        ...values,
        client_order: clientOrder,
      })
        .then((person) => {
          personDispatch({ type: 'LOAD_PERSON_DATA', payload: person })
          setIsSubmitting({ formik: false, google: false })
          setCurrentDialog('Success')
          resetForm()
        })
        .catch((error) => {
          const formatMessageId = handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate,
          )
          setCurrentDialog('Failure')
          setErrorMessage(formatMessageId)
          setIsSubmitting({ formik: false, google: false })
        })
    },
  })
  const [isOrdering, setIsOrdering] = useState(false)
  const clientPurchase = () => {
    setIsOrdering(true)
    purchase({ client_order: clientOrder })
      .then(() => {
        setCurrentDialog('Success')
        setIsOrdering(false)
      })
      .catch((error) => {
        setCurrentDialog('Failure')
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate,
        )
        setIsOrdering(false)
      })
  }

  const googleConnectionHandler = () => {
    setIsSubmitting({ google: true })
    setTimeout(() => {
      setIsSubmitting({ google: false })
    }, 3000)
  }

  return (
    <form onSubmit={formik.handleChange} className={classes.Form}>
      <Typography
        variant="body2"
        color="textSecondary"
        className={classes.SubTitle}
      >
        {person.email
          ? `${formatMessage({ id: 'continueAs' })} ${person.email}`
          : formatMessage({ id: 'signInMessage' })}
      </Typography>
      {!person.email ? (
        <>
          <TextField
            name="email"
            variant="filled"
            size="small"
            disabled={isUserInfoLoading || person.isConnected}
            value={formik.values.email}
            onChange={formik.handleChange}
            label={formatMessage({ id: 'emailAddress' })}
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email || formik.errors.email}
          />
          <TextField
            name="password"
            variant="filled"
            size="small"
            type="password"
            disabled={isUserInfoLoading || person.isConnected}
            value={formik.values.password}
            onChange={formik.handleChange}
            label={formatMessage({ id: 'password' })}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password || formik.errors.password}
          />
          <Button
            color="textSecondary"
            onClick={() => setCurrentDialog('SignUp')}
            disabled={isUserInfoLoading}
            className={classes.NoAccount}
          >
            {formatMessage({ id: 'notSignUp' })}
          </Button>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            onClick={formik.handleSubmit}
            disabled={isUserInfoLoading}
            style={{ marginBottom: '10px' }}
          >
            {isSubmitting.formik && (
              <CircularProgress
                color="secondary"
                style={{ marginInline: '5px', width: '20px', height: '20px' }}
              />
            )}
            {formatMessage({ id: 'goOn' })}
          </Button>
          <Divider />
          <Button
            variant="outlined"
            onclick={googleConnectionHandler}
            disabled={isUserInfoLoading}
            color="secondary"
          >
            {isSubmitting.google && (
              <CircularProgress
                color="secondary"
                style={{ marginInline: '5px', width: '20px', height: '20px' }}
              />
            )}
            {formatMessage({ id: 'orderConnection' })}
          </Button>
          <Typography variant="body2" color="textSecondary">
            {formatMessage({ id: 'confidentiality' })}
          </Typography>
        </>
      ) : (
        <Button
          variant="outlined"
          color="secondary"
          onClick={clientPurchase}
          disabled={isUserInfoLoading || isOrdering}
          style={{
            height: 'fit-content',
            width: 'fit-content',
            justifySelf: 'center',
          }}
        >
          {isOrdering && (
            <CircularProgress
              color="secondary"
              style={{ marginInline: '5px', width: '20px', height: '20px' }}
            />
          )}
          {formatMessage({ id: 'goOn' })}
        </Button>
      )}
    </form>
  )
}

export default injectIntl(UserSignIn)
