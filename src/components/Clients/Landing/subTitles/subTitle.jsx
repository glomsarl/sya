import { Typography, makeStyles } from "@material-ui/core";
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';

const useStyles = makeStyles((theme) => ({
    SubTitle:{
        color:theme.palette.secondary.main,
        margin:'5px',
        fontStyle: 'normal',
        fontWeight: 600,
        fontSize: '36px',
        letterSpacing: '1.5px',
        [theme.breakpoints.down('xs')]:{
            fontSize: '25px',
        }
    }
}))

export default function SubTitle({subTitle, style}){
    let classes = useStyles()

    return (
        <Typography variant='h2' className={classes.SubTitle} style={{justifySelf:'center'}}>
            <DoubleArrowIcon color='primary' />
            {subTitle}
        </Typography>
    )
}