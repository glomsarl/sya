import { injectIntl } from 'react-intl'
import { Box, Typography, makeStyles, Grid, Button } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  LastGrid: {
    width: '65%',
    justifySelf: 'center',
    border: '1px solid ' + theme.palette.secondary.main,
    padding: '15px',
    borderRadius: 15,
    [theme.breakpoints.down('xs')]: {
      width: '85%',
    },
  },
  GreatQuestion: {
    fontWeight: 'bold',
    textAlign: 'center',
    width: '50%',
    justifySelf: 'center',
    marginTop: '50px'
  },
  CallToSignBox: {
    display: 'grid',
    padding: '20px',
    marginBottom: '20px',
    gridAutoFlow: 'column',
    justifySelf: 'center',
  },
}))

export default injectIntl(function QuestionBox({ intl: { formatMessage } }) {
  let classes = useStyles()
  return (
    <>
      <Typography
        className={classes.GreatQuestion}
        variant='h3'
        color='secondary'
      >
        {formatMessage({ id: 'greatQuestion' })}
      </Typography>
      <Box className={classes.CallToSignBox}>
        <Grid
          container
          direction='row'
          justify='center'
          alignItems='center'
          className={classes.LastGrid}
        >
          <Grid items xs>
            <Typography
              gutterBottom
              variant='h3'
              component='h2'
              color='Secondary'
            >
              {formatMessage({ id: 'callToSignUp' })}
            </Typography>
            <Typography variant='body1' component='p'>
              {formatMessage({ id: 'callToSignUpText' })}
            </Typography>
            <Button variant='outlined' color='primary' href='/demand'>
              {formatMessage({ id: 'moreInfoButton' })}
            </Button>
          </Grid>
          <Grid items xs style={{ marginLeft: '25px' }}>
            <Typography
              gutterBottom
              variant='h3'
              component='h2'
              color='Secondary'
            >
              {formatMessage({ id: 'callToSignIn' })}
            </Typography>
            <Typography variant='body1' component='p'>
              {formatMessage({ id: 'callToSignInText' })}
            </Typography>
            <Button variant='outlined' color='primary' href='/sign-in'>
              {formatMessage({ id: 'goToSignInButton' })}
            </Button>
          </Grid>
        </Grid>
      </Box>
    </>
  )
})
