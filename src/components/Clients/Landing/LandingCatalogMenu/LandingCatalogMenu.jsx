import { Typography, Box, makeStyles } from '@material-ui/core'

const useStyles = makeStyles({
  ServiceItem: {
    fontWeight: 'bold',
    display: 'grid',
    gridAutoFlow: 'coloumn',
    marginTop: '5px',
  },
  Service: {
    display: 'grid',
    width: '98%',
    marginTop: '5px',
  },
  MenuName: {
    display: 'grid',
    gridAutoFlow: 'column',
    fontWeight: 'bold',
    cursor: 'pointer',
  },
})

function LandingCatalogMenu({ service, handleSelect }) {
  let classes = useStyles()

  let { service_name, unit_price, description } = service

  return (
    <Box className={classes.Service}>
      <Typography variant='body2' className={classes.ServiceItem}>
        <Typography
          variant='body1'
          color='secondary'
          className={classes.MenuName}
          onClick={handleSelect}
        >
          {service_name}
          <Typography
            variant='body2'
            style={{ fontWeight: 'bold', justifySelf: 'right' }}
          >
            {unit_price} FCFA
          </Typography>
        </Typography>
      </Typography>
      <Typography variant='body2' color='textSecondary'>
        {description}
      </Typography>
    </Box>
  )
}

export default LandingCatalogMenu
