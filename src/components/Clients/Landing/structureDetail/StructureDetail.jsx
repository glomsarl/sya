import { useState, useEffect } from 'react'
import { injectIntl } from 'react-intl'
import Carousel from 'react-material-ui-carousel'
import { Box, Typography, makeStyles, Button } from '@material-ui/core'

import Purchasing from '../popUps/Purchasing'
import { notifyInfo } from '../../../../utils/toastMessages'
import { useLandingOngoingOrder } from '../../../../contexts/LandingOngoingOrderContext/LandingOngoingOrder.provider'
import maxPromotion from '../../../../utils/maxPromotion'
import { getAddress } from '../../../../utils/geocodeAddress'

const useStyles = makeStyles((theme) => ({
  StructureDetail: {
    display: 'grid',
    borderRadius: '10px',
    gridAutoFlow: 'row',
    padding: '10px',
    gridGap: '5px',
    backgroundColor: '#FFFF',
    maxWidth: '350px',
    boxShadow:
      '0px 4px 4px rgba(0, 0, 0, 0.25), 0px 4px 4px rgba(0, 0, 0, 0.25)',
  },
  ActionButton: {
    width: 'fit-content',
    height: 'fit-content',
    color: theme.palette.secondary.main,
    boxSizing: 'border-box',
    backgroundColor: theme.palette.success.main,
    '&:hover': {
      backgroundColor: theme.palette.success.dark,
    },
  },
  ActionBox: {
    display: 'grid',
    gridAutoFlow: 'row',
    justifyContent: 'center',
    backgroundColor: '#F7F7FC',
    textAlign: 'center',
    height: 'fit-content',
    padding: '15px',
    width: '100%',
    fontSize: '17px',
  },
  Actions: {
    display: 'flex',
    alignItems: 'center',
    gridGap: '5px',
    justifySelf: 'center',
  },
  Information: {
    display: 'grid',
    backgroundColor: theme.palette.secondary.main,
    color: 'white',
    textAlign: 'center',
    gridAutoFlow: 'row',
    justifyContent: 'center',
    padding: '5px',
  },
  PhoneNumber: {
    justifySelf: 'center',
    width: '85%',
    color: theme.palette.secondary.main,
    backgroundColor: '#FFFF',
    height: 'fit-content',
  },
  Details: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
    padding: '10px',
    paddingBottom: '0px',
  },
  Home: {
    backgroundColor: '#38B000',
    borderRadius: '5px',
    padding: '0 5px',
    fontSize: '0.8rem',
    width: 'fit-content',
  },
  Deliver: {
    backgroundColor: '#1A759F',
    borderRadius: '5px',
    padding: '0 5px',
    fontSize: '0.8rem',
    width: 'fit-content',
  },
  Reserve: {
    backgroundColor: '#38B000',
    borderRadius: '5px',
    padding: '0 5px',
    fontSize: '0.8rem',
    width: 'fit-content',
  },
  TagHolder: {
    display: 'grid',
    gridAutoFlow: 'column',
    height: '20px',
    gridGap: '5px',
    width: 'fit-content',
    padding: '0 10px',
  },
  PromotionEtiquet: {
    width: '40px',
    height: 'fit-content',
    borderRadius: '10px',
    textAlign: 'center',
  },
  navButton: {
    '&:hover': {
      backgroundColor: theme.palette.primary.main,
    },
  },
}))
function StructureDetail({
  intl: { formatMessage },
  structure,
  gotoCatalog,
  selectedService,
}) {
  let classes = useStyles()
  let {
    name,
    rating,
    logo_ref,
    longitude,
    latitude,
    owner_name,
    description,
    owner_number,
    is_take_away,
    structure_id,
    is_reservable,
    average_price,
    is_on_promotion,
    is_structure_sy,
    is_home_delivery,
    promotion_percentage,
    specific_indications,
  } = structure

  let { landingOngoingOrderDispatch } = useLandingOngoingOrder()

  let [isDialogVisible, setIsDialogVisible] = useState(false)
  let [reduction, setReduction] = useState(0)
  let [promotion, setPromotion] = useState(0)

  const ongoingOrderHandler = () => {
    if (selectedService) {
      setIsDialogVisible(true)
      let order = { service: selectedService, structure_id }
      landingOngoingOrderDispatch({
        type: 'LOAD_ONGOING_ORDER_DETAILS',
        payload: order,
      })
    } else {
      gotoCatalog()
      notifyInfo(formatMessage({ id: 'noService' }))
    }
  }

  const closePurchaseHandler = () => {
    setIsDialogVisible(false)
    landingOngoingOrderDispatch({
      type: 'RESET_ONGOING_ORDER',
    })
  }

  useEffect(() => {
    if (selectedService) {
      const { promotions } = selectedService
      console.log(promotions)
      setReduction(maxPromotion(promotions, 'Reduction').max_promotion_value)
      setPromotion(maxPromotion(promotions, 'Bonus').max_promotion_value)
    }
    // eslint-disable-next-line
  }, [selectedService?.promotions])

  const [address, setAddress] = useState()
  if (!address && longitude && latitude)
    getAddress(latitude, longitude).then((address) => setAddress(address))

  return (
    <Box className={classes.StructureDetail}>
      {selectedService ? (
        <Carousel autoPlay={false}>
          {[
            {
              service_image_id: 'main_image_ref',
              service_image_ref: selectedService.main_image_ref,
            },
            ...selectedService.service_images,
          ].map(({ service_image_id, service_image_ref }) => (
            <div
              key={service_image_id}
              style={{
                height: '495px',
                backgroundSize: 'contain',
                backgroundImage: `url(${process.env.REACT_APP_BASE_URL}/${service_image_ref})`,
              }}
            ></div>
          ))}
        </Carousel>
      ) : (
        <img
          src={`${process.env.REACT_APP_BASE_URL}/${logo_ref}`}
          alt='service ref img'
          style={{ maxHeight: '250px' }}
        />
      )}
      <Box className={classes.Details}>
        <Typography
          variant='h4'
          color='secondary'
          style={{ fontWeight: 'bold', display: 'grid', gridAutoFlow: 'row' }}
        >
          {selectedService ? selectedService.service_name : name}
          <Typography variant='caption' style={{ color: '#38B000' }}>
            {selectedService
              ? `${selectedService.unit_price} FCFA`
              : formatMessage({ id: 'seePlan' })}
          </Typography>
        </Typography>
        <Typography
          variant='h3'
          color='secondary'
          style={{ fontWeight: 'bold', justifySelf: 'right' }}
        >
          {selectedService ? selectedService.rating : rating}
          <span style={{ fontSize: '15px' }}>/10</span>
        </Typography>
      </Box>
      <Typography variant='body2' color='textSecondary'>
        {address ? `${address}, ${specific_indications}` : specific_indications}
        , {description}
      </Typography>
      <Typography variant='body2' color='textSecondary'>
        {selectedService ? (
          selectedService.description
        ) : (
          <>
            <Typography
              variant='body1'
              color='secondary'
              style={{ fontWeight: 'bold' }}
            >
              {formatMessage({ id: 'structureAvgPrice' })}{' '}
              {`${average_price} francs CFA`}
            </Typography>
          </>
        )}
      </Typography>
      <Box className={classes.TagHolder}>
        {selectedService && selectedService.is_take_away ? (
          <Typography className={classes.Home}>
            {formatMessage({ id: 'takeAway' })}
          </Typography>
        ) : selectedService === undefined && is_take_away ? (
          <Typography className={classes.Home}>
            {formatMessage({ id: 'takeAway' })}
          </Typography>
        ) : null}
        {selectedService && selectedService.is_reservable ? (
          <Typography className={classes.Reserve}>
            {formatMessage({ id: 'reserve' })}
          </Typography>
        ) : selectedService === undefined && is_reservable ? (
          <Typography className={classes.Reserve}>
            {formatMessage({ id: 'reserve' })}
          </Typography>
        ) : null}
        {selectedService && selectedService.is_home_delivery ? (
          <Typography className={classes.Deliver}>
            {formatMessage({ id: 'delivery' })}
          </Typography>
        ) : selectedService === undefined && is_home_delivery ? (
          <Typography className={classes.Deliver}>
            {formatMessage({ id: 'delivery' })}
          </Typography>
        ) : null}
        {selectedService && reduction !== 0 ? (
          <Typography
            variant='caption'
            style={{ backgroundColor: 'red' }}
            className={classes.PromotionEtiquet}
          >
            {`${reduction}%`}
          </Typography>
        ) : null}
        {(selectedService && promotion !== 0) || is_structure_sy ? (
          <Typography
            variant='caption'
            color='primary'
            className={classes.PromotionEtiquet}
          >
            {promotion !== 0 ? `${promotion}SY` : 'SY'}
          </Typography>
        ) : null}
        {!selectedService && is_on_promotion ? (
          <Typography
            variant='caption'
            style={{ backgroundColor: 'red' }}
            className={classes.PromotionEtiquet}
          >
            {promotion_percentage}%
          </Typography>
        ) : null}
      </Box>
      <Box className={classes.ActionBox}>
        <Box className={classes.Actions}>
          {/* command button */}
          <Button
            className={classes.ActionButton}
            variant='contained'
            onClick={() => ongoingOrderHandler()}
          >
            {formatMessage({ id: 'command' })}
          </Button>
          {/* reserve button */}
          <Button
            className={classes.ActionButton}
            variant='contained'
            onClick={() => ongoingOrderHandler()}
          >
            {formatMessage({ id: 'reserve' })}
          </Button>
        </Box>
        <span style={{ textAlign: 'center', justifySelf: 'center' }}>
          {formatMessage({ id: 'freeServiceInfo' })}
        </span>
      </Box>
      {selectedService ? null : (
        <Box className={classes.Information}>
          <Typography variant='h6'>
            {formatMessage({ id: 'importantMessage' })}
          </Typography>
          <Typography variant='body2' style={{ opacity: 0.8, margin: '5px 0' }}>
            {description}
          </Typography>
          <Typography variant='h6' className={classes.PhoneNumber}>
            {`${owner_name ?? 'owner'}: ${owner_number}`}
          </Typography>
        </Box>
      )}
      {/* *
       * here are all pop component use by this component also use by
       * src/pages/the LandingDetails page.
       * The popUps that are found here interact with the reserve and
       * command buttons
       * */}
      <Purchasing
        logoRef={logo_ref}
        show={isDialogVisible}
        structureId={structure_id}
        formatMessage={formatMessage}
        handleClose={closePurchaseHandler}
        location={{ lat: latitude, lng: longitude }}
      />
    </Box>
  )
}

export default injectIntl(StructureDetail)
