import { Skeleton } from "@material-ui/lab";
import {Box, makeStyles} from '@material-ui/core';

const useStyles = makeStyles ((theme)=>({
    root:{
        width: "300px",
        marginLeft: "10px",
    },
    noteSkeleton:{
        display: "grid",
        gridTemplateColumns:"1fr auto auto",
        gridGap: "10px",
        width:'100%'
    },
    tagsSkeleton: {
        display: "grid",
        gridAutoFlow: "column",
        gridGap: `${10/3}px`,
        margin:'10px'
    }
}))
function StructureDetailSkeleton(){
    const classes = useStyles();
    return(
        <Box className={classes.root}>
            <Box className={classes.noteSkeleton}>
                <Skeleton variant="text" width="60%"/>
                <Skeleton variant="text" width={20} height={20} style={{justifySelf:"end"}}/>
            </Box>
            <Skeleton variant="text" width="95%" />
            <Skeleton variant="text" width="45%" />
            <Skeleton variant="text" width="25%" />
            <Box className={classes.tagsSkeleton} >
                <Skeleton variant="text" width="100%" />
                <Skeleton variant="text" width="100%" />
                <Skeleton variant="text" width="100%" />
            </Box>
            <Box className={classes.tagsSkeleton} >
                <Skeleton variant="rect" width="100%" />
                <Skeleton variant="rect" width="100%" />
            </Box>
            <Skeleton variant="rect" width='100%' height={50} />
            <Skeleton variant="rect" width='100%' height={50} />
            <Skeleton variant="rect" width='100%' height={50} />
        </Box>
    )
}

export default StructureDetailSkeleton