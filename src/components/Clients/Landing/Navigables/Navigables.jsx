import { useState } from 'react'
import { IconButton, Tooltip } from '@material-ui/core'
import { NavigateBeforeRounded, NavigateNextRounded } from '@material-ui/icons'

export default function Navigables() {
  let [wantedDivId, setWantedDivId] = useState(0)

  let scrollHandler = (e, elementWidth, direction, indicator) => {
    let scrollbarWidth = document
      .getElementById(indicator)
      .getClientRects()[0].width
    let wantedId = parseInt(scrollbarWidth / elementWidth)
    let wantedDiv = null
    let id = 0
    if (direction === 'left') {
      id = wantedDivId - wantedId + 1
      wantedDiv = document.getElementById(indicator + '' + id)
      while (wantedDiv === null && id > 0) {
        id--
        wantedDiv = document.getElementById(indicator + '' + id)
      }
    } else {
      id = wantedId
      wantedDiv = document.getElementById(indicator + '' + id)
      let nextId = id + 1
      if (document.getElementById(indicator + '' + nextId)) {
        id++
      }
    }
    if (wantedDiv) {
      setWantedDivId(id)
      wantedDiv.scrollIntoViewIfNeeded()
    }
  }
  return (
    <>
      <Tooltip arrow title='back'>
        <IconButton
          size='small'
          onClick={(e) => scrollHandler(e, 250, 'left', 'sy')}
          style={{
            marginLeft: '5px',
            borderRadius: '5px',
            border: '1px solid yellow',
          }}
        >
          <NavigateBeforeRounded style={{ fontSize: 15 }} />
        </IconButton>
      </Tooltip>
      <Tooltip arrow title='next'>
        <IconButton
          size='small'
          style={{
            marginLeft: '5px',
            borderRadius: '5px',
            border: '1px solid yellow',
          }}
        >
          <NavigateNextRounded style={{ fontSize: 15 }} />
        </IconButton>
      </Tooltip>
    </>
  )
}
