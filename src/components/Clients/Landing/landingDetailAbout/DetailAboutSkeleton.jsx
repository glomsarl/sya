import { Skeleton } from "@material-ui/lab";
import {Box, makeStyles, Divider, Button } from '@material-ui/core';

const useStyles = makeStyles ((theme)=>({
    root:{
        width: "400px",
        marginLeft: "10px"
    },
    noteSkeleton:{
        display: "grid",
        gridTemplateColumns:"1fr auto auto",
        gridGap: "10px"
    },
    tagsSkeleton: {
        display: "grid",
        gridAutoFlow: "column",
        gridGap: `${10/3}px`
    }
}))
function DetailAboutSkeleton(){
    const classes = useStyles();
    return(
        <Box className={classes.root}>
            <Skeleton variant="text" width="85%" />
            <Skeleton variant="text" width="25%" />
            <Skeleton variant="text" width="75%" />
            <Skeleton variant="text" width="100%"/>
            <Divider />
            <Skeleton variant="text" width="10%" />
            <Skeleton variant="text" width="90%" />
            <Skeleton variant="text" width="50%" />
            <Divider />
            <Skeleton variant="text" width="90%" />
            <Skeleton variant="text" width="50%" />
            <Skeleton variant="text" width="100%"/>
            <Divider />
            <Skeleton variant="text" width="10%" />
            <Skeleton variant="text" width="90%" />
            <Skeleton variant="text" width="50%" />
            <Skeleton variant="text" width="90%" />
            <Skeleton variant="text" width="50%" />
            <Divider />
            <Box style={{display:'grid', gridAutoFlow:'column'}}>
                <Skeleton variant="rect" style={{width:'50px', hieght:'50px', borderRadius:'100%'}} />
                <Box style={{display:'grid', gridAutoFlow:'row',}}>
                    <Skeleton variant="text" width="95%" />
                    <Skeleton variant="text" width="60%" />
                </Box>
                <Skeleton variant="text" width="20%" style={{justifySelf:'right'}}/>
            </Box>
            <Skeleton variant="text" width="85%" />
            <Skeleton variant="text" width="50%" />
            <Skeleton variant="text" width="60%" />
            <Divider />
            <Button style={{color:'#38B000', textAlign:'right', justifySelf:'right'}} disabled={true}>
                Read More
            </Button>
        </Box>
    )
}

export default DetailAboutSkeleton