import { FormattedTime, injectIntl, FormattedDateTimeRange } from 'react-intl'
import { useEffect, useState } from 'react'
import { AccountCircle } from '@material-ui/icons'
import {
  Typography,
  Box,
  makeStyles,
  Divider,
  Tooltip,
  Button,
  ListItemText,
} from '@material-ui/core'

import { useLandingAvisContext } from '../../../../contexts/LandingAvisContext/LandingAvis.provider'
import { getFeedbacks } from '../../../../services/feedback.service'
import handleSessionExpiredError from '../../../../utils/handleSessionExpiry'
import { usePerson } from '../../../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import { getAddress } from '../../../../utils/geocodeAddress'

const useStyles = makeStyles((theme) => ({
  SubTitle: {
    paddingTop: '20px',
    fontWeight: 'bold',
  },
  Title: {
    color: theme.palette.secondary.main,
    fontWeight: 'bold',
    display: 'grid',
    gridAutoFlow: 'column',
    padding: '5px',
  },
  UserProfil: {
    width: 'fit-content',
    display: 'grid',
    gridAutoFlow: 'column',
  },
  CommunBox: {
    display: 'grid',
    padding: '5px',
    wordSpacing: '5px',
    width: '98%',
  },
  AllRemarksButton: {
    width: 'fit-content',
    justifySelf: 'center',
  },
}))

function UserAvis({ feedback: feebackData }) {
  let classes = useStyles()

  let { given_by, rating, feedback, given_at } = feebackData

  return (
    <>
      <Typography variant='body1' className={classes.Title}>
        <Typography className={classes.UserProfil}>
          <Tooltip arrow title='user'>
            <AccountCircle style={{ fontSize: '50px' }} />
          </Tooltip>
          <ListItemText
            primary={given_by}
            secondary={
              <FormattedTime
                value={new Date(given_at)}
                hour='2-digit'
                minute='2-digit'
                day='2-digit'
                month='long'
                year='numeric'
              />
            }
          />
        </Typography>
        <Typography variant='body1' style={{ justifySelf: 'right' }}>
          <span style={{ fontSize: '15px', fontWeight: 'bold' }}>{rating}</span>
          /10
        </Typography>
      </Typography>
      <Typography
        variant='body1'
        color='textSecondary'
        style={{ paddingInline: '10px' }}
      >
        {feedback}
      </Typography>
    </>
  )
}

function LandingDetailAbout({ intl: { formatMessage }, structure }) {
  let classes = useStyles()

  let {
    landingAvisState: { usersAvis },
    landingAvisDispatch,
  } = useLandingAvisContext()

  const { personDispatch } = usePerson()
  const location = useLocation()
  const navigate = useNavigate()

  let [isDataLoading, setIsDataLoading] = useState(true)
  let [isAllShowned, setIsAllShowned] = useState(false)

  let {
    latitude,
    longitude,
    schedules,
    description,
    structure_id,
    specific_indications,
  } = structure

  const daysOfTheWeek = [
    formatMessage({ id: 'firstDay' }),
    formatMessage({ id: 'secondDay' }),
    formatMessage({ id: 'thirdDay' }),
    formatMessage({ id: 'fouthDay' }),
    formatMessage({ id: 'fithDay' }),
    formatMessage({ id: 'sithDay' }),
    formatMessage({ id: 'sevethDay' }),
  ]

  const showAllAvisHandler = () => {
    setIsAllShowned(!isAllShowned)
  }

  const [address, setAddress] = useState('')
  useEffect(() => {
    getAddress(latitude, longitude)
      .then((address) => setAddress(address))
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
    getFeedbacks({ structure_id })
      .then((feedbacks) => {
        landingAvisDispatch({
          type: 'LOAD_AVIS_DATA',
          payload: feedbacks,
        })
        if (feedbacks.length <= 5) setIsAllShowned(true)
        setIsDataLoading(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })

    // eslint-disable-next-line
  }, [])

  const sumRation = usersAvis.reduce(
    (previous, current) => previous + current.rating,
    0
  )
  const rate = usersAvis.length === 0 ? 0 : sumRation / usersAvis.length

  return (
    <>
      <Box className={classes.CommunBox}>
        <Typography variant='h4' className={classes.Title}>
          {formatMessage({ id: 'aboutTitle' })}
        </Typography>
        <Typography variant='body1' className={classes.SubTitle}>
          {formatMessage({ id: 'openHours' })}
          {schedules !== undefined ? (
            <>
              {schedules.map((schedle) => {
                return schedle.date ? null : (
                  <Typography variant='body1' color='textSecondary'>
                    {`${daysOfTheWeek[schedle.day - 1]}: `}
                    <FormattedDateTimeRange
                      from={new Date(`2022-01-09 ${schedle.open_at}`)}
                      to={new Date(`2022-01-09 ${schedle.close_at}`)}
                      hour='2-digit'
                      minute='2-digit'
                    />
                  </Typography>
                )
              })}
              {formatMessage({ id: 'specialHours' })}
              {schedules.map(({ date, open_at, close_at }) => {
                const starts_from = new Date(date).setHours(
                  new Date(`2022-01-09 ${open_at}`).getHours() - 2
                )
                const ends_at = new Date(date).setHours(
                  new Date(`2022-01-09 ${close_at}`).getHours() - 2
                )
                return date ? (
                  <Typography variant='body1' color='textSecondary'>
                    <FormattedDateTimeRange
                      from={new Date(starts_from)}
                      to={new Date(ends_at)}
                      year='numeric'
                      month='short'
                      day='numeric'
                      weekday='short'
                      hour='2-digit'
                      minute='2-digit'
                    />
                  </Typography>
                ) : null
              })}
            </>
          ) : null}
        </Typography>
        <Typography variant='body1' className={classes.SubTitle}>
          {formatMessage({ id: 'howTogetToPlace' })}
          <Typography variant='body1' color='textSecondary'>
            {address}
          </Typography>
        </Typography>
        <Typography variant='body1' className={classes.SubTitle}>
          {formatMessage({ id: 'particularIndications' })}
          <Typography variant='body1' color='textSecondary'>
            {specific_indications}
          </Typography>
        </Typography>
        <Typography variant='body1' className={classes.SubTitle}>
          {formatMessage({ id: 'charateristics' })}
          <Typography variant='body1' color='textSecondary'>
            {description}
          </Typography>
        </Typography>
      </Box>
      <Divider />
      <Box className={classes.CommunBox}>
        <Typography variant='body1' className={classes.Title}>
          {formatMessage({ id: 'noteRemarks' })}
          <Typography variant='body1'>
            {rate === 0 ? (
              'No Rating yet'
            ) : (
              <>
                <span style={{ fontSize: '15px', fontWeight: 'bold' }}>
                  {rate.toPrecision(3)}
                </span>
                /10
              </>
            )}
          </Typography>
        </Typography>
        <Typography variant='body1' color='textSecondary'>
          {formatMessage({ id: 'noteRemarksText' })}
        </Typography>
      </Box>
      {isDataLoading || usersAvis.length === 0 ? null : (
        <>
          <Divider />
          <Box className={classes.CommunBox}>
            <UserAvis feedback={usersAvis[0]} />
            {usersAvis.length > 5 && (
              <Button
                style={{
                  color: '#38B000',
                  textAlign: 'right',
                  justifySelf: 'right',
                }}
                onClick={showAllAvisHandler}
              >
                {isAllShowned
                  ? formatMessage({ id: 'hideAvis' })
                  : formatMessage({ id: 'readMore' })}
              </Button>
            )}
          </Box>
        </>
      )}
      <Divider />
      {isAllShowned && usersAvis
        ? usersAvis.map((userAvis, index) => {
            return usersAvis[0].feedback_id === usersAvis.feedback_id ? null : (
              <>
                <Box key={index} className={classes.CommunBox}>
                  <UserAvis feedback={userAvis} />
                </Box>
                <Divider />
              </>
            )
          })
        : null}
      {isAllShowned && usersAvis.length > 5 ? (
        <Button
          variant='outlined'
          color='secondary'
          className={classes.AllRemarksButton}
          onClick={showAllAvisHandler}
        >
          {isAllShowned
            ? formatMessage({ id: 'noRemarks' })
            : formatMessage({ id: 'allRemarks' })}
        </Button>
      ) : null}
    </>
  )
}

export default injectIntl(LandingDetailAbout)
