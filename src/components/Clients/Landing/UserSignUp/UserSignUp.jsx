import * as yup from 'yup'
import { useState } from 'react'
import { useFormik } from 'formik'
import { injectIntl } from 'react-intl'
import {
  makeStyles,
  Box,
  Typography,
  Button,
  TextField,
  Checkbox,
  CircularProgress,
  Grid,
} from '@material-ui/core'

import { notifyError } from '../../../../utils/toastMessages'

import { usePerson } from '../../../../contexts/PersonContext/Person.provider'
import { useLandingOngoingOrder } from '../../../../contexts/LandingOngoingOrderContext/LandingOngoingOrder.provider'
import handleSessionExpiredError from '../../../../utils/handleSessionExpiry'
import { unAuthenticatedPurchase } from '../../../../services/order.service'
import { useLocation, useNavigate } from 'react-router'

const useStyles = makeStyles((theme) => ({
  ContinueButton: {
    backgroundColor: theme.palette.success.main,
    '&:hover': {
      backgroundColor: theme.palette.success.dark,
    },
  },
  StandardTextField: {
    borderBottom: '1px solid gray',
  },
  ProfitMessage: {
    display: 'grid',
    justifyContent: 'center',
    backgroundColor: '#D9DBE9',
    paddingTop: '15px',
    gap: '10px',
  },
  Form: {
    display: 'grid',
    gridGap: '10px',
  },
  SubTitle: {
    fontWeight: 'bold',
    justifySelf: 'center',
    textAlign: 'center',
    width: '90%',
  },
  Names: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '10px',
  },
  Label: {
    maxWidth: '350px',
    fontSize: theme.typography.body1,
  },
}))

function UserSignUp({ intl: { formatMessage }, setCurrentDialog, setErrorMessage }) {
  let classes = useStyles()

  const [isUserInfoLoading, setIsUserInfoLoading] = useState(false)
  const [isSubmitting, setIsSubmitting] = useState({
    formik: false,
    google: false,
  })

  const {
    landingOngoingOrderState: { clientOrder },
    landingOngoingOrderDispatch,
  } = useLandingOngoingOrder()

  let {
    personState: { person },
    personDispatch,
  } = usePerson()
  const navigate = useNavigate()
  const location = useLocation()

  const validationSchema = yup.object({
    last_name: yup
      .string('')
      .required(formatMessage({ id: 'requiredFieldMessage' })),
    first_name: yup
      .string('')
      .required(formatMessage({ id: 'requiredFieldMessage' })),
    email: yup
      .string('')
      .email(formatMessage({ id: 'invalidEmail' }))
      .required(formatMessage({ id: 'requiredFieldMessage' })),
    password: yup
      .string('')
      .min(5, formatMessage({ id: 'shortPassword' }))
      .required(formatMessage({ id: 'requiredFieldMessage' })),
    phone: yup
      .string()
      .matches(
        /^(6|2)(2|3|[5-9])[0-9]{7}$/,
        formatMessage({ id: 'invalidPhoneNumber' })
      )
      .required(formatMessage({ id: 'requiredFieldMessage' })),
    is_privacy_accepted: yup.boolean(),
  })

  const formik = useFormik({
    initialValues: {
      last_name: '',
      first_name: '',
      password: '',
      phone: '',
      email: '',
      is_privacy_accepted: false,
    },
    validationSchema,
    onSubmit: (values, { resetForm }) => {
      if (values.is_privacy_accepted) {
        setIsSubmitting({ formik: true, google: false })
        setIsUserInfoLoading(true)
        unAuthenticatedPurchase('sign-up', {
          ...values,
          client_order: clientOrder,
        })
          .then((person) => {
            setIsUserInfoLoading(true)
            personDispatch({ type: 'LOAD_PERSON_DATA', payload: person })
            landingOngoingOrderDispatch({ type: 'RESET_ONGOING_ORDER' })
            setIsSubmitting({ formik: false, google: false })
            setCurrentDialog('Success')
            resetForm()
          })
          .catch((error) => {
            const formatMessageId = handleSessionExpiredError(
              personDispatch,
              location.pathname,
              error,
              formatMessage,
              navigate
            )
            setCurrentDialog('Failure')
            setIsUserInfoLoading(true)
            setErrorMessage(formatMessageId)
            setIsSubmitting({ formik: false, google: false })
          })
      } else notifyError(formatMessage({ id: 'checkboxRequired' }))
    },
  })

  const googleConnectionHandler = () => {
    setIsSubmitting({ formik: false, google: true })
    setTimeout(() => {
      setIsSubmitting({ formik: false, google: false })
    }, 3000)
  }

  return (
    <form onSubmit={formik.handleSubmit} className={classes.Form}>
      <Typography
        variant='body2'
        color='textSecondary'
        className={classes.SubTitle}
      >
        {formatMessage({ id: 'signInMessage' })}
      </Typography>
      <Grid container style={{ display: 'grid', gridGap: '10px' }}>
        <Grid xs={12} className={classes.Names}>
          <TextField
            name='last_name'
            variant='standard'
            size='small'
            disabled={isUserInfoLoading || person.isConnected}
            className={classes.StandardTextField}
            value={formik.values.last_name}
            onChange={formik.handleChange}
            label={formatMessage({ id: 'lastName' })}
            error={formik.touched.last_name && Boolean(formik.errors.last_name)}
            helperText={formik.touched.last_name && formik.errors.last_name}
          />
        </Grid>
        <Grid xs={12} className={classes.Names}>
          <TextField
            name='first_name'
            variant='standard'
            size='small'
            disabled={isUserInfoLoading || person.isConnected}
            className={classes.StandardTextField}
            value={formik.values.first_name}
            onChange={formik.handleChange}
            label={formatMessage({ id: 'firstName' })}
            error={
              formik.touched.first_name && Boolean(formik.errors.first_name)
            }
            helperText={formik.touched.first_name && formik.errors.first_name}
          />
        </Grid>
        <Grid xs={12} className={classes.Names}>
          <TextField
            name='email'
            variant='filled'
            size='small'
            disabled={isUserInfoLoading || person.isConnected}
            value={formik.values.email}
            onChange={formik.handleChange}
            label={formatMessage({ id: 'emailAddress' })}
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
          />
        </Grid>
        <Grid xs={12} className={classes.Names}>
          <TextField
            name='phone'
            variant='filled'
            size='small'
            disabled={isUserInfoLoading || person.isConnected}
            value={formik.values.phone}
            onChange={formik.handleChange}
            label={formatMessage({ id: 'phone' })}
            error={formik.touched.phone && Boolean(formik.errors.phone)}
            helperText={formik.touched.phone && formik.errors.phone}
          />
        </Grid>
        <Grid xs={12} className={classes.Names}>
          <TextField
            name='password'
            variant='filled'
            size='small'
            type='password'
            disabled={isUserInfoLoading || person.isConnected}
            value={formik.values.password}
            onChange={formik.handleChange}
            label={formatMessage({ id: 'password' })}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password || formik.errors.password}
          />
        </Grid>
        <Grid xs={12}>
          <label htmlFor='is_privacy_accepted' className={classes.Label}>
            <Checkbox
              id='is_privacy_accepted'
              name='is_privacy_accepted'
              size='small'
              checked={formik.values.is_privacy_accepted}
              onChange={formik.handleChange}
              disabled={isUserInfoLoading}
            />
            {formatMessage({ id: 'agreeMessage' })}
          </label>
        </Grid>
      </Grid>
      <Box className={classes.ProfitMessage}>
        <Button
          type='submit'
          variant='contained'
          className={classes.ContinueButton}
          onClick={formik.handleSubmit}
          disabled={isUserInfoLoading}
        >
          {isSubmitting.formik && (
            <CircularProgress
              color='secondary'
              style={{ marginInline: '5px', width: '25px', height: '25px' }}
            />
          )}
          {formatMessage({ id: 'goOn' })}
        </Button>
        <Typography
          variant='body1'
          color='secondary'
          style={{ textAlign: 'center', justifySelf: 'center' }}
        >
          {formatMessage({ id: 'freeServiceInfo' })}
        </Typography>
      </Box>
      <Button
        variant='outlined'
        onclick={googleConnectionHandler}
        disabled={isUserInfoLoading}
      >
        {isSubmitting.google && (
          <CircularProgress
            color='secondary'
            style={{ marginInline: '5px', width: '25px', height: '25px' }}
          />
        )}
        {formatMessage({ id: 'orderConnection' })}
      </Button>
      <Typography variant='body2' color='textSecondary'>
        {formatMessage({ id: 'confidentiality' })}
      </Typography>
    </form>
  )
}

export default injectIntl(UserSignUp)
