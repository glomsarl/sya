import { useState } from 'react'
import Failure from './failure/Failure'
import Success from './success/Success'
import Purchase from './purchase/Purchase'
import PurchaseWithSignUp from './purchaseWithSignUp/PurchaseWithSignUp'
import PurchaseWithSignIn from './PurchaseWithSignIn/PurchaseWithSignIn'

function Purchasing({
  show,
  handleClose,
  structureId,
  location,
  logoRef,
  formatMessage,
}) {
  let [errorMessage, setErrorMessage] = useState()
  let [currentDialog, setCurrentDialog] = useState('Purchase')

  const closeDialogHandler = () => {
    handleClose()
    setCurrentDialog('Purchase')
  }

  return (
    <>
      {currentDialog === 'Purchase' ? (
        <Purchase
          show={show}
          logoRef={logoRef}
          structureId={structureId}
          closeHandler={closeDialogHandler}
          setCurrentDialog={setCurrentDialog}
        />
      ) : currentDialog === 'SignIn' ? (
        <PurchaseWithSignIn
          show={show}
          closeHandler={closeDialogHandler}
          logoRef={logoRef}
          setCurrentDialog={setCurrentDialog}
          setErrorMessage={setErrorMessage}
        />
      ) : currentDialog === 'SignUp' ? (
        <PurchaseWithSignUp
          show={show}
          logoRef={logoRef}
          closeHandler={closeDialogHandler}
          setCurrentDialog={setCurrentDialog}
          setErrorMessage={setErrorMessage}
        />
      ) : currentDialog === 'Success' ? (
        <Success
          show={show}
          logoRef={logoRef}
          location={location}
          closeHandler={closeDialogHandler}
          setCurrentDialog={setCurrentDialog}
        />
      ) : currentDialog === 'Failure' ? (
        <Failure
          show={show}
          logoRef={logoRef}
          message={
            errorMessage
              ? formatMessage({ id: errorMessage })
              : formatMessage({ id: 'failureMessage' })
          }
          closeHandler={closeDialogHandler}
          setCurrentDialog={setCurrentDialog}
        />
      ) : null}
    </>
  )
}

export default Purchasing
