import * as yup from 'yup'
import { useFormik } from 'formik'
import { Close } from '@material-ui/icons'
import React, { useEffect, useState } from 'react'
import Scrollbars from 'react-custom-scrollbars-2'
import { FormattedDate, FormattedTime, injectIntl } from 'react-intl'
import {
  Button,
  Dialog,
  DialogContent,
  Box,
  Slide,
  Typography,
  TextField,
  RadioGroup,
  Radio,
  FormControlLabel,
  makeStyles,
  IconButton,
} from '@material-ui/core'

import { notifyError } from '../../../../../utils/toastMessages'

import { useSYContext } from '../../../../../contexts/SYContext/SY.provider'
import { usePerson } from '../../../../../contexts/PersonContext/Person.provider'
import { useLandingOngoingOrder } from '../../../../../contexts/LandingOngoingOrderContext/LandingOngoingOrder.provider'
import { getAllCatalogs } from '../../../../../services/catalogs.service'
import handleSessionExpiredError from '../../../../../utils/handleSessionExpiry'
import { useNavigate } from 'react-router'
import maxPromotion from '../../../../../utils/maxPromotion'
import { getStructreSchedules } from '../../../../../services/structure.service'

const useStyles = makeStyles((theme) => ({
  ActualOffer: {
    backgroundColor: 'rgb(248 198 0 / 20%)',
    display: 'grid',
    gridGap: '10px',
    padding: '5px',
    margin: '10px 0',
  },
  Offer: {
    display: 'grid',
    gridAutoFlow: 'column',
    backgroundColor: '#FFFF',
    borderRadius: '5px',
    justifySelf: 'center',
    paddingInline: '5px',
    width: '100%',
  },
  OfferControl: {
    width: '90%',
    display: 'grid',
    gridGap: '10px',
  },
  Labels: {
    display: 'grid',
    cursor: 'pointer',
  },
  TextField: {
    border: `1px solid gray`,
    opacity: 0.8,
    height: 'fit-content',
    margin: '10px 0px',
  },
  QnteLabel: {
    display: 'grid',
    fontWeight: 'bold',
    height: 'fit-content',
  },
  DateTime: {
    border: `1px solid ${theme.palette.primary.main}`,
    color: theme.palette.primary.main,
    borderRadius: '5px',
    width: 'max-content',
    paddingInlineEnd: '10px',
    display: 'grid',
    gridAutoFlow: 'column',
    justifyContent: 'center',
    height: 'fit-content',
  },
  Zone: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
    paddingInline: '15px',
  },
  DialogTitle: {
    color: '#FFFF',
    padding: '10px',
    marginLeft: '-4px',
    display: 'grid',
    gridAutoFlow: 'column',
  },
}))

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />
})

function Purchase({
  show,
  closeHandler,
  intl: { formatMessage },
  setCurrentDialog,
  structureId,
  logoRef,
}) {
  let classes = useStyles()

  const {
    landingOngoingOrderState: {
      ongoingOrder: {
        service: {
          catalogs,
          service_id,
          unit_price,
          cancellation_delay,
          service_name,
          promotions,
          is_reservable,
          is_home_delivery,
          is_take_away,
        },
      },
    },
    landingOngoingOrderDispatch,
  } = useLandingOngoingOrder()
  const {
    personState: { person },
    personDispatch,
  } = usePerson()
  const {
    syState: { sy_value },
  } = useSYContext()

  let [checked, setChecked] = useState({ date: null, time: null })
  let [calends_atar, setCalends_atar] = useState({ days: [], hours: [] })
  let [SYControl, setSYControl] = useState({
    isSyPointUsed: false,
    isStructureSyLess: false,
  })

  const validationSchema = yup.object({
    category: yup.string(''),
    quantity: yup
      .string('')
      .required(formatMessage({ id: 'requiredFieldMessage' })),
    tdate: yup.string(''),
    time: yup.string(''),
  })

  const formik = useFormik({
    initialValues: {
      order_type: '',
      quantity: 1,
      tdate: '',
      time: '',
    },
    validationSchema: validationSchema,
    onSubmit: (values, { resetForm }) => {
      if (
        values.order_type !== '' &&
        values.tdate !== '' &&
        values.time !== '' &&
        values.quantity > 0
      ) {
        let total = unit_price * values.quantity
        if (sy_value * person.SYAmount < total && SYControl.isSyPointUsed)
          notifyError(formatMessage({ id: 'LessSY' }))

        const { tdate, time, quantity, order_type } = values
        const selected_date = new Date(
          `${new Date(tdate).toDateString()} ${new Date(time).toTimeString()}`
        )
        let clientOrder = {
          quantity,
          order_type,
          service_id,
          unit_price,
          service_name,
          selected_date,
          cancellation_delay,
          sy_price: unit_price / sy_value,
          is_sy_point_used: SYControl.isSyPointUsed,
          promotions,
          promotion_id: promotions.find(
            ({ promotion_type }) => promotion_type === 'Bonus'
          )
            ? maxPromotion(promotions, 'Bonus').promotion_id
            : maxPromotion(promotions, 'Reduction').promotion_id,
          catalog_detail_id: catalogs.find(
            ({ ends_at, starts_at }) =>
              new Date(starts_at) < selected_date &&
              selected_date < new Date(ends_at)
          )?.catalog_detail_id,
        }
        landingOngoingOrderDispatch({
          type: 'SET_CLIENT_ORDER',
          payload: clientOrder,
        })
        setCurrentDialog('SignIn')
        setChecked({ date: null, time: null })
        resetForm()
      } else if (values.order_type === '')
        notifyError(formatMessage({ id: 'categoryRequired' }))
      else if (values.tdate === '')
        notifyError(formatMessage({ id: 'dateRequired' }))
      else if (values.quantity === 0)
        notifyError(formatMessage({ id: 'quantityError' }))
      else notifyError(formatMessage({ id: 'timeRequired' }))
    },
  })

  const handleClose = () => {
    closeHandler()
    formik.resetForm()
    setCalends_atar({ days: [], hours: [] })
  }

  const createTimeInterval = (open_at, close_at) => {
    let time = []
    let interval = parseInt((close_at - open_at) / (1000 * 3600)) * 2
    let m = open_at.getMinutes()
    let h = open_at.getHours()
    while (interval > 0) {
      if (m >= 30) {
        m -= 30
        h += 1
      } else {
        m += 30
      }
      let newTime = new Date(Number(open_at))
      newTime.setMinutes(m)
      newTime.setHours(h)
      time = time.find((t) => t === newTime) ? time : [...time, newTime]
      interval--
    }
    let newCalends_atar = { days: calends_atar.days, hours: time }
    if (
      newCalends_atar.hours.find((hour) => hour.toString() === 'Invalid Date')
    )
      newCalends_atar = { days: calends_atar.days, hours: [] }
    setCalends_atar(newCalends_atar)
  }

  const getDays = (ddd, ddf) => {
    let now = new Date()
    let ends_at = new Date(ddf)
    let begin = new Date(ddd)
    let days = now > begin ? [now] : [begin]
    let interval = parseInt((ends_at - days[0]) / (1000 * 3600 * 24))
    for (let i = 0; i < interval; i++) {
      let intervalDate = new Date(Number(days[0]))
      intervalDate.setDate(days[0].getDate() + (i + 1))
      days = days.find((day) => day === intervalDate)
        ? days
        : [...days, intervalDate]
    }
    return days
  }

  const createCalends_atar = (catalogs) => {
    let alldays = []
    catalogs.forEach((catalog) => {
      alldays = [...alldays, ...getDays(catalog.starts_at, catalog.ends_at)]
    })
    let newCalends_atar = { days: alldays, hours: calends_atar.hours }
    setCalends_atar(newCalends_atar)
  }

  const selectDateHandler = (e, index, tdate) => {
    setChecked({ date: index, time: checked.time })
    let horaire = {}
    getStructreSchedules(structureId)
      .then((schedules) => {
        schedules.forEach(({ open_at, close_at, day }) => {
          if (day === tdate.getDay()) horaire = { open_at, close_at }
        })
        const { open_at, close_at } = horaire
        const startInterval = new Date(
          `${tdate.toISOString().split('T')[0]} ${open_at}`
        )
        const endInterval = new Date(
          `${tdate.toISOString().split('T')[0]} ${close_at}`
        )
        const newDate = new Date()
        if (newDate < endInterval)
          createTimeInterval(
            startInterval < newDate ? newDate : startInterval,
            endInterval
          )
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
  }

  const navigate = useNavigate()
  const location = useNavigate()

  useEffect(() => {
    if (structureId)
      getAllCatalogs({ structure_id: structureId, service_id, use_today: true })
        .then((catalogs) => {
          createCalends_atar(catalogs)
          landingOngoingOrderDispatch({
            type: 'ADD_ONGOING_CATALOG',
            payload: catalogs,
          })
          let newSY = {
            ...SYControl,
            is_structure_syLess: sy_value * person.SYAmount > unit_price,
          }
          setSYControl(newSY)
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
        })
    // eslint-disable-next-line
  }, [show])

  return (
    <Dialog
      open={show}
      TransitionComponent={Transition}
      keepMounted
      onClose={handleClose}
      aria-labelledby='alert-dialog-slide-title'
      aria-describedby='alert-dialog-slide-description'
    >
      <Box
        id='alert-dialog-slide-title'
        style={{
          background: `url(${process.env.REACT_APP_BASE_URL}/${logoRef}`,
        }}
        className={classes.DialogTitle}
      >
        <Typography variant='h6'>
          {formatMessage({ id: 'orderTitle' })}
        </Typography>
        <IconButton
          variant='outlined'
          onClick={handleClose}
          style={{ justifySelf: 'right', color: 'white' }}
        >
          <Close />
        </IconButton>
      </Box>
      <DialogContent>
        <form onSubmit={formik.handleChange}>
          <Box
            style={{ display: 'grid', gridAutoFlow: 'column', gridGap: '10px' }}
          >
            <Typography
              variant='body1'
              color='secondary'
              style={{ fontWeight: 'bold' }}
            >
              {formatMessage({ id: 'orderType' })}
              <RadioGroup
                aria-label='order-type'
                name='order_type'
                value={formik.values.order_type}
                onChange={formik.handleChange}
                error={
                  formik.touched.order_type && Boolean(formik.errors.order_type)
                }
                helpertext={
                  formik.touched.order_type || formik.errors.order_type
                }
              >
                <FormControlLabel
                  color='textSecondary'
                  value={'R'}
                  disabled={!is_reservable}
                  control={<Radio size='small' />}
                  label={formatMessage({ id: 'reserveRadio' })}
                />
                <FormControlLabel
                  color='textSecondary'
                  value={'H'}
                  disabled={!is_home_delivery}
                  control={<Radio size='small' />}
                  label={formatMessage({ id: 'deliveryRadio' })}
                />
                <FormControlLabel
                  color='textSecondary'
                  value={'T'}
                  disabled={!is_take_away}
                  control={<Radio size='small' />}
                  label={formatMessage({ id: 'takeAwayRadio' })}
                />
              </RadioGroup>
            </Typography>
            <label
              htmlFor='qnte'
              variant='body1'
              color='secondary'
              className={classes.QnteLabel}
            >
              {formatMessage({ id: 'quantity' })}
              <TextField
                id='quantity'
                type='number'
                name='quantity'
                variant='filled'
                size='small'
                htmlFor={'Ex: 1'}
                onChange={formik.handleChange}
                value={formik.values.quantity}
                error={
                  formik.touched.quantity && Boolean(formik.errors.quantity)
                }
                helperText={formik.touched.quantity || formik.errors.quantity}
              />
            </label>
          </Box>
          <Box>
            <Typography variant='body1' color='secondary' style={{}}>
              {formatMessage({ id: 'date' })}
            </Typography>
            <Scrollbars style={{ width: '100%', height: '55px' }}>
              <RadioGroup
                name='tdate'
                aria-label='tdate'
                value={formik.values.tdate}
                onChange={formik.handleChange}
              >
                <Box className={classes.Zone}>
                  {calends_atar.days.map((tdate, index) => (
                    <>
                      <FormControlLabel
                        value={tdate}
                        color='textSecondary'
                        onChange={(e) => selectDateHandler(e, index, tdate)}
                        className={classes.DateTime}
                        control={
                          <Radio
                            value={tdate}
                            checked={index === checked.date}
                            size='small'
                          />
                        }
                        label={
                          <FormattedDate
                            value={tdate}
                            day='numeric'
                            month='long'
                            year='numeric'
                          />
                        }
                      />
                    </>
                  ))}
                </Box>
              </RadioGroup>
            </Scrollbars>
          </Box>
          <Box>
            <Typography variant='body1' color='secondary'>
              {formatMessage({ id: 'time' })}
            </Typography>
            <Scrollbars style={{ width: '100%', height: '55px' }}>
              <RadioGroup
                name='time'
                aria-label='time'
                value={formik.values.time}
                onChange={formik.handleChange}
              >
                <Box className={classes.Zone}>
                  {calends_atar.hours.map((hour, index) => (
                    <>
                      <FormControlLabel
                        value={hour}
                        color='textSecondary'
                        className={classes.DateTime}
                        onChange={() =>
                          setChecked({ date: checked.date, time: index })
                        }
                        control={
                          <Radio
                            name='time'
                            value={hour}
                            checked={index === checked.time}
                            size='small'
                          />
                        }
                        label={
                          <FormattedTime
                            value={hour}
                            minute='2-digit'
                            hour='2-digit'
                          />
                        }
                      />
                    </>
                  ))}
                  {calends_atar.hours.length === 0 ? (
                    <Typography
                      variant='body1'
                      color='secondary'
                      style={{ justifySelf: 'center', fontWeight: 'bold' }}
                    >
                      {formatMessage({ id: 'horaireNotify' })}
                    </Typography>
                  ) : null}
                </Box>
              </RadioGroup>
            </Scrollbars>
          </Box>
          <Box className={classes.ActualOffer}>
            <Typography variant='body1' color='textSecondary'>
              {formatMessage({ id: 'specialOffer' })}
            </Typography>
            <RadioGroup
              aria-label='useSY'
              name='useSY'
              style={{ justifySelf: 'center' }}
              className={classes.OfferControl}
            >
              {promotions.find(
                ({ promotion_type }) => promotion_type === 'Bonus'
              ) ? (
                <Box className={classes.Offer}>
                  <label
                    htmlFor='SY'
                    className={classes.Labels}
                    onClick={() =>
                      setSYControl({ ...SYControl, isSyPointUsed: true })
                    }
                  >
                    <Typography
                      variant='body2'
                      color='secondary'
                      style={{ fontWeight: 'bold' }}
                    >
                      {formatMessage({ id: 'spendSY' })}
                    </Typography>
                    <Typography variant='caption' color='textSecondary'>
                      {formatMessage({ id: 'SYuseCondition' })}
                    </Typography>
                  </label>
                  <Radio
                    id='SY'
                    name='useSY'
                    value={true}
                    onChange={() =>
                      setSYControl({ ...SYControl, isSyPointUsed: true })
                    }
                    checked={SYControl.isSyPointUsed}
                    style={{ justifySelf: 'right' }}
                  />
                </Box>
              ) : null}
              <Box className={classes.Offer}>
                <label
                  htmlFor='noSY'
                  className={classes.Labels}
                  onClick={() =>
                    setSYControl({ ...SYControl, isSyPointUsed: false })
                  }
                >
                  <Typography
                    variant='body2'
                    color='secondary'
                    style={{ fontWeight: 'bold' }}
                  >
                    {formatMessage({ id: 'withoutOffer' })}
                  </Typography>
                  <Typography variant='caption' color='textSecondary'>
                    {formatMessage({ id: 'noPromotion' })}
                  </Typography>
                </label>
                <Radio
                  id='noSY'
                  name='useSY'
                  value={false}
                  onChange={() =>
                    setSYControl({ ...SYControl, isSyPointUsed: false })
                  }
                  checked={
                    SYControl.isStructureSyLess || !SYControl.isSyPointUsed
                  }
                  style={{ justifySelf: 'right' }}
                />
              </Box>
            </RadioGroup>
          </Box>
          <Button
            type='sumbit'
            color='primary'
            variant='contained'
            style={{ width: '100%' }}
            onClick={formik.handleSubmit}
          >
            {formatMessage({ id: 'goOn' })}
          </Button>
        </form>
      </DialogContent>
    </Dialog>
  )
}

export default injectIntl(Purchase)
