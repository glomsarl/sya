import { injectIntl } from 'react-intl'
import React, { useState } from 'react'
import { LocationOn, Delete, Close, DoneAllTwoTone } from '@material-ui/icons'
import {
  Box,
  makeStyles,
  Typography,
  IconButton,
  Button,
  Dialog,
  DialogContent,
  Slide,
  CircularProgress,
} from '@material-ui/core'

import { notifyInfo } from '../../../../../utils/toastMessages'

import { useLandingOngoingOrder } from '../../../../../contexts/LandingOngoingOrderContext/LandingOngoingOrder.provider'
import maxPromotion from '../../../../../utils/maxPromotion'
import MapComponent from '../../../UserOrder/MapComponent'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />
})

const useStyles = makeStyles((theme) => ({
  Success: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '50px',
    justifyContent: 'center',
    padding: '25px 0px',
  },
  DialogTitle: {
    color: '#FFFF',
    padding: '10px',
    marginLeft: '-4px',
    fontSize: '20px',
  },
  Breffing: {
    backgroundColor: 'white',
    borderRadius: '10px',
    width: 'fit-content',
    justifySelf: 'center',
    padding: '3px',
  },
  Title: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
  },
  CloseDialog: {
    justifySelf: 'right',
    height: 'fit-content',
    color: 'white',
  },
  Content: {
    padding: '20px',
    textAlign: 'center',
    justifySelf: 'center',
  },
  Correct: {
    width: '55px',
    height: '55px',
    padding: '5px',
    borderRadius: '100%',
    backgroundColor: theme.palette.success.main,
    marginTop: '-25px',
    textAlign: 'center',
    justifySelf: 'center',
  },
  Actions: {
    height: 'fit-content',
    width: 'fit-content',
    display: 'grid',
  },
  Delete: {
    fontSize: '35px',
    color: theme.palette.error.main,
  },
  DoneAll: {
    width: '45px',
    height: '45px',
    color: 'white',
  },
  SuccessMessage: {
    width: '350px',
    fontWeight: 'bold',
    justifySelf: 'center',
  },
  ActionButton: {
    width: 'fit-content',
    justifySelf: 'center',
  },
  Location: {
    fontSize: '35px',
    color: theme.palette.success.main,
  },
  CircularProgress: {
    fontSize: '35px',
    color: theme.palette.error.main,
  },
  ActionText: {
    fontWeight: 'bold',
    margin: '10px 5px',
  },
}))

function Success({
  intl: { formatMessage },
  closeHandler,
  show,
  logoRef,
  location,
}) {
  let classes = useStyles()

  const {
    landingOngoingOrderState: {
      clientOrder: { is_sy_point_used, promotions, structure_name, logo_ref },
    },
    landingOngoingOrderDispatch,
  } = useLandingOngoingOrder()
  let [isDeletting, setIsDeletting] = useState(false)

  const cancelCommandHandler = () => {
    setIsDeletting(true)
    closeHandler()
    landingOngoingOrderDispatch({
      type: 'RESET_ONGOING_ORDER',
    })
    notifyInfo(formatMessage({ id: 'cancelMessage' }))
    setIsDeletting(false)
  }

  const [isMapShowing, setIsMapShowing] = useState(false)

  const handleTraject = () => {
    setIsMapShowing(true)
    landingOngoingOrderDispatch({
      type: 'RESET_ONGOING_ORDER',
    })
  }

  return (
    <Dialog
      open={show}
      TransitionComponent={Transition}
      keepMounted
      onClose={closeHandler}
      aria-labelledby='alert-dialog-slide-title'
      aria-describedby='alert-dialog-slide-description'
    >
      {isMapShowing ? (
        <MapComponent
          open={isMapShowing}
          logo_ref={logo_ref}
          location={location}
          structureName={structure_name}
          closeHandler={closeHandler}
        />
      ) : null}
      <div style={{ display: 'grid' }}>
        <Box
          id='alert-dialog-slide-title'
          style={{
            background: `url(${process.env.REACT_APP_BASE_URL}/${logoRef}`,
          }}
          className={classes.DialogTitle}
        >
          <Typography variant='h6' className={classes.Title}>
            <Typography
              variant='h6'
              style={{ maxWidth: '350px', textAlign: 'center' }}
            >
              {formatMessage({ id: 'orderTitleSuccess' })}
            </Typography>
            <IconButton
              variant='outlined'
              onClick={closeHandler}
              className={classes.CloseDialog}
            >
              <Close />
            </IconButton>
          </Typography>
        </Box>
        <div className={classes.Correct}>
          <DoneAllTwoTone className={classes.DoneAll} />
        </div>
        <DialogContent className={classes.Content}>
          <Typography
            variant='subtitle1'
            color='secondary'
            className={classes.SuccessMessage}
          >
            {formatMessage({ id: 'successMessage' })}
          </Typography>
          <Typography
            variant='body1'
            color='textSecondary'
            style={{ width: '350px', justifySelf: 'center' }}
          >
            {!is_sy_point_used && promotions && promotions.length > 0
              ? `${
                  localStorage.getItem('syaLang') === 'en'
                    ? `${formatMessage({ id: 'successBonus' })}
                  ${maxPromotion(promotions, 'Bonus').max_promotion_value} SY`
                    : `${
                        maxPromotion(promotions, 'Bonus').max_promotion_value
                      } SY ${formatMessage({ id: 'successBonus' })}`
                }. ${formatMessage({ id: 'successBonus2' })}`
              : null}
          </Typography>
          <Box className={classes.Success}>
            <Box className={classes.Actions}>
              <Button
                variant='outlined'
                onClick={cancelCommandHandler}
                className={classes.ActionButton}
              >
                {isDeletting ? (
                  <CircularProgress className={classes.CircularProgress} />
                ) : (
                  <Delete className={classes.Delete} />
                )}
              </Button>
              <Typography
                variant='body2'
                color='secondary'
                className={classes.ActionText}
              >
                {formatMessage({ id: 'cancel' })}
              </Typography>
            </Box>
            <Box className={classes.Actions}>
              <Button
                variant='outlined'
                onClick={handleTraject}
                className={classes.ActionButton}
              >
                <LocationOn className={classes.Location} />
              </Button>
              <Typography
                variant='body2'
                color='secondary'
                className={classes.ActionText}
              >
                {formatMessage({ id: 'traject' })}
              </Typography>
            </Box>
          </Box>
        </DialogContent>
      </div>
    </Dialog>
  )
}

export default injectIntl(Success)
