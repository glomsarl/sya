import React, { useState } from 'react'
import { Close, ArrowBack } from '@material-ui/icons'
import { injectIntl, FormattedDate, FormattedTime } from 'react-intl'
import {
  Button,
  Dialog,
  DialogContent,
  Box,
  Slide,
  Typography,
  makeStyles,
  IconButton,
} from '@material-ui/core'

import UserSignIn from '../../UserSignIn/UserSignIn'
import OrderSummary from '../../OrderSummary/OrderSummary'

import { useLandingOngoingOrder } from '../../../../../contexts/LandingOngoingOrderContext/LandingOngoingOrder.provider'

const useStyles = makeStyles((theme) => ({
  DialogTitle: {
    color: '#FFFF',
    padding: '10px',
    marginLeft: '-4px',
    display: 'grid',
    gridAutoFlow: 'row',
  },
  Breffing: {
    backgroundColor: 'white',
    borderRadius: '15px',
    width: 'fit-content',
    justifySelf: 'center',
    padding: '5px 10px',
  },
  Title: {
    display: 'grid',
    gridAutoFlow: 'column',
  },
  ActiveLink: {
    fontWeight: 'bold',
    borderBottom: `3px solid ${theme.palette.secondary.main}`,
    height: '20px',
    [theme.breakpoints.down('xs')]: {
      fontSize: '12px',
    },
  },
  Links: {
    borderBottom: `1px solid gray`,
    height: '20px',
    color: '#000000',
    [theme.breakpoints.down('xs')]: {
      fontSize: '12px',
    },
  },
  BodyNavigation: {
    display: 'grid',
    gridAutoFlow: 'column',
    textAlign: 'center',
    marginTop: '15px',
  },
  IconButton: {
    justifySelf: 'right',
    color: 'white',
  },
}))

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />
})

function PurchaseWithSignIn({
  show,
  closeHandler,
  setCurrentDialog,
  setErrorMessage,
  intl: { formatMessage },
  logoRef,
}) {
  let classes = useStyles()

  let [activeContent, setActiveContent] = useState('summary')

  const {
    landingOngoingOrderState: {
      clientOrder: { quantity, selected_date, order_type, service_name },
    },
  } = useLandingOngoingOrder()

  return (
    <Dialog
      open={show}
      keepMounted
      TransitionComponent={Transition}
      aria-labelledby='alert-dialog-slide-title'
      aria-describedby='alert-dialog-slide-description'
    >
      <Box
        id='alert-dialog-slide-title'
        style={{
          background: `url(${process.env.REACT_APP_BASE_URL}/${logoRef}`,
        }}
        className={classes.DialogTitle}
      >
        <Typography variant='body1' className={classes.Title}>
          <IconButton
            style={{ justifySelf: 'left', color: 'white' }}
            onClick={() => setCurrentDialog('Purchase')}
          >
            <ArrowBack />
          </IconButton>
          <Typography
            variant='body1'
            style={{ width: '90%', textAlign: 'center' }}
          >
            {formatMessage({ id: 'orderTitle' })} : {service_name}
          </Typography>
          <IconButton
            variant='outlined'
            onClick={closeHandler}
            className={classes.IconButton}
          >
            <Close />
          </IconButton>
        </Typography>
        {selected_date ? (
          <Typography
            variant='body1'
            color='textSecondary'
            className={classes.Breffing}
          >
            {`${quantity} ${
              order_type === 'R'
                ? formatMessage({ id: 'reserveRadio' })
                : order_type === 'H'
                ? formatMessage({ id: 'deliveryRadio' })
                : order_type === 'T'
                ? formatMessage({ id: 'takeAwayRadio' })
                : null
            }`}
            {` - `}
            <FormattedDate
              value={selected_date.date}
              weekday='long'
              day='numeric'
              month='long'
              year='numeric'
            />
            {` - `}
            <FormattedTime
              value={selected_date.time}
              hour='2-digit'
              min='2-digit'
            />
          </Typography>
        ) : null}
      </Box>
      <Typography
        variant='body1'
        color='textSecondary'
        className={classes.BodyNavigation}
      >
        <Button
          onClick={() => setActiveContent('summary')}
          className={
            activeContent === 'summary' ? classes.ActiveLink : classes.Links
          }
        >
          {formatMessage({ id: 'summary' })}
        </Button>
        <Button
          onClick={() => setActiveContent('signUp')}
          className={
            activeContent === 'signUp' ? classes.ActiveLink : classes.Links
          }
        >
          {formatMessage({ id: 'informations' })}
        </Button>
      </Typography>
      <DialogContent style={{ padding: '10px 15px' }}>
        {activeContent === 'summary' ? (
          <OrderSummary handleNext={() => setActiveContent('signUp')} />
        ) : (
          <UserSignIn
            setCurrentDialog={setCurrentDialog}
            setErrorMessage={setErrorMessage}
          />
        )}
      </DialogContent>
    </Dialog>
  )
}

export default injectIntl(PurchaseWithSignIn)
