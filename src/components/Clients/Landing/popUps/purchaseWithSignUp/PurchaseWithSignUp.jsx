import React, { useState } from 'react'
import { Close, ArrowBack } from '@material-ui/icons'
import { injectIntl, FormattedDate, FormattedTime } from 'react-intl'
import {
  Button,
  Dialog,
  DialogContent,
  Box,
  Slide,
  Typography,
  makeStyles,
  IconButton,
} from '@material-ui/core'

import UserSignUp from '../../UserSignUp/UserSignUp'
import OrderSummary from '../../OrderSummary/OrderSummary'

import { useLandingOngoingOrder } from '../../../../../contexts/LandingOngoingOrderContext/LandingOngoingOrder.provider'

const useStyles = makeStyles((theme) => ({
  DialogTitle: {
    color: '#FFFF',
    padding: '10px',
    marginLeft: '-4px',
    display: 'grid',
    gridAutoFlow: 'row',
  },
  Breffing: {
    backgroundColor: 'white',
    borderRadius: '15px',
    width: 'fit-content',
    justifySelf: 'center',
    padding: '5px 10px',
  },
  Title: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
  },
  ActiveLink: {
    fontWeight: 'bold',
    borderBottom: `3px solid ${theme.palette.secondary.main}`,
    height: '20px',
    [theme.breakpoints.down('xs')]: {
      fontSize: '12px',
    },
  },
  Links: {
    borderBottom: `1px solid gray`,
    height: '20px',
    color: '#000000',
    [theme.breakpoints.down('xs')]: {
      fontSize: '12px',
    },
  },
  BodyNavigation: {
    display: 'grid',
    gridAutoFlow: 'column',
    textAlign: 'center',
    marginTop: '15px',
  },
  CloseDialog: {
    justifySelf: 'right',
    height: 'fit-content',
  },
}))

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />
})

function PurchaseWithSignUp({
  show,
  closeHandler,
  intl: { formatMessage },
  setCurrentDialog,
  setErrorMessage,
  logoRef,
}) {
  let classes = useStyles()

  let [activeContent, setActiveContent] = useState('signUp')

  const {
    landingOngoingOrderState: {
      clientOrder: { quantity, selected_date, category, service_name },
    },
  } = useLandingOngoingOrder()

  const changeViewHandler = (viewName) => {
    setActiveContent(viewName)
  }

  return (
    <>
      <Dialog
        open={show}
        TransitionComponent={Transition}
        keepMounted
        aria-labelledby='alert-dialog-slide-title'
        aria-describedby='alert-dialog-slide-description'
      >
        <Box
          id='alert-dialog-slide-title'
          style={{
            background: `url(${process.env.REACT_APP_BASE_URL}/${logoRef}`,
          }}
          className={classes.DialogTitle}
        >
          <Typography variant='body1' className={classes.Title}>
            <IconButton
              style={{ justifySelf: 'left', color: 'white' }}
              onClick={() => setCurrentDialog('SignIn')}
            >
              <ArrowBack />
            </IconButton>
            <Typography
              variant='body1'
              style={{ width: '90%', textAlign: 'center' }}
            >
              {formatMessage({ id: 'orderTitle' })} : {service_name}
            </Typography>
            <IconButton
              variant='outlined'
              onClick={closeHandler}
              style={{ color: 'white' }}
            >
              <Close />
            </IconButton>
          </Typography>
          {selected_date ? (
            <Typography
              variant='body1'
              color='textSecondary'
              className={classes.Breffing}
            >
              {quantity}{' '}
              {category === 'reserve'
                ? formatMessage({ id: 'reserveRadio' })
                : category === 'delivery'
                ? formatMessage({ id: 'deliveryRadio' })
                : category === 'takeAway'
                ? formatMessage({ id: 'takeAwayRadio' })
                : null}{' '}
              -
              <FormattedDate
                weekday='long'
                day='numeric'
                month='long'
                year='numeric'
                value={selected_date.date}
              />
              -
              <FormattedTime value={selected_date.time} />
            </Typography>
          ) : null}
        </Box>
        <Typography
          variant='body1'
          color='textSecondary'
          className={classes.BodyNavigation}
        >
          <Button
            onClick={() => changeViewHandler('summary')}
            className={
              activeContent === 'summary' ? classes.ActiveLink : classes.Links
            }
          >
            {formatMessage({ id: 'summary' })}
          </Button>
          <Button
            onClick={() => changeViewHandler('signUp')}
            className={
              activeContent === 'signUp' ? classes.ActiveLink : classes.Links
            }
          >
            {formatMessage({ id: 'informations' })}
          </Button>
        </Typography>
        <DialogContent style={{ padding: '10px 15px' }}>
          {activeContent === 'summary' ? (
            <OrderSummary handleNext={() => setActiveContent('signUp')}/>
          ) : (
            <UserSignUp
              setCurrentDialog={setCurrentDialog}
              setErrorMessage={setErrorMessage}
            />
          )}
        </DialogContent>
      </Dialog>
    </>
  )
}

export default injectIntl(PurchaseWithSignUp)
