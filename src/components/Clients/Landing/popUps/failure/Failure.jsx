import React from 'react'
import { injectIntl } from 'react-intl'
import {
  Close,
  SentimentDissatisfiedRounded,
  CancelOutlined,
  ArrowBack,
} from '@material-ui/icons'
import {
  Box,
  makeStyles,
  Typography,
  IconButton,
  Dialog,
  DialogContent,
  Slide,
} from '@material-ui/core'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />
})

const useStyles = makeStyles((theme) => ({
  Success: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '50px',
    justifyContent: 'center',
    padding: '25px 0px',
  },
  DialogTitle: {
    color: '#FFFF',
    padding: '10px',
    marginLeft: '-4px',
    fontSize: '20px',
  },
  Breffing: {
    backgroundColor: 'white',
    borderRadius: '10px',
    width: 'fit-content',
    justifySelf: 'center',
    padding: '3px',
  },
  Title: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
  },
  CloseDialog: {
    justifySelf: 'right',
    height: 'fit-content',
    color: 'white',
  },
  Content: {
    padding: '20px',
    textAlign: 'center',
    display: 'grid',
  },
  Failure: {
    width: '50px',
    height: '50px',
    padding: '5px',
    borderRadius: '100%',
    backgroundColor: theme.palette.error.main,
    marginTop: '-25px',
    textAlign: 'center',
    justifySelf: 'center',
  },
  Actions: {
    height: 'fit-content',
    width: 'fit-content',
    display: 'grid',
  },
  Fail: {
    width: '40px',
    height: '40px',
    color: 'white',
  },
  FailureMessage: {
    width: '100%',
    fontWeight: 'bold',
    justifySelf: 'center',
  },
}))

function Failure({
  intl: { formatMessage },
  closeHandler,
  show,
  setCurrentDialog,
  message,
  logoRef,
}) {
  let classes = useStyles()
  return (
    <Dialog
      open={show}
      TransitionComponent={Transition}
      keepMounted
      onClose={closeHandler}
      aria-labelledby='alert-dialog-slide-title'
      aria-describedby='alert-dialog-slide-description'
    >
      <div style={{ display: 'grid' }}>
        <Box
          id='alert-dialog-slide-title'
          style={{
            background: `url(${process.env.REACT_APP_BASE_URL}/${logoRef}`,
          }}
          className={classes.DialogTitle}
        >
          <Typography variant='h6' className={classes.Title}>
            <IconButton
              style={{ justifySelf: 'left', color: 'white' }}
              onClick={() => setCurrentDialog('Purchase')}
            >
              <ArrowBack />
            </IconButton>
            <Typography
              variant='h6'
              style={{ maxwidth: '350px', textAlign: 'center' }}
            >
              {formatMessage({ id: 'orderTitleFailure' })}
            </Typography>
            <IconButton
              variant='outlined'
              onClick={closeHandler}
              className={classes.CloseDialog}
            >
              <Close />
            </IconButton>
          </Typography>
        </Box>
        <div className={classes.Failure}>
          <CancelOutlined className={classes.Fail} />
        </div>
        <DialogContent className={classes.Content}>
          <div
            style={{
              width: 'fit-content',
              height: 'fit-content',
              justifySelf: 'center',
            }}
          >
            <SentimentDissatisfiedRounded
              style={{ width: '70px', height: '70px' }}
            />
          </div>
          <Typography
            variant='subtitle1'
            color='secondary'
            className={classes.FailureMessage}
          >
            {message}
          </Typography>
        </DialogContent>
      </div>
    </Dialog>
  )
}

export default injectIntl(Failure)
