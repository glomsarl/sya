import { Box, Button, Divider, makeStyles, Typography } from '@material-ui/core'
import { Filter } from '@material-ui/icons'
import Pagination from '@material-ui/lab/Pagination'
import { useEffect, useState } from 'react'
import { injectIntl } from 'react-intl'
import { useLocation, useNavigate } from 'react-router'
import { useLandingOngoingOrder } from '../../../contexts/LandingOngoingOrderContext/LandingOngoingOrder.provider'

import { useLandingStructure } from '../../../contexts/LandingStructureContext/LandingStructure.provider'
import { usePerson } from '../../../contexts/PersonContext/Person.provider'
import { fetchCatalogServices } from '../../../services/catalogs.service'
import { getUserStructures } from '../../../services/structure.service'
import handleSessionExpiredError from '../../../utils/handleSessionExpiry'
import Service from './moreService/MoreService'
import Purchasing from './popUps/Purchasing'

const useStyles = makeStyles((theme) => ({
  Body: {
    display: 'grid',
    justifyContent: 'center',
    margin: '20px 0px',
    gap: '15px',
  },
  Title: {
    fontWeight: 'bold',
    width: '80%',
  },
  Header: {
    display: 'grid',
    gridAutoFlow: 'column',
    margin: '10px',
  },
  Filter: {
    justifySelf: 'right',
    width: 'fit-content',
    height: 'fit-content',
  },
  PageNumbers: {
    display: 'grid',
    gridAutoFlow: 'column',
    gridGap: '5px',
    justifySelf: 'center',
    margin: '15px',
    width: 'fit-content',
    fontSize: theme.typogra,
  },
}))

function MoreServices({ intl }) {
  let classes = useStyles()
  let { formatMessage } = intl

  let { landingStructureDispatch } = useLandingStructure()

  let [numberOfServices, setNumberOfServices] = useState(0)

  const navigate = useNavigate()
  const location = useLocation()
  const { personDispatch } = usePerson()

  let [category_id, category_name, longitude, latitude] =
    location.search.split('&')
  category_id = category_id.split('?')[1]

  const [services, setServices] = useState([])
  useEffect(() => {
    fetchCatalogServices({
      category_name,
      category_id,
      longitude,
      latitude,
    })
      .then(({ catalog_services }) => {
        setServices(catalog_services)
        setNumberOfServices(catalog_services.length)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
    getUserStructures()
      .then(({ structures, number_of_structures }) => {
        setNumberOfServices(number_of_structures)
        landingStructureDispatch({
          type: 'LOAD_SAME_CATEGORY_STRUCTURES_DATA',
          payload: structures,
        })
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
    // eslint-disable-next-line
  }, [])

  const changePageHandler = (e) => {
    const page_number = parseInt(e.target.innerHTML.split('')[0])
    getUserStructures({
      category_id,
      page_number,
      number_per_page: 5,
    })
      .then(({ structures }) => {
        landingStructureDispatch({
          type: 'LOAD_SAME_CATEGORY_STRUCTURES_DATA',
          payload: structures,
        })
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
  }

  const [logoRef, setLogRef] = useState()
  const [isDialogOpen, setIsDialogOpen] = useState()
  const [structureId, setStructureId] = useState()

  let { landingOngoingOrderDispatch } = useLandingOngoingOrder()

  const ongoingOrderHandler = (service) => {
    setIsDialogOpen(true)
    setStructureId(service.structure_id)
    setLogRef(service.main_image_ref)
    let order = { service, structure_id: service.structure_id }
    landingOngoingOrderDispatch({
      type: 'LOAD_ONGOING_ORDER_DETAILS',
      payload: order,
    })
  }

  const closePurchaseHandler = () => {
    setIsDialogOpen(false)
    landingOngoingOrderDispatch({
      type: 'RESET_ONGOING_ORDER',
    })
  }

  return (
    <Box className={classes.Body}>
      <Box className={classes.Header}>
        <Typography variant='h3' color='secondary' className={classes.Title}>
          {category_name
            ? `${formatMessage({ id: 'best' })} ${formatMessage({
                id: category_name,
              })}`
            : formatMessage({ id: 'closeStructures' })}
          {formatMessage({ id: 'location' })}
        </Typography>
        <Button
          className={classes.Filter}
          variant='contained'
          color='secondary'
          size='small'
        >
          <Filter />
          {formatMessage({ id: 'filter' })}
        </Button>
      </Box>
      <Divider />
      {services.map((service) => (
        <Service
          key={service.service_id}
          service={service}
          handleSelect={() => ongoingOrderHandler(service)}
        />
      ))}
      <Box className={classes.PageNumbers}>
        <Pagination
          count={numberOfServices / 5}
          color='primary'
          onChange={changePageHandler}
        />
      </Box>
      <Purchasing
        logoRef={logoRef}
        show={isDialogOpen}
        structureId={structureId}
        formatMessage={formatMessage}
        handleClose={closePurchaseHandler}
        location={{ lat: latitude, lng: longitude }}
      />
    </Box>
  )
}

export default injectIntl(MoreServices)
