import { injectIntl } from 'react-intl'
import { makeStyles, Box, Typography, Tooltip } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  OtherCard: {
    display: 'grid',
    gridAutoFlow: 'row',
    width: 'fit-content',
    height: '250px',
    cursor: 'pointer',
  },
}))

function OtherStructure({ intl, structure, handleSelect }) {
  let classes = useStyles()
  let { formatMessage } = intl

  let { name, rating, specific_indications, logo_ref, average_price } =
    structure

  return (
    <Tooltip title={formatMessage({ id: 'reloadWarning' })}>
      <Box className={classes.OtherCard} onClick={handleSelect}>
        <img
          alt='not available'
          height='150'
          src={`${process.env.REACT_APP_BASE_URL}/${logo_ref}`}
        />
        <Typography gutterBottom variant='h6' component='h2'>
          {name}
        </Typography>
        <Typography variant='body2' color='textSecondary' component='p'>
          {specific_indications}
        </Typography>
        <Typography variant='body2' color='textSecondary' component='p'>
          {formatMessage({ id: 'structureAvgPrice' })} : {average_price} FCFA
        </Typography>
        {rating !== 0 && (
          <Typography variant='body2' color='textSecondary' component='p'>
            {formatMessage({ id: 'structureRating' })} : {rating}
          </Typography>
        )}
      </Box>
    </Tooltip>
  )
}

export default injectIntl(OtherStructure)
