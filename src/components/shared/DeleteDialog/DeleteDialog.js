import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useDemand } from '../../../contexts/DemandContext/Demand.provider';
import { injectIntl } from 'react-intl';
import { CircularProgress } from '@material-ui/core';
import { useActionDemandSpinner } from "../../../contexts/ActionDemandSpinnerContext/ActionDemandSpinner.provider";
import Transition from '../../../utils/DialogTransition';


function DeleteDialog({deleteDialogDecision, intl, isDialogOpen}) {
    let { state } = useDemand()
    let { selectedDemandIds } = state
    let { formatMessage } = intl
    const { actionDemandSpinnerState} = useActionDemandSpinner();
    let { loading, demandAction } = actionDemandSpinnerState;

    let closeDialog=(decision)=>{
      if(!loading)deleteDialogDecision(decision)
    }

    return (
      <Dialog
        style={!isDialogOpen ? { visibility: "hidden" } : null}
        open={isDialogOpen}
        TransitionComponent={Transition}
        keepMounted
        onClose={() => closeDialog(false)}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          {selectedDemandIds.length !== 0
            ? formatMessage({ id: "deleteSelectedDemandsDialogHeader" })
            : formatMessage({ id: "deleteDemandDialogHeader" })}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            {selectedDemandIds.length !== 0
              ? formatMessage({ id: "deleteSelectedDemandsDialogMessage" })
              : formatMessage({ id: "deleteDemandDialogMessage" })}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => closeDialog(true)}
            color="default"
            variant="outlined"
            disabled = {loading}
          >
            {loading && demandAction === "delete" && (
              <CircularProgress color="secondary" size={20} />
            )}
            {formatMessage({ id: "deleteDialogAgreeText" })}
          </Button>
          <Button
            onClick={() => closeDialog(false)}
            color="primary"
            variant="contained"
            disabled = {loading}
          >
            {formatMessage({ id: "deleteDialogDisagreeText" })}
          </Button>
        </DialogActions>
      </Dialog>
    );
}

export default injectIntl(DeleteDialog)