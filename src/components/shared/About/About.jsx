import { useContext } from "react";
import LanguageContext from "../../../contexts/Language/Language.context";
import { FormattedMessage, injectIntl } from "react-intl";
import enHeaderMessage from "../../../language/En-US/en-US";
import frHeaderMessage from "../../../language/fr/fr";
import { makeStyles, Box, List, ListItem, ListItemIcon, ListItemText } from "@material-ui/core";
import FlareIcon from "@material-ui/icons/Flare";
import PhoneIcon from "@material-ui/icons/Phone";
import WhatsAppIcon from "@material-ui/icons/WhatsApp";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "0 10%",
    backgroundColor: "rgba(248, 198, 0, 0.089)",
  },
  h1: {
    fontSize: "2rem",
    margin: "2rem, 0",
  },
  h2: {
    fontSize: "1rem",
    marginTop: "32px",
    marginBottom: "16px",
  },
  p: {
    fontSize: "0.9rem",
    lineHeight: "1.6",
  },
}));

function About({ intl }) {
  const { formatMessage } = intl
  const { activeLanguage } = useContext(LanguageContext);
  const classes = useStyles()
  let defaultMessage =
      activeLanguage === "fr" ? frHeaderMessage : enHeaderMessage;
  return (
    <Box className={classes.root}>
      <Box
        component="h1"
        className={classes.h1}
      >
        <FormattedMessage
          id="aboutSYA"
          defaultMessage={defaultMessage.aboutSYA}
        />
      </Box>
      <Box component="h2" className={classes.h2}>
        <FormattedMessage
          id="conceptTitle"
          defaultMessage={defaultMessage.conceptTitle}
        />
      </Box>
      <Box component="p" className={classes.p}>
        <FormattedMessage
          id="concept"
          defaultMessage={defaultMessage.concept}
        />
      </Box>
      <Box component="h2" className={classes.h2}>
        <FormattedMessage
          id="whyAdoptTitle"
          defaultMessage={defaultMessage.whyAdoptTitle}
        />
      </Box>
      <Box component="p" className={classes.p}>
        <FormattedMessage
          id="whyAdopt"
          defaultMessage={defaultMessage.whyAdopt}
        />
      </Box>
      <Box component="h2" className={classes.h2}>
        <FormattedMessage
          id="advantagesTitle"
          defaultMessage={defaultMessage.advantagesTitle}
        />
      </Box>
      <Box component="p" className={classes.p} style={{ marginBottom: "0" }}>
        <FormattedMessage
          id="advantages"
          defaultMessage={defaultMessage.advantages}
        />
      </Box>
      <List dense={false} style={{ paddingTop: "0", paddingBottom: "0" }}>
        <ListItem style={{ paddingTop: "0" }}>
          <ListItemIcon>
            <FlareIcon />
          </ListItemIcon>
          <ListItemText primary={formatMessage({ id: "advantage1" })} />
        </ListItem>
        <ListItem>
          <ListItemIcon>
            <FlareIcon />
          </ListItemIcon>
          <ListItemText primary={formatMessage({ id: "advantage2" })} />
        </ListItem>
        <ListItem style={{ paddingBottom: "0" }}>
          <ListItemIcon>
            <FlareIcon />
          </ListItemIcon>
          <ListItemText primary={formatMessage({ id: "advantage3" })} />
        </ListItem>
      </List>
      <Box component="p" className={classes.p} style={{ marginTop: "0" }}>
        <FormattedMessage
          id="afterAdvantage"
          defaultMessage={defaultMessage.afterAdvantage}
        />
      </Box>
      <Box component="h1" className={classes.h1}>
        <FormattedMessage
          id="contactUs"
          defaultMessage={defaultMessage.contactUs}
        />
      </Box>
      <List dense={false}>
        <ListItem>
          <ListItemIcon>
            <PhoneIcon />
          </ListItemIcon>
          <ListItemText primary="(+237) 658 604 281" />
        </ListItem>
        <ListItem>
          <ListItemIcon>
            <WhatsAppIcon />
          </ListItemIcon>
          <ListItemText primary="(+237) 658 604 281" />
        </ListItem>
      </List>
    </Box>
  );
}

export default injectIntl(About)