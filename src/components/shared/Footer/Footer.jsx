import { useContext } from 'react';
import LanguageContext from '../../../contexts/Language/Language.context';
import { AppBar, Button, Container, Grid, Link, List, ListItem, ListItemIcon, ListItemText, makeStyles, TextField, Typography } from '@material-ui/core';
import { FormattedMessage, injectIntl } from 'react-intl';
import enMessages from "../../../language/En-US/en-US";
import frMessages from "../../../language/fr/fr";
import { useFormik } from "formik";
import * as yup from 'yup';
import WhatsAppIcon from "@material-ui/icons/WhatsApp";
import PhoneIcon from "@material-ui/icons/Phone";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import PersonPinCircleIcon from "@material-ui/icons/PersonPinCircle";
import './Footer.css'

const validationSchema = yup.object({
  email: yup
    .string("Enter contact email")
    .email("Enter a valid email")
});


const useStyles = makeStyles((theme) => ({
  bar2: {
    minHeight: "56px",
    backgroundColor: "rgb(248 198 0 / 20%)",
    textAlign: "center",
    lineHeight: "56px",
    verticalAlign: "center",
    color: "black",
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
    whiteSpace: "nowrap",
    marginBottom: theme.spacing(1),
  },
  typography: {
    textAlign: "start",
    color: "rgba(255, 255, 255, 0.7)",
    marginTop: theme.spacing(2),
    fontSize: theme.typography.h5.fontSize,
    "&:hover": {
      color: "#F8C600",
      textDecoration: "none",
      cursor: "pointer",
    },
  },
  typography2: {
    textAlign: "start",
    color: "rgba(255, 255, 255)",
    fontWeight: 400,
    marginTop: theme.spacing(1),
    fontSize: theme.typography.h5.fontSize,
  },
  typography3: {
    color: "rgba(255, 255, 255, 0.5)",
    fontWeight: 500,
    fontSize: theme.typography.h5.fontSize,
  },

  footerBlock: {
    paddingTop: "30px",
  },
  newsLetterInput: {
    width: "80%",
    marginTop: "20px",
    backgroundColor: theme.common.InputBackground,
  },
  NewsletterSize:{
    width:"80%",
  },
  bouton:{
    marginTop: '10px',
    fontSize: theme.typography.h5.fontSize,
  },
  newsLetterbutton:{
    marginTop: "10px",
    width: "80%"
  },
  offer: {
    textAlign: "center",
    fontSize: theme.typography.h5.fontSize,
    color: "rgba(252, 252, 252, 0.5)",
    padding: "50px 10px 20px 10px",
  },
  copyright: {
    color: "rgba(252, 252, 252, 0.5)",
    textAlign: "start",
    fontSize: theme.typography.h5.fontSize,
  },
  footerTitle: {
    paddingBottom: "20px",
  },
}));

function Footer({intl}) {
    const { activeLanguage } = useContext(LanguageContext);
    const classes = useStyles()
    let defaultMessage = activeLanguage === "fr" ? frMessages : enMessages;
    const { formatMessage } = intl

    const formik = useFormik({
      initialValues: {
          email: "",
      },
      validationSchema,
      onSubmit: (values, { resetForm }) => {
        resetForm()
      }
    });

    return (
      <>
        <AppBar elevation={0} position="static" className={classes.bar2}>
          <Grid container justify="center">
            <FormattedMessage
              id="titlePlusSlogan"
              defaultMessage={defaultMessage.titlePlusSlogan}
            />
          </Grid>
        </AppBar>
        <AppBar elevation={0} position="static" color="secondary">
          <Container>
            <Grid container justify="space-between">
              <Grid
                container
                direction="column"
                justify="space-between"
                item
                md={3}
                xs={12}
                sm={5}
                className={classes.footerBlock}
              >
                <Typography className={classes.typography2}>
                  <FormattedMessage
                    id="title1"
                    defaultMessage={defaultMessage.title1}
                  />
                </Typography>
                <Link to="#" className={classes.typography}>
                  <FormattedMessage
                    id="title1About"
                    defaultMessage={defaultMessage.title1About}
                  />
                </Link>
                <Link to="#" className={classes.typography}>
                  <FormattedMessage
                    id="title1Help"
                    defaultMessage={defaultMessage.title1Help}
                  />
                </Link>
                <Link to="#" className={classes.typography}>
                  <FormattedMessage
                    id="title1Contact"
                    defaultMessage={defaultMessage.title1Contact}
                  />
                </Link>
                <Link to="#" className={classes.typography}>
                  <FormattedMessage
                    id="title1HaveStructure"
                    defaultMessage={defaultMessage.title1HaveStructure}
                  />
                </Link>
                <Link to="#" className={classes.typography}>
                  <FormattedMessage
                    id="title1HaveNotStructure"
                    defaultMessage={defaultMessage.title1HaveNotStructure}
                  />
                </Link>
                <Link to="#" className={classes.typography}>
                  <FormattedMessage
                    id="title1Policy"
                    defaultMessage={defaultMessage.title1Policy}
                  />
                </Link>
              </Grid>
              <Grid
                container
                direction="column"
                item
                md={2}
                xs={12}
                sm={5}
                className={classes.footerBlock}
              >
                <Typography className={classes.typography2}>
                  <FormattedMessage
                    id="title2"
                    defaultMessage={defaultMessage.title2}
                  />
                </Typography>
                <Link to="#" className={classes.typography}>
                  Facebook
                </Link>
                <Link to="#" className={classes.typography}>
                  Instagram
                </Link>
                <Link to="#" className={classes.typography}>
                  Twitter
                </Link>
                <Link to="#" className={classes.typography}>
                  LinkeIn
                </Link>
              </Grid>
              <Grid
                container
                direction="column"
                item
                md={4}
                xs={12}
                sm={5}
                className={classes.footerBlock}
              >
                <Typography className={classes.typography2}>
                  <FormattedMessage
                    id="title3"
                    defaultMessage={defaultMessage.title3}
                  />
                </Typography>
                <form onSubmit={formik.handleSubmit}>
                  <TextField
                    id="outlined-search"
                    label={formatMessage({ id: "labelInputEmailAddress" })}
                    type="search"
                    variant="filled"
                    className={classes.newsLetterInput}
                    color="secondary"
                    name="email"
                    onChange={formik.handleChange}
                    value={formik.values.email}
                    error={formik.errors.email ? true : false}
                  />
                  <Button 
                    size="medium" 
                    variant="outlined" 
                    color="primary" 
                    className={[classes.bouton,classes.NewsletterSize].join(" ")}
                    type="submit">
                    <FormattedMessage
                      id="boutonNewsletter"
                      defaultMessage={defaultMessage.boutonNewsletter}
                    />
                  </Button>
                </form>
                <List dense={false} className='footerContactInfo'>
                  <ListItem>
                    <ListItemIcon className={classes.typography3}>
                      <MailOutlineIcon />
                    </ListItemIcon>
                    <ListItemText
                      primary="merlindjeumesi@gmail.com"
                      className={classes.typography3}
                    />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon className={classes.typography3}>
                      <PhoneIcon />
                    </ListItemIcon>
                    <ListItemText
                      primary="(+237) 658 604 281"
                      className={classes.typography3}
                    />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon className={classes.typography3}>
                      <WhatsAppIcon />
                    </ListItemIcon>
                    <ListItemText
                      primary="(+237) 658 604 281"
                      className={classes.typography3}
                    />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon className={classes.typography3}>
                      <PersonPinCircleIcon />
                    </ListItemIcon>
                    <ListItemText
                      primary="Douala, Cameroun"
                      className={classes.typography3}
                    />
                  </ListItem>
                </List>
              </Grid>
              <Grid 
                container 
                direction="column" 
                alignItems="flex-start"
                item md={3} xs={12} sm={5} 
                className={classes.footerBlock}>
                <Typography variant="h4" className={classes.footerTitle}>
                  <FormattedMessage
                    id="title"
                    defaultMessage={defaultMessage.copyright}
                  />
                </Typography>
                <Typography className={classes.copyright}>
                  <FormattedMessage
                    id="copyright"
                    defaultMessage={defaultMessage.copyright}
                  />{" "}
                  &reg;
                </Typography>
                <Typography className={classes.copyright}>
                  <FormattedMessage
                    id="allRightsReserved"
                    defaultMessage={defaultMessage.allRightsReserved}
                  />{" "}
                  &copy;
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography className={classes.offer}>
                  <FormattedMessage
                    id="footerOffer"
                    defaultMessage={defaultMessage.footerOffer}
                  />
                </Typography>
              </Grid>
            </Grid>
          </Container>
        </AppBar>
      </>
    );
}

export default injectIntl(Footer)
