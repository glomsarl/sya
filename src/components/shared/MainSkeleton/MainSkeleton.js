import Skeleton from "@material-ui/lab/Skeleton";
import { Box, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme)=>({
    makeSkeletonHolder:{
        height: "fit-content"
    }
}))

/**
* displays the skeleton screen while the demand data loads.
* @returns The skeleton screen for the demands page.
*/
export default function MainSkeleton(){
    const classes = useStyles()
    return(
        <Box className={classes.mainSkeletonHolder}>
        <Skeleton
            height={96}
            animation = "wave"
            style={{transform: "scale(1, 0.90)"}}
        />
        <Skeleton
            height={96}
            animation = "wave"
            style={{transform: "scale(1, 0.90)"}}
        />
        <Skeleton
            height={96}
            animation = "wave"
            style={{transform: "scale(1, 0.90)"}}
        />
        <Skeleton
            height={96}
            animation = "wave"
            style={{transform: "scale(1, 0.90)"}}
        />
        <Skeleton
            height={96}
            animation = "wave"
            style={{transform: "scale(1, 0.90)"}}
        />
        <Skeleton
            height={96}
            animation = "wave"
            style={{transform: "scale(1, 0.90)"}}
        />
        </ Box>
    )
}
