import { createMuiTheme } from "@material-ui/core/styles"

const SYAOrange = "#F8C600";
const SYADarkBlue = "#011638";
const SYARed = "#D00000";
const SYAGreen = "#38B000";
const SYAOffWhite = "#FCFCFC";
const SYABackground = "#F7F7FC";
const SYAInputBackground = "#EFF0F6";
const SYALine = "#D9DBE9";
const SYPlaceholder = "#A0A3BD";
const SYALabel = "#6E7191";
const SYABody = "#14142B";
const SYATitleActive = "#011638";

const SYABlack = "#00000";
const SYALink = "#023e8a";


let theme = createMuiTheme({
    common:{
        orange:SYAOrange,
        blue: SYADarkBlue,
        red: SYARed,
        green: SYAGreen,
        black: SYABlack,
        BlueLink: SYALink,
        primaryTransparent: "#fff6d1",
        InputBackground: SYAInputBackground,
        displayLarge: '1rem',
        displayMedium: '1rem',
        displaySmall: '1rem',
        displayLargeMobile: '1rem',
        displayMediumMobile: '08rem',
        displaySmallMobile: '1rem',
        fontWeightBold:700,
        fontWeightLight:300,
        fontWeightRegular:400,
        fontWeightRegular2:500,

    },
    palette:{
        primary:{
            main:SYAOrange
        },
        secondary:{
            main:SYADarkBlue
        },
        error:{
            main:SYARed
        },
        success:{
            main:SYAGreen
        },

        profileTheme: [
            SYATitleActive,
            SYABody,
            SYALabel,
            SYPlaceholder,
            SYALine,
            SYABackground,
            SYAOffWhite,
        ]
    },

    typography: {
        fontFamily: [
            'Raleway', 
            'Poppins', 
            '"Segoe UI"', 
            'Roboto', 
            'Oxygen', 
            'Ubuntu', 
            'Cantarell', 
            '"Fira Sans"', 
            '"Droid Sans"', 
            '"Helvetica Neue"', 
            'sans-serif'
        ].join(','),
        h1: {
            fontSize: "4rem",
        },
        h2: {
            fontSize: "2.25rem",
        },
        h3: {
            fontSize: "1.5rem",
        },
        h4: {
            fontSize: "1.125rem",
        },
        h5: {
            fontSize: "0.8rem",
        }
    },
})

export default theme