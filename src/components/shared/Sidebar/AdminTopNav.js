import MenuIcon from '@material-ui/icons/Menu'
import SpeedRoundedIcon from '@material-ui/icons/SpeedRounded'
import { SideBarLinks } from './Sidebar'
import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Drawer from '@material-ui/core/Drawer'
import Hidden from '@material-ui/core/Hidden'
import IconButton from '@material-ui/core/IconButton'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import NotificationsRoundedIcon from '@material-ui/icons/NotificationsRounded'
import { Home } from '@material-ui/icons'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  menuButton: {
    justifySelf: 'end',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    backgroundColor: theme.palette.secondary.main,
    color: 'white',
    fontSize: '1.1rem',
    letterSpacing: '1.2px',
  },
}))

export default function AdminTopNav({ window, links }) {
  const classes = useStyles()
  const theme = useTheme()
  const [mobileOpen, setMobileOpen] = React.useState(false)

  const container =
    window !== undefined ? () => window().document.body : undefined

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen)
  }

  const closeMobileDrawer = () => {
    setMobileOpen(false)
  }
  return (
    <div className={classes.root}>
      <AppBar position='static' color='secondary'>
        <Toolbar>
          <IconButton edge='start' color='inherit' aria-label='menu'>
            <SpeedRoundedIcon color='primary' fontSize='large' />
          </IconButton>
          <Typography
            variant='h6'
            className={classes.title}
            style={{
              color: '#F8C600',
              fontWeight: 'bolder',
              fontSize: '1.4rem',
              cursor: 'pointer',
            }}
          >
            SYA ADMIN
          </Typography>
          <IconButton
            edge='start'
            className={classes.menuButton}
            color='inherit'
            aria-label='menu'
            href='/'
          >
            <Home />
          </IconButton>
          <IconButton
            edge='start'
            className={classes.menuButton}
            color='inherit'
            aria-label='menu'
            onClick={handleDrawerToggle}
          >
            <NotificationsRoundedIcon />
          </IconButton>
          <IconButton
            edge='start'
            className={classes.menuButton}
            color='inherit'
            aria-label='menu'
            onClick={handleDrawerToggle}
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>

      <nav className={classes.drawer} aria-label='mailbox folders'>
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation='css'>
          <Drawer
            container={container}
            variant='temporary'
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true,
            }}
          >
            <SideBarLinks closeDrawer={closeMobileDrawer} links={links} />
          </Drawer>
        </Hidden>
      </nav>
    </div>
  )
}
