import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import Divider from '@material-ui/core/Divider'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import SpeedRoundedIcon from '@material-ui/icons/SpeedRounded'
import ExitToAppRoundedIcon from '@material-ui/icons/ExitToAppRounded'
import NavItem from './NavItem'
import AdminTopNav from './AdminTopNav'
import { Box } from '@material-ui/core'
import LINKS from '../../../AdminRoutes'
import LanguageChoice from '../../shared/LanguageChoice/LanguageChoice'
import { logUserOut } from '../../../services/authentication.service'
import handleSessionExpiredError from '../../../utils/handleSessionExpiry'
import { usePerson } from '../../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import { injectIntl } from 'react-intl'

const drawerWidth = 240

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    color: 'whitesmoke',
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: theme.palette.secondary.main,
    color: 'white',
    fontSize: '1.1rem',
    letterSpacing: '1.2px',
  },
  drawerContainer: {
    overflow: 'auto',
    flexGrow: 1,
    display: 'grid',
    gridTemplateRows: 'auto auto 1fr',
    paddingTop: theme.spacing(3),
  },
  authenticationLinkHolder: {
    alignSelf: 'end',
  },
  adminDashboardHeader: {
    fontWeight: 'bolder !important',
    fontSize: '1.4rem',
    color: theme.palette.primary.main,
    cursor: 'context-menu',
  },
  navItem: {
    '&:hover': {
      backgroundColor: theme.palette.secondary.dark,
    },
  },
}))

/**
 * display links of the admin nav bar
 * @param {Function} closeDrawer function to close drawer when link is clicked on mobile screen
 * @returns JSX
 */
const SideBarLinks = injectIntl(
  ({ closeDrawer, links, intl: { formatMessage } }) => {
    let classes = useStyles()
    const location = useLocation()
    const navigate = useNavigate()
    const { personDispatch } = usePerson()
    const [isLoggingOut, setIsLoggingOut] = useState(false)

    const logOut = () => {
      setIsLoggingOut(true)
      logUserOut()
        .then(() => {
          personDispatch({ type: 'DISCONNECT_USER' })
          setIsLoggingOut(false)
          navigate('/')
        })
        .catch((error) => {
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          )
        })
    }

    return (
      <React.Fragment>
        <List>
          <ListItem>
            <ListItemIcon>
              <SpeedRoundedIcon color='primary' fontSize='large' />
            </ListItemIcon>
            <ListItemText
              disableTypography
              className={classes.adminDashboardHeader}
              primary='SYA ADMIN'
            />
          </ListItem>
        </List>
        <div className={classes.drawerContainer}>
          <List>
            {LINKS[links].map((link, index) => (
              <NavItem
                icon={link.icon}
                text={link.text}
                path={`/${links}/${link.path}`}
                key={index}
                closeDrawer={closeDrawer}
              />
            ))}
          </List>
          <Divider style={{ backgroundColor: 'white' }} />
          <List className={classes.authenticationLinkHolder}>
            <LanguageChoice />
            <NavItem
              icon={<ExitToAppRoundedIcon style={{ fill: 'white' }} />}
              text='Logout'
              path='#'
              closeDrawer={logOut}
              disabled={isLoggingOut}
            />
          </List>
        </div>
      </React.Fragment>
    )
  }
)

export default function Sidebar({ links }) {
  const classes = useStyles()
  let [isSideBarActive, setIsSideBarActive] = useState(false)
  let toggleSideBar = () => {
    setIsSideBarActive(!isSideBarActive)
  }

  return (
    <>
      <Box>
        <Drawer
          className={classes.drawer}
          variant='permanent'
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <SideBarLinks links={links} />
        </Drawer>
      </Box>
      <AdminTopNav toggleSideBar={toggleSideBar} links={links} />
    </>
  )
}

export { SideBarLinks }
