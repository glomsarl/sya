import React from 'react'
import { NavLink as RouterLink } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import { injectIntl } from 'react-intl'

const useStyles = makeStyles((theme) => ({
  navItem: {
    '&:hover, &.active': {
      backgroundColor: theme.palette.secondary.dark,
    },
    '&.active>.activeSignal': {
      border: '1px solid #dc9607',
      alignSelf: 'stretch',
      marginRight: '14px',
      transition: 'width 2s',
    },
    '&.active': {
      paddingLeft: 0,
      paddingTop: 0,
      paddingBottom: 0,
    },
    '&.active>.listText': {
      paddingTop: '8px',
      paddingBottom: '8px',
    },
  },
}))

export default injectIntl(function NavItem({
  icon,
  text,
  path,
  closeDrawer,
  intl: { formatMessage },
}) {
  const classes = useStyles()

  return (
    <ListItem
      button
      component={RouterLink}
      className={classes.navItem}
      to={path}
      onClick={closeDrawer ? closeDrawer : null}
    >
      <div className='activeSignal' />
      <ListItemIcon>{icon}</ListItemIcon>
      <ListItemText
        className='listText'
        primary={formatMessage({ id: text })}
      />
    </ListItem>
  )
})
