import { Typography, makeStyles } from "@material-ui/core";
import { FormattedDate } from "react-intl";

const useStyles = makeStyles({
  title: {
    fontSize: 14,
    display: "inline",
  },
});

export default function DisplayDate({ dateTitle, value }) {
    const classes = useStyles();
    return (
      <>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          {dateTitle}
        </Typography>
        <Typography color="textPrimary" className={classes.title}>
          <FormattedDate
            value={new Date(value)}
            year="numeric"
            month="short"
            day="numeric"
            weekday="short"
          />
        </Typography>
      </>
    );
}
