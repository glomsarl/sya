import React, { useContext, useState } from "react";
import { Menu, ExitToApp } from "@material-ui/icons";
import {
  CssBaseline,
  Grid,
  IconButton,
  Hidden,
  Drawer,
  AppBar,
  Typography,
  Divider,
  makeStyles,
  useTheme,
  Toolbar,
  Button,
  Tooltip,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";

import { usePerson } from "../../../contexts/PersonContext/Person.provider";
import LanguageContext from "../../../contexts/Language/Language.context";

import UserProfilePage from "../../../Pages/Client/UserProfilePage";
import LanguageChoice from "../LanguageChoice/LanguageChoice";
import enMessages from "../../../language/En-US/en-US";
import frMessages from "../../../language/fr/fr";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
    display: "none",
  },
  menuButton: {
    padding: 0,
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
  "@global": {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: "none",
    },
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    position: "fixed",
  },
  toolbar: {
    flexWrap: "wrap",
    gap: "5px",
  },
  drawerPaper: {
    width: drawerWidth,
  },
  toolbarTitle: {
    flexGrow: 1,
    [theme.breakpoints.up("md")]: {
      fontSize: theme.typography.h3.fontSize,
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: theme.typography.h4.fontSize,
    },
  },
  link: {
    margin: theme.spacing(2, 2),
    color: "white",
    textDecoration: "none",
    "&:hover": {
      color: "#F8C600",
    },
  },
  bouton: {
    cursor: "pointer",
    "&:hover": {
      color: "#011638",
      backgroundColor: theme.palette.primary.main,
    },
  },
  heroContent: {
    padding: theme.spacing(8, 0, 6),
  },
  cardHeader: {
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[200]
        : theme.palette.grey[700],
  },
  cardPricing: {
    display: "flex",
    justifyContent: "center",
    alignItems: "baseline",
    marginBottom: theme.spacing(2),
  },
  footer: {
    borderTop: `1px solid ${theme.palette.divider}`,
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
    },
  },
  linkDrawer: {
    padding: `10px 25px`,
    color: theme.palette.secondary.main,
    textDecoration: "none",
    fontSize: theme.common.displaySmallMobile,
  },
  theDrawer: {
    fontWeight: theme.common.fontWeightBold,
    margin: theme.spacing(1),
    paddingLeft: theme.spacing(2),
    color: theme.palette.secondary.main,
    textDecoration: "none",
    fontSize: theme.common.displaySmallMobile,
  },
  groupTextDrawer: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(2),
  },
  groupDrawer: {
    paddingTop: theme.spacing(2),
  },
  logo: {
    width: "35px",
    height: "35px",
  },
  Profil: {
    backgroundColor: theme.palette.primary.main,
    border: "3px solid #ffff",
    borderRadius: "100%",
    textAlign: "center",
    fontWeight: "bold",
    cursor: "pointer",
    padding: "2px",
    width: "40px",
    height: "40px",
    "&:hover": {
      backgroundColor: theme.palette.primary.dark,
    },
  },
}));

export default function Header(props) {
  const { window } = props;
  const classes = useStyles();
  const theme = useTheme();

  const [mobileOpen, setMobileOpen] = useState(false);
  let [isProfileViewn, setIsProfileViewn] = useState(false);

  const { activeLanguage } = useContext(LanguageContext);
  let defaultMessage = activeLanguage === "fr" ? frMessages : enMessages;

  const {
    personState: {
      person: { isConnected, first_name, last_name },
    },
  } = usePerson();

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const container =
    window !== undefined ? () => window().document.body : undefined;

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="flex-start"
        className={classes.groupDrawer}
      >
        <LanguageChoice />
        <Link
          variant="button"
          color="primary"
          to="/about"
          className={classes.linkDrawer}
        >
          <FormattedMessage
            id="aboutLinkHeader"
            defaultMessage={defaultMessage.aboutLink}
          />
        </Link>
        <Link
          variant="button"
          color="primary"
          to="#"
          className={classes.linkDrawer}
        >
          <FormattedMessage
            id="contactLinkHeader"
            defaultMessage={defaultMessage.contactLinkHeader}
          />
        </Link>
        <Link
          variant="button"
          color="primary"
          to="#"
          className={classes.linkDrawer}
        >
          <FormattedMessage
            id="helpLinkHeader"
            defaultMessage={defaultMessage.helpLinkHeader}
          />
        </Link>
        {isConnected ? null : (
          <>
            <Link
              variant="button"
              color="primary"
              to="/sign-up"
              className={classes.linkDrawer}
            >
              <FormattedMessage
                id="signupButton"
                defaultMessage={defaultMessage.signup}
              />
            </Link>
            <Link
              variant="button"
              color="primary"
              to="/sign-in"
              className={classes.linkDrawer}
            >
              <FormattedMessage
                id="loginBoutonHeader"
                defaultMessage={defaultMessage.loginBoutonHeader}
              />
            </Link>
          </>
        )}
        <Divider />
        <Grid className={classes.groupTextDrawer}>
          <Link
            color="primary"
            to="/demand"
            className={[classes.linkDrawer, classes.theDrawer].join(" ")}
          >
            <FormattedMessage
              id="selectStructureText"
              defaultMessage={defaultMessage.selectStructureText}
            />
          </Link>
        </Grid>
        <Divider />
      </Grid>
    </div>
  );

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar
        position="sticky"
        color="secondary"
        elevation={6}
        className={classes.appBar}
      >
        <Toolbar className={classes.toolbar}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <Menu />
          </IconButton>
          <Typography color="inherit" noWrap className={classes.toolbarTitle}>
            <Link
              variant="button"
              color="textPrimary"
              to="/"
              className={classes.link}
            >
              See You Again
            </Link>
          </Typography>
          <Hidden smDown>
            <LanguageChoice />
          </Hidden>
          <nav>
            <Hidden smDown>
              <Link
                variant="button"
                color="textPrimary"
                to="/about"
                className={classes.link}
              >
                <FormattedMessage
                  id="aboutLinkHeader"
                  defaultMessage={defaultMessage.aboutLinkHeader}
                />
              </Link>
              <Link
                variant="button"
                color="textPrimary"
                to="#"
                className={classes.link}
              >
                <FormattedMessage
                  id="contactLinkHeader"
                  defaultMessage={defaultMessage.contactLinkHeader}
                />
              </Link>
              <Link
                variant="button"
                color="textPrimary"
                to="#"
                className={classes.link}
              >
                <FormattedMessage
                  id="helpLinkHeader"
                  defaultMessage={defaultMessage.helpLinkHeader}
                />
              </Link>
            </Hidden>
          </nav>
          <Button
            variant="outlined"
            color="primary"
            className={classes.bouton}
            style={{ border: "none" }}
          >
            <Link
              variant="button"
              color="textPrimary"
              to="/demand"
              style={{ color: "#ffff" }}
            >
              <FormattedMessage
                id="createStructureText"
                defaultMessage={defaultMessage.createStructureText}
              />
            </Link>
          </Button>
          {isConnected ? (
            <Typography
              variant="subtitle1"
              color="secondary"
              onClick={() => setIsProfileViewn(true)}
              className={classes.Profil}
            >
              {`${last_name && last_name.split("")[0]}${
                first_name && first_name.split("")[0]
              }`}
            </Typography>
          ) : (
            <React.Fragment>
              <Hidden smDown>
                <Button
                  variant="outlined"
                  color="primary"
                  className={classes.bouton}
                >
                  <Link
                    to="/sign-in"
                    color="primary"
                    variant="outlined"
                    style={{ color: "#ffff" }}
                  >
                    <FormattedMessage
                      id="loginBoutonHeader"
                      defaultMessage={defaultMessage.loginBoutonHeader}
                    />
                  </Link>
                </Button>
              </Hidden>
              <Hidden mdUp>
                <Tooltip
                  title={
                    <FormattedMessage
                      id="loginBoutonHeader"
                      defaultMessage={defaultMessage.loginBoutonHeader}
                    />
                  }
                >
                  <IconButton
                    variant="outlined"
                    color="primary"
                    href="/sign-in"
                    className={classes.menuButton}
                  >
                    <ExitToApp />
                  </IconButton>
                </Tooltip>
              </Hidden>
            </React.Fragment>
          )}
          <UserProfilePage
            view={isProfileViewn}
            setIsProfileViewn={setIsProfileViewn}
          />
        </Toolbar>
      </AppBar>
      <Toolbar id="back-to-top-anchor" />
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden xlUp>
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === "rtl" ? "right" : "left"}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
    </React.Fragment>
  );
}
