import { makeStyles, MenuItem, Select } from "@material-ui/core";
import './SelectStructure.css';
import { useContext } from 'react';
import ActiveStructureType from '../../../contexts/ActiveStructureType/ActiveStructureType.context';
  
  const useStyles = makeStyles((theme) => ({
    select: {
      borderBottomLeftRadius: "15px",
      borderTopLeftRadius: "15px",
      [theme.breakpoints.up("md")]: {
        color: "white",
      },
      [theme.breakpoints.down("sm")]: {
        margin: theme.spacing(1),
        paddingLeft: theme.spacing(2),
        textDecoration: "none",
        fontSize: theme.common.displaySmallMobile,
      },
      backgroundColor: "none",
      margin: theme.spacing(2, 2),
    },
    root: {
      display: "grid",
      gridAutoFlow: "column",
    },
  }));
  
  export default function SelectStructure() {
    const classes = useStyles();
    const { ActiveStructure, changeActiveStructureType, structureType } = useContext(ActiveStructureType)
    return (
      <div className={classes.root}>
        <Select
          onChange={e=>changeActiveStructureType(e.target.value)}
          className={classes.select}
          value={ActiveStructure}
        >
          {structureType.map(({ category_name, category_id })=>{
            return(
              <MenuItem value={category_id} key={category_id}>
                {category_name}
              </MenuItem>
            );
          })}
        </Select>
      </div>
    );
  }
  