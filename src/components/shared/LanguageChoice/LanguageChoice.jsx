import { makeStyles, MenuItem, Select } from '@material-ui/core'
import { useContext } from 'react'

import './LanguageChoice.css'
import LanguageContext from '../../../contexts/Language/Language.context'

const useStyles = makeStyles((theme) => ({
  select: {
    borderBottomLeftRadius: '15px',
    borderTopLeftRadius: '15px',
    [theme.breakpoints.up('md')]: {
      color: 'white',
    },
    [theme.breakpoints.down('sm')]: {
      margin: theme.spacing(1),
      paddingLeft: theme.spacing(2),
      textDecoration: 'none',
      fontSize: theme.common.displaySmallMobile,
    },
    backgroundColor: 'none',
    margin: theme.spacing(2, 2),
  },
  root: {
    display: 'grid',
    gridAutoFlow: 'column',
  },
}))

export default function LanguageChoice() {
  const classes = useStyles()
  const { activeLanguage, changeLanguage } = useContext(LanguageContext)
  return (
    <div className={classes.root}>
      <Select
        onChange={(e) => changeLanguage(e.target.value)}
        className={classes.select}
        value={activeLanguage}
      >
        <MenuItem value='fr'>Français</MenuItem>
        <MenuItem value='en'>English</MenuItem>
      </Select>
    </div>
  )
}
