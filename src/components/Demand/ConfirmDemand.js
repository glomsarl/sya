import ConfirmDemandHeader from "./ConfirmDemandHeader";
import ConfirmDemandBody from './ConfirmDemandBody';
import { Box } from "@material-ui/core";


/**
 * confirm demand ui.
 * @returns {JSX} confirm demand interface
 */
function ConfirmDemand() {
    return (
      <Box>
        <ConfirmDemandHeader />
        <ConfirmDemandBody />
      </Box>
    );
}

export default ConfirmDemand