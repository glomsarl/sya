import { useState, useEffect } from "react";
import {
  Button,
  Checkbox,
  CircularProgress,
  Grow,
  IconButton,
  List,
  makeStyles,
  Tooltip,
  Grid,
  Typography,
} from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";
import DeleteIcon from "@material-ui/icons/Delete";
import EmailRoundedIcon from "@material-ui/icons/EmailRounded";
import DraftsRoundedIcon from "@material-ui/icons/DraftsRounded";
import VerifiedUserIcon from "@material-ui/icons/VerifiedUser";
import BlockIcon from "@material-ui/icons/Block";
import { injectIntl, FormattedDate } from "react-intl";
import { useDemand } from "../../contexts/DemandContext/Demand.provider";
import { useActionDemandSpinner } from "../../contexts/ActionDemandSpinnerContext/ActionDemandSpinner.provider";
import ActionDemandDialog from "./RejectDemandDialog";
import DeleteDialog from "../shared/DeleteDialog/DeleteDialog";
import "react-toastify/dist/ReactToastify.css";
import { notifyError, notifySuccess } from "../../utils/toastMessages";
import {
  deleteDemands,
  fetchDemands,
  viewDemands,
} from "../../services/demand.service";
import handleSessionExpiredError from "../../utils/handleSessionExpiry";
import { useLocation, useNavigate } from "react-router";
import { usePerson } from "../../contexts/PersonContext/Person.provider";
import { getAddress } from "../../utils/geocodeAddress";

const useStyles = makeStyles(() => ({
  demand: {
    borderBottom: "1px solid grey",
    cursor: "pointer",
    alignItems: "center",
    padding: "8px 16px",
    "& .demand-icons": {
      visibility: "hidden",
      display: "flex",
    },
    "&:hover .demand-icons": {
      display: "flex",
      visibility: "visible",
      animation: "myfirst 5s linear 2s infinite alternate",
    },
  },
  demandData: {
    paddingLeft: "16px",
  },
  demandDetails: {
    padding: "0 36px",
  },
  demandDetailHeader: {
    fontWeight: "500",
  },
  demandDetailsContainer: {
    borderTop: "1px solid #0000006e",
    padding: "10px 0",
  },
  demandActionsContainer: {
    marginTop: "20px",
  },
  demandActionButton: {
    display: "grid",
    justifyContent: "center",
    justifySelf: "center",
  },
  demandActionned: {
    padding: "9px",
    borderRadius: "10px",
    color: "white",
    display: "grid",
    gridAutoFlow: "column",
  },
  tagClassName: {
    marginRight: "10px",
    backgroundColor: "green",
    "&:hover": {
      backgroundColor: "green",
    },
  },
}));

/**
 * confirm demand part that carries all demands to be confirmed.
 * @returns {JSX} confirm Demand ui body
 */
function ConfirmDemandBody({ intl }) {
  const { dispatch, state } = useDemand();
  const [demandItemsGrow, setDemandItemsGrow] = useState(false);
  const [itemToBeDeletedId, setItemToBeDeletedId] = useState("");
  let [isActionDialogOpen, setIsActionDialogOpen] = useState(false);
  let [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
  let [dialogOpenner, setDialogOpenner] = useState("");
  let [actionnedDemandId, setActionnedDemandId] = useState("");
  const classes = useStyles();
  const {
    demands,
    activeDemand,
    selectedDemandIds,
    isDemandDataLoading,
    isActiveDemandLoading,
  } = state;
  const { formatMessage } = intl;
  const { actionDemandSpinnerState, actionDemandSpinnerDispatch } =
    useActionDemandSpinner();
  let { loading, demandAction } = actionDemandSpinnerState;

  const location = useLocation();
  const navigate = useNavigate();
  const { personDispatch } = usePerson();

  useEffect(() => {
    fetchDemands()
      .then(({ demands }) => {
        dispatch({ type: "ADD_NEW_DEMANDS", payload: demands });
        dispatch({ type: "DEACTIVATE_SKELETON_SCREEN" });
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        );
        notifyError(formatMessage({ id: "couldNotLoadDemandBodyData" }));
      });

    // eslint-disable-next-line
  }, []);

  /**
   * close the actionDemandDialog
   */
  let closeDialog = () => {
    setIsActionDialogOpen(false);
    setDialogOpenner("");
  };

  /**
   * open the actionDemandDialog
   * @param {String} action action that openned the dialog
   */
  let openDialog = (action) => {
    setIsActionDialogOpen(true);
    setDialogOpenner(action);
  };

  /**
   * Delete demands when the dialog goes off
   * @param {Boolean} isDeleteConfirmed confimation response from the dialog
   */
  let deleteDemand = (isDeleteConfirmed) => {
    if (isDeleteConfirmed) {
      actionDemandSpinnerDispatch({ type: "OPEN_SPINNER", payload: "delete" });
      deleteDemands([itemToBeDeletedId])
        .then(() => {
          dispatch({ type: "DELETE_A_DEMAND", payload: itemToBeDeletedId });
          dispatch({ type: "EMPTY_REJECTED_DEMANDS" });
          setIsDeleteDialogOpen(false);
          actionDemandSpinnerDispatch({
            type: "CLOSE_SPINNER",
          });
          notifySuccess(formatMessage({ id: "demandDeletionSuccessMessage" }));
        })
        .catch((error) => {
          setIsDeleteDialogOpen(false);
          actionDemandSpinnerDispatch({
            type: "CLOSE_SPINNER",
          });
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate
          );
          notifyError(formatMessage({ id: "demandDeletionFailedMessage" }));
        });
    } else {
      setItemToBeDeletedId("");
      setIsDeleteDialogOpen(false);
    }
  };

  /**
   * Open the dialog to confirm rejection of a demand
   * @param {String} demand_id id of the demand to be rejected
   */
  let openConfirmRejectionDialog = (demand_id) => {
    setActionnedDemandId(demand_id);
    openDialog("reject");
    actionDemandSpinnerDispatch({ type: "OPEN_SPINNER", payload: "reject" });
  };

  /**
   * Open the dialog to confirm validation of a demand
   * @param {String} demand_id id of the demand to be validated
   */
  let openConfirmValidate = (demand_id) => {
    setActionnedDemandId(demand_id);
    openDialog("validate");
    actionDemandSpinnerDispatch({ type: "OPEN_SPINNER", payload: "validate" });
  };

  /**
   * Open dialog to confirm the deletion of a demand
   * @param {String} demand_id id of the demand whose deletion is to be confirmed
   */
  let openConfirmDeleteDialog = (demand_id) => {
    setItemToBeDeletedId(demand_id);
    setIsDeleteDialogOpen(true);
  };

  /**
   * Mark a demand as read so as to remove it's visual priority
   * @param {String} demand_id the id of the demand to be marked as read
   */
  let markAsRead = (demand_id) => {
    viewDemands([demand_id])
      .then(() => {
        dispatch({
          type: "MARK_DEMAND_AS_READ",
          payload: demand_id,
        });
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        );
      });
  };

  /**
   * Add a demand to the list of selected demands or remove from the list
   * @param {String} demand_id the id of the demand in question
   * @returns
   */
  let selectDemand = (demand_id) => {
    if (selectedDemandIds.includes(demand_id)) {
      dispatch({
        type: "REMOVE_FROM_SELECTED_DEMAND_IDS",
        payload: demand_id,
      });
      return;
    }
    dispatch({ type: "ADD_SELECTED_DEMAND_IDS", payload: demand_id });
  };

  /**
   * show demand details to an admin. it will open the demand and also
   * mark it as viewed if it was not viewed yet
   * @param {HTMLInputElement} event event generated when clicked
   * @param {String} demand_id id of the demand whose details want to be seen
   * @param {Boolean} is_viewed the view status of the demand
   */
  let activateDemand = (event, demand_id, is_viewed) => {
    let toggleOpenTargets = [
      "demandData1",
      "demandData2",
      "demandData3",
      "demandData4",
    ];

    if (toggleOpenTargets.includes(event.target.id) && !loading) {
      if (activeDemand.demand_id === demand_id) {
        dispatch({ type: "CLOSE_ACTIVE_DEMAND" });
        dispatch({ type: "CLOSE_LOAD_ACTIVE_DEMAND" });
      } else {
        dispatch({
          type: "SET_ACTIVE_DEMAND",
          payload: demands.find((_) => _.demand_id === demand_id),
        });
        dispatch({ type: "CLOSE_LOAD_ACTIVE_DEMAND" });
        if (!is_viewed) {
          viewDemands([demand_id])
            .then(() => {
              dispatch({
                type: "MARK_DEMAND_AS_READ",
                payload: demand_id,
              });
            })
            .catch((error) => {
              handleSessionExpiredError(
                personDispatch,
                location.pathname,
                error,
                formatMessage,
                navigate
              );
            });
        }
      }
    }
  };
  const [address, setAddress] = useState("");
  const [activeAddress, setActiveAddress] = useState("");

  let styledDemands = demands
    .sort((a, b) =>
      a.structure.category_name > b.structure.category_name ? 1 : -1
    )
    .map((demand, index) => {
      const {
        structure: {
          longitude,
          latitude,
          name: structure_name,
          category_name,
          specific_indications,
        },
        demand_id,
        is_viewed,
        rejected_at,
        owner: { first_name, last_name },
      } = demand;
      getAddress(latitude, longitude).then((address) => setAddress(address));
      getAddress(
        activeDemand.structure ? activeDemand.structure.latitude : 0,
        activeDemand.structure ? activeDemand.structure.longitude : 0
      ).then((address) => setActiveAddress(address));

      return (
        <Grid
          container
          onMouseOver={() => setDemandItemsGrow(demand.demand_id)}
          onMouseOut={() => setDemandItemsGrow("")}
          className={classes.demand}
          key={index}
          style={
            demand.demand_id ===
            (activeDemand !== undefined ? activeDemand.demand_id : "")
              ? { backgroundColor: "rgb(248 198 0 / 6%)" }
              : demand.is_viewed
              ? { backgroundColor: "rgba(160, 163, 189, 0.14)" }
              : null
          }
          onClick={(event) => activateDemand(event, demand_id, is_viewed)}
        >
          <Grid item xs={12} sm={1}>
            <Tooltip
              arrow
              title={
                selectedDemandIds.includes(demand_id)
                  ? formatMessage({ id: "unselectDemand" })
                  : formatMessage({ id: "selectDemand" })
              }
            >
              <Checkbox
                style={{ width: "fit-content" }}
                checked={selectedDemandIds.includes(demand_id)}
                disabled={rejected_at === null}
                onChange={() => selectDemand(demand_id)}
                color="secondary"
              />
            </Tooltip>
          </Grid>

          <Grid item xs={12} sm={3}>
            <Typography id="demandData1" noWrap className={classes.demandData}>
              {`${first_name} ${last_name}`}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={2}>
            <Typography id="demandData2" noWrap className={classes.demandData}>
              {`${structure_name}`}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3}>
            <Typography id="demandData3" noWrap className={classes.demandData}>
              {`${address ?? specific_indications}`}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={2}>
            <Typography id="demandData4" noWrap className={classes.demandData}>
              {`${category_name}`}
            </Typography>
          </Grid>
          <Grow
            in={demandItemsGrow === demand_id}
            style={{ transformOrigin: "0 0 0" }}
            {...(demandItemsGrow === demand_id ? { timeout: 200 } : {})}
          >
            <Grid
              item
              xs={12}
              sm={1}
              className="demand-icons"
              style={{ paddingLeft: "16px" }}
            >
              {rejected_at !== null && selectedDemandIds.length === 0 ? (
                <Tooltip arrow title={formatMessage({ id: "deleteDemand" })}>
                  <IconButton
                    onClick={() => openConfirmDeleteDialog(demand_id)}
                  >
                    <DeleteIcon />
                  </IconButton>
                </Tooltip>
              ) : null}
              {is_viewed ? (
                <Tooltip arrow title={formatMessage({ id: "viewed" })}>
                  <IconButton>
                    <DraftsRoundedIcon />
                  </IconButton>
                </Tooltip>
              ) : (
                <Tooltip arrow title={formatMessage({ id: "markAsRead" })}>
                  <IconButton onClick={() => markAsRead(demand_id)}>
                    <EmailRoundedIcon />
                  </IconButton>
                </Tooltip>
              )}
            </Grid>
          </Grow>
          {activeDemand.demand_id === demand_id ? (
            <Grid
              container
              item
              xs={12}
              className={classes.demandDetailsContainer}
            >
              <Grid
                className={classes.demandDetails}
                container
                item
                xs={12}
                sm={6}
              >
                <Grid item xs={12} sm={3}>
                  <Typography className={classes.demandDetailHeader} noWrap>
                    {formatMessage({ id: "demandDetailsNames" })}
                  </Typography>
                </Grid>
                {isActiveDemandLoading ? (
                  <Grid item xs={12} sm={9}>
                    <Skeleton
                      height={25}
                      animation="wave"
                      style={{ transform: "scale(1, 0.90)" }}
                    />
                  </Grid>
                ) : (
                  <Grid item xs={12} sm={9}>
                    <Typography
                      noWrap
                    >{`${first_name} ${last_name}`}</Typography>
                  </Grid>
                )}
                <Grid item xs={12} sm={3}>
                  <Typography className={classes.demandDetailHeader} noWrap>
                    {formatMessage({ id: "demandDetailsDateOfBirth" })}
                  </Typography>
                </Grid>
                {isActiveDemandLoading ? (
                  <Grid item xs={12} sm={9}>
                    <Skeleton
                      height={25}
                      width={"50%"}
                      animation="wave"
                      style={{ transform: "scale(1, 0.90)" }}
                    />
                  </Grid>
                ) : (
                  <Grid item xs={12} sm={9}>
                    <FormattedDate
                      value={
                        new Date(activeDemand.owner.date_of_birth || Date.now())
                      }
                      year="numeric"
                      month="long"
                      day="2-digit"
                    />
                  </Grid>
                )}

                <Grid item xs={12} sm={3}>
                  <Typography className={classes.demandDetailHeader} noWrap>
                    {formatMessage({ id: "demandDetailGender" })}
                  </Typography>
                </Grid>
                {isActiveDemandLoading ? (
                  <Grid item xs={12} sm={9}>
                    <Skeleton
                      height={25}
                      animation="wave"
                      width={"30%"}
                      style={{ transform: "scale(1, 0.90)" }}
                    />
                  </Grid>
                ) : (
                  <Grid item xs={12} sm={9}>
                    <Typography
                      noWrap
                    >{`${activeDemand.owner.gender}`}</Typography>
                  </Grid>
                )}
                <Grid item xs={12} sm={3}>
                  <Typography className={classes.demandDetailHeader} noWrap>
                    {formatMessage({ id: "demandDetailNationalIdNumber" })}
                  </Typography>
                </Grid>
                {isActiveDemandLoading ? (
                  <Grid item xs={12} sm={9}>
                    <Skeleton
                      height={25}
                      animation="wave"
                      width={"50%"}
                      style={{ transform: "scale(1, 0.90)" }}
                    />
                  </Grid>
                ) : (
                  <Grid item xs={12} sm={9}>
                    <Typography
                      noWrap
                    >{`${activeDemand.owner.national_id_number}`}</Typography>
                  </Grid>
                )}

                <Grid item xs={12} sm={3}>
                  <Typography className={classes.demandDetailHeader} noWrap>
                    {formatMessage({ id: "demandDetailEmailAndPhone" })}
                  </Typography>
                </Grid>
                {isActiveDemandLoading ? (
                  <Grid item xs={12} sm={9}>
                    <Skeleton
                      height={25}
                      animation="wave"
                      width={"80%"}
                      style={{ transform: "scale(1, 0.90)" }}
                    />
                  </Grid>
                ) : (
                  <Grid item xs={12} sm={9}>
                    <Typography
                      noWrap
                    >{`${activeDemand.owner.email} - ${activeDemand.owner.phone}`}</Typography>
                  </Grid>
                )}
              </Grid>
              <Grid
                className={classes.demandDetails}
                container
                item
                xs={12}
                sm={6}
              >
                <Grid item xs={12} sm={3}>
                  <Typography className={classes.demandDetailHeader} noWrap>
                    {formatMessage({ id: "demandDetailsStructureName" })}
                  </Typography>
                </Grid>
                {isActiveDemandLoading ? (
                  <Grid item xs={12} sm={9}>
                    <Skeleton
                      height={25}
                      width={"80%"}
                      animation="wave"
                      style={{ transform: "scale(1, 0.90)" }}
                    />
                  </Grid>
                ) : (
                  <Grid item xs={12} sm={9}>
                    <Typography noWrap>{`${structure_name}`}</Typography>
                  </Grid>
                )}
                <Grid item xs={12} sm={3}>
                  <Typography className={classes.demandDetailHeader} noWrap>
                    {formatMessage({ id: "demandDetailsStructureAddress" })}
                  </Typography>
                </Grid>
                {isActiveDemandLoading ? (
                  <Grid item xs={12} sm={9}>
                    <Skeleton
                      height={25}
                      animation="wave"
                      style={{ transform: "scale(1, 0.90)" }}
                    />
                  </Grid>
                ) : (
                  <Grid item xs={12} sm={9}>
                    <Typography noWrap>
                      {activeAddress ?? specific_indications}
                    </Typography>
                  </Grid>
                )}
                <Grid item xs={12} sm={3}>
                  <Typography className={classes.demandDetailHeader} noWrap>
                    {formatMessage({ id: "demandDetailsStructureCategory" })}
                  </Typography>
                </Grid>
                {isActiveDemandLoading ? (
                  <Grid item xs={12} sm={9}>
                    <Skeleton
                      height={25}
                      width={"40%"}
                      animation="wave"
                      style={{ transform: "scale(1, 0.90)" }}
                    />
                  </Grid>
                ) : (
                  <Grid item xs={12} sm={9}>
                    <Typography
                      noWrap
                    >{`${activeDemand.structure.category_name.join(
                      ", "
                    )}`}</Typography>
                  </Grid>
                )}
                <Grid item xs={12} sm={3}>
                  <Typography className={classes.demandDetailHeader} noWrap>
                    {formatMessage({ id: "demandDetailsSpecificIndications" })}
                  </Typography>
                </Grid>
                {isActiveDemandLoading ? (
                  <Grid item xs={12} sm={9}>
                    <Skeleton
                      height={25}
                      animation="wave"
                      style={{ transform: "scale(1, 0.90)" }}
                    />
                  </Grid>
                ) : (
                  <Grid item xs={12} sm={9}>
                    <Typography
                      noWrap
                    >{`${activeDemand.structure.specific_indications}`}</Typography>
                  </Grid>
                )}
                <Grid item xs={12} sm={3}>
                  <Typography className={classes.demandDetailHeader} noWrap>
                    {formatMessage({ id: "demandDetailsDemandDateTime" })}
                  </Typography>
                </Grid>
                {isActiveDemandLoading ? (
                  <Grid item xs={12} sm={9}>
                    <Skeleton
                      height={25}
                      width={"50%"}
                      animation="wave"
                      style={{ transform: "scale(1, 0.90)" }}
                    />
                  </Grid>
                ) : (
                  <Grid item xs={12} sm={9}>
                    <Typography noWrap>
                      <FormattedDate
                        value={new Date(activeDemand.created_at)}
                        year="numeric"
                        month="long"
                        day="numeric"
                        weekday="long"
                      />
                    </Typography>
                  </Grid>
                )}
              </Grid>
              <Grid container item className={classes.demandActionsContainer}>
                {!activeDemand.is_valid && activeDemand.rejected_at === null ? (
                  <Grid item xs={6} className={classes.demandActionButton}>
                    <Button
                      variant="outlined"
                      color="default"
                      onClick={() =>
                        openConfirmRejectionDialog(activeDemand.demand_id)
                      }
                      disabled={loading || isActiveDemandLoading}
                    >
                      {loading && demandAction === "reject" && (
                        <CircularProgress color="secondary" size={20} />
                      )}

                      {formatMessage({ id: "rejectDemandButton" })}
                    </Button>
                  </Grid>
                ) : activeDemand.rejected_at !== null ? (
                  <Grid item xs={12} className={classes.demandActionButton}>
                    <Typography
                      className={classes.demandActionned}
                      style={{
                        background: "#ff0000b5",
                        border: "1px solid #ff0000b5",
                      }}
                    >
                      {activeDemand.rejected_at !== null ? <BlockIcon /> : null}
                      {formatMessage({ id: "demandActionnedRejected" })}
                    </Typography>
                  </Grid>
                ) : null}

                {!activeDemand.is_valid && activeDemand.rejected_at === null ? (
                  <Grid item xs={6} className={classes.demandActionButton}>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() =>
                        openConfirmValidate(activeDemand.demand_id)
                      }
                      disabled={loading || isActiveDemandLoading}
                    >
                      {loading && demandAction === "validate" && (
                        <CircularProgress color="secondary" size={20} />
                      )}
                      {formatMessage({ id: "validateDemandButton" })}
                    </Button>
                  </Grid>
                ) : activeDemand.is_valid ? (
                  <Grid item xs={12} className={classes.demandActionButton}>
                    <Typography
                      className={classes.demandActionned}
                      style={{
                        background: "#008000b5",
                        border: "1px solid #008000b5",
                      }}
                    >
                      {activeDemand.is_valid ? <VerifiedUserIcon /> : null}
                      {formatMessage({ id: "demandActionnedValidated" })}
                    </Typography>
                  </Grid>
                ) : null}
              </Grid>
            </Grid>
          ) : null}
        </Grid>
      );
    });

  /**
   * displays the skeleton screen while the demand data loads.
   * @returns The skeleton screen for the demands page.
   */
  function DemandsSkeleton() {
    return (
      <>
        <Skeleton
          height={96}
          animation="wave"
          style={{ transform: "scale(1, 0.90)" }}
        />
        <Skeleton
          height={96}
          animation="wave"
          style={{ transform: "scale(1, 0.90)" }}
        />
        <Skeleton
          height={96}
          animation="wave"
          style={{ transform: "scale(1, 0.90)" }}
        />
        <Skeleton
          height={96}
          animation="wave"
          style={{ transform: "scale(1, 0.90)" }}
        />
        <Skeleton
          height={96}
          animation="wave"
          style={{ transform: "scale(1, 0.90)" }}
        />
        <Skeleton
          height={96}
          animation="wave"
          style={{ transform: "scale(1, 0.90)" }}
        />
      </>
    );
  }

  return isDemandDataLoading ? (
    <DemandsSkeleton />
  ) : (
    <>
      <List dense={false} style={{ paddingTop: "0" }}>
        {styledDemands}
      </List>
      <DeleteDialog
        deleteDialogDecision={deleteDemand}
        isDialogOpen={isDeleteDialogOpen}
      />
      <ActionDemandDialog
        isDialogOpen={isActionDialogOpen}
        dialogOpenner={dialogOpenner}
        closeDialog={closeDialog}
        actionnedDemandId={actionnedDemandId}
        isActiveDemandLoading={isActiveDemandLoading}
      />
    </>
  );
}

export default injectIntl(ConfirmDemandBody);
