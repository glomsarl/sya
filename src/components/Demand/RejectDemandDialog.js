import { useState } from 'react'
import { injectIntl } from 'react-intl'
import 'react-toastify/dist/ReactToastify.css'
import {
  TextareaAutosize,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Dialog,
  Button,
} from '@material-ui/core'

import { useDemand } from '../../contexts/DemandContext/Demand.provider'
import { useActionDemandSpinner } from '../../contexts/ActionDemandSpinnerContext/ActionDemandSpinner.provider'

import { notifySuccess } from '../../utils/toastMessages'
import Transition from '../../utils/DialogTransition'
import { validateDemand } from '../../services/demand.service'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'

function ActionDemandDialog({
  intl,
  isDialogOpen,
  dialogOpenner,
  closeDialog,
  actionnedDemandId,
  isActiveDemandLoading,
}) {
  const { formatMessage } = intl
  const { dispatch } = useDemand()
  const [rejectionReason, setRejectionReason] = useState('')
  const [isDemandActionSubmitting, setIsDemandActionSubmitting] =
    useState(false)
  const { actionDemandSpinnerDispatch } = useActionDemandSpinner()

  const { personDispatch } = usePerson()
  const location = useLocation()
  const navigate = useNavigate()

  /**
   * handle the change during the writing of the rejection reason
   * @param {HTMLInputElement} event the event of the textarea
   */
  const handleRejectionReasonChange = (event) => {
    setRejectionReason(event.target.value)
  }

  /**
   * close reject demand dialog
   */
  const handleClose = () => {
    closeDialog()
    actionDemandSpinnerDispatch({ type: 'CLOSE_SPINNER' })
    setRejectionReason('')
  }

  const handleConfirmValidate = () => {
    setIsDemandActionSubmitting(true)
    validateDemand(actionnedDemandId, {
      rejection_reason: rejectionReason !== '' ? rejectionReason : undefined,
    })
      .then(() => {
        closeDialog()
        dispatch({
          type:
            dialogOpenner === 'reject'
              ? 'REJECT_A_DEMAND'
              : 'VALIDATE_A_DEMAND',
          payload: actionnedDemandId,
        })
        setRejectionReason('')
        actionDemandSpinnerDispatch({ type: 'CLOSE_SPINNER' })
        notifySuccess(
          dialogOpenner === 'reject'
            ? formatMessage({ id: 'demandRejectionSuccessMessage' })
            : formatMessage({ id: 'demandValidationSuccessMessage' })
        )
        setIsDemandActionSubmitting(false)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsDemandActionSubmitting(false)
      })
  }

  return (
    <div>
      <Dialog
        open={isDialogOpen}
        onClose={handleClose}
        TransitionComponent={Transition}
        aria-labelledby='form-dialog-title'
      >
        <DialogTitle id='form-dialog-title'>
          {dialogOpenner === 'reject'
            ? formatMessage({ id: 'rejectDemandReasonDialogHeader' })
            : formatMessage({ id: 'validateDemandDialogHeader' })}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            {dialogOpenner === 'reject'
              ? formatMessage({ id: 'rejectDemandDialogMessage' })
              : formatMessage({ id: 'validateDemandDialogMessage' })}
          </DialogContentText>
          {dialogOpenner === 'reject' ? (
            <TextareaAutosize
              rowsMax={5}
              rowsMin={5}
              placeholder={formatMessage({ id: 'rejectDemandPlaceholder' })}
              value={rejectionReason}
              onChange={handleRejectionReasonChange}
              style={{
                minWidth: '100%',
                borderRadius: '10px',
                outline: 'none',
                borderColor: 'red',
              }}
            />
          ) : null}
        </DialogContent>
        <DialogActions>
          <Button
            onClick={handleConfirmValidate}
            variant='outlined'
            color='default'
            disabled={isDemandActionSubmitting}
          >
            {dialogOpenner === 'reject'
              ? formatMessage({ id: 'rejectDemandButton' })
              : formatMessage({ id: 'validateDemandButton' })}
          </Button>
          <Button
            disabled={isDemandActionSubmitting}
            onClick={handleClose}
            variant='contained'
            color='primary'
          >
            {formatMessage({ id: 'cancelRejectionButton' })}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}
export default injectIntl(ActionDemandDialog)
