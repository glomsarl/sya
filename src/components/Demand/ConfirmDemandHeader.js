import {
  makeStyles,
  Box,
  Checkbox,
  IconButton,
  Tooltip,
} from '@material-ui/core'
import SortIcon from '@material-ui/icons/Sort'
import RefreshIcon from '@material-ui/icons/Refresh'
import DeleteIcon from '@material-ui/icons/Delete'
import NavigateNextIcon from '@material-ui/icons/NavigateNext'
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore'
import { useDemand } from '../../contexts/DemandContext/Demand.provider'
import DeleteDialog from '../shared/DeleteDialog/DeleteDialog'
import { injectIntl } from 'react-intl'
import Skeleton from '@material-ui/lab/Skeleton'
import { useState } from 'react'
import { useActionDemandSpinner } from '../../contexts/ActionDemandSpinnerContext/ActionDemandSpinner.provider'
import { notifyError, notifySuccess } from '../../utils/toastMessages'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { deleteDemands, fetchDemands } from '../../services/demand.service'

const useStyles = makeStyles((theme) => ({
  headerRoot: {
    display: 'grid',
    gridAutoFlow: 'column',
    padding: '10px 16px',
    borderBottom: '1px solid grey',
  },
  headerFirst: {
    justifySelf: 'start',
    alignSelf: 'center',
  },
  headerLast: {
    justifySelf: 'end',
    alignSelf: 'center',
    display: 'grid',
    gridAutoFlow: 'column',
    alignItems: 'center',
  },
  pageNumber: {
    color: 'rgba(0, 0, 0, 0.5)',
    fontWeight: '600',
  },
}))

/**
 * Header to the confirm demand page
 * @param {FunctionConstructor} intl react-intl prop to use formatMessage
 * @returns {JSX} confirm Demand ui header
 */
let ConfirmDemandHeader = ({ intl }) => {
  let [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false)
  const { dispatch, state } = useDemand()
  const {
    totalNumberOfDemands,
    pageNumber,
    selectedDemandIds,
    demands,
    isDemandDataLoading,
  } = state
  const { formatMessage } = intl
  const { actionDemandSpinnerDispatch } = useActionDemandSpinner()

  const numberOfRejectedDemands = demands.filter(
    (demand) => demand.rejected_at !== null,
  ).length

  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  /**
   * used to toggle the state of the selection.
   * @param {HTMLInputElement} event the event triggered when the checkbox is clicked.
   */
  let handleChange = (event) => {
    if (event.target.checked) {
      dispatch({ type: 'SELECT_REJECTED_DEMANDS' })
    } else dispatch({ type: 'EMPTY_REJECTED_DEMANDS' })
  }

  /**
   * Open dialog to confirm the deletion of the selected demands
   */
  let openConfirmDeleteDialog = () => {
    setIsDeleteDialogOpen(true)
  }

  /**
   * handle deletion based on user's choice
   * @param {Boolean} isDeleteConfirmed the user's decision
   */
  let deleteDialogDecision = (isDeleteConfirmed) => {
    if (isDeleteConfirmed) {
      actionDemandSpinnerDispatch({
        type: 'OPEN_SPINNER',
        payload: 'delete',
      })
      deleteDemands(selectedDemandIds)
        .then(() => {
          dispatch({ type: "DELETE_SELECTED_DEMANDS" })
          dispatch({ type: 'EMPTY_REJECTED_DEMANDS' })
          setIsDeleteDialogOpen(false)
          actionDemandSpinnerDispatch({
            type: 'CLOSE_SPINNER',
          })
          notifySuccess(formatMessage({ id: 'demandDeletionSuccessMessage' }))
        })
        .catch((error) => {
          setIsDeleteDialogOpen(false)
          actionDemandSpinnerDispatch({
            type: 'CLOSE_SPINNER',
          })
          handleSessionExpiredError(
            personDispatch,
            location.pathname,
            error,
            formatMessage,
            navigate,
          )
          notifyError(formatMessage({ id: 'demandDeletionFailedMessage' }))
        })
    } else {
      setIsDeleteDialogOpen(false)
      dispatch({ type: 'EMPTY_REJECTED_DEMANDS' })
    }
  }

  const handleRefresh = () => {
    fetchDemands({
      rows_per_page: 50,
      page_number: 0,
    })
      .then(({demands}) => {
        dispatch({ type: 'ADD_NEW_DEMANDS', payload: demands })
        dispatch({ type: 'DEACTIVATE_SKELETON_SCREEN' })
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate,
        )
        notifyError(formatMessage({ id: 'couldNotLoadDemandBodyData' }))
      })
  }

  const classes = useStyles()

  return (
    <Box className={classes.headerRoot}>
      <Box className={classes.headerFirst}>
        <Tooltip
          arrow
          title={
            numberOfRejectedDemands === selectedDemandIds.length
              ? formatMessage({ id: 'unSelectRejected' })
              : formatMessage({ id: 'selectRejected' })
          }
        >
          <Checkbox
            checked={
              numberOfRejectedDemands === selectedDemandIds.length &&
              numberOfRejectedDemands !== 0
            }
            onChange={handleChange}
            color="secondary"
            disabled={numberOfRejectedDemands === 0}
          />
        </Tooltip>

        <Tooltip arrow title={formatMessage({ id: 'filter' })}>
          <IconButton color="secondary" component="span">
            <SortIcon />
          </IconButton>
        </Tooltip>

        <Tooltip arrow title={formatMessage({ id: 'refresh' })}>
          <IconButton
            color="secondary"
            component="span"
            onClick={handleRefresh}
            disabled={isDemandDataLoading}
          >
            <RefreshIcon />
          </IconButton>
        </Tooltip>

        {selectedDemandIds.length === 0 ? null : (
          <Tooltip arrow title={formatMessage({ id: 'deleteSelected' })}>
            <IconButton
              color="secondary"
              component="span"
              onClick={openConfirmDeleteDialog}
            >
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        )}
      </Box>

      <Box className={classes.headerLast}>
        {isDemandDataLoading ? (
          <Box component="span" className={classes.pageNumber}>
            <Skeleton
              height={25}
              width={75}
              animation="wave"
              style={{ transform: 'scale(1, 0.90)' }}
            />
          </Box>
        ) : (
          <Box component="span" className={classes.pageNumber}>
            {`
                ${
                  (pageNumber - 1) *
                    process.env.REACT_APP_NUMBER_OF_DEMANDS_PER_PAGE +
                  1
                } - 
                ${
                  pageNumber *
                    process.env.REACT_APP_NUMBER_OF_DEMANDS_PER_PAGE >
                  totalNumberOfDemands
                    ? totalNumberOfDemands
                    : pageNumber *
                      process.env.REACT_APP_NUMBER_OF_DEMANDS_PER_PAGE
                } ${formatMessage({ id: 'on' })}
                ${totalNumberOfDemands}
            `}
          </Box>
        )}
        <Tooltip arrow title={formatMessage({ id: 'previousPage' })}>
          <IconButton
            color="secondary"
            component="span"
            disabled={isDemandDataLoading}
            onClick={() => {
              dispatch({ type: 'DECREMENT_PAGE_NUMBER' })
            }}
          >
            <NavigateBeforeIcon />
          </IconButton>
        </Tooltip>

        <Tooltip arrow title={formatMessage({ id: 'nextPage' })}>
          <IconButton
            color="secondary"
            component="span"
            disabled={isDemandDataLoading}
            onClick={() => {
              dispatch({ type: 'INCREMENT_PAGE_NUMBER' })
            }}
          >
            <NavigateNextIcon />
          </IconButton>
        </Tooltip>
      </Box>
      <DeleteDialog
        isDialogOpen={isDeleteDialogOpen}
        deleteDialogDecision={deleteDialogDecision}
      />
    </Box>
  )
}

export default injectIntl(ConfirmDemandHeader)
