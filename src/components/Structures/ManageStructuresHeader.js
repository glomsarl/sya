import {
  makeStyles,
  Box,
  IconButton,
  Tooltip,
} from "@material-ui/core";
import SortIcon from "@material-ui/icons/Sort";
import RefreshIcon from "@material-ui/icons/Refresh";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";
import { injectIntl } from "react-intl";
import Skeleton from "@material-ui/lab/Skeleton";
import { useStructure } from "../../contexts/Structures/Structure.provider";
import ToggleStructureAbilityDialog from "./ToggleStructureAbilityDialog";
import { getAllStructures } from "../../services/structure.service";
import handleSessionExpiredError from "../../utils/handleSessionExpiry";
import { usePerson } from "../../contexts/PersonContext/Person.provider";
import { useLocation, useNavigate } from "react-router";
import { notifyError } from "../../utils/toastMessages";

const useStyles = makeStyles((theme) => ({
  headerRoot: {
    display: "grid",
    gridAutoFlow: "column",
    padding: "10px 16px",
    borderBottom: "1px solid grey",
  },
  headerFirst: {
    justifySelf: "start",
    alignSelf: "center",
  },
  headerLast: {
    justifySelf: "end",
    alignSelf: "center",
    display: "grid",
    gridAutoFlow: "column",
    alignItems: "center",
  },
  pageNumber: {
    color: "rgba(0, 0, 0, 0.5)",
    fontWeight: "600",
  },
}));

/**
 * Header to the confirm demand page
 * @param {FunctionConstructor} intl react-intl prop to use formatMessage
 * @returns {JSX} confirm Demand ui header
 */
let ConfirmDemandHeader = ({ intl }) => {
  const { dispatch, state } = useStructure();
  const {
    totalNumberOfStructures,
    pageNumber,
    isStructureDataLoading,
  } = state;
  const { formatMessage } = intl;
  const { personDispatch } = usePerson();
  const location = useLocation();
  const navigate = useNavigate();

  let handleRefresh = () => {
    getAllStructures()
      .then(({ structures }) => {
        dispatch({ type: 'ADD_NEW_STRUCTURES', payload: structures })
        dispatch({ type: 'DEACTIVATE_SKELETON_SCREEN' })
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate,
        )
        notifyError(formatMessage({ id: 'couldNotLoadStructureBodyData' }))
      })
  };

  const classes = useStyles();

  return (
    <Box className={classes.headerRoot}>
      <Box className={classes.headerFirst}>
        <Tooltip arrow title={formatMessage({ id: "filter" })}>
          <IconButton color="secondary" component="span">
            <SortIcon />
          </IconButton>
        </Tooltip>

        <Tooltip arrow title={formatMessage({ id: "refresh" })}>
          <IconButton
            color="secondary"
            component="span"
            onClick={handleRefresh}
            disabled={isStructureDataLoading}
          >
            <RefreshIcon />
          </IconButton>
        </Tooltip>

      </Box>

      <Box className={classes.headerLast}>
        {isStructureDataLoading ? (
          <Box component="span" className={classes.pageNumber}>
            <Skeleton
              height={25}
              width={75}
              animation="wave"
              style={{ transform: "scale(1, 0.90)" }}
            />
          </Box>
        ) : (
          <Box component="span" className={classes.pageNumber}>
            {`
                ${
                  (pageNumber - 1) *
                    process.env.REACT_APP_NUMBER_OF_STRUCTURES_PER_PAGE +
                  1
                } - 
                ${
                  pageNumber *
                    process.env.REACT_APP_NUMBER_OF_STRUCTURES_PER_PAGE >
                  totalNumberOfStructures
                    ? totalNumberOfStructures
                    : pageNumber *
                      process.env.REACT_APP_NUMBER_OF_STRUCTURES_PER_PAGE
                } ${formatMessage({ id: "on" })}
                ${totalNumberOfStructures}
            `}
          </Box>
        )}
        <Tooltip arrow title={formatMessage({ id: "previousPage" })}>
          <IconButton
            color="secondary"
            component="span"
            disabled={isStructureDataLoading}
            onClick={() => {
              dispatch({ type: "DECREMENT_PAGE_NUMBER" });
            }}
          >
            <NavigateBeforeIcon />
          </IconButton>
        </Tooltip>

        <Tooltip arrow title={formatMessage({ id: "nextPage" })}>
          <IconButton
            color="secondary"
            component="span"
            disabled={isStructureDataLoading}
            onClick={() => {
              dispatch({ type: "INCREMENT_PAGE_NUMBER" });
            }}
          >
            <NavigateNextIcon />
          </IconButton>
        </Tooltip>
      </Box>
      <ToggleStructureAbilityDialog />
    </Box>
  );
};

export default injectIntl(ConfirmDemandHeader);
