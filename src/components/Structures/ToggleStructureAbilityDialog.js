import { useState } from 'react'
import {
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Dialog,
  Button,
  CircularProgress,
} from '@material-ui/core'
import { injectIntl } from 'react-intl'
import { useStructure } from '../../contexts/Structures/Structure.provider'
import { notifyError, notifySuccess } from '../../utils/toastMessages'
import Transition from '../../utils/DialogTransition'
import { setStructureAbility } from '../../services/structure.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { useLocation, useNavigate } from 'react-router'
import { usePerson } from '../../contexts/PersonContext/Person.provider'

function ToggleStructureAbilityDialog({ intl, isDialogOpen, closeDialog }) {
  const { dispatch, state } = useStructure()
  let [isSpinnerLoading, setIsSpinnerLoading] = useState(false)
  const { formatMessage } = intl
  let { activeStructure } = state
  let actionnedStructureId = activeStructure.structure_id
  const location = useLocation()
  const navigate = useNavigate()
  const { personDispatch } = usePerson()

  let handleCloseDialog = () => {
    if (!isSpinnerLoading) closeDialog()
  }

  let handleToggleStructureAbility = () => {
    setIsSpinnerLoading(true)
    setStructureAbility([actionnedStructureId], activeStructure.is_disabled)
      .then(() => {
        setIsSpinnerLoading(false)
        dispatch({
          type: 'TOGGLE_STRUCTURE_ABILITY',
          payload: actionnedStructureId,
        })
        notifySuccess(
          activeStructure.is_disabled
            ? formatMessage({ id: 'enableStructureAbilitySuccessMessage' })
            : formatMessage({ id: 'disableStructureAbilitySuccessMessage' })
        )
        setIsSpinnerLoading(false)
        closeDialog()
      })
      .catch((error) => {
        notifyError(
          activeStructure.is_disabled
            ? formatMessage({ id: 'enableStructureAbilityErrorMessage' })
            : formatMessage({ id: 'disableStructureAbilityErrorMessage' })
        )
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsSpinnerLoading(false)
      })
  }

  return (
    <Dialog
      open={isDialogOpen}
      onClose={handleCloseDialog}
      TransitionComponent={Transition}
      aria-labelledby='form-dialog-title'
    >
      <DialogTitle id='form-dialog-title'>
        {activeStructure.is_disabled
          ? formatMessage({ id: 'enableStructureAbilityDialogHeader' })
          : formatMessage({ id: 'disableStructureAbilityDialogHeader' })}
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          {activeStructure.is_disabled
            ? formatMessage({ id: 'enableStructureAbilityDialogMessage' })
            : formatMessage({ id: 'disableStructureAbilityDialogMessage' })}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={handleToggleStructureAbility}
          variant='outlined'
          color='default'
          disabled={isSpinnerLoading}
        >
          {isSpinnerLoading && <CircularProgress color='secondary' size={20} />}
          {formatMessage({ id: 'validateToggleStructureAbilityButton' })}
        </Button>
        <Button
          onClick={handleCloseDialog}
          variant='contained'
          color='primary'
          disabled={isSpinnerLoading}
        >
          {formatMessage({ id: 'cancelToggleStructureAbilityButton' })}
        </Button>
      </DialogActions>
    </Dialog>
  )
}
export default injectIntl(ToggleStructureAbilityDialog)
