import ManageStructuresHeader from './ManageStructuresHeader';
import ManageStructuresBody from './ManageStructuresBody';
import { Box } from "@material-ui/core";

/**
 * confirm demand ui.
 * @returns {JSX} confirm demand interface
 */
function ManageStructures() {
  return (
    <Box>
      <ManageStructuresHeader />
      <ManageStructuresBody />
    </Box>
  );
}

export default ManageStructures;
