import PieChartRoundedIcon from "@material-ui/icons/PieChartRounded";
import EmailRoundedIcon from "@material-ui/icons/EmailRounded";
import HomeRoundedIcon from "@material-ui/icons/HomeRounded";
import MessageRoundedIcon from "@material-ui/icons/MessageRounded";
import SettingsRoundedIcon from "@material-ui/icons/SettingsRounded";
import {
  EventNoteRounded,
  MonetizationOnRounded,
  RoomServiceRounded,
  ShoppingCartRounded,
  ReceiptRounded,
  BusinessCenterRounded,
} from "@material-ui/icons";

const LINKS = {
  admin: [
    {
      text: "Overview",
      icon: <PieChartRoundedIcon style={{ fill: "white" }} />,
      path: "overview",
    },
    {
      text: "Demands",
      icon: <EmailRoundedIcon style={{ fill: "white" }} />,
      path: "demands",
    },
    {
      text: "Structures",
      icon: <HomeRoundedIcon style={{ fill: "white" }} />,
      path: "structures",
    },
    {
      text: "Mails",
      icon: <MessageRoundedIcon style={{ fill: "white" }} />,
      path: "mails",
    },
    {
      text: "configurations",
      icon: <SettingsRoundedIcon style={{ fill: "white" }} />,
      path: "configurations",
    },
    {
      text: "subscriptions",
      icon: <ReceiptRounded style={{ fill: "white" }} />,
      path: "subscriptions",
    },
  ],

  owner: [
    {
      text: "Overview",
      icon: <PieChartRoundedIcon style={{ fill: "white" }} />,
      path: "overview",
    },
    {
      text: "Commandes",
      icon: <ShoppingCartRounded style={{ fill: "white" }} />,
      path: "orders",
    },
    {
      text: "Catalogs",
      icon: <EventNoteRounded style={{ fill: "white" }} />,
      path: "catalogs",
    },
    {
      text: "Services",
      icon: <RoomServiceRounded style={{ fill: "white" }} />,
      path: "services",
    },
    {
      text: "Promotions",
      icon: <MonetizationOnRounded style={{ fill: "white" }} />,
      path: "promotions",
    },
    {
      text: "Publications",
      icon: <PieChartRoundedIcon style={{ fill: "white" }} />,
      path: "publication",
    },
    {
      text: "Receipts",
      icon: <ReceiptRounded style={{ fill: "white" }} />,
      path: "payment-history",
    },
    {
      text: "myStructures",
      icon: <BusinessCenterRounded style={{ fill: "white" }} />,
      path: "owner-structures",
    },
    {
      text: "Settings",
      icon: <SettingsRoundedIcon style={{ fill: "white" }} />,
      path: "profile",
    },
  ],
};

export default LINKS;
