/**
 * generate a random number between min and max inclusive
 * @param {Number} min the minimum number in the range
 * @param {Number} max the maximum number of the range
 * @returns random number gotten
 */
 const randomNumber = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };
  
  export default randomNumber;
