import { toast } from "react-toastify";

/**
 * toast message to notify the user of an event
 * @param {String} message message that will be displayed
 */
const notifyInfo = (message) => {
  toast.dark(message);
};

/**
 * used to toast success messages
 * @param {String} message the success message to be returned
 */
const notifySuccess = (message) => {
    toast.success(message);
};

/**
 * Used to toast an error message
 * @param {String} message the error message the user wants to return
 */
const notifyError = (message) => {
    toast.error(message);
};

export {notifySuccess, notifyError, notifyInfo}