import { notifyError } from './toastMessages'

export default function handleSessionExpiredError(
  personDispatch,
  path,
  error,
  formatMessage,
  navigate
) {
  const BACKEND_ERRORS = [
    { formatMessageId: 'unknowMessageError', backendError: '' },
    { formatMessageId: 'notAuthenticated', backendError: 'User could be not authentificated' },
    {
      formatMessageId: 'BonusNotSet',
      backendError: 'Bonus setting not set yet',
    },
    { formatMessageId: 'sessionExpired', backendError: 'jwt expired' },
    { formatMessageId: 'undefinedValueMessage', backendError: undefined },
    {
      formatMessageId: 'authFailed',
      backendError: 'incorrect email or password !',
    },
    {
      formatMessageId: 'overload',
      backendError: 'Failed to creation for overlap reason',
    },
    {
      formatMessageId: 'orderServingError',
      backendError: 'Cannot serve order before serving time selected by order',
    },
    {
      formatMessageId: 'statusNotChangeble',
      backendError: 'Action can no more be done on this order',
    },
    {
      formatMessageId: 'structureNotSelected',
      backendError: 'A structure needs to be selected',
    },
    {
      formatMessageId: 'validationErrorMessage',
      backendError: 'Validation error',
    },
    {
      formatMessageId: 'sessionNotFoundMessage',
      backendError: 'jwt must be provided',
    },
    {
      formatMessageId: 'accessDeniedMessage',
      backendError: 'Access denied for current user',
    },
    {
      formatMessageId: 'overloadSchedule',
      backendError: 'A schedule already exist with the given date or day',
    },
    {
      formatMessageId: 'fileTypeError',
      backendError: 'Error: File type is not supported',
    },
  ]

  const redirectErrors = [
    'jwt expired',
    'jwt must be provided',
    'A structure needs to be selected',
    'Session not found'
  ]
  const notToastErrors = [
    'Session not found'
  ]
  let errorMessage = error.response ? error.response.data.message : ''
  const private_routes = ['owner', 'admin']
  if (redirectErrors.includes(errorMessage)) {
    personDispatch({ type: 'DISCONNECT_USER' })
    if (path) {
      localStorage.setItem('sya_last_path', path)
      localStorage.setItem('sya_token', '')
      const route = path.split('/')[1]
      if (private_routes.includes(route)) navigate(`/sign-in`)
      if (errorMessage === 'A structure needs to be selected')
        navigate('/owner/owner-structures')
    }
  } else {
    if (errorMessage === 'Access denied for current session') navigate('/')
    const toastMessage = BACKEND_ERRORS.find(
      (error) => error.backendError === errorMessage
    )
    if(errorMessage==='Session not found') localStorage.setItem('sya_token', '')
    !notToastErrors.includes(errorMessage) &&  notifyError(
      toastMessage
        ? formatMessage({ id: toastMessage.formatMessageId })
        : `${errorMessage} Add Error To formatMessage`
    )
    return toastMessage?.formatMessageId || errorMessage;
  }
}
