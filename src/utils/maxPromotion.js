export default function maxPromotion(arrayOfPromotions, promotionType) {
  let max_promotion_value = 0
  let promotion_id = null
  arrayOfPromotions.forEach(
    ({
      value,
      promotion_type,
      purchasing_bonus,
      promotion_id: _promotion_id,
    }) => {
      if (promotion_type === promotionType) {
        if (promotion_type === 'Reduction' && value > max_promotion_value) {
          max_promotion_value = value
          promotion_id = _promotion_id
        } else if (
          promotion_type === 'Bonus' &&
          purchasing_bonus > max_promotion_value
        ) {
          promotion_id = _promotion_id
          max_promotion_value = purchasing_bonus
        }
      }
    }
  )
  return { max_promotion_value, promotion_id }
}
