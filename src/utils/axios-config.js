import axios from 'axios'

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL,
  headers: {
    'Content-type': 'application/json',
  },
})

axiosInstance.interceptors.request.use(
  (request) => {
    request.headers = {
      ...request.headers,
      sya_token: localStorage.getItem('sya_token') || '',
    }
    return request
  },
  (error) => Promise.reject({ message: error.message }),
)

axiosInstance.interceptors.response.use(
  (response) => {
    const {
      data: { new_token, token },
    } = response
    if (token) localStorage.setItem('sya_token', token)
    else if (new_token) localStorage.setItem('sya_token', new_token)
    return response
  },
  (error) => {
    const error_string = error.response.data
    if (typeof error_string === 'string') {
      delete error.response.data
      error.response.data = {
        message: error_string.substring(
          error_string.indexOf('Error:'),
          error_string.indexOf('<br>'),
        ),
      }
    }
    return Promise.reject(error)
  },
)

export default axiosInstance
