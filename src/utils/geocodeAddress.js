import Geocode from 'react-geocode'
Geocode.enableDebug()
Geocode.setApiKey(process.env.REACT_APP_GOOGLE_MAP_API_KEY)

export async function getAddress(latitude, longitude) {
  try {
    const response = await Geocode.fromLatLng(latitude, longitude)
    return response?.results[0]?.formatted_address
  } catch (error) {
    console.error(error)
  }
}
