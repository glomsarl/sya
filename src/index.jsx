import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import LanguageContextProvider from './contexts/Language/Language.provider'
import { BrowserRouter } from 'react-router-dom'
import './index.css'

ReactDOM.render(
  <LanguageContextProvider>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </LanguageContextProvider>,
  document.getElementById('root'),
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
