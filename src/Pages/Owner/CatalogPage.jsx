import React from 'react'

import Catalogs from "../../components/Catalog/Catalogs.jsx";
import AdminPageSkeleton from "../Common/AdminPageSkeleton";
// import StyledCatalog from "../components/Catalog/StyledCatalog";

export default function CatalogPage() {
    
    return (
      <AdminPageSkeleton links='owner'>
        <Catalogs />
      </AdminPageSkeleton>
    );
}
