import React from 'react'
import Service from '../../components/Service/Service'
import AdminPageSkeleton from '../Common/AdminPageSkeleton'

export default function ManageServicesPage() {
  return (
    <AdminPageSkeleton links='owner'>
      <Service />
    </AdminPageSkeleton>
  )
}
