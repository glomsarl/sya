import AdminPageSkeleton from "../Common/AdminPageSkeleton";
import OwnerStructures from "../../components/OwnerStructures/OwnerStructures";

const OwnerStructuresPage = () => {
  return (
    <AdminPageSkeleton links="owner">
      <OwnerStructures />
    </AdminPageSkeleton>
  );
};

export default OwnerStructuresPage;