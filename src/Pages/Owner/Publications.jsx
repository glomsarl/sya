import Publications from '../../components/Publications/Publications';
import AdminPageSkeleton from '../Common/AdminPageSkeleton';

export default function PublicationsPage() {
    return (
      <AdminPageSkeleton links="owner">
        <Publications />
      </AdminPageSkeleton>
    );
}
