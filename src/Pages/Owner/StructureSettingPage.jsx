import React from 'react';
import AdminPageSkeleton from '../Common/AdminPageSkeleton';
import StructureSetting from '../../components/StructureSetting/StructureSetting';

export default function StructureSettingPage() {
    return (
        <AdminPageSkeleton links="owner">
            <StructureSetting />
        </AdminPageSkeleton>
    )
}
