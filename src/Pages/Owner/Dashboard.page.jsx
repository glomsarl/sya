import AdminDashboard from '../../components/Dashboard/AdminDashboard.component';
import AdminPageSkeleton from "../Common/AdminPageSkeleton";

export default function OwnerDashboard() {
  return (
    <AdminPageSkeleton links="owner">
      <AdminDashboard user="owner" />
    </AdminPageSkeleton>
  );
}
