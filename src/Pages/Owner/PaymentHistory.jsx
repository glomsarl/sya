import OwnerSubscriptionHistory from "../../components/SubscriptionTracker/OwnerSubscriptionHistory.jsx";
import AdminPageSkeleton from "../Common/AdminPageSkeleton";

export default function PaymentHistory() {
  return (
    <AdminPageSkeleton links="owner">
      <OwnerSubscriptionHistory />
    </AdminPageSkeleton>
  );
}
