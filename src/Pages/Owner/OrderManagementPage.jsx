import React from 'react';
import OrderManagement from '../../components/OrderManagement/OrderManagement';
import AdminPageSkeleton from '../Common/AdminPageSkeleton';

export default function OrderManagementPage() {
    return (
      <AdminPageSkeleton links="owner">
        <OrderManagement />
      </AdminPageSkeleton>
    );
}
