import Promotions from '../../components/Promotion/Promotions';
import AdminPageSkeleton from '../Common/AdminPageSkeleton';

export default function PromotionsPage() {
    return (
        <AdminPageSkeleton links="owner">
            <Promotions />
        </AdminPageSkeleton>
    )
}
