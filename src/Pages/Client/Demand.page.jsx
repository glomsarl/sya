import React from 'react';
import Demandes from '../../components/Clients/Demandes/Demandes';
import ClientPageSkeleton from './ClientPageSkeleton';

const DemandPage = () => {
    return (
        <ClientPageSkeleton>
            <Demandes />
        </ClientPageSkeleton>
    );
}
 
export default DemandPage;