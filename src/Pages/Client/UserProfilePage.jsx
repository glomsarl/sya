import { injectIntl } from 'react-intl'
import { useEffect, useState } from 'react'
import Scrollbars from 'react-custom-scrollbars-2'
import { Close, ClearAll } from '@material-ui/icons'
import {
  Box,
  makeStyles,
  IconButton,
  Tooltip,
  Typography,
  SwipeableDrawer,
  TextField,
  MenuItem,
} from '@material-ui/core'

import UserOrder from '../../components/Clients/UserOrder/UserOrder'
import UserProfile from '../../components/Clients/UserProfile/UserProfile'
import ProfileEdit from '../../components/Clients/ProfileEdit/ProfileEdit'

import { usePerson } from '../../contexts/PersonContext/Person.provider'
import { useLocation, useNavigate } from 'react-router'
import { findUserInfo, logUserOut } from '../../services/authentication.service'
import handleSessionExpiredError from '../../utils/handleSessionExpiry'
import { getUserOrders } from '../../services/order.service'
import MapComponent from '../../components/Clients/UserOrder/MapComponent'
import { useSYContext } from '../../contexts/SYContext/SY.provider'
import { getBonusSetting } from '../../services/settings.service'

const useStyles = makeStyles((theme) => ({
  UserProfile: {
    height: '85vh',
    width: 'fit-content',
    boxShadow: '1px 1px 4px black',
  },
  Details: {
    width: '355px',
    padding: '10px',
    display: 'grid',
    gridAutoFlow: 'row',
    height: '100%',
  },
  Close: {
    justifySelf: 'right',
    width: '50px',
    height: '50px',
  },
  Header: {
    display: 'grid',
    gridAutoFlow: 'column',
    alignItems: 'center',
    paddingInline: '10px',
  },
  HeadTitle: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  Orders: {
    display: 'grid',
    gridGap: '25px',
  },
  '.MuiFilledInput-root': {
    height: '40px',
  },
}))

function UserProfilePage({ intl: { formatMessage }, view, setIsProfileViewn }) {
  let classes = useStyles()
  let [visibleDetails, setVisibleDetails] = useState(null)

  const {
    personState: { person, orderHistory },
    personDispatch,
  } = usePerson()
  const navigate = useNavigate()
  const location = useLocation()

  const visibleDetailsHandler = (detailsName) => {
    setVisibleElement({ ...visibleElement, [detailsName]: true })
    setVisibleDetails(detailsName)
  }

  const [status, setStatus] = useState('*')
  const [selectedDate, setSelectedDate] = useState()

  const getHistories = (status, selected_date) => {
    getUserOrders({ status, selected_date })
      .then((order_histories) => {
        personDispatch({
          type: 'LOAD_ORDER_HISTORY',
          payload: order_histories,
        })
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
  }

  const { syDispatch } = useSYContext()

  useEffect(() => {
    getBonusSetting()
      .then((bonus) => {
        syDispatch({ type: 'LOAD_SY_VALUE', payload: bonus?.sy_value })
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })
    findUserInfo()
      .then((person) => {
        personDispatch({ type: 'LOAD_PERSON_DATA', payload: person })
        getHistories(status, selectedDate)
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
      })

    // eslint-disable-next-line
  }, [])

  const [visibleElement, setVisibleElement] = useState({
    info: false,
    order: false,
  })

  useEffect(() => {
    if (person.isConnected) getHistories(status, selectedDate)
    // eslint-disable-next-line
  }, [status, selectedDate, person.isConnected, visibleElement.order])

  const toggleDrawer = (element, open) => (event) => {
    if (
      event &&
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return
    }
    setVisibleElement({ ...visibleElement, [element]: open })
  }

  const handleClose = () => {
    setIsProfileViewn(false)
    setVisibleDetails(null)
  }

  const logOuthandler = () => {
    logUserOut()
      .then(() => {
        personDispatch({ type: 'DISCONNECT_USER' })
        setIsProfileViewn(false)
        navigate('/')
      })
      .catch((error) => {
        handleSessionExpiredError(
          personDispatch,
          location.pathname,
          error,
          formatMessage,
          navigate
        )
        setIsProfileViewn(false)
      })
  }

  const handleStatusChange = (event) => {
    const status = event.target.value
    setStatus(status)
    getHistories(status, selectedDate)
  }

  const handleDateChange = (event) => {
    const selected_date = event.target.value
    setSelectedDate(selected_date)
    getHistories(status, selected_date)
  }

  let [isMapShowing, setIsMapShowing] = useState(false)
  let [address, setAddress] = useState({
    logoRef: '',
    location: '',
    structureName: '',
  })

  const openMapHandler = ({ logoRef, structureName, lat, lng }) => {
    setAddress({
      logoRef,
      structureName,
      location: { lat, lng },
    })
    setIsMapShowing(true)
  }

  return (
    <>
      <SwipeableDrawer
        anchor={'right'}
        open={view}
        onClose={handleClose}
        onOpen={() => setIsProfileViewn(true)}
      >
        <UserProfile
          person={person}
          details={visibleDetails}
          handleLogOut={logOuthandler}
          handleDetails={visibleDetailsHandler}
        />
      </SwipeableDrawer>
      <SwipeableDrawer
        anchor={'right'}
        open={visibleElement.info}
        onClose={toggleDrawer('info', false)}
        onOpen={toggleDrawer('info', true)}
      >
        <ProfileEdit closeHandler={toggleDrawer('info', false)} />
      </SwipeableDrawer>
      <SwipeableDrawer
        anchor={'right'}
        open={visibleElement.order}
        onClose={toggleDrawer('order', false)}
        onOpen={toggleDrawer('order', true)}
        className={classes.Main}
      >
        <Box className={classes.Header}>
          <Typography
            variant='h4'
            color='secondary'
            className={classes.HeadTitle}
          >
            {formatMessage({ id: 'myOrder' })}
          </Typography>
          <Tooltip arrow title='Close'>
            <IconButton
              className={classes.Close}
              onClick={toggleDrawer('order', false)}
            >
              <Close />
            </IconButton>
          </Tooltip>
        </Box>
        <Box
          style={{
            display: 'grid',
            gridAutoFlow: 'column',
            gap: '15px',
            paddingInline: '10px',
            maxWidth: '360px',
          }}
        >
          <TextField
            name='date'
            size='small'
            type='date'
            value={selectedDate}
            onChange={handleDateChange}
            variant='outlined'
          />
          <TextField
            select
            name='status'
            label={'Status'}
            variant='standard'
            defaultValue={status}
            onChange={handleStatusChange}
          >
            <MenuItem value='*'>*</MenuItem>
            <MenuItem value='CONFIRMED'>
              {formatMessage({ id: 'confirmed' })}
            </MenuItem>
            <MenuItem value='UNCONFIRMED'>
              {formatMessage({ id: 'unconfirmed' })}
            </MenuItem>
            <MenuItem value='CANCELLED'>
              {formatMessage({ id: 'cancelled' })}
            </MenuItem>
            <MenuItem value='SERVED'>
              {formatMessage({ id: 'served' })}
            </MenuItem>
          </TextField>
          <Box>
            <Tooltip arrow title='Clear all'>
              <IconButton
                onClick={() => {
                  setStatus(null)
                  setSelectedDate(null)
                }}
              >
                <ClearAll />
              </IconButton>
            </Tooltip>
          </Box>
        </Box>
        <Box className={classes.Details}>
          <Scrollbars autoHeight autoHide autoHeightMin={'100%'}>
            <Box className={classes.Orders}>
              {orderHistory && orderHistory.length === 0 ? (
                <Typography
                  style={{ textAlign: 'center', fontWeight: 'bold' }}
                  variant='h5'
                >
                  No Content Yet
                </Typography>
              ) : (
                orderHistory.map((order) => (
                  <UserOrder order={order} openMapHandler={openMapHandler} />
                ))
              )}
            </Box>
          </Scrollbars>
        </Box>
      </SwipeableDrawer>
      {isMapShowing ? (
        <MapComponent
          {...address}
          open={isMapShowing}
          closeHandler={() => setIsMapShowing(false)}
        />
      ) : null}
    </>
  )
}

export default injectIntl(UserProfilePage)
