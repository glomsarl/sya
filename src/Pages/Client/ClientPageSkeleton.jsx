import React from 'react'
import Footer from '../../components/shared/Footer/Footer'
import Header from '../../components/shared/Header/Header'

export default function ClientPageSkeleton({children}){
    return(
        <>
            <Header />
                {children}
            <Footer />
        </>
    )
}