import React from 'react';
import SignUp from '../../components/Clients/SignUp/SignUp';
import ClientPageSkeleton from './ClientPageSkeleton';

const SignupPage = () => {
    return (
        <ClientPageSkeleton>
            <SignUp />
        </ClientPageSkeleton>
    );
}
 
export default SignupPage;