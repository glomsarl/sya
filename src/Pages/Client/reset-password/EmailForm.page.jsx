import React from 'react';
import EmailForm from '../../../components/Clients/ResetPassword/EmailForm';
import ClientPageSkeleton from '../ClientPageSkeleton';

const EmailFormPage = () => {
    return ( 
        <ClientPageSkeleton>
            <EmailForm />
        </ClientPageSkeleton>
    );
}
 
export default EmailFormPage;