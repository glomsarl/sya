import React from 'react';
import Reset from '../../../components/Clients/ResetPassword/Reset';
import ClientPageSkeleton from '../ClientPageSkeleton';

const ResetFormPage = () => {
    return ( 
        <ClientPageSkeleton>
            <Reset />
        </ClientPageSkeleton>
    );
}
 
export default ResetFormPage;