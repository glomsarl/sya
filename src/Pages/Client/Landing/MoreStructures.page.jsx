import MoreStructures from "../../../components/Clients/Landing/MoreStructures"
import ClientPageSkeleton from "../ClientPageSkeleton";

const LandingStructuresPage = () => {
    return (
        <ClientPageSkeleton>
            <MoreStructures />
        </ClientPageSkeleton> 
     );
}
 
export default LandingStructuresPage;