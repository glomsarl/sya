import { useState } from 'react'
import Landing from '../../../components/Clients/Landing/Landing'
import SearchBox from '../../../components/Clients/Landing/searchBox/SearchBox'
import ClientPageSkeleton from '../ClientPageSkeleton'
import landing_background from '../../../assets/landing_background.png'
import { Box, makeStyles, Typography } from '@material-ui/core'
import { injectIntl } from 'react-intl'
import bigLogo from '../../../assets/big_logo.png'
const useStyles = makeStyles((theme) => ({
  slogan: {
    color: theme.palette.secondary.main,
    margin: '5px',
    fontStyle: 'normal',
    fontWeight: 700,
    fontSize: '3rem',
    letterSpacing: '1.5px',
    // width: '500px',
    [theme.breakpoints.down('xs')]: {
      fontSize: '25px',
    },
  },
  logoBox: {
    alignSelf: 'center',
    height: '80%',
    justifySelf: 'end',
    marginRight: '10%',
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  styledImagePart: {
    background: `url(${landing_background})`,
    minHeight: '40vh',
    backgroundSize: '100%',
    backgroundPosition: 'left',
    backgroundRepeat: 'no-repeat',
    display: 'grid',
    gridTemplateColumns: '2fr 1fr',
    padding: '4vw',

    [theme.breakpoints.down('sm')]: {
      gridTemplateColumns: '1fr',
      backgroundPosition: 'inherit',
    },
  },
}))

const LandingPage = ({ intl: { formatMessage } }) => {
  const classes = useStyles()
  const [isSearchActive, setIsSearchActive] = useState(false)
  const [isSearching, setIsSearching] = useState(false)
  const [searchResults, setSearchResults] = useState([])

  return (
    <ClientPageSkeleton>
      <Box className={classes.styledImagePart}>
        <Box style={{ paddingInline: 20 }}>
          <Typography className={classes.slogan}>
            {formatMessage({ id: 'slogan' })}
          </Typography>
          <SearchBox
            isSearching={isSearching}
            isSearchActive={isSearchActive}
            setIsSearching={setIsSearching}
            setIsSearchActive={setIsSearchActive}
            handleSearch={(structures) => setSearchResults(structures)}
          />
        </Box>
        <Box className={classes.logoBox}>
          <img src={bigLogo} alt='SYA Logo' height='120%' />
        </Box>
      </Box>
      <Landing
        isSearchActive={isSearchActive}
        isSearching={isSearching}
        searchStructures={searchResults}
      />
    </ClientPageSkeleton>
  )
}

export default injectIntl(LandingPage)
