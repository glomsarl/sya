import SelectedStructure from "../../../components/Clients/Landing/SelectedStructure";
import ClientPageSkeleton from "../ClientPageSkeleton";

const LandingStructurePage = () => {
    return ( 
        <ClientPageSkeleton>
            <SelectedStructure />
        </ClientPageSkeleton>
     );
}
 
export default LandingStructurePage;