import SignIn from "../../components/Clients/SignIn/SignIn";
import ClientPageSkeleton from "../Client/ClientPageSkeleton";

const SignInPage = () => {
    return (
        <ClientPageSkeleton>
            <SignIn />
        </ClientPageSkeleton>
    );
}
 
export default SignInPage;