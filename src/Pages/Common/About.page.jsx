import About from "../../components/shared/About/About";
import ClientPageSkeleton from "../Client/ClientPageSkeleton";

const AboutPage = () => {
    return (
        <ClientPageSkeleton>
            <About />
        </ClientPageSkeleton>
    );
}
 
export default AboutPage;