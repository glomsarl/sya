import { Box, IconButton, makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import Sidebar from '../../components/shared/Sidebar/Sidebar'
import { useLocation } from 'react-router-dom'
import Scrollbars from 'react-custom-scrollbars-2'
import { injectIntl } from 'react-intl'
import { Home, NotificationsRounded } from '@material-ui/icons'

let useStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    [theme.breakpoints.up('md')]: {
      display: 'grid',
      gridAutoFlow: 'column',
      gridTemplateColumns: 'auto 1fr',
    },
    [theme.breakpoints.down('md')]: {
      gridAutoFlow: 'row',
      gridTemplateRows: 'auto 1fr',
    },
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
      flexDirection: 'column',
      height: '100vh',
    },
  },
  sidebarContent: {
    display: 'grid',
    gridAutoFlow: 'row',
    [theme.breakpoints.up('md')]: {
      gridTemplateRows: 'auto 1fr',
      height: '100vh',
    },
    backgroundColor: 'rgba(168, 175, 213, 1)',
    padding: '0 5%',
    [theme.breakpoints.down('sm')]: {
      height: '100%',
      padding: '5%',
    },
  },
  sidebarContentHeader: {
    display: 'grid',
    gridAutoFlow: 'column',
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  pageTitle: {
    paddingLeft: '20px',
    alignSelf: 'center',
    fontWeight: 'bolder',
    textTransform: 'uppercase',
  },
  notificationIcon: {
    justifySelf: 'end',
    marginRight: '20px',
  },
  pageContent: {
    borderRadius: '7px',
    backgroundColor: 'white',
    padding: '5px',
    [theme.breakpoints.up('md')]: {
      marginBottom: '50px',
    },
  },
}))

function AdminPageSkeleton({ children, links, intl: { formatMessage } }) {
  const classes = useStyles()
  const location = useLocation()

  const pageTitle = {
    '/admin/confirm-demand': formatMessage({ id: 'DemandesPageTitle' }),
    '/admin/structures': formatMessage({ id: 'StructuresPageTitle' }),
    '/admin/overview': formatMessage({ id: 'dashboardPageTitle' }),
    '/admin/demands': formatMessage({ id: 'demandsPageTitle' }),
    '/admin/mails': formatMessage({ id: 'mailsPageTitle' }),
    '/admin/configurations': formatMessage({ id: 'configurationsPageTitle' }),
    '/admin/subscriptions': formatMessage({ id: 'subscriptions' }),
    '/owner/owner-structures': formatMessage({ id: 'myStructuresPageTitle' }),
    '/owner/profile': formatMessage({ id: 'ParametresPageTitle' }),
    '/owner/publication': formatMessage({ id: 'publicationPageTitle' }),
    '/owner/promotions': formatMessage({ id: 'promotionsPageTitle' }),
    '/owner/services': formatMessage({ id: 'ServicesPageTitle' }),
    '/owner/catalogs': formatMessage({ id: 'catalogsPageTitle' }),
    '/owner/orders': formatMessage({ id: 'ordersPageTitle' }),
    '/owner/overview': formatMessage({ id: 'dashboardPageTitle' }),
    '/owner/payment-history': formatMessage({ id: 'Receipts' }),
  }

  return (
    <Box component='div' className={classes.root}>
      <Sidebar links={links} />
      <Box className={classes.sidebarContent}>
        <Box className={classes.sidebarContentHeader}>
          <Typography className={classes.pageTitle}>
            {pageTitle[location.pathname]}
          </Typography>
          <Box className={classes.notificationIcon}>
            <IconButton
              edge='start'
              color='inherit'
              aria-label='menu'
            >
              <NotificationsRounded />
            </IconButton>
            <IconButton
              edge='start'
              color='inherit'
              aria-label='menu'
              href='/'
            >
              <Home />
            </IconButton>
          </Box>
        </Box>
        <Box className={classes.pageContent}>
          <Scrollbars autoHide>{children}</Scrollbars>
        </Box>
      </Box>
    </Box>
  )
}
export default injectIntl(AdminPageSkeleton)
