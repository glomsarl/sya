import React from 'react';
import { Typography, Box, makeStyles } from '@material-ui/core';
import ClientPageSkeleton from './Client/ClientPageSkeleton';
import { injectIntl } from 'react-intl';

const useStyles = makeStyles(theme => ({
    Container: {
        display: "grid",
        gridAutoFlow: "column",
        justifyContent: "center",
        margin: 50,
    },
    Message: {
        // backgroundColor: theme.palette.primary.light,
        textAlign: "center", 
        width: "70vw",
        padding: 10,
        borderRadius: 5, 
        minHeight: "50vh",
    },
    NotFound: {
        fontSize: "bold",
        letterSpacing: 15,

    }
}))

const PageNotFound = ({ intl }) => {
    let classes = useStyles()
    let { formatMessage } = intl;

    return ( 
        <ClientPageSkeleton>
            <Box className={classes.Container}>
                <Box className={classes.Message}>
                    <Typography variant='h3' style={{textAlign: "center", margin: 10,}}>
                        <img 
                            width="500"
                            height="300"
                            alt="PAGE NOT FOUND"
                            src="https://www.pngkey.com/png/detail/212-2123917_404-404-error-icon-png.png"
                        />
                    </Typography>
                    <Box style={{display: "grid", gridAutoColumns: "colunm" }}>
                        <Typography variant="h3" color="secondary" style={{ width: "65%", justifySelf: "center" }}>
                            {formatMessage({id: "message4041"})}
                        </Typography>
                    </Box>
                </Box>
            </Box>
        </ClientPageSkeleton>
     );
}
 
export default injectIntl(PageNotFound);