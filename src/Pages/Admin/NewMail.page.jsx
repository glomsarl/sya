import NewMail from "../../components/Mail/NewMail/NewMail.component";
import AdminPageSkeleton from "../Common/AdminPageSkeleton";

const NewMailPage = () => {
  return (
    <AdminPageSkeleton links="admin">
      <NewMail />
    </AdminPageSkeleton>
  );
};

export default NewMailPage;
