import AdminDashboard from "../../components/Dashboard/AdminDashboard.component";
import AdminPageSkeleton from "../Common/AdminPageSkeleton";

export default function AdminDashboardPage() {
  return (
    <AdminPageSkeleton links="admin">
      <AdminDashboard user="admin" />
    </AdminPageSkeleton>
  );
}
