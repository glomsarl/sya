import ManageStructures from "../../components/Structures/ManageStructures";
import AdminPageSkeleton from "../Common/AdminPageSkeleton";

const ManageStructurePage = () => {
    return (
        <AdminPageSkeleton links="admin">
            <ManageStructures />
        </AdminPageSkeleton>
    );
}
 
export default ManageStructurePage;