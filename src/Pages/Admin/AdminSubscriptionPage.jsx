import AdminSubscriptionDashboard from "../../components/SubscriptionTracker/AdminSubscriptionDashboard";
import AdminPageSkeleton from "../Common/AdminPageSkeleton";

export default function AdminSubscriptionPage() {
  return (
    <AdminPageSkeleton links="admin">
      <AdminSubscriptionDashboard/>
    </AdminPageSkeleton>
  );
}
