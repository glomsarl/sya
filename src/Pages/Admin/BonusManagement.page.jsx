import BonusManagement from "../../components/BonusManagement/BonusManagement";
import AdminPageSkeleton from "../Common/AdminPageSkeleton";


const BonusManagementPage = () => {
  return (
    <AdminPageSkeleton links="admin">
      <BonusManagement />
    </AdminPageSkeleton>
  );
};

export default BonusManagementPage;
