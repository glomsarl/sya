import ConfirmDemand from "../../components/Demand/ConfirmDemand";
import AdminPageSkeleton from "../Common/AdminPageSkeleton";

const ConfirmDemandPage = () => {
    return (
        <AdminPageSkeleton links="admin">
            <ConfirmDemand />
        </AdminPageSkeleton>
    );
}
 
export default ConfirmDemandPage;