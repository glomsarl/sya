import "./App.css";
import "react-toastify/dist/ReactToastify.css";
import "react-perfect-scrollbar/dist/css/styles.css";

import routes from "./routes";
import { useRoutes } from "react-router-dom";
import { useContext } from "react";
import { ToastContainer } from "react-toastify";
import { IntlProvider } from "react-intl";

import { ThemeProvider, Box } from "@material-ui/core";

import frMessages from "./language/fr/fr";
import enMessages from "./language/En-US/en-US";

import theme from "./components/shared/ui/Theme";
import Ancre from "./components/shared/ScrollTop.jsx/ScrollTop";

import SYContextProvider from "./contexts/SYContext/SY.provider";
import LanguageContext from "./contexts/Language/Language.context";
import InfosDemandeProvider from "./contexts/Demandes/Demande.provider";
import ServiceContextProvider from "./contexts/Service/Service.provider";
import CatalogContextProvider from "./contexts/Catalog/Catalog.provider";
import PersonContextProvider from "./contexts/PersonContext/Person.provider";
import PromotionContextProvider from "./contexts/Promotion/Promotion.provider";
import DemandContextProvider from "./contexts/DemandContext/Demand.provider.js";
import StructureContextProvider from "./contexts/Structures/Structure.provider";
import LandingAvisContextProvider from "./contexts/LandingAvisContext/LandingAvis.provider";
import ActiveStructureType from "./contexts/ActiveStructureType/ActiveStructureType.provider";
import LandingCategoryContextProvider from "./contexts/LandingCategoryContext/LandingCategory.provider";
import LandingStructureContextProvider from "./contexts/LandingStructureContext/LandingStructure.provider";
import ActionDemandSpinnerProvider from "./contexts/ActionDemandSpinnerContext/ActionDemandSpinner.provider";
import ToggleStructureAbilityProvider from "./contexts/ToggleStructureAbility/ToggleStructureAbility.provider";
import LandingOngoingOrderContextProvider from "./contexts/LandingOngoingOrderContext/LandingOngoingOrder.provider";
import PublicationContextProvider from "./contexts/PublicationContext/Publication.provider";

function App() {
  const { activeLanguage } = useContext(LanguageContext);
  let activeMessage = activeLanguage === "fr" ? frMessages : enMessages;

  const routing = useRoutes(routes);

  return (
    <ThemeProvider theme={theme}>
      <ToastContainer />
      <IntlProvider
        messages={activeMessage}
        locale={activeLanguage}
        defaultLocale="fr"
        key={activeLanguage}
      >
        <div className="App">
          <ActiveStructureType>
            <LandingCategoryContextProvider>
              <LandingStructureContextProvider>
                <LandingAvisContextProvider>
                  <LandingOngoingOrderContextProvider>
                    <SYContextProvider>
                      <PersonContextProvider>
                        <InfosDemandeProvider>
                          <DemandContextProvider>
                            <ActionDemandSpinnerProvider>
                              <StructureContextProvider>
                                <ToggleStructureAbilityProvider>
                                  <ServiceContextProvider>
                                    <CatalogContextProvider>
                                      <PromotionContextProvider>
                                        <PublicationContextProvider>
                                          <Ancre />
                                          <Box>{routing}</Box>
                                        </PublicationContextProvider>
                                      </PromotionContextProvider>
                                    </CatalogContextProvider>
                                  </ServiceContextProvider>
                                </ToggleStructureAbilityProvider>
                              </StructureContextProvider>
                            </ActionDemandSpinnerProvider>
                          </DemandContextProvider>
                        </InfosDemandeProvider>
                      </PersonContextProvider>
                    </SYContextProvider>
                  </LandingOngoingOrderContextProvider>
                </LandingAvisContextProvider>
              </LandingStructureContextProvider>
            </LandingCategoryContextProvider>
          </ActiveStructureType>
        </div>
      </IntlProvider>
    </ThemeProvider>
  );
}

export default App;
