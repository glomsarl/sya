import http from '../utils/axios-config'

const getAllOrders = async (queryOptions) => {
  const {
    data: { orders },
  } = await http.get('/owner/order/all', {
    params: queryOptions,
  })
  return orders
}

const getUserOrders = async (queryOptions) => {
  const {
    data: { order_histories },
  } = await http.get('/client/order/all', { params: queryOptions })
  return order_histories.map((order) => {
    return {
      ...order,
      service: {
        ...order.service,
        main_image_ref: `${process.env.REACT_APP_BASE_URL}/${order.service.main_image_ref}`,
      },
    }
  })
}

const confirmOrder = (order_id) => {
  return http.put(`/owner/order/${order_id}/served`)
}

const changeOrderStatus = ({ order_id, status }) => {
  return http.put('/client/order/status', { order_id, status })
}

const unAuthenticatedPurchase = async (action, data) => {
  const {
    data: { token, person },
  } = await http.post(`/client/${action}/purchase`, data)
  if (token) localStorage.setItem('sya_token', token)
  return person
}

const purchase = (data) => {
  return http.post(`/client/purchase`, data)
}

export {
  purchase,
  confirmOrder,
  getAllOrders,
  getUserOrders,
  changeOrderStatus,
  unAuthenticatedPurchase,
}
