import http from '../utils/axios-config'

const sendEmails = (data) => {
  return http.post('/admin/mail/sent', data)
}

const getStructures = async (queryOptions) => {
  const {
    data: { structures },
  } = await http.get('/admin/mail/structures', {
    params: queryOptions,
  })
  return structures
}

export { sendEmails, getStructures }
