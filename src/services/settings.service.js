import http from '../utils/axios-config'

const updateSubscription = (data) => {
  return http.put(
    `/admin/subscription/${process.env.REACT_APP_SUBSCRIPTION_1}/edit`,
    data,
  )
}

const setBonusSetting = (data) => {
  return http.post('/admin/bonus-setting/new', data)
}

const editBonusSetting = (data) => {
  return http.put(`/admin/bonus-setting/edit`, data)
}

const getBonusSetting = async () => {
  const {
    data: { bonus_setting },
  } = await http.get('/user/bonus-setting')
  return bonus_setting
}

const getSubscriptions = async (subscription_id) => {
  const {
    data: { subscriptions },
  } = await http.get('/user/subscriptions', { params: { subscription_id } })
  return subscriptions
}

export {
  getBonusSetting,
  setBonusSetting,
  editBonusSetting,
  getSubscriptions,
  updateSubscription,
}
