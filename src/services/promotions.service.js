import http from '../utils/axios-config'

const addServiceToPromotion = async (data) => {
  const {
    data: { services },
  } = await http.post('/owner/promotion/add-services', data)
  return services
}

const createPromotion = async (data) => {
  const {
    data: { promotion },
  } = await http.post('/owner/promotion/new', data)
  return promotion
}

const editPromotion = (promotion_id, data) => {
  return http.put(`/owner/promotion/${promotion_id}/edit`, data)
}

const deletePromotion = (promotion_id) => {
  return http.put(`/owner/promotion/${promotion_id}/delete`)
}

const fetchPromotionServices = async (promotion_id) => {
  const {
    data: { services },
  } = await http.get('/owner/promotion/all-services', {
    params: { promotion_id },
  })
  return services
}

const fetchAllPromotions = async (structure_id) => {
  const {
    data: { promotions },
  } = await http.get('/owner/promotion/all', {
    params: { structure_id },
  })
  return promotions
}

const removeServiceFromPromotions = (service_id, promotion_id) => {
  return http.put(`/owner/promotion/${promotion_id}/${service_id}/remove`)
}

const getPromotionStatistics = async ({ promotion_id, structure_id }) => {
  const {
    data: { bonus_statistics },
  } = await http.get('/user/statistic/promotions', {
    params: { promotion_id, structure_id },
  })
  return bonus_statistics;
}

const getCodeValuePercentage = async (code_value) => {
  const {
    data: { promotion_code },
  } = await http.get('/user/promotion-code', {
    params: { code_value },
  })
  return promotion_code
}

export {
  editPromotion,
  createPromotion,
  deletePromotion,
  fetchAllPromotions,
  addServiceToPromotion,
  fetchPromotionServices,
  getCodeValuePercentage,
  getPromotionStatistics,
  removeServiceFromPromotions,
}
