import http from '../utils/axios-config'

const getAllCatalogs = async (queryOptions) => {
  const {
    data: { catalogs },
  } = await http.get('/user/structures/catalogs', {
    params: queryOptions,
  })
  return catalogs
}

const fetchCatalogServices = async (queryOptions) => {
  const { data } = await http.get('/user/structure/catalog-services', {
    params: queryOptions ?? {},
  })
  return data
}

const addServicesToCatalog = (data) => {
  return http.post('/owner/catalog/add-services', data)
}

const removeServicesFromCatalog = (data) => {
  return http.post('/owner/catalog/remove-services', data)
}

const createCatalog = async (data) => {
  const {
    data: { catalog },
  } = await http.post('/owner/catalog/new', data)
  return catalog
}

const editCatalog = (catalog_id, data) => {
  return http.put(`/owner/catalog/${catalog_id}/edit`, data)
}

const deleteCatalog = (catalog_id) => {
  return http.put(`/owner/catalog/${catalog_id}/delete`)
}

export {
  editCatalog,
  deleteCatalog,
  createCatalog,
  getAllCatalogs,
  addServicesToCatalog,
  fetchCatalogServices,
  removeServicesFromCatalog,
}
