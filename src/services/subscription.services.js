import http from '../utils/axios-config'

const getAllSubscriptions = async (queryOptions) => {
  const { data } = await http.get('/user/subscriptions-tracker', {
    params: queryOptions,
  })
  return data
}

const getActiveSubscription = async (queryOptions) => {
  const {
    data: { active_subscription },
  } = await http.get('/user/subscription/active', {
    params: queryOptions,
  })
  return active_subscription
}

const paySubscription = async (data) => {
  const {
    data: { subscription_payment },
  } = await http.post('/owner/subscription/new', data)
  return subscription_payment
}

const subscriptionStatistics = async () => {
  const {
    data: { statistics },
  } = await http.get('/admin/statistic/subscriptions')
  return statistics
}

export {
  paySubscription,
  getAllSubscriptions,
  getActiveSubscription,
  subscriptionStatistics,
}
