import http from '../utils/axios-config'

const getOrderStatistics = async (structure_id) => {
  const { data } = await http.get('/owner/statistic/orders', {
    params: { structure_id },
  })
  return data
}

const getOrderStatisticsByServices = async (structure_id) => {
  const {
    data: { services },
  } = await http.get('/owner/statistic/service-orders', {
    params: { structure_id },
  })
  return services
}

const getClientStatistics = async (queryOptions) => {
  const {
    data: { client_statistics },
  } = await http.get('/admin/statistic/clients', {
    params: queryOptions,
  })
  client_statistics.unshift({
    date: new Date('2022-05-01'),
    quantity: 0,
  })
  return client_statistics
}

const getOwnerOverviews = async (structure_id) => {
  const {
    data: { overviews },
  } = await http.get('/owner/statistic/overview', {
    params: { structure_id },
  })
  return overviews
}

const getAdminOverviews = async (structure_id) => {
  const {
    data: { overviews },
  } = await http.get('/admin/statistic/overview', {
    params: { structure_id },
  })
  return overviews
}

const getSaleStatistics = async (queryOptions) => {
  const {
    data: { sales_statistics },
  } = await http.get('/user/statistic/sales', {
    params: queryOptions,
  })
  sales_statistics.unshift({
    date: new Date('2022-05-01'),
    quantity: 0,
  })
  return sales_statistics
}

const getFinanceStatistics = async (queryOptions) => {
  let {
    data: { entries },
  } = await http.get('/owner/statistic/finances', {
    params: queryOptions,
  })
  entries.unshift({
    date: new Date('2022-05-01'),
    quantity: 0,
  })
  return entries
}

export {
  getOwnerOverviews,
  getAdminOverviews,
  getSaleStatistics,
  getOrderStatistics,
  getClientStatistics,
  getFinanceStatistics,
  getOrderStatisticsByServices,
}
