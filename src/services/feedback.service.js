import http from '../utils/axios-config'

const sendNewFeedback = (data) => {
  return http.post('/client/order/feedback', data)
}

const getFeedbacks = async ({ structure_id }) => {
  const {
    data: { feedbacks },
  } = await http.get('/user/feedbacks', { params: { structure_id } })
  return feedbacks
}

export { sendNewFeedback, getFeedbacks }
