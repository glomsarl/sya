import http from '../utils/axios-config'

const searchUserByEmail = () => {
  return http.get('/user/email-search')
}

const sendResetPasswordLink = (data) => {
  return http.post('/user/reset-password', data)
}

const setNewPassword = (data) => {
  const { link, email, new_password } = data
  return http.post(`/user/new-password/${link}/${email}`, { new_password })
}

const sendStructureDemand = (data) => {
  const { structure_info: { logo_ref } } = data
  delete data.structure_info.log_ref
  const formData = new FormData()
  formData.append('data', JSON.stringify(data))
  formData.append('logo_ref', logo_ref)
  return http.post('/user/demand', formData)
}

const signIn = async (data) => {
  const {
    data: { token, person },
  } = await http.post('/user/sign-in', data)
  localStorage.setItem('sya_token', token)
  return person
}

const register = async (data) => {
  const {
    data: { token, person },
  } = await http.post('/client/register', data)
  localStorage.setItem('sya_token', token)
  return person
}

const findUserInfo = async () => {
  const {
    data: { profile },
  } = await http.get('/user/profile/info')
  return profile
}

const getSyPoints = async (person_id) => {
  const {
    data: { sy_points },
  } = await http.get('/client/sy-points', { params: { person_id } })
  return sy_points
}

const updateProfile = async (person_id, data) => {
  const formData = new FormData()
  const { person_image_ref } = data
  delete data.person_image_ref
  formData.append('data', JSON.stringify(data))
  formData.append('person_image_ref', person_image_ref)
  return http.put(`/user/profile/${person_id}/edit`, formData)
}

const logUserOut = async () => {
  await http.put('/user/log-out')
  localStorage.setItem('sya_token', '')
}

export {
  signIn,
  register,
  logUserOut,
  getSyPoints,
  findUserInfo,
  updateProfile,
  setNewPassword,
  searchUserByEmail,
  sendStructureDemand,
  sendResetPasswordLink,
}
