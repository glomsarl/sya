import http from '../utils/axios-config'

const addNewPublications = async (structure_id, newImages) => {
  const formatData = new FormData()
  formatData.append('structure_id', structure_id)
  newImages.forEach((image) => {
    formatData.append('publications', image)
  })
  const {
    data: { publications },
  } = await http.post('/owner/publication/new', formatData)
  return publications.map(({ publication_ref, publication_id }) => ({
    publication_id,
    publication_ref: `${process.env.REACT_APP_BASE_URL}/${publication_ref}`,
  }))
}

const fetchAllPublications = async (structure_id) => {
  const {
    data: { publications },
  } = await http.get('/user/structure/publications', { params: { structure_id } })
  return publications.map(({ publication_ref, publication_id }) => ({
    publication_id,
    publication_ref: `${process.env.REACT_APP_BASE_URL}/${publication_ref}`,
  }))
}

const deletePublication = (publication_id) => {
  return http.put(`/owner/publication/${publication_id}/delete`)
}

export { deletePublication, addNewPublications, fetchAllPublications }
