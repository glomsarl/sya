import http from '../utils/axios-config'

const fetchDemands = async (offsets) => {
  const { rows_per_page, page_number } = offsets ?? {}
  const {
    data: { demands, total_number_of_demands },
} = await http.get('/admin/demand/all', {
  params: {
    offsets:
      page_number && rows_per_page
        ? {
            offset: rows_per_page * page_number,
            limit: rows_per_page * (page_number + 1),
          }
        :  { offset: 0, limit: process.env.REACT_APP_NUMBER_OF_STRUCTURES_PER_PAGE },
  },
})
  return { demands, total_number_of_demands }
}

const deleteDemands = (demands) => {
  return http.put('/admin/demand/delete', { demands })
}

const viewDemands = (demands) => {
  return http.put('/admin/demand/view', { demands })
}

const validateDemand = (demand_id, data) => {
  return http.put(`/admin/demand/${demand_id}/validate`, data)
}

export { viewDemands, fetchDemands, deleteDemands, validateDemand }
