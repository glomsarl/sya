import http from '../utils/axios-config'

const getStructureCagetories = async () => {
  const {
    data: { structure_categories },
  } = await http.get('/user/structure/categories')
  return structure_categories
}

const getAllStructures = async (offsets) => {
  const { rows_per_page, page_number } = offsets ?? {}
  const { data } = await http.get('/admin/structure/all', {
    params: {
      page_number,
      rows_per_page,
    },
  })
  return data
}

const setStructureAbility = async (structure_ids, is_disabled) => {
  return http.put('/admin/structure/ability', { structure_ids, is_disabled })
}

const getOwnerStructures = async () => {
  const {
    data: { owner_structures },
  } = await http.get('/user/owner-structures')
  return owner_structures.map((structure) => ({
    ...structure,
    logo_ref:
      structure.logo_ref.split('/')[1] !== 'null'
        ? `${process.env.REACT_APP_BASE_URL}/${structure.logo_ref}`
        : null,
  }))
}

const setActiveStructure = (structure_id) => {
  return http.put('/user/structure/active', { structure_id })
}

const updateStructure = async (structure_id, data) => {
  const { logo_ref } = data
  delete data.log_ref
  const formData = new FormData()
  formData.append('data', JSON.stringify(data))
  formData.append('logo_ref', logo_ref)
  return http.put(`/owner/profile/${structure_id}/edit`, formData)
}

const getStructreSchedules = async (structure_id) => {
  const {
    data: { schedules },
  } = await http.get('/user/structure/schedules', {
    params: { structure_id },
  })
  return schedules
}

const createNewSchedule = async (new_schedule) => {
  const {
    data: { schedule_id },
  } = await http.post('/owner/schedule/new', new_schedule)
  return schedule_id
}

const updateSchedule = async (schedule_id, data) => {
  return http.put(`/owner/schedule/${schedule_id}/edit`, data)
}

const deleteSchedule = async (schedule_id) => {
  return http.put(`/owner/schedule/${schedule_id}/delete`)
}

const editService = (service_id, data) => {
  const { main_image_ref } = data
  const formData = new FormData()
  delete data.main_image_ref
  formData.append('data', JSON.stringify(data))
  formData.append('main_image_ref', main_image_ref)
  return http.put(`/owner/service/${service_id}/edit`, formData)
}

const createService = async (data) => {
  const { main_image_ref } = data
  const formData = new FormData()
  delete data.main_image_ref
  formData.append('data', JSON.stringify(data))
  formData.append('main_image_ref', main_image_ref)
  const {
    data: { service },
  } = await http.post('/owner/service/new', formData)
  return {
    ...service,
    main_image_ref: `${process.env.REACT_APP_BASE_URL}/${service.main_image_ref}`,
  }
}

const fetchStructureServives = async (structure_id) => {
  const {
    data: { services },
  } = await http.get('/owner/service/all', {
    params: { structure_id },
  })
  return services
}

const fetchServiceData = async (service_id) => {
  const {
    data: { service },
  } = await http.get(`/owner/service/${service_id}/info`)
  return {
    ...service,
    main_image_ref: `${process.env.REACT_APP_BASE_URL}/${service.main_image_ref}`,
  }
}

const addServiceImages = async (service_id, images) => {
  const formData = new FormData()
  Object.keys(images).forEach((key) => {
    formData.append('service_images', images[key])
  })
  const {
    data: { service_images },
  } = await http.put(`/owner/service/${service_id}/images`, formData)
  return service_images
}

const getServiceImages = async (service_id) => {
  const {
    data: { service_images },
  } = await http.get(`/owner/service/${service_id}/images`)
  return service_images
}

const deleteService = (service_id) => {
  return http.put(`/owner/service/${service_id}/delete`)
}

const getUserStructures = async (queryOptions) => {
  const { data } = await http.get('/user/structure/all', {
    params: queryOptions,
  })
  return data
}

const getStructureInfo = async (structure_id) => {
  const {
    data: { structure },
  } = await http.get('/user/structure/info', { params: { structure_id } })
  return structure
}

export {
  editService,
  createService,
  deleteService,
  updateSchedule,
  deleteSchedule,
  updateStructure,
  fetchServiceData,
  getAllStructures,
  getStructureInfo,
  addServiceImages,
  getServiceImages,
  createNewSchedule,
  getUserStructures,
  getOwnerStructures,
  setActiveStructure,
  setStructureAbility,
  getStructreSchedules,
  fetchStructureServives,
  getStructureCagetories,
}
