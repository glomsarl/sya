import { Navigate } from 'react-router'
import StructureSettingPage from './Pages/Owner/StructureSettingPage'
import ManageServicesPage from './Pages/Owner/ManageServices.pages'
import EmailFormPage from './Pages/Client/reset-password/EmailForm.page'
import ResetFormPage from './Pages/Client/reset-password/Reset.page'
import DemandPage from './Pages/Client/Demand.page'
import SignupPage from './Pages/Client/SignUp.page'
import SignInPage from './Pages/Common/SignIn.page'
import AboutPage from './Pages/Common/About.page'
import ConfirmDemandPage from './Pages/Admin/ConfirmDemand.page'
import NewMailPage from './Pages/Admin/NewMail.page'
import ManageStructurePage from './Pages/Admin/ManageStructures.pages'
import PageNotFound from './Pages/PageNotFound'
import CatalogPage from './Pages/Owner/CatalogPage'
import PromotionsPage from './Pages/Owner/Promotions'
import LandingPage from './Pages/Client/Landing/Landing.page'
import SelectedStructurePage from './Pages/Client/Landing/SelectedStructure.page'
import MoreStructuresPage from './Pages/Client/Landing/MoreStructures.page'
import OrderManagementPage from './Pages/Owner/OrderManagementPage'
import PublicationsPage from './Pages/Owner/Publications'
import AdminDashboardPage from './Pages/Admin/Dashboard.page'
import OwnerDashboard from './Pages/Owner/Dashboard.page'
import BonusManagementPage from './Pages/Admin/BonusManagement.page'
import AdminSubscriptionPage from './Pages/Admin/AdminSubscriptionPage'
import PaymentHistory from './Pages/Owner/PaymentHistory'
import OwnerStructuresPage from './Pages/Owner/OwnerStructures.page'

const routes = [
  {
    path: '/',
    element: <LandingPage />,
  },
  {
    path: '/structures',
    element: <MoreStructuresPage />,
  },
  {
    path: '/structure/:struture_id',
    element: <SelectedStructurePage />,
  },
  {
    path: '/reset-password',
    children: [
      {
        path: '',
        element: <EmailFormPage />,
      },
      {
        path: ':link_id/:email',
        element: <ResetFormPage />,
      },
    ],
  },
  {
    path: '/demand',
    element: <DemandPage />,
  },
  {
    path: '/sign-in',
    element: <SignInPage />,
  },
  {
    path: '/sign-up',
    element: <SignupPage />,
    children: [{ path: ':person_id', element: <SignupPage /> }],
  },
  {
    path: '/about',
    element: <AboutPage />,
  },
  {
    path: '/admin',
    children: [
      {
        path: '',
        element: <Navigate to='/admin/overview' />,
      },
      {
        path: 'demands',
        element: <ConfirmDemandPage />,
      },
      {
        path: 'structures',
        element: <ManageStructurePage />,
      },
      {
        path: 'mails',
        element: <NewMailPage />,
      },
      {
        path: 'overview',
        element: <AdminDashboardPage />,
      },
      { path: 'configurations', element: <BonusManagementPage /> },
      { path: 'subscriptions', element: <AdminSubscriptionPage /> },
    ],
  },
  {
    path: '/owner',
    children: [
      {
        path: '',
        element: <Navigate to='/owner/owner-structures' />,
      },
      {
        path: 'profile',
        element: <StructureSettingPage />,
      },
      {
        path: 'services',
        element: <ManageServicesPage />,
      },
      {
        path: 'catalogs',
        element: <CatalogPage />,
      },
      {
        path: 'promotions',
        element: <PromotionsPage />,
      },
      {
        path: 'orders',
        element: <OrderManagementPage />,
      },
      {
        path: 'publication',
        element: <PublicationsPage />,
      },
      {
        path: 'overview',
        element: <OwnerDashboard />,
      },
      {
        path: 'payment-history',
        element: <PaymentHistory />,
      },
      { path: 'owner-structures', element: <OwnerStructuresPage /> },
    ],
  },
  {
    path: '*',
    element: <PageNotFound />,
  },
]

export default routes
